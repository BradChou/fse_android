package tw.com.fsexpress;

import android.util.Log;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;
import tw.com.fsexpress.okhttputils.OkHttpUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WareHouseManager {


    public static WareHouseManager amg = null;
    public List<DataObject> arrWare = new ArrayList<DataObject>();

    public static WareHouseManager instance(){

        if (WareHouseManager.amg == null){
            WareHouseManager.amg = new WareHouseManager();
        }
        return WareHouseManager.amg;
    }

    public List<DataObject> getWareHouse(){
        return this.arrWare;
    }

    public void init(){

        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");
        RequestObject requestObject = (RequestObject) new RequestObject();
        requestObject.setUrl(Constants.API_POST_GET_WAREHOUSE);
        requestObject.setMethod(RequestObject.METHOD.POST);
        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {
                if (requestStatus == RequestStatus.Finish){

                    try{

                        arrWare.clear();

                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                        JSONObject objJson = new JSONObject(result);

                        JSONArray arrJSON = objJson.getJSONArray("stations");
                        for (int idx = 0 ; idx < arrJSON.length(); idx++){
                            JSONObject obj = arrJSON.getJSONObject(idx);
                            DataObject data = new DataObject();
                            data.setVariable("code", obj.get("supplier_code").toString());
                            data.setVariable("name", obj.get("supplier_name").toString());
                            arrWare.add(data);
                        }


                    }catch (Exception e){
                        Log.e("Jerry", e.toString());
                    }
                }

            }
        });
        request.request(requestObject);

    }


}
