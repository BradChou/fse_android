package tw.com.fsexpress;

import static tw.com.fsexpress.ChooseItemPopWindow.TRANS_IN;
import static tw.com.fsexpress.Menu.transfer.TransferActivity.HOLD_BACK;
import static tw.com.fsexpress.Menu.transfer.TransferActivity.INVENTORY;
import static tw.com.fsexpress.Menu.transfer.TransferActivity.SCAN_ITEM;
import static tw.com.fsexpress.Menu.transfer.TransferActivity.TYPE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import tw.com.fsexpress.Menu.Appeal.CbmSizeFragment;
import tw.com.fsexpress.Menu.Appeal.SpecialAreaFragment;
import tw.com.fsexpress.Menu.DriverSend.DriverSendFragment;
import tw.com.fsexpress.Menu.FL.FLFragment;
import tw.com.fsexpress.Menu.Overview.OverviewFragment;
import tw.com.fsexpress.Menu.Reverse.ReverseFragment;
import tw.com.fsexpress.Menu.RfidCage.CageTestFragment;
import tw.com.fsexpress.Menu.Ships.ShipsFragment;
import tw.com.fsexpress.Menu.arrive.ArriveFragment;
import tw.com.fsexpress.Menu.collection.CollectionFragment;
import tw.com.fsexpress.Menu.collection.CollectionFragment2;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment2;
import tw.com.fsexpress.Menu.dispatchTask.DispatchTaskFragment;
import tw.com.fsexpress.Menu.linkwork.LinkWorkFragment;
import tw.com.fsexpress.Menu.send.SendFragment;
import tw.com.fsexpress.Menu.transfer.TransferActivity;
import tw.com.fsexpress.Menu.unload_collection.UnloadCollectionFragment;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.CollectGoodsData;
import tw.com.fsexpress.framework.database.room.entity.DeliveryData;
import tw.com.fsexpress.framework.database.room.entity.DischargeData;
import tw.com.fsexpress.framework.database.room.entity.StatisticsData;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.model.MatchStatisticsData;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;
import tw.com.fsexpress.okhttputils.OkHttpUtil;
import tw.com.fsexpress.request.RequestManager;

/**
 * Created by Shin-Mac on 2018/2/12.
 */


public class MainFragment extends GHWFragment {

    // TAG
    private static final String TAG = "SHAWN";

    private Activity act = null;
    private AlertDialog dialog;

    private ChooseItemPopWindow popupWindow = null;

    private ArrayList<Integer> resIdList;

    private String fcmNewsId;

    @SuppressLint("SetTextI18n")
    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        Log.e("------openTime1------", ((MainActivity) (this.getFragmentActivity())).openTime.toString());
        // 2022-02-18 v.43
        //每次登入主選單 都更新最後進入主選單的時間
        ((MainActivity) (this.getFragmentActivity())).openTime = Calendar.getInstance().getTime();
        Log.e("------openTime2------", ((MainActivity) (this.getFragmentActivity())).openTime.toString());

        this.resIdList = new ArrayList<Integer>();

        this.act = this.getActivity();
        WareHouseManager.instance().init();
        TransferDataManager.instance().checkTransferData();

        try {

            JSONObject objJSON = AccountManager.instance().getMemberInfo(this.getFragmentActivity());
            String strDriver = objJSON.get("driver_name").toString();
            TextView txtTitle = (TextView) view.findViewById(R.id.txt_name);
            txtTitle.setText(strDriver);

            String station = AccountManager.instance().getWorkStationName(this.getFragmentActivity());
            String stationCode = AccountManager.instance().getWorkStationCode(this.getFragmentActivity());
            String workSendCode = AccountManager.instance().getWorkSendCode(this.getFragmentActivity());
            String workDeliveryCode = AccountManager.instance().getWorkDeliveryCode(this.getFragmentActivity());

            TextView textViewStation = view.findViewById(R.id.txt_station);
            TextView textViewStationCode = view.findViewById(R.id.txt_station_code);
            textViewStation.setText(stationCode + station);
            if (workSendCode != null && workDeliveryCode != null) {
                textViewStationCode.setText("集:" + workSendCode + " 配:" + workDeliveryCode);
            } else if (workSendCode != null) {
                textViewStationCode.setText("集:" + workSendCode + " 配:");
            } else if (workDeliveryCode != null) {
                textViewStationCode.setText("集: 配:" + workDeliveryCode);
            }


        } catch (Exception e) {
            Log.e("Jerry", e.toString());
        }

        view.findViewById(R.id.btn_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogFragment fg = (LogFragment) new LogFragment().initWithLayoutResourceId(R.layout.layout_log);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });


        view.findViewById(R.id.btn_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.ErrorAlertDialog(act, "登出", "確定要登出嗎？", "確定", "取消", new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestLogout();
                            }
                        },
                        null).show();


            }
        });

        // ========== 目前的畫面 ============

        // 新的 集貨2.0 view
        view.findViewById(R.id.btnCollectGoods).setOnClickListener(v -> {
            CollectionFragment2 fg = (CollectionFragment2) new CollectionFragment2().initWithLayoutResourceId(R.layout.layout_collection2);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnCollectGoods);

        // 新的 卸集 view
        view.findViewById(R.id.btnDischarge).setOnClickListener(v -> {
            UnloadCollectionFragment fg = (UnloadCollectionFragment) new UnloadCollectionFragment().initWithLayoutResourceId(R.layout.layout_unload_collection);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnDischarge);

        // 新的 發送 view
        view.findViewById(R.id.btnSend).setOnClickListener(v -> {
            SendFragment fg = (SendFragment) new SendFragment().initWithLayoutResourceId(R.layout.layout_send);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnSend);

        // 新的 到著 view
        view.findViewById(R.id.btnArrive).setOnClickListener(v -> {
            ArriveFragment fg = (ArriveFragment) new ArriveFragment().initWithLayoutResourceId(R.layout.layout_arrive);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnArrive);

        // 新的 配送 view
        view.findViewById(R.id.btnDelivery).setOnClickListener(v -> {
            ShipsFragment fg = (ShipsFragment) new ShipsFragment().initWithLayoutResourceId(R.layout.layout_ships);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnDelivery);

        // 新的 配達2.0 view
        view.findViewById(R.id.btnMatched).setOnClickListener(v -> {
            DeliveryFragment2 fg = (DeliveryFragment2) new DeliveryFragment2().initWithLayoutResourceId(R.layout.layout_delivery2);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnMatched);

        // 新的 行動派遣 view
        view.findViewById(R.id.btnActionDispatch).setOnClickListener(v -> {
            DriverSendFragment fg = (DriverSendFragment) new DriverSendFragment().initWithLayoutResourceId(R.layout.layout_driversend);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            fcmNewsId = null; // 傳遞一次後就清空
        });
        this.resIdList.add(R.id.btnActionDispatch);

        // 新的 材積追補 view
        view.findViewById(R.id.btnVolumeHunt).setOnClickListener(v -> {
            CbmSizeFragment fg = (CbmSizeFragment) new CbmSizeFragment().initWithLayoutResourceId(R.layout.layout_cbmsize);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnVolumeHunt);

        // 新的 站所盤點 view
        view.findViewById(R.id.btnStationInventory).setOnClickListener(v -> {
            showChoosePopWindow();
        });
        this.resIdList.add(R.id.btnStationInventory);

        // 新的 特服區申訴 view
        view.findViewById(R.id.btnAppeal).setOnClickListener(v -> {
            SpecialAreaFragment fg = (SpecialAreaFragment) new SpecialAreaFragment().initWithLayoutResourceId(R.layout.layout_specialarea);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnAppeal);

        // 新的 設定 view
        view.findViewById(R.id.btnSetting).setOnClickListener(v -> {
            SettingFragment settingFragment = (SettingFragment) new SettingFragment().initWithLayoutResourceId(R.layout.fragment_setting);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(settingFragment, true);
        });
        this.resIdList.add(R.id.btnSetting);

        // 持回 view
        view.findViewById(R.id.btnHoldBack).setOnClickListener(v -> {
            Intent transfer = new Intent(MainFragment.this.getActivity(), TransferActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(TYPE, TRANS_IN);
            bundle.putString(SCAN_ITEM, HOLD_BACK);
            transfer.putExtras(bundle);
            Objects.requireNonNull(MainFragment.this.getActivity()).startActivity(transfer);
        });
        this.resIdList.add(R.id.btnHoldBack);

        // 新的 轉運配達(配達1.0) view
        view.findViewById(R.id.btnTransshipment).setOnClickListener(v -> {
            DeliveryFragment fg = (DeliveryFragment) new DeliveryFragment().initWithLayoutResourceId(R.layout.layout_delivery);
            ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
        });
        this.resIdList.add(R.id.btnTransshipment);

        // ========== 目前的畫面 End ============

        // ============= 舊畫面 ================
        // 集貨1.0
        view.findViewById(R.id.btn01).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CollectionFragment fg = (CollectionFragment) new CollectionFragment().initWithLayoutResourceId(R.layout.layout_collection);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn01);

        // 集貨2.0
        view.findViewById(R.id.btn15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CollectionFragment2 fg = (CollectionFragment2) new CollectionFragment2().initWithLayoutResourceId(R.layout.layout_collection2);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn15);

        // 配送
        view.findViewById(R.id.btn05).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShipsFragment fg = (ShipsFragment) new ShipsFragment().initWithLayoutResourceId(R.layout.layout_ships);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn05);

        // 卸集
        view.findViewById(R.id.btn02).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnloadCollectionFragment fg = (UnloadCollectionFragment) new UnloadCollectionFragment().initWithLayoutResourceId(R.layout.layout_unload_collection);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn02);

        // 配達
        view.findViewById(R.id.btn06).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliveryFragment fg = (DeliveryFragment) new DeliveryFragment().initWithLayoutResourceId(R.layout.layout_delivery);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn06);

        // 配達2.0
        view.findViewById(R.id.btn13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliveryFragment2 fg = (DeliveryFragment2) new DeliveryFragment2().initWithLayoutResourceId(R.layout.layout_delivery2);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn13);

        // 貨號綁定
        view.findViewById(R.id.btn14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkWorkFragment fg = (LinkWorkFragment) new LinkWorkFragment().initWithLayoutResourceId(R.layout.layout_linkwork);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn14);

        // 發送
        view.findViewById(R.id.btn03).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendFragment fg = (SendFragment) new SendFragment().initWithLayoutResourceId(R.layout.layout_send);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn03);

        // 到著
        view.findViewById(R.id.btn04).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArriveFragment fg = (ArriveFragment) new ArriveFragment().initWithLayoutResourceId(R.layout.layout_arrive);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn04);

        // 逆物流
        view.findViewById(R.id.btn07).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReverseFragment fg = (ReverseFragment) new ReverseFragment().initWithLayoutResourceId(R.layout.layout_reverse);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn07);

        // 傳輸
        view.findViewById(R.id.btn09).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OverviewFragment fg = (OverviewFragment) new OverviewFragment().initWithLayoutResourceId(R.layout.layout_overview);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn09);

        // 派遣任務
        view.findViewById(R.id.btn08).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DispatchTaskFragment fg = (DispatchTaskFragment) new DispatchTaskFragment().initWithLayoutResourceId(R.layout.layout_dispatch_main);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn08);

        // 站所作業
        view.findViewById(R.id.btn10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChoosePopWindow();
            }
        });
        this.resIdList.add(R.id.btn10);

        // 特服區 申訴
        view.findViewById(R.id.btn16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpecialAreaFragment fg = (SpecialAreaFragment) new SpecialAreaFragment().initWithLayoutResourceId(R.layout.layout_specialarea);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn16);

        // 材積追補
        view.findViewById(R.id.btn17).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CbmSizeFragment fg = (CbmSizeFragment) new CbmSizeFragment().initWithLayoutResourceId(R.layout.layout_cbmsize);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn17);

        // 行動派遣
        view.findViewById(R.id.btn18).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DriverSendFragment fg = (DriverSendFragment) new DriverSendFragment().initWithLayoutResourceId(R.layout.layout_driversend);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
                fcmNewsId = null; // 傳遞一次後就清空
            }
        });
        this.resIdList.add(R.id.btn18);

        // 籠車管理
        view.findViewById(R.id.btn19).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CageTestFragment fg = (CageTestFragment) new CageTestFragment().initWithLayoutResourceId(R.layout.layout_cage_test);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btn19);

        // 正物流
        view.findViewById(R.id.btnFL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FLFragment fg = (FLFragment) new FLFragment().initWithLayoutResourceId(R.layout.layout_fl);
                ((GHWFragmentActivity) MainFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        });
        this.resIdList.add(R.id.btnFL);
        // ============= 舊畫面 End ================

        requestInstallPermission();
        PermissionManager.requestAllPermission(this.getFragmentActivity());
        requestGetAppVersion_SimpleCheck();

        if (fcmNewsId != null) {
            if (!fcmNewsId.isEmpty()) {
                view.findViewById(R.id.btnActionDispatch).performClick();
            }
        }

        getCountText();
        GetDeliveryWhiteUser();
        checkRoomData();
        requestGetAppList();
    }


    private void doLogout() {
        AccountManager.instance().doLogout(MainFragment.this.getFragmentActivity(), new AccountManager.AccountManagerCallback() {
            @Override
            public void doLogout(boolean isLogout, String reason) {
                if (isLogout) {
                    Intent intent = new Intent(MainFragment.this.getFragmentActivity(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    MainFragment.this.getFragmentActivity().finish();
                }
            }
        });
    }

    // 登出時call 登入 api 清空 token
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void requestLogout() {

        String strDevice = "";
        String strProduct = "";
        String strBrand = "";
        String strVersionCode = "";
        String strIMEI = "";

        this.dialog = Constants.ProgressBar(this.getFragmentActivity(), "登出中...請稍候");
        this.dialog.show();


        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");


        RequestObject requestObject = (RequestObject) new RequestObject();

        //為了相容有人使用非公司設備
        //可能會有參數取不到值
        //所以分開判斷
        try {
            strDevice = Build.FINGERPRINT.toString();//Build.ID.toString();//  Build.DEVICE.toString();
        } catch (Exception ex) {
            strDevice = "";
        }
        try {
            strProduct = Build.PRODUCT.toString();
        } catch (Exception ex) {
            strProduct = "";
        }
        try {
            strBrand = Build.BRAND.toString();
        } catch (Exception ex) {
            strBrand = "";
        }
        try {
            strVersionCode = Constants.getVersionCode(MainFragment.this.getFragmentActivity()).toString();
        } catch (Exception ex) {
            strVersionCode = "";
        }

        try {
            strIMEI = getIMEIDeviceId();
        } catch (Exception ex) {

        }

        String drverCode = Constants.get(getFragmentActivity(), "previous_account");
        String password = Constants.get(getFragmentActivity(), "previous_password");

        requestObject.setUrl(Constants.API_POST_LOGIN_CHECK_V3);

        requestObject.setMethod(RequestObject.METHOD.POST);
        requestObject.addParameter("driver_code", drverCode);
        requestObject.addParameter("login_password", Constants.md5(password));
        requestObject.addParameter("device", strIMEI); //strIMEI// strDevice);
        requestObject.addParameter("product", strProduct);
        requestObject.addParameter("brand", strBrand);
        requestObject.addParameter("versionCode", strVersionCode);
        requestObject.addParameter("mobiletoken", "");
        requestObject.addParameter("mobiletype", "android");


        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {


                if (requestStatus == RequestStatus.Finish) {

                    try {

                        Log.e("Jerry", "login;" + requestHandler.getReceiveString());
                        MainFragment.this.dialog.cancel();

                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                        JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);

                        if (Boolean.valueOf(convertedObject.get("resultcode").toString()) == false) {

                            String des = convertedObject.get("resultdesc").toString();
                            Constants.ErrorAlertDialog(MainFragment.this.getFragmentActivity(), "登出失敗", des, "確定", null, null, null);

                            return;
                        }

                        doLogout();

                    } catch (Exception e) {
                        Log.e("Jerry", e.toString());
                        MainFragment.this.dialog.cancel();
                        Constants.ErrorAlertDialog(MainFragment.this.getFragmentActivity(), "登出失敗", e.toString(), "確定", null, null, null);
                    }
                } else if (requestStatus == RequestStatus.Fail) {
                    Log.e("Jerry", "fail");
                    MainFragment.this.dialog.cancel();
                    Constants.ErrorAlertDialog(MainFragment.this.getFragmentActivity(), "登出失敗", "登出失敗，請確認網路狀態", "確定", null, null, null);
                } else if (requestStatus == RequestStatus.NoNetworkConnection) {
                    MainFragment.this.dialog.cancel();
                    Constants.ErrorAlertDialog(MainFragment.this.getFragmentActivity(), "登出失敗", "登出失敗", "確定", null, null, null);
                    Log.e("Jerry", "nonetwork");
                }

            }
        });
        request.request(requestObject);
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getIMEIDeviceId() {
        //return "123";
        String deviceId = "";
        String deviceId_0 = "";
        String deviceId_1 = "";
        String deviceId_2 = "";
        String deviceId_3 = "";
        if (Build.VERSION.SDK_INT >= 29)//Build.VERSION_CODES.Q
        {
            //Manifest.permission.READ_PRIVILEGED_PHONE_STATE

            if (MainFragment.this.getFragmentActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(MainFragment.this.getFragmentActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                //return "";
            }

            deviceId_0 = Settings.Secure.getString(MainFragment.this.getFragmentActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) MainFragment.this.getFragmentActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (MainFragment.this.getFragmentActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(MainFragment.this.getFragmentActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                    //return "";
                }
            }
            assert mTelephony != null;
            if (mTelephony.getDeviceId() != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    deviceId_1 = mTelephony.getImei();
                } else {
                    deviceId_2 = mTelephony.getDeviceId();
                }
            } else {
                deviceId_3 = Settings.Secure.getString(MainFragment.this.getFragmentActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        deviceId = deviceId_0 + " ; " + deviceId_1 + " ; " + deviceId_2 + " ; " + deviceId_3;
        Log.d("deviceId", deviceId);

        return deviceId;
    }

    private void setWindowAlpha(float paramFloat) {
        if (paramFloat == 1.0F) {
            Iterator<Integer> iterator = this.resIdList.iterator();
            while (iterator.hasNext())
                this.getActivity().findViewById(((Integer) iterator.next()).intValue()).setEnabled(true);
        } else {
            Iterator<Integer> iterator = this.resIdList.iterator();
            while (iterator.hasNext())
                this.getActivity().findViewById(((Integer) iterator.next()).intValue()).setEnabled(false);
        }
        WindowManager.LayoutParams layoutParams = this.getActivity().getWindow().getAttributes();
        layoutParams.alpha = paramFloat;
        this.getActivity().getWindow().setAttributes(layoutParams);
    }

    private void showChoosePopWindow() {
        if (this.popupWindow == null) {
            ChooseItemPopWindow chooseItemPopWindow = new ChooseItemPopWindow((Context) this.getActivity());
            this.popupWindow = chooseItemPopWindow;
            chooseItemPopWindow.setChooseItemPopWindowListener(new ChooseItemPopWindow.ChooseItemPopWindowListener() {
                @Override
                public void onChoice(int choice) {
                    Log.e(TAG, "choice = " + choice);

                    switch (choice) {
                        default:
                            break;
                        case TRANS_IN:
                            Intent transferIn = new Intent(MainFragment.this.getActivity(), TransferActivity.class);
                            Bundle transferInBundle = new Bundle();
                            transferInBundle.putInt(TYPE, choice);
                            transferInBundle.putString(SCAN_ITEM, INVENTORY);
                            transferIn.putExtras(transferInBundle);
                            Objects.requireNonNull(MainFragment.this.getActivity()).startActivity(transferIn);
                            break;
                        case ChooseItemPopWindow.TRANS_OUT:
                            Intent transferOut = new Intent(MainFragment.this.getActivity(), TransferActivity.class);
                            Bundle transferOutBundle = new Bundle();
                            transferOutBundle.putInt(TYPE, choice);
                            transferOut.putExtras(transferOutBundle);
                            Objects.requireNonNull(MainFragment.this.getActivity()).startActivity(transferOut);
                            break;
                    }

                    popupWindow.dismiss();
                    setWindowAlpha(1.0F);
                }
            });
            this.popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    setWindowAlpha(1.0F);
                }
            });
        }
        this.setWindowAlpha(0.4F);
        this.popupWindow.showAtLocation(this.getActivity().findViewById(R.id.parent), 80, 0, 0);
    }

    private void requestGetAppVersion_SimpleCheck() {

        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");
        RequestObject requestObject = (RequestObject) new RequestObject();
        requestObject.setUrl(Constants.API_POST_GET_APPVERSION);
        requestObject.setMethod(RequestObject.METHOD.POST);
        //requestObject.addParameter("app_id", "FSE");
        requestObject.addParameter("app_id", Constants.APP_CHECK_VERSION_ID);

        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {
                if (requestStatus == RequestStatus.Finish) {

                    try {

                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                        JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);
                        String strVersion = convertedObject.get("ttVersion").toString();
                        String str1 = strVersion.replaceAll(" ", "");
                        String str2 = str1.replace("\\n", "");
                        String str3 = str2.replace("\\r", "");
                        String str4 = str3.replace("\\", "");
                        String str5 = str4.substring(1);
                        String str6 = str5.substring(0, str5.length() - 1);
                        JSONArray arrJson = new JSONArray(str6);

                        String versionCode = Constants.getVersionCode(MainFragment.this.getFragmentActivity());
                        String versionName = Constants.getVersionName(MainFragment.this.getFragmentActivity());
                        final JSONObject objJson = (JSONObject) arrJson.get(0);
                        if (!versionCode.equals(String.valueOf(objJson.get("version_code"))) ||
                                !versionName.equals(objJson.get("version_name").toString())) {


                            Constants.ErrorAlertDialog(act, getString(R.string.version_update_title), getString(R.string.version_update_desc), getString(R.string.yes), null, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            try {
                                                Log.e("version_url", objJson.get("version_url").toString());
                                                MainFragment.this.doDownload(objJson.get("version_url").toString());
                                            } catch (Exception e) {
                                                Log.e("json", e.toString());
                                            }
                                        }
                                    },
                                    null).show();


                        }


                    } catch (Exception e) {
                        Log.e("Jerry", e.toString());
                    }
                }

            }
        });
        request.request(requestObject);
    }

    private void doDownload(String url) {

        final ProgressDialog progressDialog = new ProgressDialog(this.getFragmentActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(100);
        progressDialog.setMessage("更新下載中...請稍候");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Constants.createFolder(getFragmentActivity());
        FileDownloadController.intance(this.getFragmentActivity()).startDownload(this.getFragmentActivity(), url, new FileDownloadController.FileDownloadCallback() {
            @Override
            public void onDownloading(int prgress) {

                progressDialog.setProgress(prgress);

            }

            @Override
            public void onDownloadFinished() {
//                Log.e("Jerry", "onDownloadFinished");


                try {

                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    String fileName = Constants.findFileName(getFragmentActivity(), url);
                    File apkFile = new File(getFragmentActivity().getExternalFilesDir(null).getAbsolutePath() + "/mtbs/" + fileName);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        install.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Uri contentUri = FileProvider.getUriForFile(getFragmentActivity(), BuildConfig.APPLICATION_ID + ".fileProvider", apkFile);
                        install.setDataAndType(contentUri, "application/vnd.android.package-archive");
                    } else {
                        install.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                    }

                    getFragmentActivity().startActivity(install);
                    getFragmentActivity().finish();


                } catch (Exception e) {

                    Log.e("install", e.toString());

                }


            }

            @Override
            public void onDownloadFail() {
                progressDialog.dismiss();


            }
        });
    }

    public final static int REQUEST_READ_PHONE_STATE = 1;

    private void requestInstallPermission() {
        if (Build.VERSION.SDK_INT >= 26 && !this.getFragmentActivity().getPackageManager().canRequestPackageInstalls()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("package:");
            stringBuilder.append(this.getFragmentActivity().getPackageName());
            startActivityForResult(new Intent("android.settings.MANAGE_UNKNOWN_APP_SOURCES", Uri.parse(stringBuilder.toString())), 1);
        }

        if (MainFragment.this.getFragmentActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainFragment.this.getFragmentActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
            //return "";
        }
    }


    private List<String> WhiteUserlist = new ArrayList<String>();

    private void GetDeliveryWhiteUser() {


        WhiteUserlist = new ArrayList<String>();

        //Constants.toast(getFragmentActivity(), "取得使用權限中,請稍後...");
        HashMap<String, String> data = new HashMap<String, String>();
        RequestManager.instance().doRequest(Constants.API_POST_GET_DELIVERY_WHITE_USER, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("----------------", jsonObject.toString());
                Log.e("----------------", jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true) {

                    JsonArray objData = jsonObject.getAsJsonArray("whiteuser");
                    //Log.e("----------------",objData.toString());
                    //Log.e("----------------",""+objData.size() );
                    //Log.e("----------------",objData.get(0).toString());
                    //Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
                    for (int i = 0; i < objData.size(); i++) {
                        WhiteUserlist.add(((JsonObject) objData.get(i)).get("driver_code").toString().toUpperCase().replace("\"", "").replace("\"", ""));
                    }

                    Log.e("WhiteUserlist----------------", WhiteUserlist.toString());

                } else {


                }

                SetWhiteUserButton();

            }

            @Override
            public void onFail(String reason) {
                SetWhiteUserButton();
            }
        });


    }

    private void SetWhiteUserButton() {
        boolean isWhiteUserOk = false;
        String strDriverCode = AccountManager.instance().getDriverCode(MainFragment.this.getFragmentActivity());
        strDriverCode = strDriverCode.toUpperCase();

        for (int i = 0; i < WhiteUserlist.size(); i++) {
            Log.e("equals----------------", strDriverCode.toString() + "   " + (WhiteUserlist.toArray(new String[0]))[i]);
            Log.e("equals----------------", "" + strDriverCode.equals((WhiteUserlist.toArray(new String[0]))[i]));

            if (strDriverCode.equals((WhiteUserlist.toArray(new String[0]))[i])) {
                isWhiteUserOk = true;
            }
        }

//        view.findViewById(R.id.btn06).setVisibility(View.GONE);
        view.findViewById(R.id.btnTransshipment).setVisibility(View.GONE);

        if (isWhiteUserOk == true) {
//            view.findViewById(R.id.btn06).setVisibility(View.VISIBLE);
            view.findViewById(R.id.btnTransshipment).setVisibility(View.VISIBLE);
        }


        Date dTime = new Date(System.currentTimeMillis());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String format = formatter.format(dTime);
        int ymd = Integer.parseInt(format);

        if (ymd <= 20220630) {
//            view.findViewById(R.id.btn06).setVisibility(View.VISIBLE);
            view.findViewById(R.id.btnTransshipment).setVisibility(View.VISIBLE);
        }

    }

    public void setFcmNewsId(String fcmNewsId) {
        this.fcmNewsId = fcmNewsId;
    }

    // 檢查room資料庫有沒有資料需要刪除
    private void checkRoomData() {
        String deleteDate = Constants.get(MainFragment.this.getFragmentActivity(), "deleteDate"); // 上次刪除資料的日期
        boolean delete = !Constants.getTimeMinute().equals("00:00");
        if (deleteDate == null || deleteDate.isEmpty() || (!deleteDate.equals(Constants.getTodayDate()) && delete)) {
            new Thread(() -> {
                AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getCollectGoodsDao().deleteDataFormTime(Constants.getThreeDayAgoTimestamp());
                AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getDischargeDao().deleteDataFormTime(Constants.getThreeDayAgoTimestamp());
                AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getMatchedDao().deleteDataFormTime(Constants.getThreeDayAgoTimestamp());
                AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getDeliveryDao().deleteDataFormTime(Constants.getThreeDayAgoTimestamp());
                Constants.set(MainFragment.this.getFragmentActivity(), "deleteDate", Constants.getTodayDate());
            }).start();
        }
    }

    // 取得當日統計的掃讀數量
    private void getCountText() {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(MainFragment.this.getFragmentActivity()).toUpperCase();
            String nowHourMinute = Constants.getTimeMinute();
            String startTime = Constants.getTodayDate() + " 00:01:00";
            String endTime = Constants.getTomorrowDate() + " 00:00:59";

            if (nowHourMinute.equals("00:00")) {
                startTime = Constants.getYesterdayDate() + " 00:01:00";
                endTime = Constants.getTodayDate() + " 00:00:59";
            }

            List<CollectGoodsData> collectGoodsData = AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getCollectGoodsDao().findDataByDriverCodeAndDate(driverCode, startTime, endTime);
            List<DischargeData> dischargeData = AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getDischargeDao().findDataByDriverCodeAndDate(driverCode, startTime, endTime);
            List<DeliveryData> deliveryData = AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getDeliveryDao().findDataByDriverCodeAndDate(driverCode, startTime, endTime);
            List<MatchStatisticsData> matchedData = AppDataBase.getInstance(MainFragment.this.getFragmentActivity()).getMatchedDao().findDataByDriverCodeAndDate(driverCode, startTime, endTime);

            TextView txtCollectGoodsCounts = (TextView) view.findViewById(R.id.txtCollectGoodsCounts);
            TextView txtDischargeCounts = (TextView) view.findViewById(R.id.txtDischargeCounts);
            TextView txtDeliveryCounts = (TextView) view.findViewById(R.id.txtDeliveryCounts);
            TextView txtMatchedCounts = (TextView) view.findViewById(R.id.txtMatchedCounts);

            // 集貨2.0掃讀數量
            if (collectGoodsData.size() == 0) {
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    txtCollectGoodsCounts.setText("0/0");
                });
            } else {
                Set<String> collectGoodsDataSet = removeDuplicate(collectGoodsData);
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    String text = collectGoodsDataSet.size() + "/" + collectGoodsData.size();
                    txtCollectGoodsCounts.setText(text);
                });
            }

            // 卸集掃讀數量
            if (dischargeData.size() == 0) {
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    txtDischargeCounts.setText("0/0");
                });
            } else {
                Set<String> dischargeDataSet = removeDuplicate(dischargeData);
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    String text = dischargeDataSet.size() + "/" + dischargeData.size();
                    txtDischargeCounts.setText(text);
                });
            }

            // 配送掃讀數量
            if (deliveryData.size() == 0) {
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    txtDeliveryCounts.setText("0/0");
                });
            } else {
                Set<String> deliveryDataSet = removeDuplicate(deliveryData);
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    String text = deliveryDataSet.size() + "/" + deliveryData.size();
                    txtDeliveryCounts.setText(text);
                });
            }

            // 配達掃讀數量
            if (matchedData.size() == 0) {
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    txtMatchedCounts.setText("0/0");
                });
            } else {
                List<MatchStatisticsData> matchedDataSet = removeMatchDuplicate(matchedData);
                int case_count = 0;
                for (int i = 0; i < matchedDataSet.size(); i++) {
                    case_count = case_count + Integer.parseInt(matchedDataSet.get(i).getCase_count());
                }
                int finalCase_count = case_count;
                MainFragment.this.getFragmentActivity().runOnUiThread(() -> {
                    String text = matchedDataSet.size() + "/" + finalCase_count;
                    txtMatchedCounts.setText(text);
                });
            }
        }).start();
    }

    // 去除重複的
    private <T extends StatisticsData> Set<String> removeDuplicate(List<T> list) {
        List<String> removeDuplicateList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            removeDuplicateList.add(list.get(i).getCheckNumber());
        }
        return new HashSet<>(removeDuplicateList);
    }

    private List<MatchStatisticsData> removeMatchDuplicate(List<MatchStatisticsData> list) {
        HashSet<MatchStatisticsData> matchStatisticsDataHashSet = new HashSet<>(list);
        List<MatchStatisticsData> matchStatisticsData = new ArrayList<>(matchStatisticsDataHashSet);
        return matchStatisticsData;
    }

    // 取讀行動派遣單
    private void requestGetAppList() {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("driver_code", AccountManager.instance().getDriverCode(MainFragment.this.getFragmentActivity()));
        params.put("ListOrderType", "1");

        TextView txtActionDispatchCounts = (TextView) view.findViewById(R.id.txtActionDispatchCounts);
        txtActionDispatchCounts.setText("讀取中");

        RequestManager.instance().doRequest(Constants.API_GET_Driver_Send_Prepare_GetAppList, "POST", params, new RequestManager.RequestManagerCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("---test---", "----4----");
                Log.e("---test---", jsonObject.toString());

                JsonArray jArray = jsonObject.get("result").getAsJsonArray();
                Log.e("---test---", "----5----");
                Log.e("---test---", jArray.toString());

                if (jArray.size() < 1) {
                    txtActionDispatchCounts.setText("0");
                }

                List<String> keys = Arrays.asList("customer_code", "send_cityarea", "send_address", "send_contact", "send_tel", "send_station",
                        "driver_code", "YYYYMMDDHHMM", "YYYYMMDDHHMM_max",
                        "is_read", "counts", "pieces", "collects",
                        "driver_name", "customer_name", "customer_shortname",
                        "shipments_city", "shipments_area", "shipments_road", "telephone",
                        "packageType", "vechileType", "pickUpDate", "remark", "target_date", "id_list", "id_list2", "DeliveryType"
                );

                int notRead = 0; // 計算未讀數量
                for (int i = 0; i < jArray.size(); i++) {
                    JsonObject jObj = (JsonObject) jArray.get(i);

                    DataObject d = new DataObject();
                    for (String key : keys) {
                        String value = !jObj.get(key).isJsonNull() ? jObj.get(key).getAsString() : "";
                        d.setVariable(key, value);
                        if (Objects.equals(key, "is_read") && Objects.equals(value, "0")) {
                            notRead++;
                        }
                    }
                }
                txtActionDispatchCounts.setText(Integer.toString(notRead));
            }

            @Override
            public void onFail(String reason) {
                txtActionDispatchCounts.setText("讀取失敗");
            }
        });
    }

}