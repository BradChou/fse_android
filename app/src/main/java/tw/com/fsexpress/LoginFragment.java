package tw.com.fsexpress;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;
import tw.com.fsexpress.okhttputils.OkHttpUtil;

public class LoginFragment extends GHWFragment {

    // TAG
    private static final String TAG = "SHAWN";

    private Activity act = null;
    private AlertDialog dialog;

    private EditText editAccount = null, editPassword = null;
    private Button btnLogin = null;
    private TextView textVersion = null;


    private String strDevice = "";
    private String strProduct = "";
    private String strBrand = "";
    private String strVersionCode = "";
    private String strIMEI = "";

    private String fcmNewsId;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

//        this.fragmentBar.setVisibility(View.GONE);
        this.act = this.getActivity();

//        Constants.createFolder(getFragmentActivity());
//        FileDownloadController.intance(this.getFragmentActivity()).startDownload(this.getFragmentActivity(), "http://220.128.210.180:10080/JUNFU_APP/apk/junfu.apk");

//        if (AccountManager.instance().isLogin(this.getFragmentActivity())) {
//            Intent intent = new Intent(LoginFragment.this.getFragmentActivity(), MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
//            LoginFragment.this.getFragmentActivity().finish();
//            return;
//        }

        String versionName = Constants.getVersionName2(LoginFragment.this.getFragmentActivity());
        this.textVersion = (TextView) view.findViewById(R.id.tv_version);
        String version = String.format("版本：%s", versionName);
        this.textVersion.setText(version);


        String previousA = Constants.get(getFragmentActivity(), "previous_account");
        String previousP = Constants.get(getFragmentActivity(), "previous_password");

        this.editAccount = (EditText) view.findViewById(R.id.et_account);
        this.editPassword = (EditText) view.findViewById(R.id.et_password);
        if (previousA != null) {
            this.editAccount.setText(previousA);
        }
        if (previousP != null) {
            this.editPassword.setText(previousP);
        }

        this.btnLogin = (Button) view.findViewById(R.id.btn_login);
        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin();
            }
        });

        requestGetAppVersion();
        if (AccountManager.instance().isLogin(this.getFragmentActivity())) {
            doLogin();
            return;
        }

        requestInstallPermission();
        PermissionManager.requestAllPermission(this.getFragmentActivity());
    }

    protected void doLogin() {

//        editAccount.setText("C0001");
//        editPassword.setText("0000");
//        editAccount.setText("Z010006");
//        editPassword.setText("1234");
        if (editAccount.getText().toString().equals("")) {
            Constants.ErrorAlertDialog(act, getString(R.string.notice), getString(R.string.account_missing), getString(R.string.yes), null, null,
                    null).show();
            return;
        }

        if (editPassword.getText().toString().equals("")) {
            Constants.ErrorAlertDialog(act, getString(R.string.notice), getString(R.string.password_missing), getString(R.string.yes), null, null,
                    null).show();
            return;
        }

        this.requestLogin();
    }


    private void requestGetAppVersion() {

        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");
        RequestObject requestObject = (RequestObject) new RequestObject();
        requestObject.setUrl(Constants.API_POST_GET_APPVERSION);
        requestObject.setMethod(RequestObject.METHOD.POST);
        //requestObject.addParameter("app_id", "FSE");
        requestObject.addParameter("app_id", Constants.APP_CHECK_VERSION_ID);

        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {
                if (requestStatus == RequestStatus.Finish) {

                    try {

                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                        JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);
                        String strVersion = convertedObject.get("ttVersion").toString();
                        String str1 = strVersion.replaceAll(" ", "");
                        String str2 = str1.replace("\\n", "");
                        String str3 = str2.replace("\\r", "");
                        String str4 = str3.replace("\\", "");
                        String str5 = str4.substring(1);
                        String str6 = str5.substring(0, str5.length() - 1);
                        JSONArray arrJson = new JSONArray(str6);

                        String versionCode = Constants.getVersionCode(LoginFragment.this.getFragmentActivity());
                        String versionName = Constants.getVersionName(LoginFragment.this.getFragmentActivity());
                        final JSONObject objJson = (JSONObject) arrJson.get(0);
                        if (!versionCode.equals(String.valueOf(objJson.get("version_code"))) ||
                                !versionName.equals(objJson.get("version_name").toString())) {

                            Constants.ErrorAlertDialog(act, getString(R.string.version_update_title), getString(R.string.version_update_desc), getString(R.string.yes), null, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            try {
                                                Log.e("version_url", objJson.get("version_url").toString());
                                                LoginFragment.this.doDownload(objJson.get("version_url").toString());
                                            } catch (Exception e) {
                                                Log.e("json", e.toString());
                                            }
                                        }
                                    },
                                    null).show();

                        }


                    } catch (Exception e) {
                        Log.e("Jerry", e.toString());
                    }
                }

            }
        });
        request.request(requestObject);
    }

    //原本想刪除舊的APP
    //但是發生神秘的異常.......
    //直接先略過這一段
    //改成手動處理好了
    private void doDeleteOtherApp() {
        /*
        String app_pkg_name = "com.skyeyes.junfu_new";
        int UNINSTALL_REQUEST_CODE = 1;

        Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
        intent.setData(Uri.parse("package:" + app_pkg_name));
        intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
        startActivityForResult(intent, UNINSTALL_REQUEST_CODE);
        */
        /*
        Intent i = new Intent(Intent.ACTION_DELETE, Uri.parse(new StringBuilder().append("package:")
                .append("com.skyeyes.junfu_new").toString()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(i);
*/

        /*
            Uri packageUri = Uri.parse("package:org.klnusbaum.test");
            Intent uninstallIntent =
              new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
            startActivity(uninstallIntent);
         */
    }

    private void doDownload(String url) {

        final ProgressDialog progressDialog = new ProgressDialog(this.getFragmentActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(100);
        progressDialog.setMessage("更新下載中...請稍候");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Constants.createFolder(getFragmentActivity());
        FileDownloadController.intance(this.getFragmentActivity()).startDownload(this.getFragmentActivity(), url, new FileDownloadController.FileDownloadCallback() {
            @Override
            public void onDownloading(int prgress) {

                progressDialog.setProgress(prgress);

            }

            @Override
            public void onDownloadFinished() {
//                Log.e("Jerry", "onDownloadFinished");


                try {

                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    String fileName = Constants.findFileName(getFragmentActivity(), url);
                    File apkFile = new File(getFragmentActivity().getExternalFilesDir(null).getAbsolutePath() + "/mtbs/" + fileName);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        install.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Uri contentUri = FileProvider.getUriForFile(getFragmentActivity(), BuildConfig.APPLICATION_ID + ".fileProvider", apkFile);
                        install.setDataAndType(contentUri, "application/vnd.android.package-archive");
                    } else {
                        install.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                    }

                    getFragmentActivity().startActivity(install);
                    getFragmentActivity().finish();


                } catch (Exception e) {

                    Log.e("install", e.toString());

                }


            }

            @Override
            public void onDownloadFail() {
                progressDialog.dismiss();


            }
        });
    }

    private void requestInstallPermission() {
        if (Build.VERSION.SDK_INT >= 26 && !this.getFragmentActivity().getPackageManager().canRequestPackageInstalls()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("package:");
            stringBuilder.append(this.getFragmentActivity().getPackageName());
            startActivityForResult(new Intent("android.settings.MANAGE_UNKNOWN_APP_SOURCES", Uri.parse(stringBuilder.toString())), 1);
        }

        if (LoginFragment.this.getFragmentActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(LoginFragment.this.getFragmentActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
            //return "";
        }
    }

    protected void requestLogin() {


        this.dialog = Constants.ProgressBar(this.getFragmentActivity(), "登入中...請稍候");
        this.dialog.show();

        Map<String, String> param = new HashMap<String, String>() {{
            put("driver_code", editAccount.getText().toString());
            put("login_password", Constants.md5(editPassword.getText().toString()));
        }};


        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");


        RequestObject requestObject = (RequestObject) new RequestObject();
        //2021-11-08 改版登入判斷
        //增加資料紀錄
        //requestObject.setUrl(Constants.API_POST_LOGIN_CHECK);


        //測試取道甚麼值
        //Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "DEVICE", Build.DEVICE, "確定", null, null, null);
        //Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "PRODUCT", Build.PRODUCT, "確定", null, null, null);
        //Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "BRAND", Build.BRAND, "確定", null, null, null);
        //Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "VersionCode", Constants.getVersionCode(LoginFragment.this.getFragmentActivity()), "確定", null, null, null);

        //為了相容有人使用非公司設備
        //可能會有參數取不到值
        //所以分開判斷
        try {
            strDevice = Build.FINGERPRINT.toString();//Build.ID.toString();//  Build.DEVICE.toString();
        } catch (Exception ex) {
            strDevice = "";
        }
        try {
            strProduct = Build.PRODUCT.toString();
        } catch (Exception ex) {
            strProduct = "";
        }
        try {
            strBrand = Build.BRAND.toString();
        } catch (Exception ex) {
            strBrand = "";
        }
        try {
            strVersionCode = Constants.getVersionCode(LoginFragment.this.getFragmentActivity()).toString();
        } catch (Exception ex) {
            strVersionCode = "";
        }

        try
        {
            strIMEI = getIMEIDeviceId();
        } catch (Exception ex)
        {

        }
        /*
        if (ActivityCompat.checkSelfPermission(LoginFragment.this.getFragmentActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return;
            //requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE }, PERMISSION_READ_STATE);
            //LoginFragment.perm
            try {
                strIMEI = getDeviceImei();
            }
            catch(Exception ex)
            {
                strIMEI = "";
            }
        }
        else
        {
            //  new android.telephony.TelephonyManager().getDeviceId()
            try {
                strIMEI = getDeviceImei();
            }
            catch(Exception ex)
            {
                strIMEI = "";
            }

        }
        */

        String mobileToken = Constants.get(getFragmentActivity(), "fcm_token"); // 取得推播token

        requestObject.setUrl(Constants.API_POST_LOGIN_CHECK_V3);

        requestObject.setMethod(RequestObject.METHOD.POST);
        requestObject.addParameter("driver_code", editAccount.getText().toString());
        requestObject.addParameter("login_password", Constants.md5(editPassword.getText().toString()));
        requestObject.addParameter("device", strIMEI); //strIMEI// strDevice);
        requestObject.addParameter("product", strProduct);
        requestObject.addParameter("brand", strBrand);
        requestObject.addParameter("versionCode", strVersionCode);
        requestObject.addParameter("mobiletoken", mobileToken);
        requestObject.addParameter("mobiletype", "android");

        Log.e("PASS", Constants.md5(editPassword.getText().toString()));

        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {


                if (requestStatus == RequestStatus.Finish) {

                    try {

                        Constants.set(getFragmentActivity(), "previous_account", editAccount.getText().toString());
                        Constants.set(getFragmentActivity(), "previous_password", editPassword.getText().toString());


                        Log.e("Jerry", "login;" + requestHandler.getReceiveString());
                        LoginFragment.this.dialog.cancel();

//                        if (requestHandler.getResponseCode() == 200){
                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                        JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);

                        if (Boolean.valueOf(convertedObject.get("resultcode").toString()) == false) {

                            String des = convertedObject.get("resultdesc").toString();
                            Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "登入失敗", des, "確定", null, null, null);

                            return;
                        }

                        String stationCode = convertedObject.get("station").getAsString();
                        String stationName = convertedObject.get("station_name").getAsString();
                        String workStationCode = convertedObject.get("Work_Station_Scode").getAsString();
                        String workStationName = convertedObject.get("work_Station_Name").getAsString();
                        String workSendCode = convertedObject.get("Work_Send_Code").getAsString();
                        String workDeliveryCode = convertedObject.get("Work_Delivery_Code").getAsString();
                        AccountManager.instance().saveDriverCode(getFragmentActivity(), editAccount.getText().toString());
                        AccountManager.instance().saveMemberInfo(getFragmentActivity(), convertedObject.toString());

                        if (workStationCode.isEmpty()) {
                            AccountManager.instance().saveWorkStationCode(getFragmentActivity(),stationCode);
                        } else {
                            AccountManager.instance().saveWorkStationCode(getFragmentActivity(), workStationCode);
                        }

                        if (workStationName.isEmpty()) {
                            AccountManager.instance().saveWorkStationName(getFragmentActivity(), stationName);
                        } else {
                            AccountManager.instance().saveWorkStationName(getFragmentActivity(), workStationName);
                        }

                        AccountManager.instance().saveWorkSendCode(getFragmentActivity(), workSendCode);
                        AccountManager.instance().saveWorkDeliveryCode(getFragmentActivity(), workDeliveryCode);

                        /*
                        2022-02-16 v.43

                        修正登入之後不需要輸入司機的資訊
                        先直接跳過
                        看看會發生甚麼錯誤再視情況修改
                         */
                        /*
                        Intent intent = new Intent(LoginFragment.this.getFragmentActivity(), DriverInputActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        */
//                        Intent intent = new Intent(LoginFragment.this.getFragmentActivity(), MainActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//
//                        LoginFragment.this.getFragmentActivity().finish();

                        if (fcmNewsId == null) { // 無推播訊息
                            WorkStationFragment fg = (WorkStationFragment) new WorkStationFragment().initWithLayoutResourceId(R.layout.fragment_work_station);
                            ((GHWFragmentActivity) LoginFragment.this.getFragmentActivity()).pushFragment(fg, true);
                        } else { // 點選推播訊息進APP
                            Intent intent = new Intent(LoginFragment.this.getFragmentActivity(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("fcmNewsId", fcmNewsId);
                            startActivity(intent);

                            LoginFragment.this.getFragmentActivity().finish();
                        }
//                        }

                    } catch (Exception e) {
                        Log.e("Jerry", e.toString());
                        LoginFragment.this.dialog.cancel();
                        Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "登入失敗", e.toString(), "確定", null, null, null);
                    }
                } else if (requestStatus == RequestStatus.Fail) {
                    Log.e("Jerry", "fail");
                    LoginFragment.this.dialog.cancel();
                    Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "登入失敗", "登入失敗，請確認網路狀態", "確定", null, null, null);
                } else if (requestStatus == RequestStatus.NoNetworkConnection) {
                    LoginFragment.this.dialog.cancel();
                    Constants.ErrorAlertDialog(LoginFragment.this.getFragmentActivity(), "登入失敗", "登入失敗", "確定", null, null, null);
                    Log.e("Jerry", "nonetwork");
                }

            }
        });
        request.request(requestObject);

    }

    public final static int REQUEST_READ_PHONE_STATE = 1;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getIMEIDeviceId() {
        //return "123";
        String deviceId = "";
        String deviceId_0 = "";
        String deviceId_1 = "";
        String deviceId_2 = "";
        String deviceId_3 = "";
        if (Build.VERSION.SDK_INT >= 29)//Build.VERSION_CODES.Q
        {
            //Manifest.permission.READ_PRIVILEGED_PHONE_STATE

            if (LoginFragment.this.getFragmentActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(LoginFragment.this.getFragmentActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                //return "";
            }

            deviceId_0 = Settings.Secure.getString(LoginFragment.this.getFragmentActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        }else {
            final TelephonyManager mTelephony = (TelephonyManager) LoginFragment.this.getFragmentActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (LoginFragment.this.getFragmentActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(LoginFragment.this.getFragmentActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                    //return "";
                }
            }
            assert mTelephony != null;
            if (mTelephony.getDeviceId() != null)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    deviceId_1 = mTelephony.getImei();
                }else {
                    deviceId_2 = mTelephony.getDeviceId();
                }
            } else {
                deviceId_3 = Settings.Secure.getString(LoginFragment.this.getFragmentActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        deviceId = deviceId_0 + " ; " + deviceId_1 + " ; " + deviceId_2 + " ; " + deviceId_3 ;
        Log.d("deviceId", deviceId);

        return deviceId;
    }

    private String getDeviceImei() {
        TelephonyManager mTelephonyManager = null;
        String mDeviceIMEI = "0";
        mTelephonyManager = (TelephonyManager) LoginFragment.this.getFragmentActivity().getSystemService(LoginFragment.this.getFragmentActivity().TELEPHONY_SERVICE);
        try {
            if( Build.VERSION.SDK_INT >= 26 ) {
                mDeviceIMEI = mTelephonyManager.getImei();
            }else {
                mDeviceIMEI = mTelephonyManager.getDeviceId();
            }
        } catch (SecurityException e) {
                Log.d(TAG, "SecurityException e");

        }

        return mDeviceIMEI ;
    }

    public void setFcmNewsId(String fcmNewsId) {
        this.fcmNewsId = fcmNewsId;
    }

}
