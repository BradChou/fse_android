package tw.com.fsexpress;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by Shin-Mac on 2018/1/17.
 */


public class AccountManager {

    public interface AccountManagerCallback {
        public void doLogout(boolean isLogout, String reason);
    }

    public AccountManagerCallback callback = null;
    public static AccountManager amg = null;


    public static AccountManager instance() {

        if (AccountManager.amg == null) {
            AccountManager.amg = new AccountManager();
        }
        return AccountManager.amg;
    }


    public static boolean isLogin(Context c) {
        if (Constants.get(c, "memberinfo") != null) {
            return true;
        }

        return false;
    }

    public void doLogout(final Context c, final AccountManagerCallback _callback) {
        this.callback = _callback;

        Constants.clear(c, "drivercode");
        Constants.clear(c, "memberinfo");
        Constants.clear(c, "carnum");
        Constants.clear(c, "driver_phone");
        Constants.clear(c, "driver_name");


        if (callback != null) {
            callback.doLogout(true, "");
        }
    }

    // driver phone
    public void saveDriverPhone(Context c, String phone) {
        Constants.set(c, "driver_phone", Constants.Encode64(phone));
    }

    public String getDriverPhone(Context c) {
        return Constants.Decode64(Constants.get(c, "driver_phone"));
    }

    // driver name
    public void saveDriverName(Context c, String name) {
        Constants.set(c, "driver_name", Constants.Encode64(name));
    }

    public String getDriverName(Context c) {
        return Constants.Decode64(Constants.get(c, "driver_name"));
    }


    // carnum
    public void saveCarNum(Context c, String carNum) {
        Constants.set(c, "carnum", Constants.Encode64(carNum));
    }

    public String getCarNum(Context c) {
        return Constants.Decode64(Constants.get(c, "carnum"));
    }


    // DriverCode
    public void saveDriverCode(Context c, String driveCode) {
        Constants.set(c, "drivercode", Constants.Encode64(driveCode));
    }

    public String getDriverCode(Context c) {
        return Constants.Decode64(Constants.get(c, "drivercode"));
    }

    public void saveMemberInfo(Context c, String memberJson) {
        Constants.set(c, "memberinfo", Constants.Encode64(memberJson));
    }

    public void saveWorkStationCode(Context c, String workStationCode) {
        Constants.set(c, "workStationCode", Constants.Encode64(workStationCode));
    }

    public String getWorkStationCode(Context c) {
        return Constants.Decode64(Constants.get(c, "workStationCode"));
    }

    public void saveWorkStationName(Context c, String workStationName) {
        Constants.set(c, "workStationName", Constants.Encode64(workStationName));
    }

    public String getWorkStationName(Context c) {
        return Constants.Decode64(Constants.get(c, "workStationName"));
    }

    public void saveWorkSendCode(Context c, String workSendCode) {
        Constants.set(c, "workSendCode", Constants.Encode64(workSendCode));
    }

    public String getWorkSendCode(Context c) {
        return Constants.Decode64(Constants.get(c, "workSendCode"));
    }

    public void saveWorkDeliveryCode(Context c, String workDeliveryCode) {
        Constants.set(c, "workDeliveryCode", Constants.Encode64(workDeliveryCode));
    }

    public String getWorkDeliveryCode(Context c) {
        return Constants.Decode64(Constants.get(c, "workDeliveryCode"));
    }

    // 行動派遣 預設點選的排序
    public void saveListOrderType(Context c, String listOrderType) {
        Constants.set(c, "listOrderType", listOrderType);
    }

    public String getListOrderType(Context c) {
        return Constants.get(c, "listOrderType");
    }

    public JSONObject getMemberInfo(Context c) {
        try {
            String strMember = Constants.Decode64(Constants.get(c, "memberinfo"));
            JSONObject objJSON = new JSONObject(strMember);

            return objJSON;
        } catch (Exception e) {
            return null;
        }

    }


}
