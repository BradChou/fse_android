package tw.com.fsexpress.framework.database.room.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "DeliveryData")
public class DeliveryData implements StatisticsData {

    @PrimaryKey
    @NonNull
    private String id;
    private String driver_code;
    private String check_number;
    private String date;
    private long timestamp;

    public DeliveryData(@NonNull String id, String driver_code, String check_number, String date, long timestamp) {
        this.id = id;
        this.driver_code = driver_code;
        this.check_number = check_number;
        this.date = date;
        this.timestamp = timestamp;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getDriver_code() {
        return driver_code;
    }

    public void setDriver_code(String driver_code) {
        this.driver_code = driver_code;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "DeliveryData{" +
                "id='" + id + '\'' +
                ", driver_code='" + driver_code + '\'' +
                ", check_number='" + check_number + '\'' +
                ", date='" + date + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public String getCheckNumber() {
        return getCheck_number();
    }
}
