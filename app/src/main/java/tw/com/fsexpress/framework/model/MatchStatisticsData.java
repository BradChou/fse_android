package tw.com.fsexpress.framework.model;

import java.util.Objects;

public class MatchStatisticsData {
    private String check_number;
    private String case_count;

    public MatchStatisticsData(String check_number, String case_count) {
        this.check_number = check_number;
        this.case_count = case_count;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getCase_count() {
        return case_count;
    }

    public void setCase_count(String case_count) {
        this.case_count = case_count;
    }

    @Override
    public String toString() {
        return "MatchStatisticsData{" +
                "check_number='" + check_number + '\'' +
                ", case_count='" + case_count + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchStatisticsData that = (MatchStatisticsData) o;
        return Objects.equals(check_number, that.check_number) && Objects.equals(case_count, that.case_count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(check_number, case_count);
    }
}
