package tw.com.fsexpress.framework.database.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import tw.com.fsexpress.framework.database.room.dao.CollectGoodsDao;
import tw.com.fsexpress.framework.database.room.dao.DeliveryDao;
import tw.com.fsexpress.framework.database.room.dao.DischargeDao;
import tw.com.fsexpress.framework.database.room.dao.MatchedDao;
import tw.com.fsexpress.framework.database.room.entity.CollectGoodsData;
import tw.com.fsexpress.framework.database.room.entity.DeliveryData;
import tw.com.fsexpress.framework.database.room.entity.DischargeData;
import tw.com.fsexpress.framework.database.room.entity.MatchedData;

@Database(entities = {CollectGoodsData.class, DischargeData.class, DeliveryData.class, MatchedData.class}, version = 1, exportSchema = true)
public abstract class AppDataBase extends RoomDatabase {
    public static final String DB_NAME = "FSE.db";
    private static volatile AppDataBase instance;

    public abstract CollectGoodsDao getCollectGoodsDao();

    public abstract DischargeDao getDischargeDao();

    public abstract DeliveryDao getDeliveryDao();

    public abstract MatchedDao getMatchedDao();

    public static synchronized AppDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDataBase.class, DB_NAME).build();
        }
        return instance;
    }
}
