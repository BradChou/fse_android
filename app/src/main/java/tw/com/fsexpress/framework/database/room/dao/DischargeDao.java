package tw.com.fsexpress.framework.database.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import tw.com.fsexpress.framework.database.room.entity.DischargeData;

@Dao
public interface DischargeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertData(DischargeData dischargeData);

    /**
     * 撈取此帳號在此段日期區間的資料
     */
    @Query("SELECT * FROM DischargeData WHERE driver_code = :driverCode AND date BETWEEN :startTime AND :endTime")
    List<DischargeData> findDataByDriverCodeAndDate(String driverCode, String startTime, String endTime);

    /**
     * 撈取全部資料
     */
    @Query("SELECT * FROM DischargeData")
    List<DischargeData> displayAllData();

    @Update
    void updateData(DischargeData dischargeData);

    @Delete
    void deleteData(DischargeData dischargeData);

    @Query("DELETE FROM DischargeData WHERE timestamp < :timestamp")
    void deleteDataFormTime(long timestamp);
}
