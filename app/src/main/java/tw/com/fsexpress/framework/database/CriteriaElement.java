package tw.com.fsexpress.framework.database;

public abstract class CriteriaElement
{
    public String orderBy = null, groupBy = null;

    public int start, limit;

    public abstract String render();
    public abstract String renderLdap();
    public abstract String renderWhere();
}

