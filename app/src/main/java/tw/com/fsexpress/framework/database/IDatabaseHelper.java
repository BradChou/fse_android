package tw.com.fsexpress.framework.database;

import android.database.Cursor;


public interface IDatabaseHelper {

    public Cursor query(String sql) throws SqliteHelperException;

    public boolean execute(String sql) throws DatabaseHelperException;

    public int insertedId () throws SqliteHelperException;

    public void open ();

    public void close ();
}
