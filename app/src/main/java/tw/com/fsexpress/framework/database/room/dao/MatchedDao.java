package tw.com.fsexpress.framework.database.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import tw.com.fsexpress.framework.database.room.entity.MatchedData;
import tw.com.fsexpress.framework.model.MatchStatisticsData;

@Dao
public interface MatchedDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertData(MatchedData matchedData);

    /**
     * 撈取此帳號在此段日期區間的資料
     */
    @Query("SELECT check_number,case_count FROM MatchedData WHERE driver_code = :driverCode AND date BETWEEN :startTime AND :endTime")
    List<MatchStatisticsData> findDataByDriverCodeAndDate(String driverCode, String startTime, String endTime);

    /**
     * 撈取全部集貨2.0資料
     */
    @Query("SELECT * FROM MatchedData")
    List<MatchedData> displayAllData();

    @Update
    void updateData(MatchedData matchedData);

    @Delete
    void deleteData(MatchedData matchedData);

    @Query("DELETE FROM MatchedData WHERE timestamp < :timestamp")
    void deleteDataFormTime(long timestamp);
}
