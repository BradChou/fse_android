package tw.com.fsexpress.framework.database;


import java.util.ArrayList;
import java.util.List;

public class CriteriaCompo extends CriteriaElement
{
    private List<Criteria> elements = new ArrayList<Criteria>();
    private List<String> conditions = new ArrayList<String>();


    public CriteriaCompo ()
    {
        super();
    }

    public CriteriaCompo (Criteria element, String condition)
    {
        super();

        this.elements.add(element);
        this.conditions.add(condition);
    }

    public CriteriaCompo (Criteria element)
    {
        this(element, "AND");
    }

    public void add (Criteria element)
    {
        this.add(element, "AND");
    }

    public void add (Criteria element, String condition)
    {
        this.elements.add(element);
        this.conditions.add(condition);
    }
    @Override
    public String render()
    {
        String claues = new String();
        Integer count = this.elements.size();

        if (count > 0)
        {
            claues = String.format("( %s", this.elements.get(0).render());

            for (int idx = 1; idx < count; idx++)
            {
                claues += String.format(" %s %s", this.conditions.get(idx), this.elements.get(idx).render());
            }

            claues += String.format(")");
        }

        return claues;
    }

    @Override
    public String renderLdap()
    {
        String claues = new String();

        Integer count = this.elements.size();

        if (count > 0)
        {
            claues += String.format("( %s", this.elements.get(0).renderLdap());

            for (int idx = 1; idx < count; idx++)
            {
                String condition = this.conditions.get(idx).toUpperCase();

                String operator = (condition.equals("AND")) ? "&" : "|";

                claues += String.format("( %s %s %s )", operator, claues, this.elements.get(idx).renderLdap());
            }
        }

        return claues;
    }

    @Override
    public String renderWhere()
    {
        String claues = this.render();

        return (claues.length() != 0) ? String.format("WHERE %s", claues) : claues;
    }

}
