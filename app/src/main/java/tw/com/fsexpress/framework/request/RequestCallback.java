package tw.com.fsexpress.framework.request;

public interface RequestCallback
{

    public static enum RequestStatus
    {
        Fail,
        NoNetworkConnection,
        WillRequest,
        StartResponse,
        ReceiveProcess,
        ReceiveProcessInThread,
        Finish,
        End
    };


    public void detectRequestStatus (RequestHandler handler, RequestStatus status);
}