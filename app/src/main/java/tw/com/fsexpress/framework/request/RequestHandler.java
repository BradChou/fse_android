package tw.com.fsexpress.framework.request;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import tw.com.fsexpress.framework.IGHWHandler;
import tw.com.fsexpress.framework.request.RequestCallback.RequestStatus;

public class RequestHandler extends IGHWHandler {

    private ArrayList<RequestCallback> listeners = new ArrayList<RequestCallback> ();

    private Integer progress = 0;
    private long totalSize = 0;

    private ByteArrayOutputStream receiveOutputStream = null;

    private int responseCode = 0;
    private String receiveString = new String();

    private RequestStatus[] what = RequestStatus.values();
    private RequestTask mRequestTask = null;
    public RequestHandler () { super(); }

    public long getTotalSize () { return this.totalSize; }

    public void cleanReceiveOutputStream ()
    {
        receiveOutputStream = new ByteArrayOutputStream();
    }
    public RequestObject createObject ()
    {
        return new RequestObject();
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public RequestTask request (RequestObject object)
    {
        Log.e("ALVINHSIEH", "REUEST");
        mRequestTask = new RequestTask();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            mRequestTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, object);
        else
            mRequestTask.execute(object);


        return mRequestTask;
    }

    public void request (RequestObject object, RequestCallback callback)
    {
        this.addListener(callback); this.request(object);
    }
    public void addListener(RequestCallback callback)
    {
        this.listeners.add((RequestCallback) callback);
    }
    public void abort()
    {
        Log.e("ALVINHSIEH", "ABORT");

        mRequestTask.abort();

        mRequestTask = null;

    }
    public Integer getProgress() { return progress; }

    public ByteArrayOutputStream getReveiveOutputStream() { return this.receiveOutputStream; }

    public int getResponseCode() { return this.responseCode; };

    public String getReceiveString() { return this.receiveString; }

    public void setReceiveString(String string) { this.receiveString = string; }

    private RequestMessageHandler messageHandler = new RequestMessageHandler () {

        @Override
        public void handleMessage(Message msg)
        {

            for (RequestCallback callback : RequestHandler.this.listeners)
            {

                RequestHandler handler = (RequestHandler) msg.obj;

                callback.detectRequestStatus(handler, RequestHandler.this.what[msg.what]);
            }
        }
    };

    @Override
    public boolean reload()
    {
        return true;
    }
    private static class RequestMessageHandler extends Handler {}

    // task class
    public class RequestTask extends AsyncTask<RequestObject, Integer, String>
    {
        private boolean isCanceled = false;

        public void abort() { this.isCanceled = true; }

        @Override
        protected String doInBackground(RequestObject... params)
        {
            // create message to callback
            // request callback
            Message message = new Message();
            message.obj = RequestHandler.this;
            message.what = RequestStatus.WillRequest.ordinal();
            RequestHandler.this.messageHandler.sendMessage(message);

            // start request
            RequestObject object = params[0];


            HttpURLConnection connection = object.render();

            // this will be used in reading the data from the internet
            InputStream inputStream = null;

            try
            {
//				for (String key : connection.getHeaderFields().keySet())
//				{
//					System.out.println(key + "=" + connection.getHeaderFields().get(key));
//				}

                if (connection == null)
                {
                    throw new Exception("connect rander fail.");
                }

                if (this.isCanceled) return new String();

                connection.connect();

                // init receive
                RequestHandler.this.receiveOutputStream = new ByteArrayOutputStream();


                Log.e("con.getResponseCode()", "code = " + connection.getResponseCode());
                responseCode = connection.getResponseCode();

                //inputStream =  connection.getInputStream();//connection.getResponseCode() >= 400 ? connection.getErrorStream() : connection.getInputStream();

                inputStream = connection.getResponseCode() >= 400 ? connection.getErrorStream() : connection.getInputStream();

                long totalSize = connection.getContentLength(), downloadedSize = 0;

                RequestHandler.this.totalSize = totalSize;

                // request callback
                message = new Message();
                message.obj = RequestHandler.this;
                message.what = RequestStatus.StartResponse.ordinal();
                RequestHandler.this.messageHandler.sendMessage(message);

                byte[] buffer = new byte[1024];

                // used to store a temporary size of the buffer
                int bufferLength = 0;

                // now, read through the input buffer and write the contents
                while ((bufferLength = inputStream.read(buffer)) > 0)
                {
                    if (this.isCanceled)
                    {
                        inputStream.close();

                        connection.disconnect();

                        return new String();
                    }
                    RequestHandler.this.receiveOutputStream.write(buffer, 0, bufferLength);

                    if (totalSize > 0 )
                    {
                        downloadedSize += bufferLength;

                        Integer progress = (int) (((float) downloadedSize / (float) totalSize) * 100.0);

                        // set progress and callback
                        RequestHandler.this.progress = progress;

                        // call in thread
                        for (RequestCallback callback : RequestHandler.this.listeners)
                        {
                            callback.detectRequestStatus(RequestHandler.this, RequestStatus.ReceiveProcessInThread);
                        }

                        message = new Message();
                        message.obj = RequestHandler.this;
                        message.what = RequestStatus.ReceiveProcess.ordinal();
                        RequestHandler.this.messageHandler.sendMessage(message);
                    }
                }


                String contentType = connection.getHeaderField("Content-type");

                String[] types = contentType.split("/");

                //if (types[0].equals("text"))
                {
                    RequestHandler.this.setReceiveString(new String(RequestHandler.this.receiveOutputStream.toByteArray(), "UTF-8"));
                }

                message = new Message();
                message.obj = RequestHandler.this;
                message.what = RequestStatus.Finish.ordinal();
                RequestHandler.this.messageHandler.sendMessage(message);

            }
            catch (Exception e)
            {
                message = new Message();
                message.obj = RequestHandler.this;
                message.what = RequestStatus.Fail.ordinal();
                RequestHandler.this.messageHandler.sendMessage(message);

                System.out.println("request exception");
                e.printStackTrace();

                return new String();
            }
            finally
            {
                message = new Message();
                message.obj = RequestHandler.this;
                message.what = RequestStatus.End.ordinal();
                RequestHandler.this.messageHandler.sendMessage(message);
            }

            return new String();
        }

        @Override
        protected void onPreExecute()  { super.onPreExecute(); }

        @Override
        protected void onProgressUpdate(Integer... progress) { super.onProgressUpdate(progress); }

        @Override
        protected void onPostExecute(String result) { super.onPostExecute(result); }
    }


    @Override
    public boolean renewable() { return true;}
}
