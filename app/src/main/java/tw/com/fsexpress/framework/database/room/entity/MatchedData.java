package tw.com.fsexpress.framework.database.room.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import tw.com.fsexpress.framework.model.MatchStatisticsData;

@Entity(tableName = "MatchedData")
public class MatchedData implements StatisticsData {

    @PrimaryKey
    @NonNull
    private String id;
    private String driver_code;
    private String check_number;
    private String case_count;
    private String date;
    private long timestamp;

    public MatchedData(@NonNull String id, String driver_code, String check_number, String case_count, String date, long timestamp) {
        this.id = id;
        this.driver_code = driver_code;
        this.check_number = check_number;
        this.case_count = case_count;
        this.date = date;
        this.timestamp = timestamp;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getDriver_code() {
        return driver_code;
    }

    public void setDriver_code(String driver_code) {
        this.driver_code = driver_code;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getCase_count() {
        return case_count;
    }

    public void setCase_count(String case_count) {
        this.case_count = case_count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public MatchStatisticsData getMatchData() {
        return new MatchStatisticsData(getCheck_number(), getCase_count());
    }


    @Override
    public String toString() {
        return "MatchedData{" +
                "id='" + id + '\'' +
                ", driver_code='" + driver_code + '\'' +
                ", check_number='" + check_number + '\'' +
                ", case_count='" + case_count + '\'' +
                ", date='" + date + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public String getCheckNumber() {
        return getCheck_number();
    }
}

