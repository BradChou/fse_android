package tw.com.fsexpress.framework.database;


public class Criteria extends CriteriaElement
{
    private String column = null,
            operator = null;

    private Object value = null;

    private Criteria.TYPE type = Criteria.TYPE.NORMAL;

    public static enum TYPE {
        NORMAL,
        SPECIAL
    };

    public Criteria (String column, Object value, String operator, Criteria.TYPE type)
    {
        super();

        this.column = column;
        this.value = value;
        this.operator = operator;
        this.type = type;
    }
    public Criteria (String column, Object value, String operator)
    {
        this (column, value, operator, Criteria.TYPE.NORMAL);
    }
    public Criteria (String column, Object value)
    {
        this (column, value, "=", Criteria.TYPE.NORMAL);
    }

    @Override
    public String render()
    {
        String query = null, claues = null;

        if ("IN".equals(this.operator.toUpperCase()) || "NOT IN".equals(this.operator.toUpperCase()))
        {
            query = this.renderLdap();
        }
        else
        {
            if (this.value == null)
            {
                return new String();
            }

            if (this.value instanceof Number)
            {
                this.value = this.value.toString();
            }

            query = ((String) this.value).trim().replaceAll("\n", "");

            if (query.length() == 0)
            {
                return new String();
            }

            if (this.type == Criteria.TYPE.NORMAL)
            {
                if (! query.substring(0, 1).equals("`") &&
                        ! query.substring(query.length() - 1, query.length()).equals("`"))
                {
                    query = query.replace("'", "'''");
                }

                query = String.format("'%s'", query);
            }
        }

        claues = String.format("`%s` %s %s", this.column, this.operator, query);

        return claues;

    }

    @Override
    public String renderLdap()
    {
        String claues = null;

        if (this.operator.equals(">")) { this.operator = ">="; }

        if (this.operator.equals("<")) { this.operator = "<="; }

        if (this.value instanceof Number)
        {
            this.value = this.value.toString();
        }

        if (this.operator.equals("!=") || this.operator.equals("<>"))
        {
            claues = String.format("(!(%s = %s))", this.column, this.value);
        }
        else
        {
            String value = (String) this.value;

            if ("IN".equals(this.operator.toUpperCase()) ||
                    "NOT IN".equals(this.operator.toUpperCase()))
            {
                String newString = value.replace("(", "").replace(")", "").replace("(", "");

                claues = new String();

                String[] splitString= newString.split(",");

                for(int idx = 0; idx < splitString.length; idx++)
                {
                    claues = claues.concat(String.format("'%s',", splitString[idx]));
                }

                if (claues.length() > 0)
                {
                    claues = claues.substring(0, claues.length() - 1);
                }

                claues = String.format("(%s)", claues);

            }
            else
            {
                claues = String.format("(%s %s %s)", this.column, this.operator, this.value);
            }
        }

        return claues;
    }

    @Override
    public String renderWhere()
    {
        String condition = null;

        condition = this.render();

        return  (condition.length() == 0) ? new String() : String.format("WHERE %s", condition);
    }

}
