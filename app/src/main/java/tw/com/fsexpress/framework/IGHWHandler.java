package tw.com.fsexpress.framework;

public abstract class IGHWHandler implements Cloneable {

    protected String identify = null;

    public abstract boolean renewable ();
    public abstract boolean reload ();

    public String getIdentify ()
    {
        return this.identify;
    }

    public void setIdentify (String identify)
    {
        if (this.identify != null) return;

        this.identify = identify;
    }

    public Object newInstance()
    {
        try
        {
            return this.getClass().newInstance();
        }
        catch (IllegalAccessException e)
        {
            return null;
        }
        catch (InstantiationException e)
        {
            return null;
        }
    }
    public Object clone()
    {
        try
        {

            IGHWHandler handler = (IGHWHandler) super.clone();

            return handler;

        }
        catch (CloneNotSupportedException ex)
        {
            return null;
        }
    }
}
