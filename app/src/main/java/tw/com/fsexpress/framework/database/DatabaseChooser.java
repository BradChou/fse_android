package tw.com.fsexpress.framework.database;



import tw.com.fsexpress.framework.dataobject.DataObject;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class DatabaseChooser {

    private static DatabaseChooser instance = new DatabaseChooser();

    private HashMap<String, IDatabaseHelper> helpers = new HashMap<String, IDatabaseHelper>();

    private DatabaseChooser ()
    {

    }

    public static DatabaseChooser instance ()
    {
        return DatabaseChooser.instance;
    }

    public IDatabaseHelper helperWithName(DataObject helperConfig)
    {
        String helperName 	   = (String) helperConfig.getVariable("db");
        String helperPath 	   = (String) helperConfig.getVariable("path");
        String helperSavePath  = (String) helperConfig.getVariable("save");
        String helperClassName = (String) helperConfig.getVariable("helper");

        IDatabaseHelper helper = this.helpers.get(helperName);

        if (helper != null) return helper;

        try
        {
            String  className = String.format("%s.%sHelper", this.getClass().getPackage().getName(),
                    helperClassName.substring(0, 1).toUpperCase() +
                            helperClassName.substring(1));

            if (! classExists(className))
            {
                className = String.format("%s.%s", this.getClass().getPackage().getName(),
                        helperClassName.substring(0, 1).toUpperCase() +
                                helperClassName.substring(1));

                if (! classExists(className))
                {
                    throw new ClassNotFoundException ("helper class not instanceof IDatabaseHelper");
                }
            }

            // deploy db file
            helperSavePath = DatabaseDeployer.deploy (helperPath, helperSavePath);

            Class<?> helperClass = Class.forName(className);

            Object helperInstance = helperClass.getDeclaredConstructor(new Class[] { String.class})
                    .newInstance(new Object[] { helperSavePath });

            if (! (helperInstance instanceof IDatabaseHelper) )
            {
                throw new ClassNotFoundException ("helper class not instanceof IDatabaseHelper");
            }

            this.helpers.put(helperName, (IDatabaseHelper) helperInstance);
        }
        catch (ClassNotFoundException e)
        {
            System.out.println (e.getMessage());
            return null;
        }
        catch (InstantiationException e)
        {
            System.out.println (e.getMessage());
            return null;
        }
        catch (IllegalAccessException e)
        {
            System.out.println (e.getMessage());
            return null;
        }
        catch (IllegalArgumentException e)
        {
            return null;
        }
        catch (SecurityException e)
        {
            return null;
        }
        catch (InvocationTargetException e)
        {
            return null;
        }
        catch (NoSuchMethodException e)
        {
            return null;
        }
        catch (DatabaseDeployerException e)
        {
            return null;
        }
        catch (IOException e)
        {
            return null;
        }

        return this.helpers.get(helperName);
    }

    private boolean classExists (String className)
    {
        try
        {
            Class.forName(className);

            return true;
        }
        catch ( ClassNotFoundException ex)
        {
            return false;
        }
    }
}
