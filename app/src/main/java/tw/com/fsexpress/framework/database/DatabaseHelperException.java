package tw.com.fsexpress.framework.database;

import android.util.Log;

public class DatabaseHelperException extends Exception
{

    private static final long serialVersionUID = 1L;

    public DatabaseHelperException(String message)
    {
        super(message);

        message = String.format("%s : %s",  Thread.currentThread().getStackTrace().toString(),
                message);

        Log.e(this.getClass().getName(), message);

    }
}
