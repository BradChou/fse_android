package tw.com.fsexpress.framework.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.File;

public class SqliteHelper implements IDatabaseHelper
{
    private String filePath = null;

    public SQLiteDatabase handler = null;

    public SqliteHelper(String databasePath) throws SqliteHelperException, SQLiteException
    {
        File databaseFile = new File(databasePath);

        if (! databaseFile.exists())
        {
            throw new SqliteHelperException ("databse file not found.");
        }

        this.filePath = databasePath;

        this.open();
    }

    /**
     * open database
     */
    public void open ()
    {
        try
        {
            this.handler = SQLiteDatabase.openDatabase(this.filePath,
                    null,
                    SQLiteDatabase.OPEN_READWRITE);

            if (this.handler == null)
            {
                throw new SQLiteException ("open database fail.");
            }
        }
        catch (Exception e)
        {
            throw new SQLiteException ("open database fail.");
        }
    }

    /**
     * close database
     */
    @Override
    public void close ()
    {
        try
        {
            if (this.handler != null)
            {
                this.handler.close();
            }
        }
        catch (Exception e)
        {
            throw new SQLiteException ("close database file.");
        }
    }

    @Override
    public boolean execute(String sql) throws SqliteHelperException //chekc null
    {
        if (this.handler == null)
        {
            throw new SqliteHelperException("execute: handler is null");
        }

        if ( ! (sql.toLowerCase().substring(0, "insert".length()).equals("insert")) &&
                ! (sql.toLowerCase().substring(0, "update".length()).equals("update")) &&
                ! (sql.toLowerCase().substring(0, "delete".length()).equals("delete")) )
        {
            throw new SqliteHelperException("execute: this method can't use for insert or update");
        }

        try
        {
            this.handler.execSQL(sql);

            return true;
        }
        catch (SQLiteConstraintException ex)
        {
            Log.e(this.getClass().getName(), "sql exception : " + sql);

            return false;
        }
    }

    @Override
    public Cursor query(String sql) throws SqliteHelperException
    {
        if (this.handler == null)
        {
            throw new SqliteHelperException("handler is null");
        }

        if ( (sql.toLowerCase().substring(0, "insert".length()).equals("insert")) ||
                (sql.toLowerCase().substring(0, "update".length()).equals("update")) ||
                (sql.toLowerCase().substring(0, "delete".length()).equals("delete")) )
        {
            throw new SqliteHelperException("this method only use for select");
        }

        try
        {
            return this.handler.rawQuery(sql, null);
        }
        catch (SQLiteConstraintException ex)
        {
            Log.e(this.getClass().getName(), "sql exception : " + sql);

            return null;
        }

    }

    @Override
    public int insertedId() throws SqliteHelperException {

        Cursor cursor = this.query("SELECT last_insert_rowid();");


        if (! cursor.moveToFirst())
        {
            Log.e(this.getClass().getName(), "get inserted Id fail");
        }

        if (cursor.getCount() == 0)
        {
            Log.e(this.getClass().getName(), "get inserted cursor count fail");
        }

        int insertId = cursor.getInt(0);

        cursor.close();

        return insertId;
    }
}
