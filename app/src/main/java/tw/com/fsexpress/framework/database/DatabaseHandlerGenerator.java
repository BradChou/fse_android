package tw.com.fsexpress.framework.database;

import tw.com.fsexpress.framework.IHandlerGenerator;
import tw.com.fsexpress.framework.IGHWHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;


import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseHandlerGenerator implements IHandlerGenerator {

    @Override
    public ArrayList<IGHWHandler> generator(HashMap<String, String> settings) {

        String dbName 	  = settings.get("db");
        String dbPath 	  = settings.get("path");
        String dbHelper   = settings.get("helper");
        String dbType 	  = settings.get("type");
        String dbSavePath = settings.get("save");

        if (dbName == null || dbPath == null || dbHelper == null || dbType == null ||
                dbSavePath == null)
        {
            System.out.println("one of the (dbName & dbPath & dbHelper & dbType & dbSavePath) not set");
        }

        DataObject dbConfig = new DataObject();

        // quick init variables

        dbConfig.initVariable("db",   	(Object) dbName);
        dbConfig.initVariable("path",	(Object) dbPath);
        dbConfig.initVariable("helper", (Object) dbHelper);
        dbConfig.initVariable("type",   (Object) dbType);
        dbConfig.initVariable("save", 	(Object) dbSavePath);

        DatabaseHandler handler = new DatabaseHandler(dbConfig);

        ArrayList<DataObject> tableList = handler.invincibleSql("select * from sqlite_master where ( type = 'table' or type = 'view' )");

        ArrayList<IGHWHandler> handlers = new ArrayList<IGHWHandler>();

        for (DataObject object : tableList)
        {
            String tableName = (String) object.getVariable("name");
            String identify = String.format("ghw.db.%s.%s", dbName, tableName);

            DatabaseHandler tableHandler = new DatabaseHandler(dbConfig);

            tableHandler.setIdentify(identify);

            tableHandler.setTableName(tableName);

            handlers.add(tableHandler);
        }

        return handlers;
    }
}
