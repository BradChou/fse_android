package tw.com.fsexpress.framework;


import tw.com.fsexpress.framework.utility.Functions;

import java.util.ArrayList;
import java.util.HashMap;

public class Commander {

    private static Commander instance = new Commander();
    private HashMap<String, IGHWHandler> handlers = new HashMap<String, IGHWHandler>();

    private Commander ()
    {
        super ();

        Configure config = Configure.instance();

        // get database setting
        ArrayList<HashMap<String, String>> settings = config.getList("database");

        if (settings != null && settings.size() > 0)
        {
            for (HashMap<String, String> setting : settings)
            {
                this.addHandler("database", setting);
            }
        }

        this.addHandler("request", null);

    }

    public static Commander instance ()
    {
        return Commander.instance;
    }

    public void reload()
    {
        for (String key : this.handlers.keySet())
        {
            IGHWHandler handler = this.handlers.get(key);

            handler.reload();
        }
    }
    public void addHandler(String name, HashMap<String, String> settings)
    {
        String packageName = settings == null || settings.get("package") == null ?
                this.getClass().getPackage().getName() + "." + name :
                settings.get("package");

        String className = packageName.concat(".").concat(name);

        if (! Functions.classExists(className))
        {
            String appendName = name.substring(0, 1).toUpperCase() +
                    name.substring(1).concat("HandlerGenerator");

            className = packageName.concat(".").concat(appendName);

            if (! Functions.classExists(className))
            {
                System.out.println("handler " + className + " not exists");
                return;
            }
        }

        try
        {
            Class<?> handlerClass = Class.forName(className);

            IHandlerGenerator handlerGenerator = (IHandlerGenerator) handlerClass.newInstance();

            for (IGHWHandler handler : handlerGenerator.generator(settings))
            {
                this.handlers.put(handler.getIdentify(), handler);
            }
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    public IGHWHandler handlerWithName(String name)
    {
        IGHWHandler handler = this.handlers.get(name);

        if (handler == null)
        {
            System.out.println("handler not found, name = " + name);

            return null;
        }
        return (handler.renewable()) ? (IGHWHandler) handler.newInstance() : this.handlers.get(name);
    }

    public ArrayList<String> allHandlerName()
    {
        return new ArrayList<String> (this.handlers.keySet());
    }
}
