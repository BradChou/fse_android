package tw.com.fsexpress.framework;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class Configure {
    private final static String CONFIGURE_FILE_NAME = "/assets/GHWConfig.ini";

    private static Configure instance = new Configure();;

    private Properties properties = null;

    private Configure()
    {
        try
        {
            this.properties = new Properties();

            InputStream configfile = Configure.class.getResourceAsStream(Configure.CONFIGURE_FILE_NAME);

            this.properties.load(configfile);
        }
        catch (IOException ex)
        {
            System.out.println("Configuration error: " + ex.getMessage());
        }
    }

    public static Configure instance ()
    {
        return Configure.instance;
    }


    /**
     * get list by section name
     *
     * @param sectionName  config prefix name
     */
    public ArrayList<HashMap<String, String>> getList(String sectionName)
    {
        //database.1.db

        // get array


        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();

        ArrayList<String> matchKeys = new ArrayList<String>();

        ArrayList<Object> allKeys = new ArrayList<Object>(this.properties.keySet());

        for (Object key : allKeys)
        {
            if (key.toString().length() < sectionName.length()) { continue; }

            // check section name
            String perfixName = key.toString().substring(0, sectionName.length());

            if (perfixName.equals(sectionName))
            {
                matchKeys.add(key.toString());
            }
        }

        int idx = 1;

        while (matchKeys.size() != 0)
        {
            String matchSectionRegx = String.format("^%s\\.%d.+", sectionName, idx);
            String matchSectionKey = String.format("%s.%d.", sectionName, idx);

            HashMap<String, String> keyValue = new HashMap<String, String>();

            // set remove keys
            ArrayList<String> removeKeys = new ArrayList<String>();

            for (String match : matchKeys)
            {

                if (match.matches(matchSectionRegx))
                {
                    keyValue.put(match.replace(matchSectionKey, new String()),
                            this.properties.getProperty(match));

                    removeKeys.add(match);
                }
            }

            // remove matchkeys
            for (String remove : removeKeys)
            {
                matchKeys.remove(remove);
            }

            removeKeys = null;

            Log.e("keyValue", keyValue.toString());

            result.add(keyValue);

            idx ++;

        }

        return result.size() > 0 ? result : null;
    }

    public String getString(String key)
    {
        return this.properties.getProperty(key);
    }
    /**
     * get list or string by section name
     *
     * @param key  config name
     */

    public Object get(String key)
    {
        String value = this.properties.getProperty(key);

        if (value != null) { return value; }

        int idx = 1;

        ArrayList<String> result = new ArrayList<String>();

        do
        {
            String match = String.format("%s.%d", key, idx);

            value = this.properties.getProperty(match);

            if (value == null) { break; }

            result.add(value);

            idx ++;
        } while (value != null);

        return result.size() == 0 ? null : result;
    }
}
