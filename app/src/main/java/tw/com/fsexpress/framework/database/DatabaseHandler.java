package tw.com.fsexpress.framework.database;

import android.database.Cursor;
import android.util.Log;

import tw.com.fsexpress.framework.IGHWHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DatabaseHandler extends IGHWHandler
{
    private IDatabaseHelper helper = null;

    private ArrayList<DataObject> schemas = new ArrayList<DataObject>();

    private HashMap<String, DataObject.DataType> typeMappings = new HashMap<String, DataObject.DataType>();

    private String tableName = null, primaryKey = null;

    /*
     * helper name is database identify
     */
    public DatabaseHandler (DataObject helperConfig)
    {
        this.helper = DatabaseChooser.instance().helperWithName(helperConfig);
    }

    public boolean reload ()
    {
        try
        {
            if (this.helper != null)
            {
                this.helper.close();

                this.helper.open();

            }
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public void setTableName(String tableName)
    {
        this.tableName = tableName;

        String sql = String.format("pragma table_info (`%s`)", tableName);

        this.schemas = this.invincibleSql(sql);

        for (DataObject object : this.schemas)
        {
            String columnName = (String) object.getVariable("name");

            this.typeMappings.put(columnName, this.sqliteType2DataType( (String) object.getVariable("type")));


            if (Integer.parseInt((String) object.getVariable("pk")) == 1)
            {
                System.out.println(columnName +"="+  (String) object.getVariable("pk") +"="+ Boolean.valueOf((String) object.getVariable("pk")));

                this.primaryKey = columnName;
            }
        }
    }

    private DataObject.DataType sqliteType2DataType(String type)
    {
        type = type.toLowerCase();

        if (type.equals("integer") || type.equals("int") || type.equals("tinyint") ||
                type.equals("smallint") || type.equals("mediumint") || type.equals("bitint") ||
                type.equals("unsigned big int") || type.equals("int2") || type.equals("int8") ||
                type.equals("boolean") || type.equals("numeric") )
        {
            return DataObject.DataType.INT;
        }
        if (type.equals("real") || type.equals("double") || type.equals("double precision") ||
                type.equals("float"))
        {
            return DataObject.DataType.FLOAT;
        }
        if (type.equals("bolb"))
        {
            return DataObject.DataType.BLOB;
        }

        return DataObject.DataType.STRING;

    }
    public ArrayList<DataObject> invincibleSql(String sql)
    {
        try
        {
            if (this.helper == null)
            {
                Log.e("DataObjectHandler", "helper is null");

                return null;
            }
            else{
                ArrayList<DataObject> objects = new ArrayList();
                if (!sql.toLowerCase().substring(0, "insert".length()).equals("insert") &&
                        !sql.toLowerCase().substring(0, "update".length()).equals("update") &&
                        !sql.toLowerCase().substring(0, "delete".length()).equals("delete"))
                {
                    Cursor cursor = this.helper.query(sql);
                    if (cursor == null)
                    {
                        throw new DatabaseHelperException("cursor is null");
                    }
                    else if (!cursor.moveToFirst())
                    {
                        throw new DatabaseHelperException("move curosr to first fail");
                    }
                    else {
                        for(int idx = 0; idx < cursor.getCount(); ++idx) {
                            DataObject object = new DataObject();

                            for(int iterate = 0; iterate < cursor.getColumnCount(); ++iterate) {
                                String columnName = cursor.getColumnName(iterate);
                                if (this.typeMappings.size() == 0) {
                                    object.initVariable(columnName, DataObject.DataType.STRING, cursor.getString(iterate));
                                } else {
                                    DataObject.DataType columnType = (DataObject.DataType)this.typeMappings.get(columnName);
                                    if (columnType == DataObject.DataType.INT) {
                                        object.initVariable(columnName, columnType, cursor.getInt(iterate));
                                    }

                                    if (columnType == DataObject.DataType.FLOAT) {
                                        object.initVariable(columnName, columnType, cursor.getFloat(iterate));
                                    }

                                    if (columnType == DataObject.DataType.BLOB) {
                                        object.initVariable(columnName, columnType, cursor.getBlob(iterate));
                                    }

                                    if (columnType == DataObject.DataType.STRING) {
                                        object.initVariable(columnName, columnType, cursor.getString(iterate));
                                    }
                                }
                            }

                            object.setStatus(DataObject.Status.OLD);
                            objects.add(object);
                            cursor.moveToNext();
                        }

                        cursor.close();
                        return objects;
                    }
                } else {
                    return this.helper.execute(sql) ? objects : null;
                }
            }

//            ArrayList<DataObject> objects = new ArrayList<DataObject>();
//
//            if ( (sql.toLowerCase().substring(0, "insert".length()).equals("insert")) ||
//                    (sql.toLowerCase().substring(0, "update".length()).equals("update")) ||
//                    (sql.toLowerCase().substring(0, "delete".length()).equals("delete")) )
//            {
//
//                if (this.helper.execute(sql))
//                {
//                    return objects;
//                }
//                return null;
//            }
//
//            Log.e("Jerry", "cursor sql:"+sql);
//
//
//            Cursor cursor = this.helper.query(sql);
//
//            Log.e("Jerry", "cursor count:"+ cursor.getCount());
//
//            if (cursor == null)
//            {
//                throw new DatabaseHelperException("cursor is null");
//            }
//
//            if (! cursor.moveToFirst())
//            {
//                throw new DatabaseHelperException("move curosr to first fail");
//            }
//
//
//            for (int idx = 0; idx < cursor.getCount(); idx ++)
//            {
//                DataObject object = new DataObject();
//
//                for (int iterate = 0; iterate < cursor.getColumnCount(); iterate ++)
//                {
//                    String columnName = cursor.getColumnName(iterate);
//
//                    if (this.typeMappings.size() == 0)
//                    {
//
//                        object.initVariable(columnName, DataObject.DataType.STRING, cursor.getString(iterate));
//                    }
//                    else
//                    {
//                        DataObject.DataType columnType = this.typeMappings.get(columnName);
//
//                        if (columnType == DataObject.DataType.INT)
//                        {
//                            object.initVariable(columnName, columnType, cursor.getInt(iterate));
//                        }
//                        if (columnType == DataObject.DataType.FLOAT)
//                        {
//                            object.initVariable(columnName, columnType, cursor.getFloat(iterate));
//                        }
//                        if (columnType == DataObject.DataType.BLOB)
//                        {
//                            object.initVariable(columnName, columnType, cursor.getBlob(iterate));
//                        }
//                        if (columnType == DataObject.DataType.STRING)
//                        {
//                            object.initVariable(columnName, columnType, cursor.getString(iterate));
//                        }
//                    }
//                }
//
//                object.setStatus(DataObject.Status.OLD);
//
//                objects.add(object);
//
//                cursor.moveToNext();
//            }
//
//            cursor.close();

//            return objects;
        }
        catch (DatabaseHelperException e)
        {
            System.out.println("query fail, reason = " + e.getMessage());
            return new ArrayList<DataObject>();

        }
        catch (Exception e)
        {
            System.out.println("query fail, reason = " + e.getMessage());
            return new ArrayList<DataObject>();
        }
    }

    /**
     * get data by private key
     * @param value private key
     * @return null, DataObject
     */
    public DataObject getDataByPrivateKey(Object value)
    {
        // convert number to string
        if (value instanceof Number) { value = value.toString(); }

        String sql = String.format("SELECT * FROM `%s` WHERE `%s` = '%s'",
                this.tableName, this.primaryKey, value);

        ArrayList<DataObject> objects = this.invincibleSql(sql);

        if (objects == null || objects.size() != 1)
        {
            System.out.println("object count not equal 1");
            return null;
        }

        return objects.get(0);
    }


    /**
     * transaction method
     *
     */
    public void begin ()   { try { this.helper.execute("BEGIN;"); }  catch (DatabaseHelperException e)  { System.out.println("transaction begin fail."); }  }
    public void end ()  { try { this.helper.execute("END;"); }  catch (DatabaseHelperException e)  { System.out.println("transaction end fail."); } }
    public void rollback ()  { try { this.helper.execute("ROLLBACK"); }  catch (DatabaseHelperException e)  { System.out.println("transaction rollback fail."); } }
    /**
     *
     * @param criteria  Criteria
     * @return null, List<DataObject>
     */
    public ArrayList<DataObject> getDataByCriteria (CriteriaElement criteria)
    {
        String sql = String.format("SELECT * FROM `%s`", this.tableName);

        if (criteria != null)
        {
            sql = sql.concat(criteria.renderWhere());

            // set group by
            if (criteria.groupBy != null)
            {
                sql = sql.concat(String.format(" GROUP BY `%s`", criteria.groupBy));
            }

            if (criteria.orderBy != null)
            {
                sql = sql.concat(String.format(" ORDER BY %s", criteria.orderBy));
            }
            else if (this.primaryKey != null)
            {
                sql = sql.concat(String.format(" ORDER BY `%s` ASC", this.primaryKey));
            }
        }
        System.out.println(sql);
        return this.invincibleSql(sql);
    }

    /**
     * get data count
     * @param criteria
     * @return int data count
     */
    public int getCount(CriteriaElement criteria)
    {
        int count = 0;

        String sql = String.format("SELECT COUNT(*) FROM `%s`", this.tableName);

        if (criteria != null)
        {
            sql = sql.concat(String.format(" %s", criteria.renderWhere()));
        }

        try
        {
            Cursor cursor = this.helper.query(sql);

            if (! cursor.moveToFirst()) return 0;

            count = cursor.getInt(0);

            cursor.close();
        }
        catch (Exception ex)
        {
            System.out.println("query count fail");
        }
        return count;
    }

    public boolean insertByDataObject(DataObject object)
    {
        if (this.tableName == null || this.helper == null)
        {
            System.out.println("table name is null");

            return false;
        }

        HashMap<String, Object> variables = object.getVariables();

        String sql = null;

        if (object.getStatus() == DataObject.Status.NEW)
        {
            String mappingKeys = new String (), mappingValues = new String ();

            for (DataObject keyValue : this.schemas)
            {

                String columnName = (String) keyValue.getVariable("name");

                Object myValue = variables.get(columnName);

                String value = null;

                value =  (String) ((myValue instanceof Number) ? ((Number) myValue).toString() :  myValue);

                if (value == null) { value = ""; }

                if (columnName.equals(this.primaryKey))
                {
                    if (value.length() == 0)
                    {
                        mappingKeys = mappingKeys.concat(String.format("`%s`,", columnName));
                        mappingValues = mappingValues .concat("NULL,");

                        continue; // next column
                    }
                }
                System.out.println("te2t" + mappingKeys);
                mappingKeys = mappingKeys.concat(String.format("`%s`,", columnName));
                mappingValues = mappingValues.concat(String.format("'%s',", value.replace("'", "''")));
            }
            System.out.println("=====");
            if (mappingKeys.length() > 0)
            {
                mappingKeys = mappingKeys.substring(0, mappingKeys.length() - 1);
            }

            if (mappingValues.length() > 0)
            {
                mappingValues = mappingValues.substring(0, mappingValues.length() - 1);
            }

            sql = String.format("INSERT INTO `%s` (%s) VALUES (%s)", this.tableName, mappingKeys, mappingValues);

            System.out.println("sql" + sql);
        }
        else
        {
            String mappingValues = new String ();

            String whereValues = null;

            for (DataObject keyValue : this.schemas)
            {
                String columnName = (String) keyValue.getVariable("name");

                Object myValue = variables.get(columnName);

                String value =  (String) ((myValue instanceof Number) ? ((Number) myValue).toString() :  myValue);

                if (value == null) continue;

                if (columnName.equals(this.primaryKey))
                {
                    whereValues = String.format("`%s` = '%s'", columnName, value);
                }
                else
                {
                    mappingValues = mappingValues.concat(String.format("`%s` = '%s',", columnName,  value.replace("'", "''")));
                }
            }

            if (mappingValues.length() > 0)
            {
                mappingValues = mappingValues.substring(0, mappingValues.length() - 1);
            }

            sql = String.format("UPDATE `%s` SET %s WHERE %s;", this.tableName, mappingValues, whereValues);
        }

        try
        {
//            Log.e("Jerry", "insert sql:"+sql);

            this.helper.execute(sql);

            if (object.getStatus() == DataObject.Status.NEW)
            {
                object.setVariable(this.primaryKey, this.helper.insertedId());
            }

            return true;
        }
        catch (DatabaseHelperException ex)
        {
            System.out.println("fail : " + sql);

            return false;
        }

    }


    public boolean updateByHashMap(HashMap<String, Object> conditions, CriteriaElement criteria)
    {
        if (this.tableName == null || this.helper == null)
        {
            System.out.println("table name is null");

            return false;
        }

        String sql = String.format("UPDATE `%s` SET ", this.tableName);

        if (conditions.isEmpty())
        {
            System.out.println("condition is empty");

            return false;
        }

        for (Map.Entry<String, Object> set : conditions.entrySet())  //get all key and value
        {
            if (set.getValue() instanceof Number)
            {
                sql = sql.concat(String.format("`%s` = %d,",
                        set.getKey(),
                        ((Number) set.getValue()).intValue()));
            }
            else if (set.getValue() instanceof String)
            {
                sql = sql.concat(String.format("`%s` = '%s',",
                        set.getKey(),
                        set.getValue().toString()));
            }
        }

        if (sql.length() != 0)
        {
            sql = sql.substring(0, sql.length() - 1);
        }

        if (criteria != null)
        {
            sql = sql.concat(" ").concat(criteria.renderWhere());
        }

        try
        {
            return this.helper.execute(sql);

        }
        catch (DatabaseHelperException ex)
        {
            System.out.println("fail : " + sql);

            return false;
        }

    }


    public boolean deleteByDataObject(DataObject object)
    {
        if (this.tableName == null || this.helper == null)
        {
            System.out.println("table name is null");

            return false;
        }

        String sql = String.format("DELETE FROM `%s` WHERE `%s` = %s;",
                this.tableName,
                this.primaryKey, object.getVariable(this.primaryKey));

        try
        {
            this.helper.execute(sql);

            return true;
        }
        catch (DatabaseHelperException ex)
        {
            System.out.println("fail : " + sql);

            return false;
        }

    }

    public boolean deleteByCriteria(CriteriaElement criteria)
    {
        if (this.tableName == null || this.helper == null)
        {
            System.out.println("table name is null");

            return false;
        }

        String sql = String.format("DELETE FROM `%s`", this.tableName);

        if (criteria != null)
        {
            sql = sql.concat(String.format(" %s", criteria.renderWhere()));
        }

        try
        {
            return this.helper.execute(sql);
        }
        catch (DatabaseHelperException e)
        {
            System.out.println("fail : " + sql);

            return false;
        }
    }

    @Override
    public boolean renewable() { return false;}
}
