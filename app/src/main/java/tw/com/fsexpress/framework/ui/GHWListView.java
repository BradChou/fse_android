package tw.com.fsexpress.framework.ui;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class GHWListView extends ListView
{
    public GHWListView(Context context) { super(context); this.setAdapter(this.adapter); this.init(); }

    public GHWListView(Context context, AttributeSet attrs)
    {
        super(context, attrs); this.setAdapter(this.adapter);

        this.init();
    }
    @Override
    public void addHeaderView(View v)
    {
        if (this.getAdapter() != null)
        {
            this.setAdapter(null);
        }

        super.addHeaderView(v);

        this.setAdapter(this.adapter);
    }
    @Override
    public void addFooterView(View v)
    {
        if (this.getAdapter() != null)
        {
            this.setAdapter(null);
        }

        super.addFooterView(v);

        this.setAdapter(this.adapter);
    }
    public GHWListView(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle); this.setAdapter(this.adapter); this.init();}

    private void init (
    )
    {
        this.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position,
                                    long arg3) {

                if (delegate == null) return;

                delegate.didSelectRowAtIndexPath(v, adapter.indexPathForPosition(position));
            }
        });
    }

    public Delegate delegate = null;

    public interface Delegate
    {
        // select row
        public void didSelectRowAtIndexPath(View cell, GHWIndexPath indexPath);
    }

    // list view's data source
    public DataSource dataSource = null;

    public interface DataSource
    {
        public int  numberOfSectionsInListView (GHWBaseAdapter ad);
        public int  numberOfRowsInSection	   (GHWBaseAdapter ad, int section);
        public View getItemView    (GHWListView parent, View convertView, GHWIndexPath indexPath);
        public View getSectionView (GHWListView parent, View convertView, int section);

    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.adapter.notifyDataSetChanged();
    }

    public void reloadData() {
        this.adapter.notifyDataSetChanged();
    };

    /*
     * listview adapter
     */

    public abstract class GHWBaseAdapter extends BaseAdapter
    {
        public abstract GHWIndexPath indexPathForPosition (int position);
    }
    public GHWBaseAdapter adapter = new GHWBaseAdapter () {

        private int _count = 0;
        // calculate position on which index path
        public GHWIndexPath indexPathForPosition (int position)
        {
            int sectionCount = dataSource.numberOfSectionsInListView(this);

            if (sectionCount == 0)
            {
                return GHWIndexPath.instance(0, position);
            }
            for (int section = 0; section < sectionCount; section ++)
            {
                int childCount = dataSource.numberOfRowsInSection(this, section);

                if (position == 0)
                {
                    return GHWIndexPath.instance(section, -1);
                }

                if (position != 0 && childCount != 0 && position < (childCount + 1))
                {
                    return GHWIndexPath.instance(section, position - 1);
                }

                position -= (childCount + 1);
            }
            return null;
        }
        @Override
        public int getCount() {

            if (dataSource == null) return 0;

            int count = 0;
            // no section mode
            if (dataSource.numberOfSectionsInListView(this) == 0)
            {
                count = dataSource.numberOfRowsInSection(this, 0);
//				if (dataSource.viewForFooterInSection(this, 0) != null)
//				{
//					count = count + 1;
//				}
                this._count = count;

                return count;
            }

            for (int idx = 0; idx < dataSource.numberOfSectionsInListView(this); idx ++)
            {
                count += dataSource.numberOfRowsInSection(this, idx) + 1;

//				if (dataSource.viewForFooterInSection(this, idx) != null)
//				{
//					count = count + 1;
//				}
            }

            this._count = count;

            return count;
        }

        @Override
        public Object getItem(int position)  { return null; }
        @Override
        public long getItemId(int position) { return position; }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            GHWIndexPath indexPath = indexPathForPosition(position);

            if (indexPath.row < 0)
            {
                return dataSource.getSectionView((GHWListView) parent, convertView, indexPath.section);
            }
            else
            {
//				if (position == this._count)
//				{
//					View v = dataSource.viewForFooterInSection(this, indexPath.section);
//
//					if (v != null)
//					{
//						return v;
//					}
//				}

                return dataSource.getItemView((GHWListView) parent, convertView, indexPath);
            }
        }
    };
}