package tw.com.fsexpress.framework;


import java.util.ArrayList;
import java.util.HashMap;

public interface IHandlerGenerator
{
    public ArrayList<IGHWHandler> generator (HashMap<String, String> settings);
}
