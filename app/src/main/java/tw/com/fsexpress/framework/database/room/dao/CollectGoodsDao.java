package tw.com.fsexpress.framework.database.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import tw.com.fsexpress.framework.database.room.entity.CollectGoodsData;

@Dao
public interface CollectGoodsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertData(CollectGoodsData collectGoodsData);

    /**
     * 撈取此帳號在此段日期區間的資料
     */
    @Query("SELECT * FROM CollectGoodsData WHERE driver_code = :driverCode AND date BETWEEN :startTime AND :endTime")
    List<CollectGoodsData> findDataByDriverCodeAndDate(String driverCode, String startTime, String endTime);

    /**
     * 撈取全部集貨2.0資料
     */
    @Query("SELECT * FROM CollectGoodsData")
    List<CollectGoodsData> displayAllData();

    @Update
    void updateData(CollectGoodsData collectGoodsData);

    @Delete
    void deleteData(CollectGoodsData collectGoodsData);

    @Query("DELETE FROM CollectGoodsData WHERE timestamp < :timestamp")
    void deleteDataFormTime(long timestamp);
}
