package tw.com.fsexpress.framework.dataobject;

import android.util.Log;

import tw.com.fsexpress.framework.IGHWObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataObject extends IGHWObject implements Serializable
{

    private static String DATA_OBJ_DTYPE_KEY   = "DATA_OBJ_DTYPE_KEY",
            DATA_OBJ_VALUE_KEY   = "DATA_OBJ_VALUE_KEY",
            DATA_OBJ_REQUIRE_KEY = "DATA_OBJ_REQUIRE_KEY";

    public static enum DataType { STRING, INT, FLOAT, BLOB };

    public static enum Status { NEW, OLD };

    public static DataObject.Status NEW = DataObject.Status.NEW;
    public static DataObject.Status OLD = DataObject.Status.OLD;

    private HashMap<String, HashMap<String, Object>> variables = new HashMap<String, HashMap<String, Object>>();

    private DataObject.Status objectStatus = DataObject.Status.NEW;

    public DataObject () { }
    /**
     * Initial object by one key and value's data type
     * @param key   Initial object name
     * @param type  Initial object value type
     */

    public void initVariable(String key, DataObject.DataType type)
    {
        this.initVariable(key, type, new String(), true);
    }

    /**
     * Initial object by one key ,one value and value's data type
     * @param key   Initial object name
     * @param type  Initial object value type
     * @param value Initial object value
     */
    public void initVariable(String key, DataObject.DataType type, Object value)
    {
        this.initVariable(key, type, value, true);
    }

    /**
     * Initial object by one key ,one value and value's data type
     * @param key   Initial object name
     * @param value Initial object value
     */
    public void initVariable(String key, Object value)
    {
        this.initVariable(key, DataObject.DataType.STRING, value, true);
    }

    /**
     * Initial object by one key, one value, value's data type and whether the value allow null values
     * @param key       Initial object name
     * @param type      Initial object value type
     * @param value     Initial object value
     * @param required  Whether the value allow null values
     */
    public void initVariable (String key, DataObject.DataType type, Object value, boolean required)
    {
        HashMap<String, Object> variable = this.variables.get(key);

        if (variable == null) { variable = new HashMap<String, Object>(); }

        variable.put(DataObject.DATA_OBJ_DTYPE_KEY	, type);
        variable.put(DataObject.DATA_OBJ_VALUE_KEY	, value);
        variable.put(DataObject.DATA_OBJ_REQUIRE_KEY, required);

        this.variables.put(key, variable);
    }

    public void initVariables (HashMap<String, Object> keyValue)
    {
        for (String key : keyValue.keySet())
        {
            this.initVariable(key, keyValue.get(key));
        }
    }
    /**
     * set Data Status
     * @param dataStatus
     */

    public void setStatus(DataObject.Status dataStatus) { this.objectStatus = dataStatus; }

    /**
     * get Data Status
     * @return dataStatus
     */
    public DataObject.Status getStatus() { return this.objectStatus; }

    /**
     * set Key's value to the object
     * @param key    Set object name
     * @param value  Set object value
     */
    public void setVariable(String key, Object value) {

        HashMap<String, Object> variable = this.variables.get(key);

        if (variable == null)
        {
            this.initVariable(key, value);

            variable = this.variables.get(key);
        }

        if (value == null) { value = new String(); }

        variable.put(DataObject.DATA_OBJ_VALUE_KEY, value);
    }

    /**
     * set List Key's value to the object
     * @param keys    For list set object name
     * @param values  For list set object value
     */
    public void setVariables(List<String> keys, List<?> values)
    {
        if (keys.size() != values.size())
        {
            String message = String.format("%s : key and value count not match",
                    Thread.currentThread().getStackTrace().toString());

            Log.e(this.getClass().toString(), message);

            return;
        }

        for (int idx = 0; idx < keys.size(); idx++)
        {
            this.setVariable(keys.get(idx), values.get(idx));
        }

    }

    /**
     * get value by key
     * @param Key  Get value by key
     * @return  Value
     */
    public Object getVariable(String Key)
    {
        HashMap<String, Object> map = this.variables.get(Key);

        if (map == null) return map;

        Object value = this.variables.get(Key).get(DataObject.DATA_OBJ_VALUE_KEY);;

        return value;
    }


    /**
     * get all values
     * @return  List All Values
     */
    public List<Object> getValues()
    {

        // get hashmap all value
        List<Object> variableValues = new ArrayList<Object>();

        // hashmap object
        List<HashMap<String, Object>> variables = new ArrayList<HashMap<String, Object>>(this.variables.values());

        for (HashMap<String, Object> variable : variables) {

            variableValues.add(variable.get(DataObject.DATA_OBJ_VALUE_KEY));
        }

        return variableValues;
    }

    /**
     * get all keys and all values
     * @return  All Keys and Values
     */
    public HashMap<String, Object> getVariables()
    {
        HashMap<String, Object> allVariables = new HashMap<String, Object>();

        for (String key : new ArrayList<String>(this.variables.keySet()))
        {
            HashMap<String, Object> var = this.variables.get(key);

            allVariables.put(key, var.get(DataObject.DATA_OBJ_VALUE_KEY));
        }
        return allVariables;
    }

    public void removeVariable(String key)
    {
        HashMap<String, Object> map = this.variables.get(key);

        if (map == null) return;

        this.variables.remove(key);
    }
    /**
     * check data format
     * @return ture data format correct, false data format error
     */
    public boolean allowable()
    {

        for(String key : new ArrayList<String>(this.variables.keySet()))
        {
            Object value = this.variables.get(key).get(DataObject.DATA_OBJ_VALUE_KEY);

            Object type = this.variables.get(key).get(DataObject.DATA_OBJ_DTYPE_KEY);

            if (value == null || value.toString().equals("")) { continue; }

            // check object type
            if (type == DataObject.DataType.INT)
            {
                if (! (value instanceof Number))
                {
                    String message = String.format("%s : value type not equal Number, Value instance is %s",
                            Thread.currentThread().getStackTrace().toString(),
                            value.getClass().getName());

                    Log.e(this.getClass().getName(), message);

                    return false;
                }
            }
            // string
            if (type == DataObject.DataType.STRING)
            {
                if (! (value instanceof String))
                {
                    String message = String.format("%s : value type not equal String, Value instance is %s",
                            Thread.currentThread().getStackTrace().toString(),
                            value.getClass().getName());

                    Log.e(this.getClass().getName(), message);
                    return false;
                }
            }
        }
        return true;
    }

}
