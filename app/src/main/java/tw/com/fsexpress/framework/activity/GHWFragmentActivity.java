package tw.com.fsexpress.framework.activity;

import static tw.com.fsexpress.LoginFragment.REQUEST_READ_PHONE_STATE;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.LoginActivity;
import tw.com.fsexpress.WorkStationFragment;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;
import tw.com.fsexpress.okhttputils.OkHttpUtil;

public class GHWFragmentActivity extends FragmentActivity {

    private static final int identify = 0x12991234;
    private static final int layoutId = 0x3421234;

    private static final int fragmentBarId = 0x4321567;

    private AlertDialog dialog;

    private int enterInAnimationResId = -1, enterOutAnimationResId = -1,
            popInAnimationResId = -1, popOutAnimationResId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // create root group
        LinearLayout layout = new LinearLayout(this);

        layout.setOrientation(LinearLayout.VERTICAL);

        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        layout.setId(layoutId);

        // finished add
        setContentView(layout);

        getSupportFragmentManager().addOnBackStackChangedListener(this.listener);
    }

    /*
     * fragment back callback
     */
    private FragmentManager.OnBackStackChangedListener listener = new FragmentManager.OnBackStackChangedListener() {

        @Override
        public void onBackStackChanged() {

            Fragment fragment = getSupportFragmentManager().findFragmentById(GHWFragmentActivity.identify);

            if (fragment instanceof GHWFragment) {
            }
        }
    };

    /*
     * set fragment
     */
    public GHWFragmentActivity initWithRootFragment(GHWFragment fragment) {
        this.addFragment(fragment, false, true);
        return this;
    }

    public void setRootFragment(GHWFragment fragment) {
        this.addFragment(fragment, false, true);
    }

    public void pushFragment(GHWFragment fragment, Boolean animation) {
        this.addFragment(fragment, animation, false);
    }

    private void addFragment(GHWFragment fragment, Boolean animation, Boolean root) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction transaction = fragmentManager.beginTransaction();

//        if (animation)
//        {
//
//            transaction.setCustomAnimations(this.enterInAnimationResId, this.enterOutAnimationResId,
//                    this.popInAnimationResId, this.popOutAnimationResId);
//        }

        // get fragment tag
        String tag = fragment.getTag() == null ? fragment.getClass().getSimpleName() : fragment.getTag();

        // check fragment exist
        if (fragmentManager.findFragmentByTag(tag) != null && fragment.isAdded()) {
            transaction.remove(fragment);
        }

//        fragment.hidesBackButton = root;
        //this.setFragmentRightButton(this.createFragmentButton());

        transaction.replace(GHWFragmentActivity.layoutId, fragment, tag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        //String backStack = root ? null :tag;

        transaction.addToBackStack(tag);

        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment workStationFragment = manager.findFragmentByTag(WorkStationFragment.class.getSimpleName());

        if (workStationFragment != null && workStationFragment.isVisible()) { // 攔截工作站資料檢視fragment返回事件(WorkStationFragment)
            ConfirmExit();
            return;
        }

        if (manager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
            return;
        }

        finish();
    }

    public void popFragment() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
        }

    }

    public void popFragmentToRoot() {
        FragmentManager manager = getSupportFragmentManager();

        for (int idx = 0; idx < (manager.getBackStackEntryCount() - 1); idx++) {
            manager.popBackStack();
        }
    }

    public void popFragmentToIndex(int index) {
        FragmentManager manager = getSupportFragmentManager();

        int count = manager.getBackStackEntryCount();

        for (int idx = 0; idx < (count - 1); idx++) {
            if (index == ((count - 1) - idx)) break;

            manager.popBackStack();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void ConfirmExit() {
        AlertDialog.Builder ad = new AlertDialog.Builder(GHWFragmentActivity.this);
        ad.setTitle("離開");
        ad.setMessage("是否要離開？");
        ad.setPositiveButton("是", (dialog, i) -> requestLogout());
        ad.setNegativeButton("否", (dialog, i) -> {

        });
        ad.show();
    }

    private void doLogout() {
        AccountManager.instance().doLogout(GHWFragmentActivity.this, new AccountManager.AccountManagerCallback() {
            @Override
            public void doLogout(boolean isLogout, String reason) {
                if (isLogout) {
                    Intent intent = new Intent(GHWFragmentActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    // 登出時呼叫登入api清空token
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void requestLogout() {

        String strDevice = "";
        String strProduct = "";
        String strBrand = "";
        String strVersionCode = "";
        String strIMEI = "";

        this.dialog = Constants.ProgressBar(GHWFragmentActivity.this, "登出中...請稍候");
        this.dialog.show();


        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");


        RequestObject requestObject = (RequestObject) new RequestObject();

        //為了相容有人使用非公司設備
        //可能會有參數取不到值
        //所以分開判斷
        try {
            strDevice = Build.FINGERPRINT;//Build.ID.toString();//  Build.DEVICE.toString();
        } catch (Exception ex) {
            strDevice = "";
        }
        try {
            strProduct = Build.PRODUCT;
        } catch (Exception ex) {
            strProduct = "";
        }
        try {
            strBrand = Build.BRAND;
        } catch (Exception ex) {
            strBrand = "";
        }
        try {
            strVersionCode = Constants.getVersionCode(GHWFragmentActivity.this);
        } catch (Exception ex) {
            strVersionCode = "";
        }

        try {
            strIMEI = getIMEIDeviceId();
        } catch (Exception ex) {

        }

        String drverCode = Constants.get(GHWFragmentActivity.this, "previous_account");
        String password = Constants.get(GHWFragmentActivity.this, "previous_password");

        requestObject.setUrl(Constants.API_POST_LOGIN_CHECK_V3);

        requestObject.setMethod(RequestObject.METHOD.POST);
        requestObject.addParameter("driver_code", drverCode);
        requestObject.addParameter("login_password", Constants.md5(password));
        requestObject.addParameter("device", strIMEI); //strIMEI// strDevice);
        requestObject.addParameter("product", strProduct);
        requestObject.addParameter("brand", strBrand);
        requestObject.addParameter("versionCode", strVersionCode);
        requestObject.addParameter("mobiletoken", "");
        requestObject.addParameter("mobiletype", "android");


        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {


                if (requestStatus == RequestStatus.Finish) {

                    try {

                        Log.e("Jerry", "login;" + requestHandler.getReceiveString());
                        GHWFragmentActivity.this.dialog.cancel();

                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                        JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);

                        if (Boolean.valueOf(convertedObject.get("resultcode").toString()) == false) {

                            String des = convertedObject.get("resultdesc").toString();
                            Constants.ErrorAlertDialog(GHWFragmentActivity.this, "登出失敗", des, "確定", null, null, null);

                            return;
                        }

                        doLogout();

                    } catch (Exception e) {
                        Log.e("Jerry", e.toString());
                        GHWFragmentActivity.this.dialog.cancel();
                        Constants.ErrorAlertDialog(GHWFragmentActivity.this, "登出失敗", e.toString(), "確定", null, null, null);
                    }
                } else if (requestStatus == RequestStatus.Fail) {
                    Log.e("Jerry", "fail");
                    GHWFragmentActivity.this.dialog.cancel();
                    Constants.ErrorAlertDialog(GHWFragmentActivity.this, "登出失敗", "登出失敗，請確認網路狀態", "確定", null, null, null);
                } else if (requestStatus == RequestStatus.NoNetworkConnection) {
                    GHWFragmentActivity.this.dialog.cancel();
                    Constants.ErrorAlertDialog(GHWFragmentActivity.this, "登出失敗", "登出失敗", "確定", null, null, null);
                    Log.e("Jerry", "nonetwork");
                }

            }
        });
        request.request(requestObject);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getIMEIDeviceId() {
        //return "123";
        String deviceId = "";
        String deviceId_0 = "";
        String deviceId_1 = "";
        String deviceId_2 = "";
        String deviceId_3 = "";
        if (Build.VERSION.SDK_INT >= 29)//Build.VERSION_CODES.Q
        {
            //Manifest.permission.READ_PRIVILEGED_PHONE_STATE

            if (GHWFragmentActivity.this.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(GHWFragmentActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                //return "";
            }

            deviceId_0 = Settings.Secure.getString(GHWFragmentActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) GHWFragmentActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (GHWFragmentActivity.this.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(GHWFragmentActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
                    //return "";
                }
            }
            assert mTelephony != null;
            if (mTelephony.getDeviceId() != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    deviceId_1 = mTelephony.getImei();
                } else {
                    deviceId_2 = mTelephony.getDeviceId();
                }
            } else {
                deviceId_3 = Settings.Secure.getString(GHWFragmentActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        deviceId = deviceId_0 + " ; " + deviceId_1 + " ; " + deviceId_2 + " ; " + deviceId_3;
        Log.d("deviceId", deviceId);

        return deviceId;
    }
}
