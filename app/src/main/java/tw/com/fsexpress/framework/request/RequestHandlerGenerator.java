package tw.com.fsexpress.framework.request;

import java.util.ArrayList;
import java.util.HashMap;
import tw.com.fsexpress.framework.IHandlerGenerator;
import tw.com.fsexpress.framework.IGHWHandler;

public class RequestHandlerGenerator implements IHandlerGenerator
{
    @Override
    public ArrayList<IGHWHandler> generator(HashMap<String, String> settings)
    {
        ArrayList<IGHWHandler> handlers = new ArrayList<IGHWHandler>();

        RequestHandler handler = new RequestHandler();

        handler.setIdentify("ghw.request");

        handlers.add(handler);

        return handlers;
    }

}
