package tw.com.fsexpress.framework.database;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DatabaseDeployer
{
    public static final String EXTERNAL_STORAGE = "ExternalStorage";
    public static final String APPLICATION_STORAGE = "ApplicationStorage";

    public static String deploy (String originPath, String savePath)  throws DatabaseDeployerException, IOException
    {
        File saveFile = new File(savePath);

        String fileName = saveFile.getName();

        // check path name
        String path = saveFile.getParent();

        if (path == null)
        {
            throw new DatabaseDeployerException("save path error, please check settings");
        }

        String paths[] = path.split("/");

        if (paths.length == 0)
        {
            throw new DatabaseDeployerException("save path can't be root, please check settings");
        }

        String folderName = paths.length > 1 ? path.replace(paths[0] + "/", "") : "";

        // check save path
        File storagePath = null;
        String fullSavePath = null;

        if (paths[0].equals("{$" + DatabaseDeployer.EXTERNAL_STORAGE + "}"))
        {
            if (Environment.getExternalStorageState() == Environment.MEDIA_REMOVED)
            {
                throw new DatabaseDeployerException("SD Card has been remove");
            }

            storagePath = Environment.getExternalStorageDirectory();

            fullSavePath = String.format("%s/%s", storagePath, folderName);

        }
        else
        {
            storagePath = new File(path);

            fullSavePath = String.format("%s", storagePath);
        }

        File folderPath = new File(fullSavePath);

        if (! folderPath.exists() && ! folderPath.mkdirs())
        {
            throw new DatabaseDeployerException("mkdir fail");
        }

        File databaseFile = new File(folderPath, fileName);

        if (databaseFile.exists())
        {
            return databaseFile.toString();
        }

        //"/res/raw/weibocards.sqlite"
        InputStream input = DatabaseDeployer.class.getResourceAsStream(originPath);

        if (input == null)
        {
            throw new DatabaseDeployerException("db file not exists");
        }

        byte[] buffer = new byte[1024];

        FileOutputStream output = new FileOutputStream(databaseFile);

        int inputLength = input.read(buffer, 0, buffer.length);

        while (inputLength >= 0)
        {
            output.write(buffer, 0, inputLength);

            inputLength = input.read(buffer, 0, buffer.length);
        }

        input.close();
        output.close();

        return databaseFile.toString();
    }
}
