package tw.com.fsexpress.framework.utility;

import android.content.Context;
import android.util.TypedValue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

public class Functions {
    public static int string2ResId(String variableName, Class<?> c)
    {
        try
        {
            Field idField = c.getDeclaredField(variableName);
            return idField.getInt(idField);
        }
        catch (Exception e)
        {
            return -1;
        }
    }
    public static boolean classExists (String className)
    {
        try
        {
            Class.forName(className);

            return true;
        }
        catch ( ClassNotFoundException ex)
        {
            return false;
        }
    }

    public static int dp2Px(Context context, int dp)
    {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());

        return px;
    }


    /*
     * on request finished will get a input stream ,
     * you can use this to convert to byte array
     */
    public static byte[] inputStream2ByteArray(InputStream is)
    {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;

            byte[] data = new byte[4096];


            while ((nRead = is.read(data, 0, data.length)) != -1)
            {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        }
        catch (IOException e)
        {
            System.out.println("inputStream2ByteArray convert fail");
            return null;
        }
    }
}
