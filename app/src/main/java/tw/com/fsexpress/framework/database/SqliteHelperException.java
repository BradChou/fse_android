package tw.com.fsexpress.framework.database;


public class SqliteHelperException extends DatabaseHelperException
{
    private static final long serialVersionUID = 1L;

    public SqliteHelperException(String message)
    {
        super(message);
    }
}
