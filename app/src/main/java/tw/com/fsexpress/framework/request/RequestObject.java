package tw.com.fsexpress.framework.request;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import tw.com.fsexpress.framework.IGHWObject;
import tw.com.fsexpress.framework.utility.Functions;

public class RequestObject extends IGHWObject
{
    private String url = null;
    private METHOD method = METHOD.GET;
    private HashMap<String,String> parameters = new HashMap<String,String>();
    private String body = null;
    private HashMap<String,String> properties = new HashMap<String,String>();
    private HashMap<String,HashMap<String, Object>> fileParameters = new HashMap<String,HashMap<String, Object>>();

    private static String RequestObjectFileNameKey = "RequestObjectFileName";
    private static String RequestObjectDataKey = "RequestObjectData";

    public static enum METHOD { POST, GET };

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public METHOD getMethod() { return method; }

    public void setMethod(METHOD method) { this.method = method; }

    public void addParameter(String key, String value)
    {
        this.parameters.put(key, value);
    }
    public void addBody(String value)
    {
        this.body = value;
    }
    public void addParameter(String key, String fileName, InputStream data)
    {
        HashMap<String, Object> values = new HashMap<String, Object>();

        values.put(RequestObject.RequestObjectFileNameKey, fileName);
        values.put(RequestObject.RequestObjectDataKey, data);

        this.fileParameters.put(key, values);
    }

    public void addProperty(String key, String value)
    {
        this.properties.put(key, value);
    }
    public HttpURLConnection render ()
    {
        try
        {
            // check parameter
            String parameter = new String();

            for (Map.Entry<String, String> set : this.parameters.entrySet())
            {
                String value = set.getValue() == null ? "" : set.getValue();

                parameter = parameter.concat(String.format("%s=%s&", set.getKey(),
                        URLEncoder.encode(value, "UTF-8")));
            }

            // remove parameter last character
            parameter = (parameter.length() > 0) ? parameter.substring(0, parameter.length() - 1) : parameter;

            // concat url string

            String urlPath = this.url;

            if (this.method == METHOD.GET && parameter.length() > 0)
            {

                String combi = (urlPath.lastIndexOf("?") > 0) ? "&" : "?";

                urlPath = String.format("%s%s%s",urlPath, combi, parameter);
            }

            // init connection
            // include ignore https(ssl)  by Alvin Hsieh 2018/1/25
            HttpURLConnection connection = null;
            URL m_url = new URL(urlPath);
            if(m_url.getProtocol().toUpperCase().equals("HTTPS"))
            {
                this.trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) m_url
                        .openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            }
            else
                connection = (HttpURLConnection) m_url.openConnection();

            // Read from the connection. Default is true.
            connection.setDoInput(true);

            String boundary = "----198310180910515427----";

            if (this.properties.size() > 0)
            {
                for (String key : this.properties.keySet())
                {
                    connection.setRequestProperty(key, this.properties.get(key));
                }
            }

            if (this.fileParameters.size() > 0)
            {
                this.method = METHOD.POST;

                connection.setRequestMethod("POST");

                connection.setDoOutput(true);
                connection.setUseCaches(false); // Post cannot use caches

                connection.setRequestProperty("Connection", "Keep-Alive");

                connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

                java.io.DataOutputStream dos = new java.io.DataOutputStream(connection.getOutputStream());

                for (String key : this.fileParameters.keySet())
                {
                    HashMap<String, Object> file = this.fileParameters.get(key);

                    dos.writeBytes("\r\n--" + boundary + "\r\n");
                    dos.writeBytes("Content-Disposition: form-data; name=\"" + key + "\";filename=\"" +	file.get(RequestObject.RequestObjectFileNameKey) + "\"" +  "\r\n");
                    dos.writeBytes("Content-Type: application/octet-stream\r\n\r\n");

                    // write data
                    byte[] data = Functions.inputStream2ByteArray( (InputStream) file.get(RequestObject.RequestObjectDataKey));

                    dos.write(data);
                }
                // set post string
                for (Map.Entry<String, String> set : this.parameters.entrySet())
                {
                    String value = set.getValue() == null ? "" : set.getValue();

                    // append boundary and separate it with new line
                    dos.writeBytes("\r\n--" + boundary + "\r\n");

                    dos.writeBytes("Content-Disposition: form-data; name=\"" + set.getKey() + "\"\r\n\r\n");

                    dos.write(value.getBytes("UTF-8"));
                    //dos.writeBytes(URLEncoder.encode(value, "UTF-8"));

                }

                dos.writeBytes("\r\n--" + boundary + "--\r\n");

            }
            else
            {
                if (this.method == METHOD.POST)
                {
                    // Set the post method. Default is GET
                    connection.setRequestMethod("POST");

                    connection.setDoOutput(false);

                    // Post cannot use caches
                    connection.setUseCaches(false);

                    // This methods only takes effects to this instance.
                    connection.setInstanceFollowRedirects(true);

                    connection.setRequestProperty("Accept-Charset", "UTF-8");
                    if (parameter.length() > 0)
                        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                    java.io.DataOutputStream dos = new java.io.DataOutputStream(connection.getOutputStream());

                    if (this.body != null)
                    {

//			        		Log.e("XXX",  "body = " + body + "," + URLEncoder.encode(this.body, "UTF-8"));
                        //dos.writeBytes(new StringEntity(this.body.getBytes(), HTTP.UTF_8));//URLEncoder.encode(this.body, "UTF-8"));

                        dos.writeBytes(this.body);
                    }
                    else if (parameter.length() > 0)
                    {

                        dos.writeBytes(parameter);
                    }
                }
                else
                {
                    connection.setRequestMethod("GET");
                }
            }
            return connection;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("RequestObject render fail.");
            System.out.println("ex = " + e);

            return null;
        }
    }

    // Ignore SSL
    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        // Android use X509 cert
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException
            {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
}

