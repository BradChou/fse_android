package tw.com.fsexpress.framework.database.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import tw.com.fsexpress.framework.database.room.entity.DeliveryData;

@Dao
public interface DeliveryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertData(DeliveryData deliveryData);

    /**
     * 撈取此帳號在此段日期區間的資料
     */
    @Query("SELECT * FROM DeliveryData WHERE driver_code = :driverCode AND date BETWEEN :startTime AND :endTime")
    List<DeliveryData> findDataByDriverCodeAndDate(String driverCode, String startTime, String endTime);

    /**
     * 撈取全部資料
     */
    @Query("SELECT * FROM DeliveryData")
    List<DeliveryData> displayAllData();

    @Update
    void updateData(DeliveryData deliveryData);

    @Delete
    void deleteData(DeliveryData deliveryData);

    @Query("DELETE FROM DeliveryData WHERE timestamp < :timestamp")
    void deleteDataFormTime(long timestamp);
}
