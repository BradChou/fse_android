package tw.com.fsexpress.framework.database;


public class DatabaseDeployerException extends DatabaseHelperException
{
    private static final long serialVersionUID = 1L;

    public DatabaseDeployerException(String message)
    {
        super(message);
    }
}
