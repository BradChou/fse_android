package tw.com.fsexpress.framework.ui;

import android.os.Parcel;

public class GHWIndexPath
{
    public int	section;
    public int	row;

    public static GHWIndexPath instance(int section, int row) { return new GHWIndexPath(section, row); }

    public GHWIndexPath (int section, int row) { this.section = section; this.row = row; }

    public GHWIndexPath (Parcel p)
    {
        section = p.readInt();
        row = p.readInt();
    }

    public static final android.os.Parcelable.Creator<GHWIndexPath> CREATOR =
            new android.os.Parcelable.Creator<GHWIndexPath>()
            {

                @Override
                public GHWIndexPath createFromParcel(Parcel source)
                {

                    return new GHWIndexPath(source);
                }

                @Override
                public GHWIndexPath[] newArray(int size)
                {
                    return new GHWIndexPath[size];
                }
            };
}
