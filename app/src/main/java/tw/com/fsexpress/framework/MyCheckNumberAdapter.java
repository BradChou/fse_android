package tw.com.fsexpress.framework;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tw.com.fsexpress.R;

public class MyCheckNumberAdapter extends BaseAdapter {


    List<String> data = new ArrayList<>();
    private Context mContext;
    ViewHolder holder;
    private boolean isShowCheckBox = false;//表示當前是否是多選狀態。
    private SparseBooleanArray stateCheckedMap = new SparseBooleanArray();//用來存放CheckBox的選中狀態，true爲選中,false爲沒有選中

    private boolean isHiddenCheckBox = false;//。

    public MyCheckNumberAdapter(Context context, List<String> data, SparseBooleanArray stateCheckedMap) {
        this.data = data;
        mContext = context;
        this.stateCheckedMap = stateCheckedMap;
    }

    public MyCheckNumberAdapter(Context context, List<String> data, SparseBooleanArray stateCheckedMap, boolean hiddenCheckBox) {
        this.data = data;
        mContext = context;
        this.stateCheckedMap = stateCheckedMap;
        isHiddenCheckBox = hiddenCheckBox ;
        isShowCheckBox = hiddenCheckBox ;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.cell_checknumber, null);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.checkBox = (CheckBox) convertView.findViewById(R.id.chb_select_way_point);
        holder.check_number = convertView.findViewById(R.id.check_number);
        //showAndHideCheckBox();//控制CheckBox的那個的框顯示與隱藏

        holder.check_number.setText(data.get(position));

        try
        {
            holder.checkBox.setChecked(stateCheckedMap.get(position));//設置CheckBox是否選中
        }
        catch(Exception ex)
        {

        }


        if ( isHiddenCheckBox == true)
        {
            holder.checkBox.setVisibility( View.GONE);

        }
        return convertView;
    }

    public class ViewHolder {
        public TextView check_number;
        public CheckBox checkBox;
    }

    private void showAndHideCheckBox() {
        if (isShowCheckBox) {
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.GONE);
        }
    }


    public boolean isShowCheckBox() {
        return isShowCheckBox;
    }

    public void setShowCheckBox(boolean showCheckBox) {
        isShowCheckBox = showCheckBox;
    }
}
