package tw.com.fsexpress.framework.activity;

import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import tw.com.fsexpress.framework.dataobject.DataObject;


public class GHWFragment extends Fragment {

    protected static final int fragmentBarId = 0x4321567;

    private boolean isNew = true;
    private int layoutResId = 0;
    public String title = new String();
    public Object object = null;
    public ViewGroup view = null;
    private DataObject data = new DataObject();


    public GHWFragment() { super();}

    public GHWFragment setLayoutResourceId(int resId) { this.layoutResId = resId;  return this;}
    public GHWFragment initWithLayoutResourceId(int resId) { this.layoutResId = resId;  return this;}

    public FragmentActivity getFragmentActivity()
    {
        return (GHWFragmentActivity)getActivity();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.view != null){

            ViewGroup parent = (ViewGroup) view.getParent();

            if (parent != null) parent.removeView(view);

            if (parent != null) parent.invalidate();
        }

        try
        {
            if (this.layoutResId == 0) return null;

            //System.out.println("1. this.view  = " + this.layoutResId);
            ViewGroup view = (ViewGroup) inflater.inflate(this.layoutResId, container, false);

            // check has navigation bar
//            if (this.hasNavigationBar)
//            {
//                view = this.addFragmentBarWithView(view);
//            }

            this.view = view;

            if (this.isNew)
            {
                this.viewDidLoad(view, container, savedInstanceState);

                this.isNew = false;
            }
            //System.out.println("this.view + " + view);
            return view;

        } catch (InflateException e)
        {

            //System.out.println("ex this.view + " + this.view);
            return this.view;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        this.viewDidAppear(this.view);

    }

    @Override
    public void onPause() {
        super.onPause();
        this.viewDidDisappear(this.view);
    }

    // reserve method
    protected void viewDidLoad(ViewGroup view, ViewGroup container, Bundle savedInstanceState) { }

    // reserve method
    protected void viewDidDisappear(ViewGroup view) {}
    protected void viewDidAppear(ViewGroup view) { }
    public void setVariable(String key, Object value) { this.data.setVariable(key, value); }
    public Object getVariable(String key) { return this.data.getVariable(key); }
    public void removeVariable(String key) { this.data.removeVariable(key); }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }
}
