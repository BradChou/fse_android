package tw.com.fsexpress;

import android.app.Activity;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

public class PermissionManager {
    private static int PERMISSION_ALL = 100;

    List<String> permissionList = new ArrayList<String>();

    private SuccessAction successAction;

    public static PermissionManager getNewInstance() {
        return new PermissionManager();
    }

    public static void requestAllPermission(Activity paramActivity) {
        if (Build.VERSION.SDK_INT >= 23) {
            int i = paramActivity.checkSelfPermission("android.permission.CAMERA");
            int j = paramActivity.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
            int k = paramActivity.checkSelfPermission("android.permission.INTERNET");
            int m = paramActivity.checkSelfPermission("android.permission.ACCESS_NETWORK_STATE");
            int n = paramActivity.checkSelfPermission("android.permission.ACCESS_WIFI_STATE");
            int i1 = paramActivity.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION");
            int i2 = paramActivity.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
            if (i != 0 || j != 0 || k != 0 || m != 0 || n != 0 || i1 != 0 || i2 != 0) {
                i = PERMISSION_ALL;
                paramActivity.requestPermissions(new String[] { "android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.ACCESS_WIFI_STATE", "android.permission.MOUNT_UNMOUNT_FILESYSTEMS", "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION" }, i);
            }
        }
    }

    public PermissionManager add(String paramString) {
        this.permissionList.add(paramString);
        return this;
    }

    public void getSuccessAction() {
        SuccessAction successAction = this.successAction;
        if (successAction != null)
            successAction.action();
    }

    public PermissionManager request(Activity paramActivity) {
        List<String> list = this.permissionList;
        byte b = 0;
        String[] arrayOfString = list.<String>toArray(new String[0]);
        int i = 0;
        while (true) {
            if (i < this.permissionList.size()) {
                if (paramActivity.checkSelfPermission(this.permissionList.get(i)) != 0) {
                    i = b;
                    break;
                }
                i++;
                continue;
            }
            i = 1;
            break;
        }
        if (i == 0) {
            paramActivity.requestPermissions(arrayOfString, 1);
            return this;
        }
        this.successAction.action();
        return this;
    }

    public PermissionManager setSuccessAction(SuccessAction paramSuccessAction) {
        this.successAction = paramSuccessAction;
        return this;
    }

    public static interface SuccessAction {
        void action();
    }
}
