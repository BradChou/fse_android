package tw.com.fsexpress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CustomDownloadProgress extends View {
    private int dy;
    private Paint linePaint;
    private int progress;
    private float textLength;
    private Paint textPaint;

    public CustomDownloadProgress(Context arg2) {
        this(arg2, null);
    }

    public CustomDownloadProgress(Context arg2, AttributeSet arg3) {
        this(arg2, arg3, 0);
    }

    public CustomDownloadProgress(Context arg2, AttributeSet arg3, int arg4) {
        super(arg2, arg3, arg4);
        this.progress = 50;
        Paint v2 = new Paint();
        this.linePaint = v2;
        v2.setAntiAlias(true);
        this.linePaint.setStyle(Paint.Style.STROKE);
        this.initLinePaint();
        Paint v2_1 = new Paint();
        this.textPaint = v2_1;
        v2_1.setTextSize(40f);
        this.textPaint.setStyle(Paint.Style.STROKE);
        this.textPaint.setColor(this.getResources().getColor(R.color.blue));  // color:progress_blue
        this.textPaint.setAntiAlias(true);
        this.textPaint.setStrokeWidth(3f);
        Paint.FontMetricsInt v2_2 = this.textPaint.getFontMetricsInt();
        this.dy = (v2_2.bottom - v2_2.top) / 4;
    }

    private void initLinePaint() {
        this.linePaint.setStrokeWidth(6f);
        this.linePaint.setColor(this.getResources().getColor(R.color.blue));  // color:progress_blue
    }

    @Override  // android.view.View
    protected void onDraw(Canvas arg11) {
        super.onDraw(arg11);
        this.initLinePaint();
        float v0 = ((float)this.progress) / 100f * (((float)this.getWidth()) - this.textPaint.measureText("100%"));
        this.textLength = this.textPaint.measureText(this.progress + "%");
        arg11.drawLine(0f, ((float)this.getHeight()) / 2f, v0 - 10f, ((float)this.getHeight()) / 2f, this.linePaint);
        arg11.drawText(this.progress + "%", v0, ((float)this.getHeight()) / 2f + ((float)this.dy), this.textPaint);
        this.linePaint.setStrokeWidth(5f);
        this.linePaint.setColor(this.getResources().getColor(R.color.gray));  // color:lightBlueGray
        arg11.drawLine(v0 + 10f + this.textLength, ((float)this.getHeight()) / 2f, ((float)this.getWidth()), ((float)this.getHeight()) / 2f, this.linePaint);
    }

    public void setProgress(int arg1) {
        this.progress = arg1;
        this.invalidate();
    }
}
