package tw.com.fsexpress.request;


import android.util.Log;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.Criteria;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;

import org.json.JSONObject;

import java.util.List;

public class RequestController {
    public static RequestController amg = null;


    public static RequestController instance(){

        if (RequestController.amg == null){
            RequestController.amg = new RequestController();
        }
        return RequestController.amg;
    }

    public void setRequest(String requestName, String requestMethod, String requestParameter){

        try{

            Commander cmd = Commander.instance();
            DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.request");
            List<DataObject> arrData = handler.getDataByCriteria(new Criteria("request_name", requestName));

            Boolean isInDB = false;
            if (arrData.size() != 0){

                for (DataObject d: arrData) {
                    String parameter = d.getVariable("request_parameter").toString();
                    if (parameter.equals(requestParameter)){
                        isInDB = true;
                        break;
                    }
                }

                if (!isInDB){

                    JSONObject dataJSON = new JSONObject(requestParameter);

                    DataObject data = new DataObject();
                    data.setVariable("request_name", requestName);
                    data.setVariable("request_method", requestMethod);
                    data.setVariable("request_parameter", requestParameter);


                    handler.insertByDataObject(data);

                }
            }
            else{
                JSONObject dataJSON = new JSONObject(requestParameter);
                DataObject data = new DataObject();
                data.setVariable("request_name", requestName);
                data.setVariable("request_method", requestMethod);
                data.setVariable("request_parameter", requestParameter);


                handler.insertByDataObject(data);
            }

        }catch (Exception e){

            Log.e("Jerry", "setRequest:"+e.toString());

        }


    }

    public List<DataObject> getRequest(){
        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.request");

        return handler.getDataByCriteria(null);

    }
}
