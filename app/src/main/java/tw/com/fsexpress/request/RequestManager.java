package tw.com.fsexpress.request;

import android.app.Activity;
import android.app.Application;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import tw.com.fsexpress.Constants;
import tw.com.fsexpress.Menu.collection.CollectionFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;
import tw.com.fsexpress.okhttputils.OkHttpUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import org.json.JSONObject;

import java.util.HashMap;

public class RequestManager {


    public static RequestManager amg = null;

    public interface RequestManagerCallback
    {
        public void onResponse(JsonObject jsonObject);
        public void onFail(String reason);
    }
    public interface RequestManagerCallback2
    {
        public void onResponse(JsonArray jArray);
        public void onFail(String reason);
    }

    public static RequestManager instance(){

        if (RequestManager.amg == null){
            RequestManager.amg = new RequestManager();
        }
        return RequestManager.amg;
    }

    /**
     * 檢查  本筆貨號資料 是否已配達 或 已銷單 , 如果已配達 或 已銷單 後續不應繼續處理
     * @param scan_item
     * @param arrive_option
     * @param act
     * @param jObj
     * @return
     */
    public boolean isDeliveryOkandAlertMsg(String scan_item , String arrive_option , Activity act ,  JsonObject jObj )
    {
        boolean isStop = false ;
        String check_number , resultdesc , status ;

        if ( jObj.has("resultdesc") && jObj.has("status")  && jObj.has("checkNumber") )
        {
            check_number = jObj.get("checkNumber").getAsString() ;
            resultdesc = jObj.get("resultdesc").getAsString() ;
            status = jObj.get("status").getAsString() ;

            String strAlertMsg = "";

            if ( resultdesc.equals("此筆已正常配達") && status.equals("正常配達")  )
            {
                isStop = true ;

                if ( !scan_item.equals("3") || !arrive_option.equals("3") )
                {
                    strAlertMsg = "此筆 " + check_number + " 已經正常配達 , 無法再變更貨態 . ";
                    Constants.ErrorAlertDialog(act, "發生異常", strAlertMsg, "確定", null, null, null);
                }
                else
                {
                    strAlertMsg = "此筆 " + check_number + " 已經正常配達 , 無法再變更貨態 . ";
                    Constants.toast(act, strAlertMsg);

                    //Constants.ErrorAlertDialog(act, "發生異常 - " + status , strAlertMsg, "確定", null, null, null);
                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(act, notification);
                        r.play();
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                }

            }
            else if ( resultdesc.equals("此筆已銷單") && status.equals("已銷單"))
            {
                isStop = true ;
                strAlertMsg = "此筆 " + check_number + " 已經銷單 , 無法再變更貨態 . ";
                Constants.ErrorAlertDialog(act, "發生異常", strAlertMsg, "確定", null, null, null);
            }
            else if ( resultdesc.equals("查不到此筆單號，請與發送站所進行確認。") && status.equals("查無單號"))
            {
                isStop = true ;
                strAlertMsg = "注意 " + check_number + "  " + resultdesc ;

                Constants.toast(act, strAlertMsg);
                //Constants.ErrorAlertDialog(act, "發生異常 - " + status , strAlertMsg, "確定", null, null, null);
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(act, notification);
                    r.play();
                } catch (Exception e) {
                    //e.printStackTrace();
                }

            }
            else if ( ! resultdesc.equals("") && ! status.equals("")  )
            {
                isStop = true ;
                strAlertMsg = "注意 " + check_number + "  " + resultdesc ;
                Constants.ErrorAlertDialog(act, "發生異常 - " + status , strAlertMsg, "確定", null, null, null);
            }


        }

        return isStop ;
    }
    /**
     * 檢查  本筆貨號資料 是否已配達 或 已銷單 , 如果已配達 或 已銷單 後續不應繼續處理
     * @param act
     * @param jObj
     * @return
     */
    public boolean isDeliveryOkandAlertMsg(Activity act , JsonObject jObj ) //String check_number , String resultdesc , String status)
    {
        boolean isStop = false ;
        String check_number , resultdesc , status ;

        if ( jObj.has("resultdesc") && jObj.has("status")  && jObj.has("checkNumber") )
        {
            check_number = jObj.get("checkNumber").getAsString() ;
            resultdesc = jObj.get("resultdesc").getAsString() ;
            status = jObj.get("status").getAsString() ;

            String strAlertMsg = "";

            //  Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));

            if ( resultdesc.equals("此筆已正常配達") && status.equals("正常配達"))
            {
                isStop = true ;
                strAlertMsg = "此筆 " + check_number + " 已經正常配達 , 無法再變更貨態 . ";
                Constants.ErrorAlertDialog(act, "發生異常", strAlertMsg, "確定", null, null, null);
            }
            else if ( resultdesc.equals("此筆已銷單") && status.equals("已銷單"))
            {
                isStop = true ;
                strAlertMsg = "此筆 " + check_number + " 已經銷單 , 無法再變更貨態 . ";
                Constants.ErrorAlertDialog(act, "發生異常", strAlertMsg, "確定", null, null, null);
            }
            else if ( resultdesc.equals("查不到此筆單號，請與發送站所進行確認。") && status.equals("查無單號"))
            {
                isStop = true ;
                strAlertMsg = "注意 " + check_number + "  " + resultdesc ;

                //2022-09-28
                //二號大天神指示
                //凡是 "查不到此筆單號" 的這一個訊息
                //不能只有快顯視窗
                //應該要跳出有 "確定" 按鈕的訊息框
                //至於之前莫名要求修改成快顯視窗的
                //可能是有人被鬼附身亂搞的
                //Constants.toast(act, strAlertMsg);
                Constants.ErrorAlertDialog(act, "發生異常 - " + status , strAlertMsg, "確定", null, null, null);

                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(act, notification);
                    r.play();
                } catch (Exception e) {
                    //e.printStackTrace();
                }

            }
            else if ( ! resultdesc.equals("") && ! status.equals("")  )
            {
                isStop = true ;
                strAlertMsg = "注意 " + check_number + "  " + resultdesc ;
                Constants.ErrorAlertDialog(act, "發生異常 - " + status , strAlertMsg, "確定", null, null, null);
            }


        }

        return isStop ;

    }

    public void doRequest(final String requestUrl, final String requestMethod, final HashMap<String, String> param, final RequestManagerCallback callback){

        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");

        RequestObject requestObject = (RequestObject) new RequestObject();
        requestObject.setUrl(requestUrl);
        if (requestMethod.equals("POST")) {
            Log.d("-----------------", "POST");
            requestObject.setMethod(RequestObject.METHOD.POST);
        }else{
            Log.d("-----------------", "GET");
            requestObject.setMethod(RequestObject.METHOD.GET);
        }

        for ( String key : param.keySet() ) {
            requestObject.addParameter(key, param.get(key));
        }

        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {

                if (requestStatus == RequestStatus.Finish){
                    try{
                        Log.e("-----------------requestStatus", RequestStatus.Finish.toString());
                        Log.e("-----------------requestHandler.getResponseCode()", "" + requestHandler.getResponseCode());

                        Log.e("-----------------", requestHandler.getReceiveString());


                        if ( requestUrl.equals( Constants.API_POST_INSERT_SPECIAL_AREA_AUDIT )
                                || requestUrl.equals( Constants.API_POST_INSERT_CBM_SIZE_AUDIT )
                                || requestUrl.equals( Constants.API_GET_Driver_Send_Prepare_UpdateIsRead )
                        )
                        {
                            Log.e("-----------------", "---1---");

                            JsonObject jsonObj = new Gson().fromJson(requestHandler.getReceiveString(), JsonObject.class);

                            if (callback != null){
                                Log.e("-----------------", "---2---");
                                callback.onResponse(jsonObj);
                            }
                            else
                            {
                                Log.e("-----------------", "---3---");
                            }
                            return ;
                        }
                        else if ( requestUrl.equals( Constants.API_GET_Driver_Send_Prepare_GetAppList ) )
                        {
                            Log.e("-----------------", "---1---");

                            JsonArray jsonArr = new Gson().fromJson(requestHandler.getReceiveString(), JsonArray.class);
                            JsonObject jsonObj = new JsonObject();
                            jsonObj.add("result",jsonArr);

                            Log.e("-----------------", "---2---");

                            if (callback != null){
                                Log.e("-----------------", "---3---");
                                callback.onResponse(jsonObj);
                            }
                            return ;
                        }

                        String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());

                        /*
                        //暫時先不要這段了
                        if ( requestHandler.getResponseCode() != 200 && requestHandler.getResponseCode() == 500 )
                        {
                            result = "{\"resultcode\":false,\"resultdesc\":\"API服務異常\"}";
                        }
                        */
                        JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);


                        if ( requestUrl.equals( Constants.DIGI_SIGN_POST_SET_SIGN_CODE ))
                        {
                            if (callback != null){
                                callback.onResponse(convertedObject);
                            }
                            return ;
                        }
                        else if (Boolean.valueOf(convertedObject.get("resultcode").toString()) == false){
                            /*
                            Commander cmd = Commander.instance();
                            DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                            DataObject data = new DataObject();
                            data.setVariable("log_description", convertedObject.get("resultdesc").toString());
                            if (convertedObject.has("serverdate")){
                                data.setVariable("log_date", convertedObject.get("serverdate").toString());
                            }
//                            data.setVariable("log_date", String.valueOf(Constants.getNow()));
                            if (param.containsKey("check_number")){
                                data.setVariable("log_barcode", param.get("check_number").toString());
                            }
                            handler.insertByDataObject(data);
                            */
                        }

//                        if (requestUrl.equals(Constants.API_POST_SEND_UPLOAD_DATA)){
//                            JSONObject data = new JSONObject(param);
//                            TransferDataManager.instance().insertTransferData(data.get("scan_item").toString(), data.get("scan_item").toString());
//                        }

                        if (callback != null){
                            callback.onResponse(convertedObject);
                        }

                    }catch (Exception e){
                        if (callback != null){
                            callback.onFail(e.toString());
                        }
                    }
                }
                if (requestStatus == RequestStatus.Fail){


                    Log.e("Jerry", "RequestStatus.Fail");
                    JSONObject oJson = new JSONObject(param);
                    RequestController.instance().setRequest(requestUrl, requestMethod, oJson.toString());

                    if (callback != null){
                        callback.onFail("Fail");
                    }


                }
                else if (requestStatus == RequestStatus.NoNetworkConnection){
                    Log.e("Jerry", "RequestStatus.NoNetworkConnection");
                    JSONObject oJson = new JSONObject(param);
                    RequestController.instance().setRequest(requestUrl, requestMethod, oJson.toString());

                    if (callback != null){
                        callback.onFail("NoNetworkConnection");
                    }

                }
            }
        });
        request.request(requestObject);
    }




}
