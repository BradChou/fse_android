package tw.com.fsexpress.request;

import tw.com.fsexpress.framework.dataobject.DataObject;
import com.google.gson.JsonObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RequestMutilController {


    public interface RequestMutilControllerCallback {

        public void onFinish(List<DataObject> data);

    }

    private static RequestMutilController mInstance = null;
    private HashMap<String, DataObject> hash = new HashMap<String, DataObject>();
    private HashMap<String, RequestMutilObject> hashRequest = new HashMap<String, RequestMutilObject>();
    private int api_count = 0;
    private int MaxLine = 3;
    private int total_count = 0;
    private RequestMutilControllerCallback callback = null;
    private List<DataObject> arrResult = new ArrayList<DataObject>(); //callback Request結果

    public static RequestMutilController instance(){
        if(mInstance == null){
            mInstance = new RequestMutilController();
        }
        return mInstance;
    }

    public void setCallback(RequestMutilControllerCallback _callback){
        this.callback = _callback;
    }


    private void checkRequest()
    {


        for (Object key : hash.keySet()) {

            DataObject requestData = hash.get(key);

            RequestMutilObject o = getRequest(requestData);

            if (o.getStatus() == RequestMutilObject.Pendding) {


                if (api_count < MaxLine)
                {
                    o.setStatus(RequestMutilObject.Ing);

                    o.setCallback(new RequestMutilObject.RequestMutilCallback() {
                        @Override
                        public void onResponse(String status, JsonObject jsonObject, String reason, DataObject data) {
                            if (status == RequestMutilObject.Finish || status == RequestMutilObject.Fail){

                                DataObject resultData = new DataObject();
                                resultData.setVariable("barcode", data.getVariable("barcode"));
                                resultData.setVariable("code", jsonObject.get("resultcode"));
                                resultData.setVariable("reason", jsonObject.get("resultdesc"));
                                arrResult.add(resultData);
                                removeRequest(data);

                            }
                        }
                    });

                    o.request();

                    api_count = api_count + 1;
                }
            }
        }
    }

    public void addRequest(DataObject data)
    {

        RequestMutilObject o = new RequestMutilObject(data);

        this.hash.put(data.getVariable("id").toString(), data);

        this.hashRequest.put(data.getVariable("id").toString(), o);
    }

    public RequestMutilObject getRequest(DataObject d)
    {
        return this.hashRequest.get(d.getVariable("id").toString());
    }

    public void removeRequest(DataObject d)
    {
        this.hashRequest.remove(d.getVariable("id").toString());

        hash.remove(d.getVariable("id").toString());

        api_count = api_count - 1;

        if (this.hash.size() <= 0){
            if (this.callback != null){
                this.hash.clear();
                this.hashRequest.clear();
                this.callback.onFinish(this.arrResult);
            }
        }else{
            checkRequest();
        }

    }

    public void startRequest()
    {
        this.total_count = hash.size();
        this.api_count = 0;
        this.arrResult.clear();

        DataObject requestData = null;
        for (Object key : hash.keySet()) {
            requestData = hash.get(key);
            break;
        }


        checkRequest();

    }

}
