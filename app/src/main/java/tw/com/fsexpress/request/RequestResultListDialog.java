package tw.com.fsexpress.request;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;

import java.util.List;

public class RequestResultListDialog {


    private static RequestResultListDialog mInstance = null;
    public Activity act = null;
    private Dialog dialog = null;
    private RequestResultDialogCallback callback = null;

    public interface RequestResultDialogCallback{

        public void onClearFinished();

    }


    public static RequestResultListDialog instance(Activity act){
        if(mInstance == null){
            mInstance = new RequestResultListDialog();
        }
        mInstance.act = act;
        return mInstance;
    }

    public void showUploadDialog(List<DataObject> arr_data, String strTitle) {
        this.showUploadDialog(arr_data, strTitle, null);
    }


    public void showUploadDialog(List<DataObject> arr_data, String strTitle, RequestResultDialogCallback _callback) {

        this.callback = _callback;

        // dialog
        this.dialog = new Dialog(this.act);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.dialog.setContentView(R.layout.dialog_upload_result);

        TextView title = (TextView) this.dialog.findViewById(R.id.txt_title);
        title.setText("錯誤紀錄");

        LinearLayout linear = (LinearLayout) this.dialog.findViewById(R.id.linearLayout);
        float factor = linear.getContext().getResources().getDisplayMetrics().density;

        for (int i = 0; i < arr_data.size(); i++) {

            DataObject d = arr_data.get(i);

            TextView text = new TextView(this.act);
            text.setText(String.format("%s %s", d.getVariable("log_barcode").toString(), d.getVariable("log_description").toString()));
            text.setTextColor(this.act.getResources().getColor(R.color.red));

//            if (Boolean.valueOf(d.getVariable("code").toString()) == true){
//                text.setText(String.format("%s 上傳成功", d.getVariable("barcode").toString()));
//                text.setTextColor(this.act.getResources().getColor(R.color.black));
//            }else{
//                text.setText(String.format("%s %s", d.getVariable("barcode").toString(), d.getVariable("reason").toString()));
//                text.setTextColor(this.act.getResources().getColor(R.color.red));
//            }

            text.setGravity(Gravity.CENTER);
            text.setTextSize(15);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
//                    (int)(50 * factor)
            );
            linear.addView(text, params);
        }

        this.dialog.findViewById(R.id.btnClearRecord).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                handler.deleteByCriteria(null);

                if (RequestResultListDialog.this.callback != null){
                    RequestResultListDialog.this.callback.onClearFinished();
                }


            }
        });


        this.dialog.show();
    }


}
