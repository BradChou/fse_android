package tw.com.fsexpress.request;

import android.util.Log;

import tw.com.fsexpress.framework.dataobject.DataObject;
import com.google.gson.JsonObject;

import java.util.HashMap;

public class RequestMutilObject {

    public interface RequestMutilCallback {

        public void onResponse(String status, JsonObject jsonObject, String reason, DataObject data);

    }

    private DataObject data = null;
    public static String Fail = "1";
    public static String Finish = "2";
    public static String Ing = "3";
    public static String Pendding = "4";
    private RequestMutilCallback callback = null;
    private String status = "4";

    public RequestMutilObject (DataObject dataRequest) {
        data = dataRequest;
        this.status = Pendding;
    }

    public void setStatus(String s){

        this.status = s;
    }

    public String getStatus(){
        return status;
    }

    public void setCallback(RequestMutilCallback _callback){
        this.callback = _callback;
    }

    public void request()  {

        this.setStatus(Ing);
        if (RequestMutilObject.this.callback != null){
            RequestMutilObject.this.callback.onResponse(RequestMutilObject.this.status, null, null, this.data);
        }

        HashMap<String, String> param = (HashMap<String, String>)data.getVariable("param");
        String method = data.getVariable("method").toString();
        String url = data.getVariable("url").toString();
        RequestManager.instance().doRequest(url, method, param, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("Jerry", jsonObject.toString());

                setStatus(Finish);
                if (RequestMutilObject.this.callback != null){
                    RequestMutilObject.this.callback.onResponse(RequestMutilObject.this.status, jsonObject, null, data);
                }
            }

            @Override
            public void onFail(String reason) {

                setStatus(Fail);
                if (RequestMutilObject.this.callback != null){
                    RequestMutilObject.this.callback.onResponse(RequestMutilObject.this.status, null, reason, data);
                }


            }
        });

    }
}
