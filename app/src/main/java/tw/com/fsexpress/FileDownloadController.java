package tw.com.fsexpress;

import android.app.Activity;
import android.util.Log;


import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;

import java.io.RandomAccessFile;

public class FileDownloadController {

    private Activity act = null;
    private RandomAccessFile accessFile = null;
    private static FileDownloadController mInstance = null;
    private FileDownloadCallback callback = null;

    public interface FileDownloadCallback {

        public void onDownloading(int prgress);
        public void onDownloadFinished();
        public void onDownloadFail();
    }

    public static FileDownloadController intance(Activity act)
    {
        if (mInstance == null) {

            mInstance = new FileDownloadController(act);
        }

        return mInstance;
    }

    public FileDownloadController(Activity act) {
        this.act = act;
    }

    public void startDownload(Activity act, final String url, FileDownloadCallback _callback){

        this.callback = _callback;

        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");
        RequestObject requestObject = (RequestObject) new RequestObject();
        requestObject.setUrl(url);
        requestObject.setMethod(RequestObject.METHOD.GET);
        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler handler, RequestStatus status) {

                if (status == RequestStatus.ReceiveProcessInThread){

                    try{

//                        Log.e("Jerry", "size"+handler.getTotalSize());
                        Constants.newFile(FileDownloadController.this.act, url, handler.getTotalSize());
                        if (accessFile == null)
                        {

                            String fileName = Constants.findFileName(FileDownloadController.this.act, url);

                            String path = String.format("%s/%s", Constants.getFolderPath(FileDownloadController.this.act), fileName);

                            accessFile = new RandomAccessFile(path,"rwd");

                            accessFile.seek(0);
                        }

                        byte bytes[] = handler.getReveiveOutputStream().toByteArray();

                        if (handler.getTotalSize() == bytes.length){

                            accessFile.write(bytes, 0, bytes.length);
                            accessFile.close();
                            if (FileDownloadController.this.callback != null){
                                FileDownloadController.this.callback.onDownloadFinished();
                            }

                            return;
                        }


                        long tsize = handler.getTotalSize();//handler.getReveiveOutputStream().size();

                        int progress = (int) (((float) (0 + bytes.length)  / (float) tsize) * 100.0);

                        Log.e("Jerry", "progerss:"+progress);

                        if (FileDownloadController.this.callback != null){
                            FileDownloadController.this.callback.onDownloading(progress);
                        }

                    }
                    catch (Exception e){

                    }
                }
                else if  (status == RequestStatus.Fail){

                    if (FileDownloadController.this.callback != null){
                        FileDownloadController.this.callback.onDownloadFail();
                    }

                }


            }
        });
        request.request(requestObject);


    }
}
