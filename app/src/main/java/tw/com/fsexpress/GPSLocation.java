package tw.com.fsexpress;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class GPSLocation {

	private GPSCallback callback = null;

	public interface GPSCallback {
		public void onLocation(Location location);
		public void onUserAccept();
		public void onUserReject();
	}

	public interface OnPermissionListener {
		void onPermissionGranted();
		void onPermissionDenied();
	}


	private Activity activity;
	private String agps_provider = LocationManager.NETWORK_PROVIDER;
	private String gps_provider = LocationManager.GPS_PROVIDER;
	private LocationManager gps_Manager, agps_Manager;
	private Location agps_location, gps_location;
	private OnPermissionListener mPermission = null;
	private boolean gpsOnly = false;
	private static final int REQUEST_PERMISSIONS = 1;

//	public void interface onLocation(Location location);

	public static GPSLocation lo = null;

	public static GPSLocation instance(Activity act, boolean gpsOnly){

		if(lo == null){
			lo = new GPSLocation(act, gpsOnly);
		}

		return lo;

	}

	public void setCallback(GPSCallback _callback){
		this.callback = _callback;
	}

//	protected void onLocation(Location location){}

//	private Activity getActivity()
//	{
//		return this.activity;
//	}


	public boolean isOpenGPS()
	{
		LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE );

		 boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		 return statusOfGPS;
	}

	public GPSLocation(Activity act, boolean gpsOnly)
	{
		this.activity = act;
		this.gpsOnly = gpsOnly;

		this.initAGPS();



	}

	private void initAGPS()
	{
		this.agps_Manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

		this.gps_Manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
	}

	public boolean checkPermissions(String[] permissions, OnPermissionListener listener) {

		String[] deniedPermissions = getDeniedPermissions(permissions);
		if (deniedPermissions != null && deniedPermissions.length > 0) {
			Log.e("Jerry", "bbbbbb");
			requestPermissions(deniedPermissions, listener);
			return false;
		}

		if (listener != null){
			listener.onPermissionGranted();
		}
		Log.e("Jerry", "aaaaaa");
		return true;
	}

	public String[] getDeniedPermissions(String[] permissions) {
		if (permissions == null || permissions.length <=0) return null;
		ArrayList<String> deniedPermissions = new ArrayList<>();

		for (String permission: permissions) {
			if (ActivityCompat.checkSelfPermission(this.activity, permission) != PackageManager.PERMISSION_GRANTED) {
				deniedPermissions.add(permission);
			}
		}
		if (deniedPermissions.size() > 0) {
			return deniedPermissions.toArray(new String[deniedPermissions.size()]);
		} else {
			return null;
		}
	}

	public void requestPermissions(final String[] permissions, OnPermissionListener listener) {
		if (permissions == null || permissions.length <=0 || listener == null) return;

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return;


		String rationaleDescription = this.getRationaleString(permissions);

		if (!TextUtils.isEmpty(rationaleDescription)) {
			android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this.activity)
					.setCancelable(false)
					.setTitle("需要以下權限")
					.setMessage(rationaleDescription)
					.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							ActivityCompat.requestPermissions(GPSLocation.this.activity, permissions, REQUEST_PERMISSIONS);
						}
					})
					.create();
			dialog.show();
		} else {
			ActivityCompat.requestPermissions(this.activity, permissions, REQUEST_PERMISSIONS);
		}
	}

	private String getRationaleString(final String[] permissions) {
		if (permissions == null || permissions.length <=0) return "";

		HashMap<String, String> permissionHashMap = new HashMap<>();
		for (String permission: permissions) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity, permission)) {
				switch (permission) {
					case Manifest.permission.CALL_PHONE:
						permissionHashMap.put("CallPhone", "通話 - 使用裝置進行通話");
						break;
				}
			}
		}
		StringBuilder rationaleDescription = new StringBuilder();
		Set<String> keySet = permissionHashMap.keySet();
		for (String key: keySet) {
			if (rationaleDescription.length() == 0) {
				rationaleDescription.append(permissionHashMap.get(key));
			} else {
				rationaleDescription.append("\n");
				rationaleDescription.append(permissionHashMap.get(key));
			}
		}
		return rationaleDescription.toString();
	}

	public void StartLocation(Activity act)
	{
//		Location alocation = this.agps_Manager.getLastKnownLocation(this.agps_provider);
//		Location blocation = this.gps_Manager.getLastKnownLocation(this.gps_provider);
//		if (location != null)
//		{
//			onLocation(location);
////			this.agps_finder();
//		}
//		else
//		{
//			this.agps_finder();
//		}
//		this.gps_finder();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


			boolean isCheck = checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, new OnPermissionListener() {
				@Override
				public void onPermissionGranted() {
					Log.e("Jerry", "22222");
					if (callback != null){
						callback.onUserAccept();
					}

					if (GPSLocation.this.gpsOnly == true)
					{
						Log.e("Jerry", "33333");
						GPSLocation.this.gps_finder();
					}
					else
					{
						Log.e("Jerry", "4444");
						GPSLocation.this.gps_finder();
						GPSLocation.this.agps_finder();
					}

				}

				@Override
				public void onPermissionDenied() {
					if (callback != null){
						callback.onUserReject();
					}
				}
			});

			if (!isCheck){
				Log.e("Jerry", "55555");
			}


		}
		else{
			if (GPSLocation.this.gpsOnly == true)
			{
				GPSLocation.this.gps_finder();
			}
			else
			{
				GPSLocation.this.gps_finder();
				GPSLocation.this.agps_finder();
			}
		}



	}

	public void StopLocation()
	{
		gpsOnly = false;
		if (agps_listener != null)
		{
			agps_Manager.removeUpdates(agps_listener);
		}

		if (gps_listener != null)
		{
			gps_Manager.removeUpdates(gps_listener);
		}
	}

	private void gps_finder()
	{
		if (this.activity.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || this.activity.checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			Log.e("jerry","1dddd");
			this.gps_Manager.requestLocationUpdates(this.gps_provider, 60000*3, 0, gps_listener); //3分鐘更新一次
		}
	}
	
	private void agps_finder()
	{
		if (this.activity.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
				|| this.activity.checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			Log.e("jerry","2dddd");
			this.agps_Manager.requestLocationUpdates(this.agps_provider, 60000*3, 0, agps_listener); //3分鐘更新一次
		}

	}
	
	private LocationListener gps_listener = new LocationListener() {
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			Log.e("Jerry", "ddddd");
		}
		
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.e("Jerry", "cccc");
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.e("Jerry", "iiii");
		}
		
		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			Log.e("Jerry", "nnnn");
			gps_location = location;

			if (gps_location != null)
			{
				callback.onLocation(gps_location);
			}
		}
	};
	
	private LocationListener agps_listener = new LocationListener() {
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			agps_location = location;

			if (agps_location != null)
			{
				callback.onLocation(agps_location);
			}
		}
	};

}
