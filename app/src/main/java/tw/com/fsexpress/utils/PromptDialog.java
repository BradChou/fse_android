package tw.com.fsexpress.utils;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import tw.com.fsexpress.R;

public class PromptDialog extends BaseHideNavigationDialog {
    private String btnText = "";

    private ButtonListener buttonListener;

    private String content = "";

    private String title = "";

    public PromptDialog(Context paramContext, int paramInt) {
        super(paramContext, paramInt);
    }

    protected int getDialogView() {
        return R.layout.dialog_alert;
    }

    protected void initView() {
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonListener.onClick(v);
                PromptDialog.this.dismiss();
            }
        });
    }

    public PromptDialog setContent(String paramString) {
        this.content = paramString;
        ((TextView)findViewById(R.id.content)).setText(paramString);
        return this;
    }

    public PromptDialog setOnClickListener(ButtonListener paramButtonListener) {
        this.buttonListener = paramButtonListener;
        return this;
    }

    public PromptDialog setTitle(String paramString) {
        this.title = paramString;
        ((TextView)findViewById(R.id.title)).setText(paramString);
        return this;
    }

    public static interface ButtonListener {
        void onClick(View param1View);
    }
}
