package tw.com.fsexpress.utils;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;

public abstract class BaseHideNavigationDialog extends Dialog {
    public BaseHideNavigationDialog(Context paramContext) {
        this(paramContext, 0);
    }

    public BaseHideNavigationDialog(Context paramContext, int paramInt) {
        super(paramContext, paramInt);
        setContentView(getDialogView());
//        if (paramContext instanceof BaseActivity)
//            setOwnerActivity((Activity) paramContext);
        initView();
        setCancelable(true);
    }

    protected abstract int getDialogView();

    protected abstract void initView();

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getOwnerActivity(): ");
        stringBuilder.append(getOwnerActivity());
        Log.d("ALVIN", stringBuilder.toString());
//        if (getOwnerActivity() instanceof BaseActivity)
//            ((BaseActivity)getOwnerActivity()).hideNavigation();
    }

    public void show() {
        super.show();
        if (getWindow() == null)
            return;
        getWindow().getDecorView().setSystemUiVisibility(4098);
        getWindow().setFlags(2048, 2048);
    }
}
