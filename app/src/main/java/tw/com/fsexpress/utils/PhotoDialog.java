package tw.com.fsexpress.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import tw.com.fsexpress.R;

public class PhotoDialog extends BaseHideNavigationDialog{
    public PhotoDialog(Context paramContext) {
        super(paramContext, R.style.DialogStyle);
    }

    protected int getDialogView() {
        return R.layout.dialog_show_image;
    }

    protected void initView() {
        setCancelable(true);
        findViewById(R.id.show_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
    public PhotoDialog setImageBitmap(Bitmap paramBitmap) {
        ((ImageView)findViewById(R.id.show_img)).setImageBitmap(paramBitmap);
        return this;
    }


}
