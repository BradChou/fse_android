package tw.com.fsexpress;

import android.media.ToneGenerator;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import tw.com.fsexpress.framework.activity.GHWFragment;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

public class BarcodeFragment extends GHWFragment implements BarcodeCallback {

    private long DELAY_TIME = 1500L;

    private DecoratedBarcodeView barcodeView;
    private TextView goodsNum;
    private boolean isStop = false;
//    private CustomList<String> list;
//    private CustomList.OnListChangeListener<String> listNumListener = new -$$Lambda$BarcodeActivity$xeAttRPtLHrbaF6wtxyYBwDOvwk(this);
    private int scanItem = 0;
    private ScrollView scrollView;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum;
    private TextView txtScanNum;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        DecoratedBarcodeView decoratedBarcodeView = (DecoratedBarcodeView)view.findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView = decoratedBarcodeView;
        decoratedBarcodeView.decodeContinuous(this);

    }
//    @Override
//    protected void onPause() {
//        super.onPause();
//        this.barcodeView.pause();
//    }
//    @Override
//    protected void onResume() {
//        super.onResume();
//        this.barcodeView.resume();
//    }

    @Override
    public void barcodeResult(BarcodeResult result) {

    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }
}
