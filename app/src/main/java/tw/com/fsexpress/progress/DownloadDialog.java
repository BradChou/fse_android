package tw.com.fsexpress.progress;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import tw.com.fsexpress.R;

public class DownloadDialog extends Dialog {
    private CustomDownloadProgress customLoadingProgress;
    private TextView dialogTitle;
    private TextView retryBtn;
    private TextView useBrowserBtn;

    public DownloadDialog(Context arg2) {
        super(arg2, 0x7F0E0167);  // style:iosDialog
        this.setContentView(R.layout.dialog_download_progerss);  // layout:dialog_down_load_progress
        this.initDialogView();
    }

    public CustomDownloadProgress getCustomLoadingProgress() {
        return this.customLoadingProgress;
    }

    public TextView getDialogTitle() {
        return this.dialogTitle;
    }

    public TextView getRetryBtn() {
        return this.retryBtn;
    }

    public TextView getUseBrowserBtn() {
        return this.useBrowserBtn;
    }

    private void initDialogView() {
        this.setCancelable(false);
        this.customLoadingProgress = (CustomDownloadProgress)this.findViewById(0x7F070089);  // id:custom_loading_progress
        this.retryBtn = (TextView)this.findViewById(0x7F07015D);  // id:txt_retry
        this.useBrowserBtn = (TextView)this.findViewById(0x7F070167);  // id:txt_use_browser
        this.dialogTitle = (TextView)this.findViewById(0x7F070164);  // id:txt_title
    }

    public void setDownLoadFail() {
        this.dialogTitle.setText("下載失敗");
        this.dialogTitle.setTextColor(this.getContext().getResources().getColor(0x7F040076));  // color:red
        this.retryBtn.setVisibility(0);
        this.useBrowserBtn.setVisibility(0);
        this.customLoadingProgress.setProgress(0);
    }
}

