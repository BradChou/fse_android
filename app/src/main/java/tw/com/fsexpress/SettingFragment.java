package tw.com.fsexpress;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;

public class SettingFragment extends GHWFragment {

    private Activity act = null;
    private TextView txtAccount, txtName, txtWorkStationName;
    private EditText edtWorkStationCode;
    private Button btnConfirm, btnCancel, btnLeft, btnRight, btnInquire;
    private Spinner spinnerSendCode, spinnerDeliveryCode;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);
        this.act = this.getActivity();

        this.btnLeft = view.findViewById(R.id.btn_left);
        this.btnRight = view.findViewById(R.id.btn_right);
        this.txtAccount = view.findViewById(R.id.txtAccount);
        this.txtName = view.findViewById(R.id.txtName);
        this.edtWorkStationCode = view.findViewById(R.id.editStationCode);
        this.btnInquire = view.findViewById(R.id.btnInquire);
        this.txtWorkStationName = view.findViewById(R.id.txtWorkStationName);
        this.spinnerSendCode = view.findViewById(R.id.spinnerSendCode);
        this.spinnerDeliveryCode = view.findViewById(R.id.spinnerDeliveryCode);
        this.btnConfirm = view.findViewById(R.id.btnConfirm);
        this.btnCancel = view.findViewById(R.id.btnCancel);

        this.btnRight.setVisibility(View.INVISIBLE);

        String stationCode = AccountManager.instance().getWorkStationCode(getFragmentActivity());
        String stationName = AccountManager.instance().getWorkStationName(getFragmentActivity());

        this.requestGetDataStationCode(stationCode);

        this.edtWorkStationCode.setText(stationCode);
        this.txtWorkStationName.setText(stationName);

        this.edtWorkStationCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                spinnerSendCode.setAdapter(null);
                spinnerDeliveryCode.setAdapter(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // 站所檢碼editText 螢幕鍵盤點選搜尋
        this.edtWorkStationCode.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                btnInquire.performClick();
                return true;
            }
            return false;
        });

        this.btnInquire.setOnClickListener(v -> {
            String code = this.edtWorkStationCode.getText().toString().trim();
            if (code.isEmpty()) {
                Constants.ErrorAlertDialog(act, getString(R.string.notice), getString(R.string.txt_work_station_code_missing), getString(R.string.yes), null, null,
                        null).show();
            } else {
                this.requestGetDataStationCode(code);
            }
        });

        this.spinnerSendCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        this.spinnerDeliveryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        TextView txtTitle = (TextView) view.findViewById(R.id.txt_title);
        txtTitle.setText("設定");

        JSONObject jsonObject = AccountManager.instance().getMemberInfo(this.getFragmentActivity());
        txtAccount.setText(Constants.get(getFragmentActivity(), "previous_account"));
        try {
            String strDriver = jsonObject.get("driver_name").toString();
            txtName.setText(strDriver);
        } catch (JSONException e) {

        }

        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(v -> {
            back();
        });

        btnConfirm.setOnClickListener(v -> {
            try {
                String chooseSendCode = this.spinnerSendCode.getSelectedItem().toString();
                String chooseDeliveryCode = this.spinnerDeliveryCode.getSelectedItem().toString();
                if (chooseSendCode.isEmpty()) {
                    Constants.ErrorAlertDialog(act, getString(R.string.notice), getString(R.string.txt_work_send_code_missing), getString(R.string.yes), null, null,
                            null).show();
                    return;
                }

                if (chooseDeliveryCode.isEmpty()) {
                    Constants.ErrorAlertDialog(act, getString(R.string.notice), getString(R.string.txt_work_delivery_code_missing), getString(R.string.yes), null, null,
                            null).show();
                    return;
                }

                this.requestUpdateDriver(chooseSendCode, chooseDeliveryCode);
            } catch (Exception e) {
                Constants.ErrorAlertDialog(act, getString(R.string.notice), getString(R.string.txt_work_code_missing), getString(R.string.yes), null, null,
                        null).show();
            }
        });

        btnCancel.setOnClickListener(v -> {
            back();
        });

    }

    private void requestGetDataStationCode(String code) {

        AlertDialog dialog = Constants.ProgressBar(SettingFragment.this.getFragmentActivity(), "讀取中...請稍候");
        dialog.show();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("Work_Station_Scode", code);

        RequestManager.instance().doRequest(Constants.API_POST_GET_DATA_BY_STATION_CODE, "POST", params, new RequestManager.RequestManagerCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JsonObject jsonObject) {
                dialog.cancel();
                hideKeyboard();
                if (Boolean.parseBoolean(String.valueOf(jsonObject.get("resultcode"))) == true) {
                    String strWorkStationName = jsonObject.get("Work_Station_Name").getAsString();
                    String strSendCode = jsonObject.get("Send_Code_Data").getAsString();
                    String strDeliveryCode = jsonObject.get("Delivery_Code_Data").getAsString();

                    String workSendCode = AccountManager.instance().getWorkSendCode(getFragmentActivity());
                    String workDeliveryCode = AccountManager.instance().getWorkDeliveryCode(getFragmentActivity());

                    txtWorkStationName.setText(strWorkStationName);

                    if (!strSendCode.isEmpty()) {
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, convertStringToList(strSendCode));
                        spinnerSendCode.setAdapter(arrayAdapter);
                        setSpinnerText(spinnerSendCode, workSendCode);
                    } else {
                        Toast.makeText(getFragmentActivity(), "查無集區", Toast.LENGTH_LONG).show();
                    }

                    if (!strDeliveryCode.isEmpty()) {
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, convertStringToList(strDeliveryCode));
                        spinnerDeliveryCode.setAdapter(arrayAdapter);
                        setSpinnerText(spinnerDeliveryCode, workDeliveryCode);
                    } else {
                        Toast.makeText(getFragmentActivity(), "查無配區", Toast.LENGTH_LONG).show();
                    }
                } else {
                    String message = String.valueOf(jsonObject.get("resultdesc"));
                    Constants.ErrorAlertDialog(act, getString(R.string.notice), "取得駐區碼失敗：" + message, getString(R.string.yes), null, null,
                            null).show();
                }
            }

            @Override
            public void onFail(String reason) {
                dialog.cancel();
                hideKeyboard();
                Constants.ErrorAlertDialog(act, getString(R.string.notice), "取得駐區碼失敗：" + reason, getString(R.string.yes), null, null,
                        null).show();
            }
        });
    }

    private void requestUpdateDriver(String workSendCode, String workDeliveryCode) {
        AlertDialog dialog = Constants.ProgressBar(SettingFragment.this.getFragmentActivity(), "修改中...請稍候");
        dialog.show();

        String workStationCode = edtWorkStationCode.getText().toString().trim();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("Driver_Code", AccountManager.instance().getDriverCode(getFragmentActivity()));
        params.put("Work_Station_Scode", workStationCode);
        params.put("Work_Send_Code", workSendCode);
        params.put("Work_Delivery_Code", workDeliveryCode);

        RequestManager.instance().doRequest(Constants.API_POST_UPDATE_DRIVER, "POST", params, new RequestManager.RequestManagerCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JsonObject jsonObject) {
                dialog.cancel();
                if (Boolean.parseBoolean(String.valueOf(jsonObject.get("resultcode"))) == true) {


                    String workStationName = txtWorkStationName.getText().toString().trim();
                    AccountManager.instance().saveWorkStationCode(getFragmentActivity(), workStationCode);
                    AccountManager.instance().saveWorkStationName(getFragmentActivity(), workStationName);
                    AccountManager.instance().saveWorkSendCode(getFragmentActivity(), workSendCode);
                    AccountManager.instance().saveWorkDeliveryCode(getFragmentActivity(), workDeliveryCode);

                    Constants.ErrorAlertDialog(act, getString(R.string.notice), "修改成功！", getString(R.string.yes), null, null,
                            null).show();
                } else {
                    String message = jsonObject.get("resultdesc").getAsString();
                    Constants.ErrorAlertDialog(act, getString(R.string.notice), "修改失敗：" + message, getString(R.string.yes), null, null,
                            null).show();
                }
            }

            @Override
            public void onFail(String reason) {
                dialog.cancel();
                Constants.ErrorAlertDialog(act, getString(R.string.notice), "修改失敗：" + reason, getString(R.string.yes), null, null,
                        null).show();
            }
        });
    }

    private List<String> convertStringToList(String string) {
        String replace = string.replace("[", "");
        String replace2 = replace.replace("]", "");
        List<String> list = new ArrayList<>(Arrays.asList(replace2.split(",")));
        try {
            List<Integer> listInt = list.stream().map(Integer::parseInt).sorted().collect(Collectors.toList()); // 排序
            List<String> listString = listInt.stream().map(Object::toString).collect(Collectors.toList());
            return listString;
        } catch (Exception e) {
            return list;
        }
    }

    // 找尋原本的文字在spinner的哪個位置 設定顯示
    public void setSpinnerText(Spinner spin, String text) {
        for (int i = 0; i < spin.getAdapter().getCount(); i++) {
            if (spin.getAdapter().getItem(i).toString().contains(text)) {
                spin.setSelection(i);
            }
        }
    }

    private void back() {
        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0) {
            ((GHWFragmentActivity) SettingFragment.this.getFragmentActivity()).popFragment();
        } else {
            RequestResultListDialog.instance(SettingFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "設定", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {

                }
            });
        }
    }

    // 隱藏鍵盤
    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getFragmentActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getFragmentActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception var2) {
        }

    }
}