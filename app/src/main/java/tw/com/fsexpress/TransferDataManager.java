package tw.com.fsexpress;

import android.icu.util.Calendar;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.Criteria;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;

import java.util.List;

public class TransferDataManager {

    public static TransferDataManager amg = null;

    public static TransferDataManager instance(){

        if (TransferDataManager.amg == null){
            TransferDataManager.amg = new TransferDataManager();
        }
        return TransferDataManager.amg;
    }


    public void insertTransferData(String type, String name, String code){

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR,1);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.MILLISECOND,0);

        long time = calendar.getTimeInMillis()/1000;
        String s_time = String.valueOf(time);

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.transfer");
        DataObject data = new DataObject();
        data.setVariable("transfer_type", type);
        data.setVariable("transfer_code", code);
        data.setVariable("transfer_name", name);
        data.setVariable("create_date", Constants.getNow());
        handler.insertByDataObject(data);

    }


    public void checkTransferData(){

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR,1);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.MILLISECOND,0);

        long nowTime = (calendar.getTimeInMillis()/1000) + (3600*8);

        List<DataObject> arrTrans = this.getTransferData();
        for (DataObject data: arrTrans) {

            long createTime = Long.valueOf((Integer)data.getVariable("create_date"));
            long resultTime = nowTime - createTime;
            if (resultTime >= (3600*24*5)){
                this.deleteTransferByCode(data.getVariable("transfer_code").toString());
            }

        }

    }

    public void deleteTransferByCode(String code){
        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.transfer");
        handler.deleteByCriteria(new Criteria("transfer_code", code));
    }

    public void deleteTransferData(){
        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.transfer");
        handler.deleteByCriteria(null);
    }

    public List<DataObject> getTransferData(){

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.transfer");

        return handler.getDataByCriteria(null);

    }

    public List<DataObject> getTransferDataByCode(String code ){

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.transfer");
        return handler.getDataByCriteria(new Criteria("transfer_code", code));

    }

    public List<DataObject> getTransferDataByType(String type){

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.transfer");

        return handler.getDataByCriteria(new Criteria("transfer_type", type));

    }







}
