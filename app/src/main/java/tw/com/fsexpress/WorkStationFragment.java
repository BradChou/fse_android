package tw.com.fsexpress;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import tw.com.fsexpress.framework.activity.GHWFragment;


public class WorkStationFragment extends GHWFragment {


    private Activity act = null;

    private TextView txtAccount, txtName, txtStation, txtSendCode, txtDeliveryCode;
    private Button btnStart;

    @SuppressLint("SetTextI18n")
    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        this.act = this.getActivity();
        this.btnStart = (Button) view.findViewById(R.id.btnStart);
        this.txtAccount = (TextView) view.findViewById(R.id.txtAccount);
        this.txtName = (TextView) view.findViewById(R.id.txtName);
        this.txtStation = (TextView) view.findViewById(R.id.txtStation);
        this.txtSendCode = (TextView) view.findViewById(R.id.txtSendCode);
        this.txtDeliveryCode = (TextView) view.findViewById(R.id.txtDeliveryCode);

        JSONObject jsonObject = AccountManager.instance().getMemberInfo(this.getFragmentActivity());
        txtAccount.setText(Constants.get(getFragmentActivity(), "previous_account"));
        try {
            String strDriver = jsonObject.get("driver_name").toString();
            String strStationCode = jsonObject.get("station").toString();
            String strStationName = jsonObject.get("station_name").toString();
            String strWorkStationCode = jsonObject.get("Work_Station_Scode").toString();
            String strWorkStationName = jsonObject.get("work_Station_Name").toString();
            String strWorkSendCode = jsonObject.get("Work_Send_Code").toString();
            String strWorkDeliveryCode = jsonObject.get("Work_Delivery_Code").toString();
            txtName.setText(strDriver);
            if (strWorkStationCode.isEmpty()) {
                txtStation.setText(strStationCode + strStationName);
            } else {
                txtStation.setText(strWorkStationCode + strWorkStationName);
            }

            txtSendCode.setText(strWorkSendCode);
            txtDeliveryCode.setText(strWorkDeliveryCode);
        } catch (JSONException e) {

        }

        btnStart.setOnClickListener(v -> {
            Intent intent = new Intent(WorkStationFragment.this.getFragmentActivity(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            WorkStationFragment.this.getFragmentActivity().finish();
        });
    }


}