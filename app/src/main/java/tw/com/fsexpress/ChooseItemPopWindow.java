package tw.com.fsexpress;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

class ChooseItemPopWindow extends PopupWindow {

    public static final int TRANS_IN = 1;

    public static final int TRANS_OUT = 2;

    private ChooseItemPopWindowListener chooseItemPopWindowListener;

    public ChooseItemPopWindow(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.popwindow_choose_item, null, false);
        setContentView(view);
        setWidth(-1);
        setHeight(-2);
        setOutsideTouchable(true);
        setBackgroundDrawable((Drawable) new ColorDrawable(context.getResources().getColor(R.color.transparent)));
        setAnimationStyle(R.anim.slide_enter_main);

        this.initListener(view);
    }

    private void initListener(View view) {
        view.findViewById(R.id.btn_trans_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseItemPopWindowListener.onChoice(TRANS_IN);
            }
        });

        view.findViewById(R.id.btn_trans_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseItemPopWindowListener.onChoice(TRANS_OUT);
            }
        });

        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseItemPopWindowListener.onChoice(0);
            }
        });
    }

    public void setChooseItemPopWindowListener(ChooseItemPopWindowListener listener) {
        this.chooseItemPopWindowListener = listener;
    }

    public static interface ChooseItemPopWindowListener {
        void onChoice(int choice);
    }
}
