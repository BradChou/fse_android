package tw.com.fsexpress.Menu.delivery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.MainActivity;
import tw.com.fsexpress.Menu.camera.TakePhotoManager;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.MatchedData;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;
import tw.com.fsexpress.utils.PhotoDialog;
import tw.com.fsexpress.utils.PromptDialog;


//配達2.0
public class DeliveryFragment2 extends GHWFragment {

    //是否已驗證完成 CheckNumber
    private boolean isCheckNumberOk = false ;
    //是否已完成簽名 ( 有按"確認"按鈕 )
    private boolean isSignOk = false ;

    private static final String TAG = "ALVIN";

    private int FULL_CAMERA = 101;
    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private TextView txtGoodNum, txtTotalNum, txtAccumulationNum;
    private ScrollView scrollView;
    private ConstraintLayout ZoneSign;
    private LinearLayout linearLayout3 ;

    // bar
    private Button btnLeft = null, btn_toSignZone = null , btn_checkdeliverystatus = null, btn_toWebQrCodeZone = null ,  signCfmOK = null,signClear = null, btnRight = null;
    private TextView textTitle = null;

    // page
    private EditText editGoodNum = null;

    private Button btnPhoto = null, btnPhoto2 = null, btnLabelStamp = null, btnSave = null;
    private Spinner spinnerAbnormalStatus;
    private Spinner spinnerAbnormalStatusE;

    private TextView editGoodPiecesNum;
    private TextView editMoney;
    private EditText editPlatesNum;

    //private String[] spinnerContentA = new String[]{"3.正常配交", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達", "A.原單退回"};
    //private String[] spinnerContentB = new String[]{"3.正常配交", "C. 振興券", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達", "A.原單退回"};

    //private String[] spinnerContentA = new String[]{"3.正常配交", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達"};
    //private String[] spinnerContentB = new String[]{"3.正常配交", "C. 振興券", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達"};

    private String[] spinnerContentA = new String[]{"3 .正常配交", "1 .客戶不在", "2 .約定再配", "4 .拒收", "5 .地址錯誤", "6 .查無此人", "11.公司行號休息","26.聯絡不上","21.到站:自取" };

    //private String[] spinnerContentE = new String[]{"配達分區", "記憶配達分區" };

    //是否記憶配達分區的選項
    private boolean isRememberDeliveryType = false ;


    private String deliverySelect = "";
    private String selectSpinnerId = "";

    private ImageView imgQrCode;

    private ImageView imgBig;
    private ImageView imgSmall;
    private ImageView imgshowImage;
    private LinearLayout llPhoto;

    private TextView txtCheck, txtGoodNumFail , txtBarcode;

    private Bitmap bigBitmap;
    private String bigBitmapUrl = "";
    private Bitmap smallBitmap;
    private String smallBitmapUrl = "";

    private ToneGenerator toneGenerator;
    private boolean isAuto = false;
    private int requestTotal = 0;

    private DigiSignDrawView dvSignViewMain  ;

    private String request_id = "";

    //數位簽名型態  電子簽名 = "1"  簽單拍照 = "3"  Qr-Code = "2"
    private String strDigiSignType = "1";

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        //接著檢查一下
        ((MainActivity)(this.getFragmentActivity())).checkOpenTime();

        isCheckNumberOk = false ;
        isSignOk = false ;

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("配達2.0");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) DeliveryFragment2.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(DeliveryFragment2.this.getFragmentActivity()).showUploadDialog(arrLogs, "配達2.0", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.txtTotalNum = (TextView) view.findViewById(R.id.txt_total_num);
//        this.txtAccumulationNum = (TextView) view.findViewById(R.id.txt_accumulation_num);

        this.scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollView.smoothScrollTo(0,0);
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);
        this.txtGoodNum = (TextView) view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);

        this.ZoneSign = ((ConstraintLayout)view.findViewById(R.id.ZoneSign));

        this.dvSignViewMain = (DigiSignDrawView)view.findViewById(R.id.signView);

        this.linearLayout3 = ((LinearLayout)view.findViewById(R.id.linearLayout3));

        // page
        if (this.editGoodNum == null){
            this.editGoodNum = (EditText) view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                boolean isNoContinue = false ;
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                    /*
                    Constants.showPromptDialog(getFragmentActivity(), "提示", s.toString().trim() , new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                    if ( s.toString().trim().length() > 0 && after >= 10)
                    {
                        //DeliveryFragment2.this.editGoodNum.setText("");
                    }
                     */
                    /*
                    if(start > 0 && after>=10)
                    {

                    }

                    Constants.showPromptDialog(getFragmentActivity(), "提示", Integer.toString(start) + "-" + Integer.toString(count) + "-" + Integer.toString(after) , new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }

                    });
                    */
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    isCheckNumberOk = false;
                    isSignOk = false;
                    if(count >= 10 || count >= 2 ) {
                        // 1次進10馬以上 判定為紅外線掃瞄
                        // 新增 > 2 以上的判斷 ( 掃 barcode 順便拍照 , 有極大的可能為異常 )
                        isAuto = true;

                        //dvSignViewMain.clear();
                        editMoney.setText("0");
                        editGoodPiecesNum.setText("0");
                        txtBarcode.setText("");
                        imgQrCode.setVisibility(View.GONE);
                        btn_checkdeliverystatus.setVisibility(View.GONE);

                        requestCheckNumber(editGoodNum.getText().toString().trim());
                        
                        /*
                        Constants.showPromptDialog(getFragmentActivity(), "提示", "11" + editGoodNum.getText().toString().trim() + "11", new PromptDialog.ButtonListener() {
                            @Override
                            public void onClick(View param1View) {

                            }
                        });
                        */                       
/*
                        if (DeliveryFragment2.this.editGoodNum.getText().toString().indexOf(" ") > 1
                                && DeliveryFragment2.this.editGoodNum.getText().toString().lastIndexOf("") != DeliveryFragment2.this.editGoodNum.getText().toString().indexOf(" ") )
                        {
                            DeliveryFragment2.this.editGoodNum.setText("");
                            DeliveryFragment2.this.editGoodNum.setText(strBarcode);
                        }
                        else
                        {
                            requestCheckNumber(editGoodNum.getText().toString());
                        }
*/


/*
                        Constants.showPromptDialog(getFragmentActivity(), "提示",
                                strEditGoodNum + "-" +s.toString() + Integer.toString(start) + "-" + Integer.toString(before) + "-" + Integer.toString(count) ,
                                new PromptDialog.ButtonListener() {
                            @Override
                            public void onClick(View param1View) {

                            }

                        });

                        if ( strEditGoodNum.length() < s.toString().length()
                            && ! strEditGoodNum.equals(s.toString()) )
                        {
                            DeliveryFragment2.this.editGoodNum.setText(strEditGoodNum);
                        }

*/
                        //requestCheckNumber(editGoodNum.getText().toString());
                        //requestCheckNumber(editGoodNum.getText().toString());


//                        checkUpload(s.toString().trim());
//                        requestUpload(s.toString().trim());


                    }else if (count > 0){
                        //1次不滿10馬 判定為手動輸入
                        //dvSignViewMain.clear();
                        editMoney.setText("0");
                        editGoodPiecesNum.setText("0");

                        imgQrCode.setVisibility(View.GONE);
                        btn_checkdeliverystatus.setVisibility(View.GONE);

                        isAuto = false;
                    }
                    else
                    {
                        //看起來如果倒退刪除資料會進入這邊
                        //dvSignViewMain.clear();
                        editMoney.setText("0");
                        editGoodPiecesNum.setText("0");

                        imgQrCode.setVisibility(View.GONE);
                        btn_checkdeliverystatus.setVisibility(View.GONE);

                        isAuto = false;
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {
                    //strEditGoodNum = "";
                    //DeliveryFragment2.this.editGoodNum.setText("");
                }
            });
        }else{
            this.editGoodNum.requestFocus();
        }

        // btn_toSignZone   btn_toWebQrCodeZone   btn_checkdeliverystatus =
        this.btn_toSignZone = (Button) view.findViewById(R.id.btn_toSignZone);
        this.btn_toWebQrCodeZone = (Button) view.findViewById(R.id.btn_toWebQrCodeZone);
        this.btn_checkdeliverystatus = (Button) view.findViewById(R.id.btn_checkdeliverystatus);
        this.imgQrCode = (ImageView) view.findViewById(R.id.img_qrcode);

        this.btnSave = (Button) view.findViewById(R.id.btn_save);

        this.btn_toSignZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if ( isCheckNumberOk == true || txtBarcode.getText().toString().length() > 0 )
                {

                    if ( selectSpinnerId.equals("3") ) // || selectSpinnerId.equals("7") || selectSpinnerId.equals("8")
                    {
                        btnSave.setVisibility(View.VISIBLE);
                        imgQrCode.setVisibility(View.GONE);
                        btn_checkdeliverystatus.setVisibility(View.GONE);

                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, editGoodNum.getTop());
                            }
                        });
                        strDigiSignType = "1" ;
                        setShowSign(true);
                    }
                    else
                    {
                        Constants.showPromptDialog(getFragmentActivity(), "提示", "正常配交才需要簽名", new PromptDialog.ButtonListener() {
                            @Override
                            public void onClick(View param1View) {

                            }
                        });
                    }

                }
                else
                {
                    Constants.showPromptDialog(getFragmentActivity(), "提示", "請先提供貨號", new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                }

            }
        });

        this.btn_toWebQrCodeZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( isCheckNumberOk == true || txtBarcode.getText().toString().length() > 0 )
                {
                    if ( isPassCodeOk == true)
                    {

                    }
                    else
                    {
                        Constants.showPromptDialog(getFragmentActivity(), "提示", "QR-Code碼產出失敗或是手機不支援,請改用電子簽名", new PromptDialog.ButtonListener() {
                            @Override
                            public void onClick(View param1View) {

                            }
                        });
                        return ;
                    }
                    if ( selectSpinnerId.equals("3"))
                    {
                        btnSave.setVisibility(View.GONE);
                        imgQrCode.setVisibility(View.VISIBLE);
                        btn_checkdeliverystatus.setVisibility(View.VISIBLE);

                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, btn_toWebQrCodeZone.getTop());
                            }
                        });
                    }
                    else
                    {
                        Constants.showPromptDialog(getFragmentActivity(), "提示", "正常配交才需要QR-Code簽名", new PromptDialog.ButtonListener() {
                            @Override
                            public void onClick(View param1View) {

                            }
                        });
                    }


                }
                else
                {
                    Constants.showPromptDialog(getFragmentActivity(), "提示", "請先提供貨號", new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                }
            }
        });



        this.signCfmOK = (Button) view.findViewById(R.id.signCfmOK);
        this.signCfmOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               boolean check = checkSignFine();

               if ( check == false)
               {
                   Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "您沒有進行電子簽名或是簽名筆跡太簡單 , 請重新簽名。", "確定", null, null, null);
                   return;
               }
                isSignOk = true;
                setShowSign(false);

                //2022-07
                //這個按鈕的農改成直接送出更新貨態+上傳
                doSave();

            }
        });

        ((Button) view.findViewById(R.id.signBackStep) ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strDigiSignType = "1" ;
                setShowSign(false);
            }
        });


        this.signClear = (Button) view.findViewById(R.id.signClear);
        this.signClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                isSignOk = false;
                dvSignViewMain.clear();
                */
                strDigiSignType = "1" ;
                setShowSign(true);

                //setShowSign(false);
            }
        });



        this.btnPhoto = (Button) view.findViewById(R.id.btn_photo);
        this.btnPhoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                getPhoto();
            }
        });

        this.btnPhoto2 = (Button) view.findViewById(R.id.btn_photo2);
        this.btnPhoto2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                strDigiSignType = "3" ;
                getPhoto2();
            }
        });

        this.btnLabelStamp = (Button) view.findViewById(R.id.btn_toLabelStamp);
        this.btnLabelStamp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                if ( isCheckNumberOk == true || txtBarcode.getText().toString().length() > 0 )
                {

                    if ( selectSpinnerId.equals("3"))
                    {
                        btnSave.setVisibility(View.VISIBLE);
                        imgQrCode.setVisibility(View.GONE);
                        btn_checkdeliverystatus.setVisibility(View.GONE);

                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, editGoodNum.getTop());
                            }
                        });

                        strDigiSignType = "3" ;
                        setShowSign(true);
                        getPhoto2();

                    }
                    else
                    {
                        Constants.showPromptDialog(getFragmentActivity(), "提示", "正常配交才需要簽名", new PromptDialog.ButtonListener() {
                            @Override
                            public void onClick(View param1View) {

                            }
                        });
                    }

                }
                else
                {
                    Constants.showPromptDialog(getFragmentActivity(), "提示", "請先提供貨號", new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                }




            }
        });


        //電子簽名上傳
        //this.btnSave = (Button) view.findViewById(R.id.btn_save);
        this.btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                /*
                btnSave.setEnabled(false);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        btnSave.setEnabled(true);
                        Log.d(TAG,"reset btnSave");

                    }
                },2000);// set time as per your requirement
                */
                doSave();
            }
        });

        //檢查 簽名配達狀態
        this.btn_checkdeliverystatus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                if ( txtBarcode.getText().toString().length() >= 10)
                {

                    String strScanTime = Constants.getTime();
                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("check_number", txtBarcode.getText().toString());

                    RequestManager.instance().doRequest(Constants.API_POST_CHECK_DELIVERY_STATUS, "POST", data, new RequestManager.RequestManagerCallback() {
                        @Override
                        public void onResponse(JsonObject jsonObject)
                        {


                            if ( jsonObject.has("resultdesc") )
                            {
                                if ( jsonObject.get("resultdesc").getAsString().equals("OK") )
                                {
                                    Constants.showPromptDialog(getFragmentActivity(), "提示", "已完成電子簽名", new PromptDialog.ButtonListener() {
                                        @Override
                                        public void onClick(View param1View) {

                                        }
                                    });
                                }
                                else
                                {
                                    Constants.showPromptDialog(getFragmentActivity(), "提示", jsonObject.get("resultdesc").getAsString(), new PromptDialog.ButtonListener() {
                                        @Override
                                        public void onClick(View param1View) {

                                        }
                                    });
                                }
                            }


                        }

                        @Override
                        public void onFail(String reason)
                        {

                        }
                    });


                }



            }


        });


        this.editPlatesNum = (EditText) view.findViewById(R.id.edit_plates_num);
        this.editGoodPiecesNum = (TextView) view.findViewById(R.id.edit_good_pieces_num);

        this.editMoney = (TextView) view.findViewById(R.id.textViewMoney);

        this.txtCheck = (TextView) view.findViewById(R.id.txt_check);
        this.txtCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheckNumberOk = false ;
                isSignOk = false;
                if (editGoodNum.getText().toString().length() == 0)
                {
                    //沒有輸入任何貨號
                    //就直接開啟掃 BarCode
                    scan();
                }
                else if ( editGoodNum.getText().toString().length() > 0  && editGoodNum.getText().toString().length() < 10)
                {
                    //貨號長度不對
                    //沒有下一步
                    Constants.toast((Context) getActivity(), "您的貨號小於10號");
                    return;
                }
                else
                {
                    //檢查貨號
                    requestCheckNumber(editGoodNum.getText().toString());
                }

            }
        });

        this.imgBig = (ImageView) view.findViewById(R.id.img_big);
        this.imgSmall = (ImageView) view.findViewById(R.id.img_small);
        this.imgshowImage = (ImageView) view.findViewById(R.id.showImageView);

        this.llPhoto = (LinearLayout) view.findViewById(R.id.ll_photo);

        this.imgBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bigBitmap != null) {
                    new PhotoDialog(getActivity()).setImageBitmap(bigBitmap).show();
                }
            }
        });

        this.imgSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (smallBitmap != null) {
                    new PhotoDialog(getActivity()).setImageBitmap(smallBitmap).show();
                }
            }
        });

        /*
        view.findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scan();
            }
        });
        */


        ((CheckBox) view.findViewById(R.id.textView50)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRememberDeliveryType = !isRememberDeliveryType ;
                if ( isRememberDeliveryType == true)
                {
                    Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "您選擇了記憶 \"配達分區\"", "我知道了", null, null, null);
                }

            }
        });


        if (this.spinnerAbnormalStatus == null){

            getDevliveryType();

        }

    }


    boolean isPassCodeOk = false ;

    public void genCode() {

        isPassCodeOk = false;

        String strPassCode = Integer.toString(100000 - ( 10000 + (int) (Math.random()*89999) ) ) ;
        setPassCode(strPassCode);
        Log.e("-------------","isPassCodeOk   3");
        String strUrl =  Constants.DIGI_SIGN_POST_SIGN_PAGE_MAIN //  "https://APP_WEB.fs-express.com.tw/SignPageMain.aspx"?"
                            + "openExternalBrowser=1&"
                            + "cn=" + txtBarcode.getText().toString()
                            + "&driver_code=" + AccountManager.instance().getDriverCode(DeliveryFragment2.this.getFragmentActivity())
                            + "&request_id=" + request_id
                            + "&pieces=" + editGoodPiecesNum.getText().toString()
                            + "&plates=" + editPlatesNum.getText().toString()
                            + "&ao=3"
                            + "&signType=2"
                            + "&code=" + strPassCode ;

        BarcodeEncoder encoder = new BarcodeEncoder();
        try {
            Bitmap bit = encoder.encodeBitmap(strUrl, BarcodeFormat.QR_CODE,300,300);
            imgQrCode.setImageBitmap(bit);
        } catch (WriterException e) {
            e.printStackTrace();
        }


    }

    private void setPassCode(String strPassCode)
    {
        Log.e("-------------","isPassCodeOk   1");
        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("cn", txtBarcode.getText().toString());
        data.put("Code", strPassCode);
        Log.e("-------------",Constants.DIGI_SIGN_POST_SET_SIGN_CODE);
        Log.e("-------------",txtBarcode.getText().toString());
        Log.e("-------------",strPassCode);
        RequestManager.instance().doRequest(Constants.DIGI_SIGN_POST_SET_SIGN_CODE, "GET", data, new RequestManager.RequestManagerCallback() {

            @Override
            public void onResponse(JsonObject jsonObject) {
                isPassCodeOk = true;
                Log.e("-------------","isPassCodeOk   2");
            }

            @Override
            public void onFail(String reason) {
                Log.e("-------------","isPassCodeOk   2 error");
            }
        });
    }

    private void setShowSign(boolean isShow)
    {
        if ( isShow == true)
        {
            try {
                isSignOk = false;
                imgBig.setImageBitmap(null);
                imgSmall.setImageBitmap(null);
                smallBitmapUrl = "";
                bigBitmapUrl = "";
                dvSignViewMain.clear();
            }
            catch(Exception ex)
            {

            }

            scrollView.setVisibility(View.GONE);
            ZoneSign.setVisibility(View.VISIBLE);
            linearLayout3.setVisibility(View.GONE);
            this.imgshowImage.setVisibility(View.GONE);
            this.dvSignViewMain.setVisibility(View.VISIBLE);
        }
        else
        {
            scrollView.setVisibility(View.VISIBLE);
            ZoneSign.setVisibility(View.GONE);
            linearLayout3.setVisibility(View.VISIBLE);
            this.imgshowImage.setVisibility(View.VISIBLE);
            this.dvSignViewMain.setVisibility(View.GONE);
        }
    }


    private void scan() {

//        //預計是 .50版 的 配達2.0 + 拍照2.0
//        Intent intent = new Intent();
//        intent.setClass(this.getFragmentActivity(),DeliveryBarcodeActivity_Special.class);
//        Bundle bundle = new Bundle();
//        bundle.putString("SCREEN_TYPE","LANDSCAPE");
//        intent.putExtras(bundle);   // 記得put進去，不然資料不會帶過去哦
//        this.startActivityForResult(intent,9);

        this.startActivityForResult(new Intent((Context) this.getFragmentActivity(), DeliveryBarcodeActivity.class), 5);
    }

    private void getPhoto() {
        Bundle bundle = new Bundle();
        Intent intent = new Intent();
        intent.setClass((Context) this.getActivity(), CameraActivity.class);
        bundle.putString("second", "不隱藏");
        bundle.putString("number", this.editGoodNum.getText().toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, this.FULL_CAMERA);
    }

    private void getPhoto2() {
        Bundle bundle = new Bundle();
        Intent intent = new Intent();
        intent.setClass((Context) this.getActivity(), _v2CameraActivity.class);
        bundle.putString("second", "不隱藏");
        bundle.putString("number", this.editGoodNum.getText().toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, this.FULL_CAMERA);
    }

    //是否有代收貨款
    private void onCheckResult(String cate, String money) {
        String msg = "";
        int hash = cate.hashCode();
        int check;
        if (hash == 0x63F) {
            check = cate.equals("21") ? 0 : -1;
        } else if (hash == 1603) {
            check = cate.equals("25") ? 1 : -1;
        } else if (hash == 0x67D && (cate.equals("41"))) {
            check = 2;
        } else {
            check = -1;
        }

        if (check == 0) {
            msg = "到付貨款,收款金額:" + money;
        } else if (check == 1) {
            msg = "到付追加貨款,收款金額" + money;
        } else if (check == 2) {
            msg = "代收貨款,收款金額" + money;
        } else {
            msg = "無須收款";
        }
        editMoney.setText(msg);

        if ( msg.equals("無須收款") == false  )
        {
            Constants.showPromptDialog(this.getFragmentActivity(), "提示", msg, new PromptDialog.ButtonListener() {
                @Override
                public void onClick(View param1View) {

                }
            });
        }

    }

    //是否有回單
    private void onCheckResult_receipt(String receipt_flag)
    {
        String msg = "";
        if ( receipt_flag.equals("1"))
        {
            msg = "注意:本件有回單";
        }

        if ( msg.equals("") == false)
        {
            Constants.showPromptDialog(this.getFragmentActivity(), "提示", msg, new PromptDialog.ButtonListener() {
                @Override
                public void onClick(View param1View) {

                }
            });
        }
    }

    //不論是手動輸入檢查  或是 掃BarCode
    //都應該要檢查
    protected void requestCheckNumber(String checkNum) {
        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("driver_code", AccountManager.instance().getDriverCode(ReverseFragment.this.getFragmentActivity()));
        params.put("check_number", checkNum);
        params.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment2.this.getFragmentActivity()));
        RequestManager.instance().doRequest(Constants.API_POST_CHECK_SCANCODE_2, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {
                //Log.e(TAG, "STATUS : " + jsonObject.toString());
                //Log.e(TAG, "resultcode : " + jsonObject.get("resultcode").getAsBoolean());
                //Log.e(TAG, "error_msg : " +jsonObject.get("error_msg").getAsString());

                if (jsonObject.get("resultcode").getAsBoolean() == true) {

                    Log.e(TAG, "check number : " + jsonObject.toString());

                    txtBarcode.setText("");
                    editGoodNum.setText("");

                    if ( jsonObject.has("resultdesc") )
                    {
                        if ( jsonObject.get("resultdesc").getAsString().equals("") )
                        {
                            isCheckNumberOk = true ;

                            txtBarcode.setText(checkNum);
                            editGoodNum.setText("");

                            //正常處理
                            request_id = jsonObject.get("request_id").getAsString();
                            editGoodPiecesNum.setText(jsonObject.get("pieces").getAsString());

                            //回單
                            onCheckResult_receipt(jsonObject.get("receipt_flag").getAsString());

                            //代收貨款
                            onCheckResult(jsonObject.get("subpoena_category").getAsString(), jsonObject.get("money").getAsString());

                            genCode();
                        }

                    }

                    if ( jsonObject.has("error_msg") )
                    {
                        if (jsonObject.get("error_msg").getAsString().equals("查無此託運單號"))
                        {
                            Constants.showPromptDialog(getFragmentActivity(), "提示", "查無此貨號或辨識失敗,請重新處理 - " + checkNum , new PromptDialog.ButtonListener() {
                                @Override
                                public void onClick(View param1View) {
                                    strDigiSignType = "1" ;
                                    setShowSign(false);
                                }
                            });
                        }

                    }

                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void initPhotoState() {
        this.llPhoto.setVisibility(View.GONE);
        this.bigBitmap = null;
        this.smallBitmap = null;
        this.bigBitmapUrl = "";
        this.smallBitmapUrl = "";
    }


    private boolean boolLockUpload = false ;
    private void doSave() {

        Log.e("test","1--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );
        String strGoodNum = DeliveryFragment2.this.txtBarcode.getText().toString();

        if ( isCheckNumberOk == true || strGoodNum.length() > 0 )
        {

        }
        else
        {
            Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "請先進行 檢查/驗證 , 確認您的貨號是否正確。", "確定", null, null, null);
            return;
        }

        if (strGoodNum.length() < 10){
            Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "請確認您的貨號是否正確。", "確定", null, null, null);
            return;
        }

        //正常配達或各種缺件破損配達 應該要完成電子簽名
        if ( this.selectSpinnerId.equals("3") || this.selectSpinnerId.equals("7") || this.selectSpinnerId.equals("8") || this.selectSpinnerId.equals("9") )
        {
            if ( isSignOk == false)
            {
                Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "請先進行電子簽名。", "確定", null, null, null);
                return;
            }
        }


        if ( boolLockUpload == true)
        {
            //正在上傳中
            Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "資料正在上傳中 , 請勿重複處理。", "確定", null, null, null);
            return;
        }
        else
        {
            boolLockUpload = true ;
            checkUpload(strGoodNum);
            //boolLockUpload = false ;
            //btnSave.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    //btnSave.setEnabled(true);
                    boolLockUpload = false ;
                    Log.d(TAG,"reset btnSave");

                }
            },2000);// set time as per your requirement
        }

    }


    private void checkUpload(String barcode){

        String userCode = AccountManager.instance().getDriverCode(DeliveryFragment2.this.getFragmentActivity());

        if ( !userCode.toUpperCase().startsWith("Z") && !userCode.toUpperCase().startsWith("P") )
        {
            Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "目前此功能限定全速配。", "確定", null, null, null);
            return;
        }

        try {
            if ( this.selectSpinnerId.equals("3") || this.selectSpinnerId.equals("7") || this.selectSpinnerId.equals("8") || this.selectSpinnerId.equals("9") )
            {
                this.smallBitmapUrl = TakePhotoManager.bitmapToBase64(this.dvSignViewMain.mCacheBitmap);
            }
            else
            {
                //非正常配交不用簽名
            }
        }
        catch(Exception ex)
        {

        }
        Log.e("test","2--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );

        //正常配達或各種缺件破損配達 應該要完成電子簽名
        if ( this.selectSpinnerId.equals("3") || this.selectSpinnerId.equals("7") || this.selectSpinnerId.equals("8") || this.selectSpinnerId.equals("9") )
        {
            //requestUpload3(barcode);
            //requestUpload4(barcode);
            requestUpload3(true , barcode);
        }
        else
        {
            requestTotal++;
            //requestUpload3(barcode);
            requestUpload3(false ,barcode);
        }

        try {
            dvSignViewMain.clear();
        }
        catch(Exception ex)
        {

        }

        //如果有記憶選項
        //執行完成就不改成預設值
        if ( isRememberDeliveryType == true)
        {

        }
        else
        {
            spinnerAbnormalStatus.setSelection(0);
        }

        imgQrCode.setImageBitmap(null);
        /*
        imgBig.setImageBitmap(null);
        imgSmall.setImageBitmap(null);
        smallBitmapUrl = "";
        bigBitmapUrl = "";

        this.dvSignViewMain.clear();
        */
    }


    private void requestUpload4(String barcode){

        Log.e("test","4--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "4");
        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment2.this.getFragmentActivity()));
        data.put("request_id",request_id);
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");
        data.put("receive_option", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", this.selectSpinnerId);
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", editGoodPiecesNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", editPlatesNum.getText().toString());
        //data.put("sign_form_image", this.smallBitmapUrl);
        data.put("sign_field_image", this.smallBitmapUrl);
        data.put("coupon", "");
        data.put("cash", "");
        //data.put("signType", "1");
        data.put("signType", strDigiSignType);
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA_NEW, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("test","5--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    requestTotal++;
                    DeliveryFragment2.this.imgBig.setImageBitmap(null);
                    DeliveryFragment2.this.imgSmall.setImageBitmap(null);
                    DeliveryFragment2.this.imgshowImage.setImageBitmap(null);

                    if (DeliveryFragment2.this.bigBitmap != null){
                        DeliveryFragment2.this.bigBitmap.recycle();
                        DeliveryFragment2.this.bigBitmap = null;
                    }
                    if (DeliveryFragment2.this.smallBitmap != null){
                        DeliveryFragment2.this.smallBitmap.recycle();
                        DeliveryFragment2.this.smallBitmap = null;
                    }
                    if (DeliveryFragment2.this.dvSignViewMain.mCacheBitmap != null){
                        DeliveryFragment2.this.dvSignViewMain.mCacheBitmap.recycle();
                        DeliveryFragment2.this.dvSignViewMain.mCacheBitmap = null;
                    }


                    if (requestTotal == 2){
                        requestTotal = 0;
                        DeliveryFragment2.this.editGoodNum.setText("");

                        //Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "上傳成功", "確定", null, null, null);
                        Constants.toast(DeliveryFragment2.this.getFragmentActivity(), "上傳成功");
                        try {
                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(DeliveryFragment2.this.getFragmentActivity(), notification);
                            r.play();
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }

                        //更新顯示列表
                        txtBarcode.setText("");
                        arrBarcode.add(barcode);
                        updateSuccessBarcodeList();
                    }

                    //Constants.toast(getFragmentActivity(), String.format("[%s]-簽單-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("4", "配達", barcode);

                }else{

                    Constants.toast(getFragmentActivity(), String.format("[%s]-簽單-上傳失敗", barcode));
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                }


                imgBig.setImageBitmap(null);
                imgSmall.setImageBitmap(null);
                smallBitmapUrl = "";
                bigBitmapUrl = "";

                dvSignViewMain.clear();

            }

            @Override
            public void onFail(String reason) {

            }
        });


    }


    private void requestUpload3(boolean isContinueStep4 , String barcode){

        Log.e("test","3--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "3");
        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment2.this.getFragmentActivity()));
        data.put("request_id",request_id);
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", this.selectSpinnerId);
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", editGoodPiecesNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", editPlatesNum.getText().toString());
        data.put("sign_form_image", "");//this.bigBitmapUrl);
        data.put("sign_field_image", "");//this.smallBitmapUrl);
        data.put("coupon", "");
        data.put("cash", "");
        data.put("receive_option", "");
        //data.put("signType", "1");
        data.put("signType", strDigiSignType);
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA_NEW, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){
//                    if (isAuto != true){
//                    }
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg("3" , DeliveryFragment2.this.selectSpinnerId , DeliveryFragment2.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }

                    requestTotal++;

                    if (requestTotal == 2){
                        requestTotal = 0;
                        DeliveryFragment2.this.editGoodNum.setText("");

                        //Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達", "上傳成功", "確定", null, null, null);
                        //Constants.ErrorAlertDialog(DeliveryFragment2.this.getFragmentActivity(), "配達2.0", "上傳成功", "確定", null, null, null);
                        Constants.toast(DeliveryFragment2.this.getFragmentActivity(), "上傳成功");
                        try {
                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(DeliveryFragment2.this.getFragmentActivity(), notification);
                            r.play();
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }


                        //更新顯示列表
                        txtBarcode.setText("");
                        arrBarcode.add(barcode);
                        updateSuccessBarcodeList();
                    }

                    if ( isContinueStep4 == true)
                    {
                        requestUpload4(barcode);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    //Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("4", "配達", barcode);

                }else{

                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    //Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));
                }


            }

            @Override
            public void onFail(String reason) {

            }
        });

        insertData(barcode);
    }


    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    public String strBarcode = "";
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
/*
        Constants.showPromptDialog(getFragmentActivity(), "提示", "request code = " + requestCode + ", result code = " + resultCode, new PromptDialog.ButtonListener() {
            @Override
            public void onClick(View param1View) {

            }
        });
*/
        Log.e(TAG, "request code = " + requestCode + ", result code = " + resultCode);
        if (requestCode == 5 ) {

            if (resultCode == getFragmentActivity().RESULT_OK) {
                try {
                    strBarcode = data.getStringExtra("barcodes");
                    Log.e("Jerry", "11111");
                    DeliveryFragment2.this.editGoodNum.setText(strBarcode);
/*
                    //清空原本的
                    editGoodNum.getText().clear();
                    editGoodNum.setText("");
                    //加上新的
                    //editGoodNum.setText(strBarcode);
                    DeliveryFragment2.this.editGoodNum.setText(strBarcode);
                    Constants.toast(this.getActivity(), String.format("[%s]", strBarcode));
*/
                    //2021-09-14 掃完 BarCode 應該先檢查 不是直接上傳
                    //checkUpload(strBarcode);

                    //2021-10-01 改到文字框裡面執行
                    //2021-09-14 檢查貨號
                    //requestCheckNumber(editGoodNum.getText().toString());

//                    requestUpload(strBarcode);

//                    for (String barcode:this.arrBarcode) {
//                        requestUpload(barcode);
//                    }

                } catch (Exception e) {
                    Log.e("Jerry", "Exception:"+e.toString());
                }

            }
        }

        if (requestCode == 9 ) {

            if (resultCode == getFragmentActivity().RESULT_OK) {
                try {
                    strBarcode = data.getStringExtra("barcodes");
                    Log.e("Jerry", "11111");

                    String str9 = ((Bundle) Objects.<Bundle>requireNonNull(data.getExtras())).getString("img9");

                    String str9_path = data.getExtras().getString("img9");

                    btnSave.setVisibility(View.VISIBLE);
                    imgQrCode.setVisibility(View.GONE);
                    btn_checkdeliverystatus.setVisibility(View.GONE);

                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.scrollTo(0, editGoodNum.getTop());
                        }
                    });

                    strDigiSignType = "3" ;
                    setShowSign(true);

                    DeliveryFragment2.this.editGoodNum.setText(strBarcode);
                    Constants.toast(this.getActivity(), String.format("[%s]", strBarcode));

                    this.imgshowImage.setImageBitmap(TakePhotoManager.getBitMap(str9, (Context) this.getActivity()));
                    this.imgshowImage.setVisibility(View.VISIBLE);
                    this.dvSignViewMain.setVisibility(View.GONE);
                    this.dvSignViewMain.mCacheBitmap = TakePhotoManager.getBitMap(str9, (Context) this.getActivity());

                    TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str9);
                    //TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str9_path);
                    isSignOk = true;

                } catch (Exception e) {
                    Log.e("----------deleteFileByPath--------", "Exception:"+e.toString());
                }

            }
        }

        if (resultCode == 3 && requestCode == this.FULL_CAMERA && data != null) {
            editGoodNum.getText().clear();
            editGoodNum.setText("");
            //這是配達1.0的殘渣
            this.llPhoto.setVisibility(View.VISIBLE);
            String str2 = ((Bundle) Objects.<Bundle>requireNonNull(data.getExtras())).getString("img1");
            String str1 = data.getExtras().getString("img2");

            String str2_path = data.getExtras().getString("img1");
            String str1_path = data.getExtras().getString("img2");
            /*
            byte[] byteArray = data.getByteArrayExtra("image");

            this.bigBitmap = TakePhotoManager.getBitMap(str2, (Context) this.getActivity());
            this.smallBitmap = TakePhotoManager.getBitMap(str1, (Context) this.getActivity());
            TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str2);
            TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str1);
            this.imgBig.setImageBitmap(this.bigBitmap);
            this.imgSmall.setImageBitmap(this.smallBitmap);

            this.imgshowImage.setImageBitmap(this.bigBitmap);
            this.imgshowImage.setVisibility(View.VISIBLE);
            this.dvSignViewMain.setVisibility(View.GONE);

            isSignOk = true;

            this.dvSignViewMain.mCacheBitmap = bigBitmap;
            */


            this.imgshowImage.setImageBitmap(TakePhotoManager.getBitMap(str2, (Context) this.getActivity()));
            this.imgshowImage.setVisibility(View.VISIBLE);
            this.dvSignViewMain.setVisibility(View.GONE);
            this.dvSignViewMain.mCacheBitmap = TakePhotoManager.getBitMap(str2, (Context) this.getActivity());

            Log.e("str2_path----------",str2_path);
            Log.e("str1_path----------",str1_path);

            //TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str2_path);
            //TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str1_path);
            TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str2);
            TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str1);

            isSignOk = true;

            //Constants.toast(getFragmentActivity(),"我在這裡");

        }
    }

    //檢查電子簽名是不是沒有簽
    private boolean checkSignFine()
    {
        Log.e("COLOR","111");
        boolean returnValue = false ;
        if ( this.dvSignViewMain.mCacheBitmap.getWidth() < 10 || this.dvSignViewMain.mCacheBitmap.getHeight() < 10 )
        {
            return false ;
        }
        Log.e("COLOR","222");
        Log.e("COLOR-getWidth",String.valueOf(this.dvSignViewMain.mCacheBitmap.getWidth()));
        Log.e("COLOR-getWidth",String.valueOf(this.dvSignViewMain.mCacheBitmap.getHeight()));
        int[] intArray = new int[this.dvSignViewMain.mCacheBitmap.getWidth()*this.dvSignViewMain.mCacheBitmap.getHeight()];
        this.dvSignViewMain.mCacheBitmap.getPixels(intArray,0,this.dvSignViewMain.mCacheBitmap.getWidth(),
                                                        0,0,
                                                        this.dvSignViewMain.mCacheBitmap.getWidth(),this.dvSignViewMain.mCacheBitmap.getHeight());

        int[][] color = new int[this.dvSignViewMain.mCacheBitmap.getWidth()][ this.dvSignViewMain.mCacheBitmap.getHeight()];

        int iCount = 0 ;
        for (int x = 0; x < this.dvSignViewMain.mCacheBitmap.getWidth(); x++)
        {
            for (int y = 0; y < this.dvSignViewMain.mCacheBitmap.getHeight(); y++)
            {
                if (intArray[x + y * this.dvSignViewMain.mCacheBitmap.getWidth()] == Color.WHITE
                        || intArray[x + y * this.dvSignViewMain.mCacheBitmap.getWidth()] == Color.TRANSPARENT )
                {

                }
                else
                {
                    Log.e("COLOR" + String.valueOf(x) + "_" + String.valueOf(y),String.valueOf(intArray[x + y * this.dvSignViewMain.mCacheBitmap.getWidth()]) );
                    iCount+=1;

                    if ( iCount > 300)
                    {
                        returnValue = true;
                        break;
                    }

                }
            }
        }

        return returnValue;
    }

    //取得配達分區
    private void getDevliveryType(){

        //Constants.toast(getFragmentActivity(), "配達分區-資料取得中,請稍後...");

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("strType", "AO");

        RequestManager.instance().doRequest(Constants.API_POST_GET_DELIVERY_TYPE, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                List<String> returnlist = new ArrayList<String>();

                Log.e("----------------",jsonObject.toString());
                Log.e("----------------",jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    JsonArray objData = jsonObject.getAsJsonArray("choicelist");
                    Log.e("----------------",objData.toString());
                    Log.e("----------------",""+objData.size() );
                    Log.e("----------------",objData.get(0).toString());
                    Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
                    for ( int i = 0 ; i < objData.size() ; i++)
                    {
                        returnlist.add( ((JsonObject)objData.get(i)).get("choice").toString().replace("\"","").replace("\"","") );
                    }

                    if ( returnlist.size() > 1 )
                    {
                        DeliveryFragment2.this.spinnerContentA = returnlist.toArray(new String[0]);
                    }

                }else{


                }

                doSpinnerContentMain();
                //doSpinnerContentMain2();
            }

            @Override
            public void onFail(String reason) {
                doSpinnerContentMain();
                //doSpinnerContentMain2();
            }
        });


    }

//
//
//    public void doSpinnerContentMain2()
//    {
//
//        this.spinnerAbnormalStatusE = (Spinner) view.findViewById(R.id.textView18);
//        ArrayAdapter arrayAdapterE = new ArrayAdapter(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, this.spinnerContentE);
//        this.spinnerAbnormalStatusE.setAdapter((SpinnerAdapter) arrayAdapterE);
//
//        this.spinnerAbnormalStatusE.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
////                imgQrCode.setVisibility(View.GONE);
////
////                deliverySelect = spinnerContentA[position];
////
////                selectSpinnerId = deliverySelect.substring(0, 2);
////                selectSpinnerId = selectSpinnerId.trim();
////
////                Log.e(TAG, "select = " + selectSpinnerId);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//    }
//



    public void doSpinnerContentMain()
    {

        this.spinnerAbnormalStatus = (Spinner) view.findViewById(R.id.spinner_partition);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgQrCode.setVisibility(View.GONE);

                deliverySelect = spinnerContentA[position];

                selectSpinnerId = deliverySelect.substring(0, 2);
                selectSpinnerId = selectSpinnerId.trim();

                Log.e(TAG, "select = " + selectSpinnerId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Constants.toast(getFragmentActivity(), "配達分區-更新完成");

        /*
        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                Constants.toast(getFragmentActivity(), "配達分區-更新完成");
            }
        }, 5000);
        */

    }

    // 將掃讀的資料寫進room
    private void insertData(String checkNumber) {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(DeliveryFragment2.this.getFragmentActivity()).toUpperCase();
            MatchedData matchedData = new MatchedData(UUID.randomUUID().toString(), driverCode, checkNumber, editGoodPiecesNum.getText().toString(), Constants.getTime(), Constants.getNowTimestamp());
            AppDataBase.getInstance(DeliveryFragment2.this.getFragmentActivity()).getMatchedDao().insertData(matchedData);
        }).start();
    }


}

// AES256
//  http://www.jysblog.com/coding/java-aes256-encrypt-decrypt/

