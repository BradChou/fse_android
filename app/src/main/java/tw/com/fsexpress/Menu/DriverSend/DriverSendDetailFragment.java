package tw.com.fsexpress.Menu.DriverSend;

import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.MyCheckNumberAdapter;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.utils.PromptDialog;

public class DriverSendDetailFragment extends GHWFragment {

    private static final String TAG = "DriverSendDetail";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    private DataObject data = null;

    private TextView text_customercode = null,
                        text_send_person = null,
                        text_send_phone = null,
                        text_send_address = null,
                        text_count = null,
                        text_packageType = null,
                        text_vechileType = null,
                        text_pickUpDate = null,
                        text_targetDate = null,
                        text_updateTime = null,
                        text_remark = null;

    private ListView lvData;
    private ListView lvData2;

    private MyCheckNumberAdapter adapter;
    private MyCheckNumberAdapter adapter2;

    private List<String> mData = new ArrayList<>();//所有數據 (未集貨)
    private List<String> mData2 = new ArrayList<>();//所有數據 (已集貨)
    private List<String> mCheckedData = new ArrayList<>();//將選中數據放入裏面
    private SparseBooleanArray stateCheckedMap = new SparseBooleanArray();//用來存放CheckBox的選中狀態，true爲選中,false爲沒有選中


    //異常集貨分區
    private String[] spinnerContentA = new String[]{"7 .已遭取走","18.無貨可取","17.客戶取消退貨","12.地址錯誤",
                                                        "6 .聯絡不上","16.約定再取件","22.公司休息","24.未用momo原包裝",
                                                        "8 .超過規格","9 .拒絕交貨","23.未貼標籤"};
    private Spinner spinnerAbnormalStatus;

    private String deliverySelect = "";
    private String selectSpinnerId = "20";

    private ToneGenerator toneGenerator;


    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("行動派遣明細");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) DriverSendDetailFragment.this.getFragmentActivity()).onBackPressed();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);


        this.text_customercode = (TextView) view.findViewById(R.id.txt_customercode);
        this.text_send_person = (TextView) view.findViewById(R.id.txt_send_person);
        this.text_send_phone = (TextView) view.findViewById(R.id.txt_send_phone);
        this.text_send_address = (TextView) view.findViewById(R.id.txt_send_address);
        this.text_count = (TextView) view.findViewById(R.id.txt_count);
        this.text_packageType = (TextView) view.findViewById(R.id.txt_packageType);
        this.text_vechileType = (TextView) view.findViewById(R.id.txt_vechileType);
        this.text_pickUpDate = (TextView) view.findViewById(R.id.txt_pickUpDate);
        this.text_targetDate = (TextView) view.findViewById(R.id.txt_targetDate);
        this.text_updateTime = (TextView) view.findViewById(R.id.txt_updateTime);
        this.text_remark = (TextView) view.findViewById(R.id.txt_remark);


        this.text_customercode.setText(String.format("%s", this.data.getVariable("customer_name").toString()));
        this.text_send_person.setText(String.format("%s", this.data.getVariable("send_contact").toString()));
        this.text_send_phone.setText(String.format("%s", this.data.getVariable("send_tel").toString()));
        this.text_send_address.setText(String.format("%s", this.data.getVariable("send_cityarea").toString())
                                                    + String.format("%s", this.data.getVariable("send_address").toString()) );
        this.text_count.setText(String.format("%s", this.data.getVariable("counts").toString())
                                                    + "/" + String.format("%s", this.data.getVariable("pieces").toString()) );

        this.text_packageType.setText(String.format("%s", this.data.getVariable("packageType").toString()));
        this.text_vechileType.setText(String.format("%s", this.data.getVariable("vechileType").toString()));
        this.text_pickUpDate.setText(String.format("%s", this.data.getVariable("pickUpDate").toString()));
        this.text_targetDate.setText(String.format("%s", convertTargetDate(this.data.getVariable("target_date").toString())));
        this.text_updateTime.setText(String.format("%s",convertUpdateTime(this.data.getVariable("YYYYMMDDHHMM_max").toString())));
        this.text_remark.setText(String.format("%s", this.data.getVariable("remark").toString()));

//        text_packageType = null,
//                txt_vechileType = null,
//                txt_pickUpDate = null,
//                text_remark = null;


        //顯示清單
        ((TextView) view.findViewById(R.id.more)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ConstraintLayout) view.findViewById(R.id.base01)).setVisibility(View.GONE);
                ((ConstraintLayout) view.findViewById(R.id.base02)).setVisibility(View.VISIBLE);
            }
        });

        //隱藏清單
        ((TextView) view.findViewById(R.id.cut)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ConstraintLayout) view.findViewById(R.id.base01)).setVisibility(View.VISIBLE);
                ((ConstraintLayout) view.findViewById(R.id.base02)).setVisibility(View.GONE);
            }
        });

        //全選
        ((TextView) view.findViewById(R.id.choice_all)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateCheckedMap(true);
            }
        });


        //更新按鈕 更新異常集貨分區
        ((TextView) view.findViewById(R.id.send2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateCollectScanLog();
            }
        });

        UpdateIsRead();


        doSpinnerContentMain();

        //  https://www.twblogs.net/a/5b89b6452b71775d1ce36152

        initView(view);
        initData();
        adapter = new MyCheckNumberAdapter(this.getFragmentActivity(), mData, stateCheckedMap);
        lvData.setAdapter(adapter);
        Log.e("--mData.size()--","" + mData.size());
        if ( mData.size() < 1)
        {
            lvData.setVisibility(View.GONE);
        }
        adapter2 = new MyCheckNumberAdapter(this.getFragmentActivity(), mData2, null , true) ;

        lvData2.setAdapter(adapter2);


        setOnListViewItemLongClickListener();

    }

    //異常集貨分區下拉選單
    public void doSpinnerContentMain()
    {

        this.spinnerAbnormalStatus = (Spinner) view.findViewById(R.id.spinner_partition);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deliverySelect = spinnerContentA[position];

                selectSpinnerId = deliverySelect.substring(0, 2);
                selectSpinnerId = selectSpinnerId.trim();

                Log.e(TAG, "select = " + selectSpinnerId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ((Spinner) view.findViewById(R.id.spinner_collecttype)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( position == 0)
                {
                    DriverSendDetailFragment.this.lvData.setVisibility(View.VISIBLE);
                    DriverSendDetailFragment.this.lvData2.setVisibility(View.GONE);

                    (((TextView) DriverSendDetailFragment.this.view.findViewById(R.id.choice_all))).setVisibility(View.VISIBLE);
                    (((TextView) DriverSendDetailFragment.this.view.findViewById(R.id.send2))).setVisibility(View.VISIBLE);

                }
                else
                {
                    DriverSendDetailFragment.this.lvData.setVisibility(View.GONE);
                    DriverSendDetailFragment.this.lvData2.setVisibility(View.VISIBLE);

                    (((TextView) DriverSendDetailFragment.this.view.findViewById(R.id.choice_all))).setVisibility(View.GONE);
                    (((TextView) DriverSendDetailFragment.this.view.findViewById(R.id.send2))).setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Constants.toast(getFragmentActivity(), "集貨分區-更新完成");
    }

    public void setData(DataObject d) {
        this.data = d;
    }

    protected void UpdateIsRead() {

        Log.d("test", "start------------------");

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("read_user", AccountManager.instance().getDriverCode(DriverSendDetailFragment.this.getFragmentActivity()));
        params.put("driver_code",  this.data.getVariable("driver_code").toString());
        params.put("customer_code",  this.data.getVariable("customer_code").toString());
        params.put("YYYYMMDDHHMM",  this.data.getVariable("YYYYMMDDHHMM").toString());
        params.put("YYYYMMDDHHMM_max",  this.data.getVariable("YYYYMMDDHHMM_max").toString());
        params.put("send_cityarea",  this.data.getVariable("send_cityarea").toString());
        params.put("send_address",  this.data.getVariable("send_address").toString());
        params.put("send_contact",  this.data.getVariable("send_contact").toString());
        //params.put("send_tel",  this.data.getVariable("send_tel").toString());
        params.put("send_station",  this.data.getVariable("send_station").toString());
        params.put("driver_code",  this.data.getVariable("driver_code").toString());
        params.put("id_list",  this.data.getVariable("id_list").toString() + "," + this.data.getVariable("id_list2").toString()  );

        Log.e("---id_list---" ,  this.data.getVariable("id_list").toString() ) ;

        RequestManager.instance().doRequest(Constants.API_GET_Driver_Send_Prepare_UpdateIsRead, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {


            }

            @Override
            public void onFail(String reason) {
                //dialog.cancel();
            }
        });
    }

    private void setOnListViewItemLongClickListener() {
//        lvData.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                adapter.setShowCheckBox(true);//CheckBox的那個方框顯示
//                updateCheckBoxStatus(view, position);
//                return true;
//            }
//        });

        lvData.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        adapter.setShowCheckBox(true);//CheckBox的那個方框顯示
                        updateCheckBoxStatus(view, position);
                        //return true;
                    }
                }
        );
    }

    private void updateCheckBoxStatus(View view, int position) {
        MyCheckNumberAdapter.ViewHolder holder = (MyCheckNumberAdapter.ViewHolder) view.getTag();
        holder.checkBox.toggle();//反轉CheckBox的選中狀態
        lvData.setItemChecked(position, holder.checkBox.isChecked());//長按ListView時選中按的那一項

        stateCheckedMap.put(position, holder.checkBox.isChecked());//存放CheckBox的選中狀態
        if (holder.checkBox.isChecked()) {
            mCheckedData.add(mData.get(position));//CheckBox選中時，把這一項的數據加到選中數據列表
        } else {
            mCheckedData.remove(mData.get(position));//CheckBox未選中時，把這一項的數據從選中數據列表移除
        }
        Constants.toast(this.getFragmentActivity(),mData.get(position));
        adapter.notifyDataSetChanged();
    }



    private void initView(ViewGroup view) {
        lvData = (ListView) view.findViewById(R.id.lv);
        lvData.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        lvData2 = (ListView) view.findViewById(R.id.lv2);

    }

    private void initData() {

        //清空資料
        mData = new ArrayList<>() ;
        mData2 = new ArrayList<>() ;
        stateCheckedMap.clear();

        //id_list
        String[] strArr = this.data.getVariable("id_list").toString().split(",");
        Log.e("strArr",strArr.toString());

        for (int i = 0; i < strArr.length; i++) {
            if ( (strArr[i].length() > 1) )
            {
                mData.add(strArr[i]);
            }

        }

        String[] strArr2= this.data.getVariable("id_list2").toString().split(",");
        Log.e("strArr2",strArr2.toString());

        for (int i = 0; i < strArr2.length; i++) {
            mData2.add(strArr2[i]);
        }

        setStateCheckedMap(false);
    }
    /**
     * 設置所有CheckBox的選中狀態
     * */
    private void setStateCheckedMap(boolean isSelectedAll) {
        for (int i = 0; i < mData.size(); i++) {
            stateCheckedMap.put(i, isSelectedAll);
            lvData.setItemChecked(i, isSelectedAll);
        }
    }

    private boolean UpdateNow = false ;

    private void UpdateCollectScanLog()
    {
//        //        lvData.getCheckedItemIds()
//        long[] idArr = lvData.getCheckedItemIds() ;
//
//        if ( idArr.length > 0)
//        {
//            for( int i = 0 ; i < idArr.length ; i++)
//            {
//                Constants.toast(this.getFragmentActivity(),mData.get(position));
//            }
//        }

        if ( UpdateNow == true)
        {
            Constants.toast(DriverSendDetailFragment.this.getFragmentActivity(),"正在更新中請稍後 , 請勿連續進行更新 "  );
            return ;
        }

        UpdateNow = true;

        NumberFormat formatter = new DecimalFormat("##0.0");
        int tempCount = 0 ;

        Log.e("---mData.size()---",""+ mData.size());
        Log.e("---stateCheckedMap.size()---",""+stateCheckedMap.size());

        for ( int i = 0 ; i < stateCheckedMap.size() ; i++)
        {
            int t = i ;
            int iGap = 250 ;
            int tCount = tempCount;

//            Log.e("----------i--------", ""+(i) );
//            Log.e("----------i % 10--------", ""+(i % 10) );
            if ( t == 0 )
            {
                Constants.toast(DriverSendDetailFragment.this.getFragmentActivity(),"開始處理更新 , 請勿退出APP或離開此畫面 "  );
            }

            if ( stateCheckedMap.get(t) == true )
            {
                tempCount+=1;
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        //requestUpload(mData.get(t),false);

                        if ( t % 12 == 1 || tCount % 12 == 1)
                        {
                            Constants.toast(DriverSendDetailFragment.this.getFragmentActivity(),"正在處理 , 請勿退出APP或離開此畫面 , 進度 (" + formatter.format(100.0 * t / stateCheckedMap.size()) + "%) "  );
                        }

                        if ( t == stateCheckedMap.size()-1)
                        {
                            requestUpload(mData.get(t),true);
                        }
                        else
                        {
                            requestUpload(mData.get(t),false);
                        }
                    }
                }, tempCount*iGap);
            }

        }

        if ( tempCount > 0 )
        {
            Handler handler2 = new Handler(Looper.getMainLooper());
            handler2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    Constants.showPromptDialog(DriverSendDetailFragment.this.getFragmentActivity(),"提示","更新處理完成",new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                    UpdateNow = false;
                }
            }, tempCount*250);
        }
        else
        {
            Handler handler2 = new Handler(Looper.getMainLooper());
            handler2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    Constants.showPromptDialog(DriverSendDetailFragment.this.getFragmentActivity(),"提示","您沒有選擇任何貨號",new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                    UpdateNow = false;
                }
            }, 250);
        }




    }




    private void requestUpload(String barcode,boolean btnReNew){

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "5");
        data.put("driver_code", AccountManager.instance().getDriverCode(DriverSendDetailFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("receive_option", this.selectSpinnerId);
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", "0");
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(DriverSendDetailFragment.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion
                    //Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                }else {
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));

                    Log.e("---上傳失敗---", String.format("[%s]-上傳失敗", barcode));
                }
                if ( btnReNew == true)
                {
                    UpdateNow = false;
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });


    }

    private String convertTargetDate(String date) {
        try {
            if (date == null || date.isEmpty()) return "";
            else return date.substring(4,6) + "-" + date.substring(6,8);
        } catch (Exception e) {
            return date;
        }
    }

    private String convertUpdateTime(String updateTime) {
        try {
            switch (updateTime.length()) {
                case 8:
                    return updateTime.substring(4,6) + "-" + updateTime.substring(6,8);
                case 12:
                    return updateTime.substring(4,6) + "-" + updateTime.substring(6,8) + " "
                            + updateTime.substring(8,10) + ":" + updateTime.substring(10,12) ;
                default: return "";
            }
        } catch (Exception e) {
            return updateTime;
        }
    }


}
