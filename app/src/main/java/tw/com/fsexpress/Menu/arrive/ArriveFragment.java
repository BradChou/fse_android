package tw.com.fsexpress.Menu.arrive;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.CustomFragment;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.WareHouseManager;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArriveFragment extends CustomFragment {

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private List<DataObject> arrWare = null;
    private TextView txtChoose = null;
    private TextView txtGoodNum, txtTotalNum, txtGoodNumFail, txtBarcode , txtSDMD_value;
    private EditText editGoodNum = null;
    private String currentSelectCode = "";
//    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.誤訂", "3.多出"};
    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.短少", "3.多出", "4.喪失", "5.誤訂"};
    private EditText editGoodPiecesNum;
    private EditText editPlatesNum;
    private Spinner spinnerAbnormalStatus;
    private String abnormalStatus = "";
    private Button btnLeft, btnRight;
    private ScrollView scrollView;
    private ToneGenerator toneGenerator;
    private boolean isAuto = false;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);

        this.arrWare = WareHouseManager.instance().getWareHouse();
        this.txtChoose = (TextView)view.findViewById(R.id.txt_choose_unloading);
        this.editGoodPiecesNum = (EditText)view.findViewById(R.id.edit_good_pieces_num);
        this.editGoodPiecesNum.clearFocus();
        this.editPlatesNum = (EditText)view.findViewById(R.id.edit_plates_num);
        this.editPlatesNum.clearFocus();

        this.txtSDMD_value = (TextView)view.findViewById(R.id.SDMD_value);

        if (this.editGoodNum == null){
            this.editGoodNum = (EditText)view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄
                        isAuto = true;
                        ArriveFragment.this.editGoodNum.setText("");
                        requestUpload(s.toString().trim());


                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入
                        isAuto = false;
                    }


                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }else{
            this.editGoodNum.requestFocus();
        }


        this.txtTotalNum = (TextView)view.findViewById(R.id.txt_total_num);
//        this.txtAccumulationNum = (TextView)view.findViewById(R.id.txt_accumulation_num);

        this.scrollView = (ScrollView)view.findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollView.smoothScrollTo(0,0);
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);
        this.txtGoodNum = (TextView)view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);


        TextView txtTitle = (TextView)view.findViewById(R.id.txt_title);
        txtTitle.setText("到著");

        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) ArriveFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(ArriveFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "到著", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });

        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);


        this.spinnerAbnormalStatus = (Spinner)view.findViewById(R.id.spinner_abnormal_status);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, abnormalArray);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter)arrayAdapter);
        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String strStatus = abnormalArray[position];

                abnormalStatus = strStatus.substring(0, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.txt_choose_unloading).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> arrTitle = new ArrayList<String>();
                for (DataObject d: arrWare) {
                    String title = String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString());
                    arrTitle.add(title);
                }
                Constants.AlertWithMultipleSelect(ArriveFragment.this.getFragmentActivity(), "卸貨站選擇：", arrTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.e("Jerry", "which:"+which);
                        DataObject d = arrWare.get(which);
                        ArriveFragment.this.currentSelectCode = d.getVariable("code").toString();
                        txtChoose.setText(String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString()));

                    }
                }, null).show();
            }
        });

        view.findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArriveFragment.this.getFragmentActivity().startActivityForResult(new Intent((Context)ArriveFragment.this.getFragmentActivity(), ArriveBarcodeActivity.class), 5);
            }
        });

        view.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!isAuto){
                    String strGoodNum = ArriveFragment.this.editGoodNum.getText().toString();
                    if (strGoodNum.equals("")){
                        Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "請確認您的貨號是否正確。", "確定", null, null, null);
                        return;
                    }else{

                        if (strGoodNum.length() < 10){
                            Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "請確認您的貨號是否正確。", "確定", null, null, null);
                            return;
                        }
                    }
                    requestUpload(strGoodNum);
                }else{
                    Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "現在為自動上傳模式，如要手動上傳請\"點擊貨號輸入框並輸入條碼\"。", "確定", null, null, null);
                }

            }
        });

    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){

        /*
        2022-02-16 v.43

        移除到著站所

         */
        /*
        if (ArriveFragment.this.currentSelectCode.equals("")) {
            Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "請選擇站所碼。", "確定", null, null, null);
            return;
        }
       */
        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "1");
        data.put("driver_code", AccountManager.instance().getDriverCode(ArriveFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("receive_option", "");
        data.put("area_arrive_code", "");//ArriveFragment.this.currentSelectCode);
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", abnormalStatus.replace("0",""));
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", this.editGoodPiecesNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", this.editPlatesNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(ArriveFragment.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    if (isAuto != true){
                        ArriveFragment.this.editGoodNum.setText("");
                        Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "上傳成功", "確定", null, null, null);
                    }

                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("1", "到著", barcode);

                    getArriveSDMD(barcode);
                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });


    }

    private void getArriveSDMD(String barcode){

        Log.e("getArriveSDMD",barcode);

        txtSDMD_value.setText("--");

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("check_number", barcode);


        RequestManager.instance().doRequest(Constants.API_POST_GET_ARRIVE_SDMD, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("jsonObject", jsonObject.toString());

                String sdmd_type = "";
                String sdmd_code = "";

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){
                    if ( jsonObject.has("resultdesc") && jsonObject.has("sdmd_type")  && jsonObject.has("sdmd_code") )
                    {
                        sdmd_type = jsonObject.get("sdmd_type").getAsString() ;
                        sdmd_code = jsonObject.get("sdmd_code").getAsString() ;
                        Log.e("sdmd_type", sdmd_type.toString());
                        Log.e("sdmd_code", sdmd_code.toString());

                        if ( sdmd_code != "" && sdmd_code != null && !sdmd_code.equals(""))
                        {
                            txtSDMD_value.setText(sdmd_code);
                        }
                    }

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-SDMD取得失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });




    }

//    private void requestMultipleUpload(){
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//        String strScanTime = Constants.getTime();
//
//        int idx = 0;
//        for (String barcode:this.arrBarcode) {
//
//            HashMap<String, String> data = new HashMap<String, String>();
//            data.put("scan_item", "1");
//            data.put("driver_code", AccountManager.instance().getDriverCode(ArriveFragment.this.getFragmentActivity()));
//            data.put("check_number", barcode);
//            data.put("scan_date", strScanTime);
//            data.put("area_arrive_code", ArriveFragment.this.currentSelectCode);
//            data.put("platform", "");
//            data.put("car_number", "");
//            data.put("sowage_rate", "");
//            data.put("area", "");
//            data.put("exception_option", abnormalStatus);
//            data.put("arrive_option", "");
//            data.put("ship_mode", "");
//            data.put("goods_type", "");
//            data.put("pieces", this.editGoodPiecesNum.getText().toString());
//            data.put("weight", "");
//            data.put("runs", "");
//            data.put("plates", this.editPlatesNum.getText().toString());
//            data.put("sign_form_image", "");
//            data.put("sign_field_image", "");
//            data.put("coupon", "");
//            data.put("cash", "");
//
//            DataObject requestData = new DataObject();
//            requestData.setVariable("param", data);
//            requestData.setVariable("barcode", barcode);
//            requestData.setVariable("url", Constants.API_POST_SEND_UPLOAD_DATA);
//            requestData.setVariable("method", "POST");
//            requestData.setVariable("id", String.format("%s%d",Constants.md5(data.toString()),idx));
//            RequestMutilController.instance().addRequest(requestData);
//            idx++;
//        }
//
//        RequestMutilController.instance().setCallback(new RequestMutilController.RequestMutilControllerCallback() {
//
//
//            @Override
//            public void onFinish(List<DataObject> data) {
//
//                dialog.cancel();
//                RequestResultListDialog.instance(ArriveFragment.this.getFragmentActivity()).showUploadDialog(data, "到著");
////                Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//
//                for (String barcode:arrBarcode) {
//                    TransferDataManager.instance().insertTransferData("1","到著", barcode);
//                }
//
//
//
//                arrBarcode.clear();
//                updateBarcodeList();
//
//            }
//
//        });
//
//
//        RequestMutilController.instance().startRequest();
//    }

//    private void requestUpload(){
//
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//
//        HashMap<String, String> data = new HashMap<String, String>();
//        data.put("scan_item", "1");
//        data.put("driver_code", AccountManager.instance().getDriverCode(ArriveFragment.this.getFragmentActivity()));
//        data.put("check_number", this.editGoodNum.getText().toString());
//        data.put("scan_date", "");
//        data.put("area_arrive_code", ArriveFragment.this.currentSelectCode);
//        data.put("platform", "");
//        data.put("car_number", "");
//        data.put("sowage_rate", "");
//        data.put("area", "");
//        data.put("exception_option", abnormalStatus);
//        data.put("arrive_option", "");
//        data.put("ship_mode", "");
//        data.put("goods_type", "");
//        data.put("pieces", this.editGoodPiecesNum.getText().toString());
//        data.put("weight", "");
//        data.put("runs", "");
//        data.put("plates", this.editPlatesNum.getText().toString());
//        data.put("sign_form_image", "");
//        data.put("sign_field_image", "");
//        data.put("coupon", "");
//        data.put("cash", "");
//
//        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
//            @Override
//            public void onResponse(JsonObject jsonObject) {
//
//                dialog.cancel();
//
//                Log.e("Jerry", jsonObject.toString());
//                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){
//                    Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "上傳成功", "確定", null, null, null);
//                }else{
//                    Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "到著", "上傳失敗", "確定", null, null, null);
//                }
//
//
//            }
//
//            @Override
//            public void onFail(String reason) {
//
//                dialog.cancel();
//
//            }
//        });
//
//
//    }
}
