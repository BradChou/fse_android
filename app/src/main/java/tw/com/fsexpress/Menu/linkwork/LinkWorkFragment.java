package tw.com.fsexpress.Menu.linkwork;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.gson.JsonObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.Menu.camera.TakePhotoManager;
import tw.com.fsexpress.Menu.collection.CollectionFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment2;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;
import tw.com.fsexpress.utils.PhotoDialog;
import tw.com.fsexpress.utils.PromptDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class LinkWorkFragment  extends GHWFragment {

    private static final String TAG = "LinkWork";

    private Button btnLeft = null,btnSave = null ;

    private String[] spinnerContentA = new String[]{"01.綁定","02.解除綁定"};
    private Spinner spinnerAbnormalStatus;

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();

    private EditText editGoodNum = null, editPlatesNum;
    private ScrollView scrollView, scrollViewFail;
    private TextView txtGoodNum, txtGoodNumFail, txtTotalNum, txtBarcode;

    private String linkSelect = "";
    private String selectSpinnerId = "01";

    private ToneGenerator toneGenerator;

    @Override
    protected void viewDidAppear(ViewGroup view) {

        super.viewDidAppear(view);

        TextView txtTitle = (TextView)view.findViewById(R.id.txt_title);
        txtTitle.setText("貨號綁定");

        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) LinkWorkFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(LinkWorkFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "貨號綁定", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });

        //this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);
        this.txtGoodNum = (TextView) view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);

        this.btnSave = (Button) view.findViewById(R.id.btn_save);
        this.btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                doSave();
            }
        });

        if (this.spinnerAbnormalStatus == null){
            this.spinnerAbnormalStatus = (Spinner) view.findViewById(R.id.spinner_partition);
            ArrayAdapter arrayAdapter = new ArrayAdapter(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
            this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

            this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    linkSelect = spinnerContentA[position];

                    selectSpinnerId = linkSelect.substring(0, 2);
                    selectSpinnerId = selectSpinnerId.trim();

                    Log.e(TAG, "select = " + selectSpinnerId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if (this.editGoodNum == null){
            this.editGoodNum = (EditText)view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄

                        //LinkWorkFragment.this.editGoodNum.setText("");
                        //requestUpload(s.toString().trim());
                        //LinkWorkFragment.this.editGoodNum.setText(LinkWorkFragment.this.editGoodNum.getText().toString().replace(" ", "").trim());
                        txtBarcode.requestFocus();

                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入

                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


        if (this.txtBarcode == null){
            this.txtBarcode = (EditText)view.findViewById(R.id.textBarcode);
            this.txtBarcode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Log.e(TAG, "STEP_0");
                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄

                        //LinkWorkFragment.this.editGoodNum.setText("");
                        //requestUpload(s.toString().trim());
                        //txtBarcode.requestFocus();
                        //LinkWorkFragment.this.txtBarcode.setText(LinkWorkFragment.this.txtBarcode.getText().toString().replace(" ", "").trim());
                        requestUpload(LinkWorkFragment.this.editGoodNum.getText().toString().replace(" ", "").trim(),
                                LinkWorkFragment.this.txtBarcode.getText().toString().replace(" ", "").trim());
                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入

                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


    }



    private void requestUpload(String barcode , String postcode){

        Log.e(TAG, "STEP_1.1 barcode- " + barcode);
        Log.e(TAG, "STEP_1.2 postcode- " + postcode);
        Log.e(TAG, "STEP_1.3 DriverCode- " + AccountManager.instance().getDriverCode(LinkWorkFragment.this.getFragmentActivity()));
        Log.e(TAG, "STEP_1.4 Type- " + this.selectSpinnerId);
        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("DriverCode", AccountManager.instance().getDriverCode(LinkWorkFragment.this.getFragmentActivity()));
        data.put("CheckNumber", barcode);
        data.put("PostId", postcode);
        data.put("Type", this.selectSpinnerId);


        RequestManager.instance().doRequest(Constants.API_POST_DO_LINK_WORK, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e(TAG, "STEP_2");

                String strDesc = jsonObject.get("resultdesc").getAsString();
                Log.e(TAG, "---"+jsonObject.toString());
                Log.e(TAG, "---"+strDesc.toString());
                Log.e(TAG, "---strDesc.toString().equals(\"成功\") "+ strDesc.toString().equals("成功") );
                Log.e(TAG, "---(strDesc.toString() == \"成功\")  "+ (strDesc.toString() == "成功") );
                if (strDesc.toString().equals("成功")){
                    Log.e(TAG, "STEP_3");
                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理

                    if ( LinkWorkFragment.this.selectSpinnerId.equals("01"))
                    {
                        Constants.ErrorAlertDialog(LinkWorkFragment.this.getFragmentActivity(), "貨號綁定", "01.綁定 執行成功", "確定", null, null, null);
                    }
                    else if ( LinkWorkFragment.this.selectSpinnerId.equals("02"))
                    {
                        Constants.ErrorAlertDialog(LinkWorkFragment.this.getFragmentActivity(), "貨號綁定", "02.解除綁定 執行成功", "確定", null, null, null);
                    }

                    txtBarcode.setText("");
                    editGoodNum.setText("");
                    editGoodNum.requestFocus();

                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));

                }else{
                    Log.e(TAG, "STEP_4");
                    /*
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    */
                    if ( strDesc.equals("無權限") )
                    {
                        strDesc = "帳號無權限 , 請用指定帳號";
                    }
                    Constants.ErrorAlertDialog(LinkWorkFragment.this.getFragmentActivity(), "貨號綁定", "失敗 : " + strDesc, "確定", null, null, null);

                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(LinkWorkFragment.this.getFragmentActivity(), notification);
                        r.play();
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                    txtBarcode.setText("");
                    editGoodNum.setText("");
                    editGoodNum.requestFocus();

                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));

                    LinkWorkFragment.this.editGoodNum.requestFocus();
                }


            }

            @Override
            public void onFail(String reason) {

            }
        });


    }


    private void updateSuccessBarcodeList(){
        String strGoods = "";

        Log.e(TAG, "---this.arrBarcode.size() "+this.arrBarcode.size());

        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            Log.e(TAG, "---str "+str);
            strGoods = strGoods + "\n" + str;
        }

        Log.e(TAG, "---strGoods  "+strGoods);
        Log.e(TAG, "---!strGoods.equals(\"\")  "+!strGoods.equals(""));
        Log.e(TAG, "---strGoods2  "+strGoods.substring(1));
        if (!strGoods.equals("")){
            //this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            //this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }

        Log.e(TAG, "---this.txtGoodNum.getText()  "+ this.txtGoodNum.getText());
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }



    private void doSave() {
        requestUpload(LinkWorkFragment.this.editGoodNum.getText().toString().replace(" ", "").trim(),
                LinkWorkFragment.this.txtBarcode.getText().toString().replace(" ", "").trim());
    }





}
