package tw.com.fsexpress.Menu.Overview;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

// 傳輸
public class OverviewFragment extends GHWFragment {

    private int BARCODE_BACK = 5;

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    private Button btnQuery = null, btnIsUpload = null, btnIsNotUpload = null;
    private List<DataObject> arr_data = new ArrayList<DataObject>();
    private Dialog dialog = null, dialogSearch = null;
    private EditText editGood = null, editGoodInvisible= null;

    TextView type_one_num_1, type_one_num_2, type_two_num_1, type_two_num_2,
            type_three_num_1, type_three_num_2,type_four_num_1, type_four_num_2,
            type_five_num_1, type_five_num_2, type_six_num_1, type_six_num_2,
            type_seven_num_1, type_seven_num_2;
    List<Integer> textViewIds = Arrays.asList(R.id.type_one_num_1, R.id.type_one_num_2, R.id.type_two_num_1, R.id.type_two_num_2,
            R.id.type_three_num_1, R.id.type_three_num_2,R.id.type_four_num_1, R.id.type_four_num_2,
            R.id.type_five_num_1, R.id.type_five_num_2, R.id.type_six_num_1, R.id.type_six_num_2,
            R.id.type_seven_num_1, R.id.type_seven_num_2);

    // total value
    DataObject totalData = new DataObject();

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("傳輸");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) OverviewFragment.this.getFragmentActivity()).popFragment();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.btnQuery = (Button) view.findViewById(R.id.btn_query);
        this.btnQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchDialog();
            }
        });
        this.btnIsUpload = (Button) view.findViewById(R.id.btn_isUpload);
        this.btnIsUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestGetUploadData(null);
            }
        });
        this.btnIsNotUpload = (Button) view.findViewById(R.id.btn_isNotUpload);
        this.btnIsNotUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestGetNotUploadData();
            }
        });


//        List<TextView> textViews = Arrays.asList(this.type_one_num_1, this.type_one_num_2, this.type_two_num_1, this.type_two_num_2,
//                this.type_three_num_1, this.type_three_num_2, this.type_four_num_1, this.type_four_num_2,
//                this.type_five_num_1, this.type_five_num_2, this.type_six_num_1, this.type_six_num_2,
//                this.type_seven_num_1, this.type_seven_num_2);
//        int i = 0;
//
//        for (TextView t : textViews) {
//            t = (TextView) view.findViewById(this.textViewIds.get(i));
//            this.type_one_num_1 = (TextView) view.findViewById(this.textViewIds.get(i));
//
//            i++;
//        }

        this.type_one_num_1 = (TextView) view.findViewById(R.id.type_one_num_1);
        this.type_one_num_2 = (TextView) view.findViewById(R.id.type_one_num_2);
        this.type_two_num_1 = (TextView) view.findViewById(R.id.type_two_num_1);
        this.type_two_num_2 = (TextView) view.findViewById(R.id.type_two_num_2);
        this.type_three_num_1 = (TextView) view.findViewById(R.id.type_three_num_1);
        this.type_three_num_2 = (TextView) view.findViewById(R.id.type_three_num_2);
        this.type_four_num_1 = (TextView) view.findViewById(R.id.type_four_num_1);
        this.type_four_num_2 = (TextView) view.findViewById(R.id.type_four_num_2);
        this.type_five_num_1 = (TextView) view.findViewById(R.id.type_five_num_1);
        this.type_five_num_2 = (TextView) view.findViewById(R.id.type_five_num_2);
        this.type_six_num_1 = (TextView) view.findViewById(R.id.type_six_num_1);
        this.type_six_num_2 = (TextView) view.findViewById(R.id.type_six_num_2);
        this.type_seven_num_1 = (TextView) view.findViewById(R.id.type_seven_num_1);
        this.type_seven_num_2 = (TextView) view.findViewById(R.id.type_seven_num_2);

        this.arr_data.clear();
        this.totalData = new DataObject();
//        this.requestOverview();
    }

    private void requestOverview() {
        List<String> types = Arrays.asList("1", "2", "4", "4-1", "5", "6", "7");
        for(String t : types) {
            List<DataObject> arr = TransferDataManager.instance().getTransferDataByType(t);

            int totalCounts = 0;
            for(DataObject o : arr) {
                totalCounts += Integer.parseInt(o.getVariable("transfer_num").toString());
            }

            switch (t) {
                case "1":
                    // 到著
                    this.type_one_num_1.setText(String.format("%s", arr.size()));
                    this.type_one_num_2.setText(String.format("%s", totalCounts));
                    break;
                case "2":
                    // 配送
                    this.type_two_num_1.setText(String.format("%s", arr.size()));
                    this.type_two_num_2.setText(String.format("%s",totalCounts));
                    break;
                case "4":
                    // 配達
                    this.type_three_num_1.setText(String.format("%s",arr.size()));
                    this.type_three_num_2.setText(String.format("%s",totalCounts));
                    break;
                case "4-1":
                    // 簽單
                    this.type_four_num_1.setText(String.format("%s",arr.size()));
                    this.type_four_num_2.setText(String.format("%s",totalCounts));
                    break;
                case "5":
                    // 集貨
                    this.type_five_num_1.setText(String.format("%s",arr.size()));
                    this.type_five_num_2.setText(String.format("%s",totalCounts));
                    break;
                case "6":
                    // 卸集
                    this.type_six_num_1.setText(String.format("%s",arr.size()));
                    this.type_six_num_2.setText(String.format("%s",totalCounts));
                    break;
                case "7":
                    // 發送
                    this.type_seven_num_1.setText(String.format("%s",arr.size()));
                    this.type_seven_num_2.setText(String.format("%s",totalCounts));
                    break;
                default:
                    break;
            }
        }
    }

    private void requestGetUploadData(String barcode) {

        this.arr_data.clear();
        AlertDialog dialog = Constants.ProgressBar(OverviewFragment.this.getFragmentActivity(), "讀取中...請稍候");
        dialog.show();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("driver_code", AccountManager.instance().getDriverCode(OverviewFragment.this.getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_GET_UPLOADDATA, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                dialog.cancel();
//                Log.e("JERRY", jsonObject.getAsString());
                if (jsonObject.get("resultcode").getAsBoolean()) {
                    List<String> keys = Arrays.asList("checknumber", "scanname");
                    for (JsonElement o : jsonObject.getAsJsonArray("uploaddatas")) {

                        DataObject d = new DataObject();
                        for (String key : keys) {

                            String value = !o.getAsJsonObject().get(key).isJsonNull() ? o.getAsJsonObject().get(key).getAsString() : "";
                            d.setVariable(key, value);
                        }
                        arr_data.add(d);
                    }
                    Log.e("SHAWN", "arr_data count = " + arr_data.size());
                    if(arr_data.size() == 0) {
                        Toast.makeText(OverviewFragment.this.getFragmentActivity(), "無已上傳資料!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        showUploadDialog();
                    }
                }
                else{
                    Toast.makeText(OverviewFragment.this.getFragmentActivity(), jsonObject.get("resultdesc").getAsString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail(String reason) {
                dialog.cancel();
            }
        });

    }

    private void requestGetNotUploadData() {
        Toast.makeText(OverviewFragment.this.getFragmentActivity(), "無未上傳資料!", Toast.LENGTH_SHORT).show();
    }

    private void showUploadDialog() {

        // dialog
        this.dialog = new Dialog(OverviewFragment.this.getFragmentActivity());
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.dialog.setContentView(R.layout.dialog_overview);

        TextView title = (TextView) this.dialog.findViewById(R.id.txt_title);
        title.setText("查詢結果");

        LinearLayout linear = (LinearLayout) this.dialog.findViewById(R.id.linearLayout);
        float factor = linear.getContext().getResources().getDisplayMetrics().density;

        for (int i = 0; i < this.arr_data.size(); i++) {

            DataObject d = this.arr_data.get(i);

            TextView text = new TextView(OverviewFragment.this.getFragmentActivity());
            text.setText(String.format("%s %s 已上傳", d.getVariable("checknumber").toString(), d.getVariable("scanname").toString()));
            text.setTextColor(this.getResources().getColor(R.color.black));
            text.setGravity(Gravity.CENTER);
            text.setTextSize(20);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    (int)(50 * factor)
            );
            linear.addView(text, params);
        }

        this.dialog.show();
    }

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case 1:
                    //處理少量資訊或UI


                    OverviewFragment.this.editGoodInvisible.setText("");
                    OverviewFragment.this.editGood.setText("");
                    String str = msg.obj.toString();
                    OverviewFragment.this.editGood.setText(str.trim());



                    break;
            }
        }
    };

    private void showSearchDialog() {

        // dialog
        this.dialogSearch = new Dialog(OverviewFragment.this.getFragmentActivity());
        this.dialogSearch.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialogSearch.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.dialogSearch.setContentView(R.layout.dialog_search);

        this.editGood = (EditText) this.dialogSearch.findViewById(R.id.edt_good_num);

        if (this.editGoodInvisible == null){
            this.editGoodInvisible = (EditText) this.dialogSearch.findViewById(R.id.edt_good_num_invisible);
            this.editGoodInvisible.requestFocus();
            this.editGoodInvisible.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) {

                        Message msg = new Message();
                        msg.what = 1;
                        msg.obj = s.toString();
                        mHandler.sendMessage(msg);
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }



        Button btnScan = (Button) this.dialogSearch.findViewById(R.id.btn_dialog_scan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(OverviewFragment.this.getFragmentActivity(), OverviewBarcodeActivity.class), BARCODE_BACK);
            }
        });
        Button btnQuery = (Button) this.dialogSearch.findViewById(R.id.btn_query);
        btnQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editGood.getText().toString().isEmpty()) {
                    Toast.makeText(OverviewFragment.this.getFragmentActivity(), "尚未輸入條碼", Toast.LENGTH_SHORT).show();
                }
                else {
                    doSearch(editGood.getText().toString());
                }
            }
        });
        Button btnCancel = (Button) this.dialogSearch.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearch.dismiss();
            }
        });

        this.dialogSearch.show();
    }

    private void doSearch(String code) {

        this.arr_data.clear();
        List<DataObject> arrTrans =  TransferDataManager.instance().getTransferDataByCode(code);
        if (arrTrans.size() != 0){
            for (DataObject d:arrTrans) {
                DataObject data = new DataObject();
                data.setVariable("checknumber", d.getVariable("transfer_code").toString());
                data.setVariable("scanname", d.getVariable("transfer_name").toString());
                this.arr_data.add(data);
            }

            this.showUploadDialog();
        }else{
            Toast.makeText(OverviewFragment.this.getFragmentActivity(), "查無此條碼", Toast.LENGTH_SHORT).show();
        }




    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BARCODE_BACK){
            if (resultCode == getFragmentActivity().RESULT_OK){

                try {
                    String strBarcode = data.getStringExtra("barcode");
//                    editGood.setText(strBarcode);
                    OverviewFragment.this.editGood.setText(strBarcode);

                }catch (Exception e){

                }
            }
        }
    }
}