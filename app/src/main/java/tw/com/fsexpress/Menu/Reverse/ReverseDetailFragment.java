package tw.com.fsexpress.Menu.Reverse;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class ReverseDetailFragment extends GHWFragment {

    // TAG
    private static final String TAG = "SHAWN";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    private DataObject data = null;
    private TextView text1 = null, text2 = null, text3 = null, text4 = null,text5 = null, text6 = null, text7_title = null, text7 = null;
    private Button btnTake = null, btnUpload = null;
    private ImageView imgShow = null;

    private List<DataObject> arr_data = new ArrayList<DataObject>();
    private Dialog statusDialog = null;

    private String[] not_take_reason = new String[] { "另約時間", "電聯不上", "資料有誤", "無件可取", "超大超重", "同業已取", "商品未到", "不退", "公司休息"};
    private int selected_not_take_reason = 0;
    private int selected_scan_item = 0;

    private String imageBase64 = "";

    private DatePickerDialog dateDialog;

    private boolean firstShow = false;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("逆物流");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) ReverseDetailFragment.this.getFragmentActivity()).onBackPressed();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.text1 = (TextView) view.findViewById(R.id.txt_good_num);
        this.text2 = (TextView) view.findViewById(R.id.txt_send_person);
        this.text3 = (TextView) view.findViewById(R.id.txt_send_phone);
        this.text4 = (TextView) view.findViewById(R.id.txt_send_address);
        this.text5 = (TextView) view.findViewById(R.id.txt_remark);
        this.text6 = (TextView) view.findViewById(R.id.txt_back_status);
        this.text7_title = (TextView) view.findViewById(R.id.txt_retake_time);
        this.text7_title.setVisibility(View.GONE);
        this.text7 = (TextView) view.findViewById(R.id.txt_retake_time_select);
        this.text7.setVisibility(View.GONE);

        this.imgShow = (ImageView) view.findViewById(R.id.show_img);

        this.btnTake = (Button) view.findViewById(R.id.btn_take_photo);
        this.btnTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lowIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (lowIntent.resolveActivity(ReverseDetailFragment.this.getFragmentActivity().getPackageManager()) == null) return;
                startActivityForResult(lowIntent, 999);
            }
        });
        this.btnUpload = (Button) view.findViewById(R.id.btn_upload);
        this.btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestUpload();
            }
        });

        this.text1.setText(String.format("%s", this.data.getVariable("check_number").toString()));
        this.text2.setText(String.format("%s", this.data.getVariable("send_contact").toString()));
        this.text3.setText(String.format("%s", this.data.getVariable("send_tel").toString()));
        this.text4.setText(String.format("%s", this.data.getVariable("send_address").toString()));
        this.text5.setText(String.format("%s", this.data.getVariable("memo").toString()));

        if (!this.firstShow) {
            this.requestReturnCode();
            this.firstShow = true;
        }

    }

    public void setData(DataObject d) {
        this.data = d;
    }

    protected void showTakeDialog() {

        // dialog
        this.statusDialog = new Dialog(ReverseDetailFragment.this.getFragmentActivity());
        this.statusDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.statusDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.statusDialog.setContentView(R.layout.dialog_choose_list);

        LinearLayout linear = (LinearLayout) this.statusDialog.findViewById(R.id.linearLayout);
        float factor = linear.getContext().getResources().getDisplayMetrics().density;

        for (int i = 0; i < this.arr_data.size(); i++) {

            DataObject d = this.arr_data.get(i);

            TextView textTaken = new TextView(ReverseDetailFragment.this.getFragmentActivity());
            textTaken.setText(d.getVariable("code_name").toString());
            textTaken.setTextColor(this.getResources().getColor(R.color.black));
            textTaken.setGravity(Gravity.CENTER);
            textTaken.setTag(i);
            textTaken.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    statusDialog.dismiss();

                    if((int)v.getTag() == 1) {
                        selected_scan_item = 2;
                        showNotTakeReasonDialog();
                    }
                    else {
                        selected_scan_item = 1;
                        text6.setText("已取");
                    }
                }
            });
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    (int)(50 * factor)
            );
            linear.addView(textTaken, params);

            if (i != this.arr_data.size() - 1) {

                View line = new View(ReverseDetailFragment.this.getFragmentActivity());
                line.setBackgroundResource(R.color.lightGray);
                params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        1
                );
                linear.addView(line, params);
            }
        }

        this.statusDialog.show();
    }

    protected void showNotTakeReasonDialog() {

        // dialog
        this.statusDialog = new Dialog(ReverseDetailFragment.this.getFragmentActivity());
        this.statusDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.statusDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.statusDialog.setContentView(R.layout.dialog_choose_list);

        LinearLayout linear = (LinearLayout) this.statusDialog.findViewById(R.id.linearLayout);
        float factor = linear.getContext().getResources().getDisplayMetrics().density;

        for (int i = 0; i < this.not_take_reason.length; i++) {

            TextView textTaken = new TextView(ReverseDetailFragment.this.getFragmentActivity());
            textTaken.setText(this.not_take_reason[i]);
            textTaken.setTextColor(this.getResources().getColor(R.color.black));
            textTaken.setGravity(Gravity.CENTER);
            textTaken.setTag(i);
            textTaken.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    text6.setText(not_take_reason[(int)v.getTag()]);
                    selected_not_take_reason = (int)v.getTag() +1;
                    if(selected_not_take_reason == 1) {
                        text7_title.setVisibility(View.VISIBLE);
                        text7.setVisibility(View.VISIBLE);
                        showDatePicker();
                    }
                    statusDialog.dismiss();
                }
            });
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    (int)(50 * factor)
            );
            linear.addView(textTaken, params);

            if (i != this.not_take_reason.length - 1) {

                View line = new View(ReverseDetailFragment.this.getFragmentActivity());
                line.setBackgroundResource(R.color.lightGray);
                params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        1
                );
                linear.addView(line, params);
            }
        }

        this.statusDialog.show();
    }

    protected void requestReturnCode() {

        HashMap<String, String> params = new HashMap<String, String>();

        RequestManager.instance().doRequest(Constants.API_POST_GET_RETURN_CODE, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    List<String> keys = Arrays.asList("code_id", "code_name", "picture", "date");
                    for (JsonElement o : jsonObject.getAsJsonArray("codes")) {

                        DataObject d = new DataObject();
                        for (String key : keys) {

                            String value = !o.getAsJsonObject().get(key).isJsonNull() ? o.getAsJsonObject().get(key).getAsString() : "";
                            d.setVariable(key, value);
                        }
                        arr_data.add(d);
                    }
                    Log.e(TAG, "arr_data count = " + arr_data.size());

                    showTakeDialog();
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void requestUpload(){

        AlertDialog dialog = Constants.ProgressBar(ReverseDetailFragment.this.getFragmentActivity(), "資料上傳中...請稍候");
        dialog.show();

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", String.format("%d", selected_scan_item));
        data.put("driver_code", AccountManager.instance().getDriverCode(ReverseDetailFragment.this.getFragmentActivity()));
        data.put("check_number", this.data.getVariable("check_number").toString());
        data.put("scan_date", Constants.getTime());
        data.put("sign_form_image", imageBase64);
        data.put("sign_field_image", imageBase64);
        data.put("next_pickup_time", text7.getText().toString());
        data.put("re_code", String.format("%d", selected_not_take_reason));


        RequestManager.instance().doRequest(Constants.API_POST_SEND_PICKUP_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                dialog.cancel();

                if (jsonObject.get("resultcode").getAsBoolean()) {
                    Constants.ErrorAlertDialog(ReverseDetailFragment.this.getFragmentActivity(), "逆物流", "上傳成功", "確定", null, null, null);
                }else{
                    Constants.ErrorAlertDialog(ReverseDetailFragment.this.getFragmentActivity(), "逆物流", "上傳失敗", "確定", null, null, null);
                }
            }

            @Override
            public void onFail(String reason) {
                dialog.cancel();
            }
        });
    }

    private void showDatePicker(){

        final Calendar cal = Calendar.getInstance();

        this.dateDialog = new DatePickerDialog(ReverseDetailFragment.this.getFragmentActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                text7.setText(String.format("%4d-%02d-%02d", year, (monthOfYear+1), dayOfMonth));
            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        this.dateDialog.getDatePicker().setMinDate(new Date().getTime());

        this.dateDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 999 && resultCode == -1) {

            Bundle getImage = data.getExtras();
            Bitmap getLowImage = (Bitmap) getImage.get("data");

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            getLowImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            imageBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);

            Glide.with(ReverseDetailFragment.this.getFragmentActivity())
                    .load(byteArray)
                    .asBitmap()
                    .into(imgShow);

        }else{
            Toast.makeText(ReverseDetailFragment.this.getFragmentActivity(), "未作任何拍攝", Toast.LENGTH_SHORT).show();
        }
    }


}
