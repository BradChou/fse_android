package tw.com.fsexpress.Menu.transfer;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.ui.GHWIndexPath;
import tw.com.fsexpress.framework.ui.GHWListView;

import java.util.ArrayList;
import java.util.List;

public class TransferListDialog extends Dialog {
    private TextView title;

    private GHWListView listView = null;

    private TransferListDialog.ListViewHolder holder;

    static class ListViewHolder
    {
        TextView textCheckNum;
    }

    private List<DataObject> arr_data = new ArrayList<DataObject>();


    public TransferListDialog(Context context) {
        super(context);
    }

    public TransferListDialog(Context context, int arg1) {
        super(context, arg1);
        setContentView(R.layout.dialog_trans_list);
        this.title = (TextView) findViewById(R.id.txt_title);

        this.listView = (GHWListView) findViewById(R.id.recyclerView);

        ((TextView) findViewById(R.id.btn_confirm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferListDialog.this.dismiss();
            }
        });

        this.setListView();

        this.listView.reloadData();
    }

    public static TransferListDialog getDialog(Context context) {
        return new TransferListDialog(context, R.style.DialogStyle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public TransferListDialog setName(String name) {
        this.title.setText(name);

        return this;
    }

    public TransferListDialog setData(List<DataObject> arr) {
        this.arr_data = arr;
        return this;
    }

    private void setListView() {
        this.listView.delegate = new GHWListView.Delegate()
        {
            @Override
            public void didSelectRowAtIndexPath(View view, GHWIndexPath indexPath)
            {
//                ReverseDetailFragment fg = (ReverseDetailFragment)new ReverseDetailFragment().initWithLayoutResourceId(R.layout.layout_reverse_detail);
//                DataObject d = arr_data.get(indexPath.row);
//                fg.setData(d);
//                ((GHWFragmentActivity) ReverseFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        };

        this.listView.dataSource = new GHWListView.DataSource()
        {
            @Override
            public int numberOfSectionsInListView(GHWListView.GHWBaseAdapter arg0)
            {
                return 0;
            }

            @Override
            public int numberOfRowsInSection(GHWListView.GHWBaseAdapter arg0, int arg1)
            {
                return arr_data.size();
            }

            @Override
            public View getSectionView(GHWListView arg0, View arg1, int arg2)
            {
                return null;
            }

            @Override
            public View getItemView(GHWListView parent, View cell, GHWIndexPath indexPath)
            {
                if (cell == null)
                {
                    cell = TransferListDialog.this.getLayoutInflater().inflate(R.layout.cell_string, null);

                    holder = new TransferListDialog.ListViewHolder();
                    holder.textCheckNum = (TextView) cell.findViewById(R.id.text_string);
                    cell.setTag(holder);

                } else
                {
                    holder = (TransferListDialog.ListViewHolder) cell.getTag();
                }

                DataObject d = arr_data.get(indexPath.row);
                holder.textCheckNum.setText(d.getVariable("check_number").toString());

                return cell;
            }
        };
    }
}
