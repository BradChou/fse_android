package tw.com.fsexpress.Menu.RfidCage;

import android.content.Intent;
import android.location.Location;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.provider.SyncStateContract;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.GPSLocation;
import tw.com.fsexpress.MainActivity;
import tw.com.fsexpress.Menu.Appeal.SpecialAreaFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment2;
import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;
import tw.com.fsexpress.utils.PromptDialog;


import com.cipherlab.rfid.AllGen2Settings;
import com.cipherlab.rfid.AllQValue;
import com.cipherlab.rfid.AllRFLink;
import com.cipherlab.rfid.AuthenticateIncRepLen;
import com.cipherlab.rfid.AuthenticateSenRep;
/*import com.cipherlab.rfid.AuthenticateIncRepLen;
import com.cipherlab.rfid.AuthenticateSenRep;*/
import com.cipherlab.rfid.BeepType;
import com.cipherlab.rfid.ClResult;
import com.cipherlab.rfid.ContinuousInventoryTime;
//import com.cipherlab.rfid.ContinuousInventoryTime;
import com.cipherlab.rfid.DeviceEvent;
import com.cipherlab.rfid.DeviceInfo;
import com.cipherlab.rfid.DeviceResponse;
import com.cipherlab.rfid.DeviceVoltageInfo;
import com.cipherlab.rfid.EPCEncodingScheme;
import com.cipherlab.rfid.Enable_State;
//import com.cipherlab.rfid.FWUpdateErrorCode;
import com.cipherlab.rfid.Gen2Settings;
import com.cipherlab.rfid.GeneralString;
import com.cipherlab.rfid.InventoryStatusSettings;
import com.cipherlab.rfid.InventoryType;
import com.cipherlab.rfid.JapanChannel;
//import com.cipherlab.rfid.JapanChannel;
import com.cipherlab.rfid.LockTarget;
import com.cipherlab.rfid.ModuleTemperature;
import com.cipherlab.rfid.NotificationParams;
import com.cipherlab.rfid.PowerMode;
//import com.cipherlab.rfid.PowerMode;
import com.cipherlab.rfid.QValue;
import com.cipherlab.rfid.RFIDMemoryBank;
import com.cipherlab.rfid.RFIDMode;
import com.cipherlab.rfid.RFLink;
import com.cipherlab.rfid.RfidEpcFilter;
import com.cipherlab.rfid.RfidOutputConfiguration;
//import com.cipherlab.rfid.RfidOutputConfiguration;
import com.cipherlab.rfid.SLFlagSettings;
import com.cipherlab.rfid.ScanMode;
import com.cipherlab.rfid.SessionSettings;
import com.cipherlab.rfid.SwitchMode;
import com.cipherlab.rfid.TriggerSwitchMode;
import com.cipherlab.rfid.UntraceableRange;
import com.cipherlab.rfid.UntraceableTID;
import com.cipherlab.rfid.UntraceableU;
import com.cipherlab.rfid.UntraceableUser;
/*import com.cipherlab.rfid.UntraceableRange;
import com.cipherlab.rfid.UntraceableTID;
import com.cipherlab.rfid.UntraceableU;
import com.cipherlab.rfid.UntraceableUser;*/
import com.cipherlab.rfid.WorkMode;
import com.cipherlab.rfidapi.RfidManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;





//implements BarcodeCallback
public class CageTestFragment extends GHWFragment  implements BarcodeCallback {

    private static final String TAG = "CageTest_RFID_sample";

    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;


    private ToneGenerator toneGenerator;

    RfidManager mRfidManager = null;


    //RFID功能選擇
    private Spinner spinnerCageTypeStatus;
    private String strCageType = "";


    //站所選擇
    private Spinner spinnerStationStatus;
    private String[] spinnerStationList;
    private String[] spinnerStationIdList;
    private String strStationType = "";


    private ArrayList<String> RfidList = new ArrayList<String>();
    private ArrayList<String> Rfid_okList = new ArrayList<String>();

    private Button btnSave = null;


    private TextView txtRFID = null,txtDel = null;

    private TextView tvTotalCount = null ;

    private DecoratedBarcodeView barcodeView;

    @Override
    protected void viewDidAppear(ViewGroup view)
    {
        super.viewDidAppear(view);

        if (toneGenerator == null)
        {
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("籠車管理");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) CageTestFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(CageTestFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "籠車管理", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);


        this.tvTotalCount = (TextView) view.findViewById(R.id.tvTotalCount);

        doGPS();

        mRfidManager = RfidManager.InitInstance(this.getFragmentActivity());



        IntentFilter filter = new IntentFilter();
        filter.addAction(GeneralString.Intent_RFIDSERVICE_CONNECTED);
        filter.addAction(GeneralString.Intent_RFIDSERVICE_TAG_DATA);
        filter.addAction(GeneralString.Intent_RFIDSERVICE_EVENT);
        filter.addAction(GeneralString.Intent_FWUpdate_ErrorMessage);
        filter.addAction(GeneralString.Intent_FWUpdate_Percent);
        filter.addAction(GeneralString.Intent_FWUpdate_Finish);
        filter.addAction(GeneralString.Intent_GUN_Attached);
        filter.addAction(GeneralString.Intent_GUN_Unattached);
        filter.addAction(GeneralString.Intent_GUN_Power);


        /*
        filter.addAction(GeneralString.EXTRA_DATA_TYPE);
        filter.addAction(GeneralString.EXTRA_DATA1);
        filter.addAction(GeneralString.EXTRA_DATA2);
        filter.addAction(GeneralString.EXTRA_ReadData);
        */

        CageTestFragment.this.getFragmentActivity().registerReceiver(myDataReceiver, filter);


        doCageTypeMain();
        getStationType();



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            ((TextView) view.findViewById(R.id.tvTitleTotalCount)).setText(Html.fromHtml(getResources().getString(R.string.txt_total_count), Html.FROM_HTML_MODE_COMPACT));
        } else
        {
            ((TextView) view.findViewById(R.id.tvTitleTotalCount)).setText(Html.fromHtml(getResources().getString(R.string.txt_total_count)));
        }

        this.txtRFID = (TextView) view.findViewById(R.id.tvDeleteRfid);



        this.txtDel = (TextView) view.findViewById(R.id.txt_check);
        this.txtDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                    txtRFID.setText(EPC);

                    RfidList.add(EPC);

                    ((TextView) view.findViewById(R.id.rfid_process)).append(EPC + "\n");
                 */

                if ( CageTestFragment.this.txtRFID.getText().toString().indexOf("請掃描RFID標記") >= 0 )
                {
                    Constants.toast(CageTestFragment.this.getFragmentActivity(),"請先掃描RFID標記");
                    return ;
                }

                String strDelRfid = CageTestFragment.this.txtRFID.getText().toString();

                for ( int i = 0 ; i < RfidList.size() ; i++)
                {
                    if ( RfidList.indexOf( strDelRfid) >= 0)
                    {
                        RfidList.remove(strDelRfid);
                    }
                }

                ((TextView) view.findViewById(R.id.rfid_process)).setText("");

                for ( int i = 0 ; i < RfidList.size() ; i++)
                {
                    ((TextView) view.findViewById(R.id.rfid_process)).append(RfidList.get(i) + "\n");
                }

                CageTestFragment.this.txtRFID.setText("請掃描RFID標記");


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">" + RfidList.size() + "</font>) / (" + Rfid_okList.size() + ")", Html.FROM_HTML_MODE_COMPACT));
                } else
                {
                    CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">" + RfidList.size() + "</font>) / (" + Rfid_okList.size() + ")"));
                }

            }
        });


        this.btnSave = (Button) view.findViewById(R.id.btn_save);
        this.btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                doSave();
            }
        });


        Constants.toast(this.getFragmentActivity(),"請注意：需要RFID設備才能使用此功能" );


        this.barcodeView = (DecoratedBarcodeView) this.getFragmentActivity().findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);


//        int time = mRfidManager.GetDevicePowerSavingState();
//        if(time==-1)
//        {
//            String m = mRfidManager.GetLastError();
//            Log.e(TAG, "GetLastError = " + m);
//        }
//
//        Log.w(TAG, "GetDevicePowerSavingState = " + time );




    }



    @Override
    public void barcodeResult(BarcodeResult result) {

        Log.e("1111111" , "1111111");
        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    private void addResultToList(String paramString) {

        String EPC = paramString.toUpperCase();

        if ( strCageType.equals("6") )
        {
            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            txtRFID.setText(EPC);
        }
        else
        {
            if ( RfidList.indexOf(EPC) < 0 )
            {

                toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                toneGenerator.stopTone();

                if ( EPC.indexOf("F") >= 0 && EPC.length() <= 10)
                {
                    RfidList.add(EPC);
                    ((TextView) view.findViewById(R.id.rfid_process)).append(EPC + "\n");
                }
                else
                {
                    RfidList.add(EPC);
                    ((TextView) view.findViewById(R.id.rfid_process)).append(EPC + "\n");
                    ////Constants.toast( CageTestFragment.this.getFragmentActivity(),EPC + "非全速配標記");
                    //Constants.ErrorAlertDialog(CageTestFragment.this.getFragmentActivity(), "籠車管理", EPC + "可能非全速配標記", "確定", null, null, null);

                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">" + RfidList.size() + "</font>) / (" + Rfid_okList.size() + ")", Html.FROM_HTML_MODE_COMPACT));
                } else
                {
                    CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">" + RfidList.size() + "</font>) / (" + Rfid_okList.size() + ")"));
                }

            }
            else
            {
                toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                toneGenerator.stopTone();
                //Constants.toast( CageTestFragment.this.getFragmentActivity(),EPC + "標記重複");
            }
        }
    }



    public void ForTest()
    {
        try {
            DeviceInfo info = mRfidManager.GetDeviceInfo();
            String m = mRfidManager.GetLastError();
            Log.e(TAG, "GetLastError = " + m);
            Log.w(TAG, "deviceInfo.SerialNumber = " + info.SerialNumber );
            Log.w(TAG, "deviceInfo.Region = " + info.Region );
            Log.w(TAG, "deviceInfo.KernelVersion = " + info.KernelVersion );
            Log.w(TAG, "deviceInfo.UserVersion = " + info.UserVersion );
            Log.w(TAG, "deviceInfo.RFIDModuleVersion = " + info.RFIDModuleVersion );
        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception = " + e.getMessage());
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        strCageType = "0";
        this.spinnerCageTypeStatus.setSelection(0);

        try {
            CageTestFragment.this.getFragmentActivity().unregisterReceiver(myDataReceiver);
            mRfidManager.Release();
        }
        catch(Exception ex)
        {

        }

        this.barcodeView.pause();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        try {
            CageTestFragment.this.getFragmentActivity().unregisterReceiver(myDataReceiver);
            mRfidManager.Release();
        }
        catch(Exception ex)
        {

        }

    }

    /*

    https://www.programminghunter.com/article/8994725961/

     */



    private final BroadcastReceiver myDataReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Log.e("-----" , "-----");
            Log.e("intent.getAction()" , intent.getAction().toString());

            if (intent.getAction().equals(GeneralString.Intent_RFIDSERVICE_CONNECTED))
            {
                String PackageName = intent.getStringExtra("PackageName");

                // / make sure this AP does already connect with RFID service (after call RfidManager.InitInstance(this)
                String ver = "";
                ver = mRfidManager.GetServiceVersion();
                String api_ver = mRfidManager.GetAPIVersion();
                ////////tv1.setText(PackageName + "," + ver + " , " + api_ver);
                ForTest();

                String m_PackageName = PackageName;
                //Toast.makeText(CageTestFragment.this.getFragmentActivity(),  m_PackageName, Toast.LENGTH_SHORT).show();
                //Toast.makeText(MainActivity.this,  "Intent_RFIDSERVICE_CONNECTED", Toast.LENGTH_SHORT).show();
            }
            else if(intent.getAction().equals(GeneralString.Intent_RFIDSERVICE_TAG_DATA))
            {
                /*
                 * type : 0=Normal scan (Press Trigger Key to receive the data) ; 1=Inventory EPC ; 2=Inventory ECP TID ; 3=Reader tag ; 5=Write tag ; 6=Lock tag ; 7=Kill tag ; 8=Authenticate tag ; 9=Untraceable tag
                 * response : 0=RESPONSE_OPERATION_SUCCESS ; 1=RESPONSE_OPERATION_FINISH ; 2=RESPONSE_OPERATION_TIMEOUT_FAIL ; 6=RESPONSE_PASSWORD_FAIL ; 7=RESPONSE_OPERATION_FAIL ;251=DEVICE_BUSY
                 * */

                int type = intent.getIntExtra(GeneralString.EXTRA_DATA_TYPE, -1);
                int response = intent.getIntExtra(GeneralString.EXTRA_RESPONSE, -1);
                double data_rssi = intent.getDoubleExtra(GeneralString.EXTRA_DATA_RSSI, 0);

                String PC = intent.getStringExtra(GeneralString.EXTRA_PC);
                String EPC = intent.getStringExtra(GeneralString.EXTRA_EPC);
                String TID = intent.getStringExtra(GeneralString.EXTRA_TID);
                String ReadData = intent.getStringExtra(GeneralString.EXTRA_ReadData);
                int EPC_length = intent.getIntExtra(GeneralString.EXTRA_EPC_LENGTH, 0);
                int TID_length = intent.getIntExtra(GeneralString.EXTRA_TID_LENGTH, 0);
                int ReadData_length = intent.getIntExtra(GeneralString.EXTRA_ReadData_LENGTH, 0);

                EPC = EPC.toUpperCase();

                if ( strCageType.equals("6") )
                {
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();

                    txtRFID.setText(EPC);
                }
                else
                {
                    if ( RfidList.indexOf(EPC) < 0 )
                    {
                        toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        toneGenerator.stopTone();

                        if ( EPC.indexOf("F") >= 0 && EPC.length() <= 10)
                        {
                            RfidList.add(EPC);
                            ((TextView) view.findViewById(R.id.rfid_process)).append(EPC + "\n");
                        }
                        else
                        {
                            RfidList.add(EPC);
                            ((TextView) view.findViewById(R.id.rfid_process)).append(EPC + "\n");
                            ////Constants.toast( CageTestFragment.this.getFragmentActivity(),EPC + "非全速配標記");
                            //Constants.ErrorAlertDialog(CageTestFragment.this.getFragmentActivity(), "籠車管理", EPC + "可能非全速配標記", "確定", null, null, null);

                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        {
                            CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">" + RfidList.size() + "</font>) / (" + Rfid_okList.size() + ")", Html.FROM_HTML_MODE_COMPACT));
                        } else
                        {
                            CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">" + RfidList.size() + "</font>) / (" + Rfid_okList.size() + ")"));
                        }

                    }
                    else
                    {
                        //Constants.toast( CageTestFragment.this.getFragmentActivity(),EPC + "標記重複");
                    }
                }


                ////////String Data = tv1.getText() + "\r " +  "response = " + response + " , EPC = " + EPC + "\r TID = " + TID + "\r ";

                ////////tv1.setText(Data);
                Log.w(TAG, "++++ [Intent_RFIDSERVICE_TAG_DATA] ++++");
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] type=" + type + ", response=" + response + ", data_rssi="+data_rssi   );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] PC=" + PC );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] EPC=" + EPC );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] EPC_length=" + EPC_length );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] TID=" + TID );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] TID_length=" + TID_length );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] ReadData=" + ReadData );
                Log.d(TAG, "[Intent_RFIDSERVICE_TAG_DATA] ReadData_length=" + ReadData_length );

                // If type=8 ; Authenticate response data in ReadData
				/*if(type==GeneralString.TYPE_AUTHENTICATE_TAG && response==GeneralString.RESPONSE_OPERATION_SUCCESS)
				{
					Log.i(TAG, "Authenticate response data=" + ReadData );
				}*/

            }
            //Intent_RFIDSERVICE_EVENT
            else if(intent.getAction().equals(GeneralString.Intent_RFIDSERVICE_EVENT))
            {
                int event  = intent.getIntExtra(GeneralString.EXTRA_EVENT_MASK, -1);
                Log.d(TAG, "[Intent_RFIDSERVICE_EVENT] DeviceEvent=" + event );
                if(event == DeviceEvent.LowBattery.getValue())
                {
                    Log.i(GeneralString.TAG, "LowBattery " );
                }
                else if(event == DeviceEvent.PowerSavingMode.getValue() )
                {
                    Log.i(GeneralString.TAG, "PowerSavingMode " );
                }
                else if(event == DeviceEvent.OverTemperature.getValue())
                {
                    Log.i(GeneralString.TAG, "OverTemperature " );

                }
                else if(event == DeviceEvent.ScannerFailure.getValue())
                {
                    Log.i(GeneralString.TAG, "ScannerFailure " );
                }

            }
            else if(intent.getAction().equals(GeneralString.Intent_FWUpdate_ErrorMessage))
            {
				/*String mse = "";
				mse = intent.getStringExtra(GeneralString.FWUpdate_ErrorMessage);
				int errorcode = intent.getIntExtra(GeneralString.FWUpdate_ErrorCode,-1);
				if(mse!=null)
				{
					Log.d(TAG,  "FWUpdate Error : " + mse  + "(" + errorcode+")");
					Toast.makeText(MainActivity.this,  mse, Toast.LENGTH_SHORT).show();

					if(errorcode==FWUpdateErrorCode.SameVersion.getValue())
					{
						Log.d(TAG,  "SameVersion");
					}
				}
				Log.d(TAG,  "Intent_FWUpdate_ErrorMessage" );*/
            }
            else if(intent.getAction().equals(GeneralString.Intent_FWUpdate_Percent))
            {
                int i = intent.getIntExtra(GeneralString.FWUpdate_Percent,0);
                if(i>=0)
                {
                    ////////tv1.setText( Integer.toString(i));
                }
                Log.d(TAG,  "Intent_FWUpdate_Percent" );
            }
            else if(intent.getAction().equals(GeneralString.Intent_FWUpdate_Finish))
            {
                Log.d(TAG,  "Intent_FWUpdate_Finish" );
                Toast.makeText(CageTestFragment.this.getFragmentActivity(),  "Intent_FWUpdate_Finish", Toast.LENGTH_SHORT).show();
            }
            else if(intent.getAction().equals(GeneralString.Intent_GUN_Attached))
            {
                Log.d(TAG,  "Intent_GUN_Attached" );
                Toast.makeText(CageTestFragment.this.getFragmentActivity(),  "Intent_GUN_Attached", Toast.LENGTH_SHORT).show();
            }
            else if(intent.getAction().equals(GeneralString.Intent_GUN_Unattached))
            {
                Log.d(TAG,  "Intent_GUN_Unattached" );
                Toast.makeText(CageTestFragment.this.getFragmentActivity(),  "Intent_GUN_Unattached", Toast.LENGTH_SHORT).show();
            }
            else if(intent.getAction().equals(GeneralString.Intent_GUN_Power))
            {
                Log.d(TAG,  "Intent_GUN_Power" );
                boolean AC = intent.getBooleanExtra(GeneralString.Data_GUN_ACPower, false);
                boolean Connect = intent.getBooleanExtra(GeneralString.Data_GUN_Connect, false);
            }

        }
    };




    private void doCageTypeMain()
    {
        strCageType = "0";
        Log.e("strCageType1",strCageType);
        this.spinnerCageTypeStatus = (Spinner) view.findViewById(R.id.tvCageType);
        this.spinnerCageTypeStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                btnSave.setVisibility(View.VISIBLE);
                txtRFID.setVisibility(View.GONE);
                txtDel.setVisibility(View.GONE);
                strCageType = "" + position ;
                Log.e("strCageType2",strCageType);
                if ( strCageType.equals("0") ) //請選擇
                {
                    //Constants.toast( CageTestFragment.this.getFragmentActivity(), "您還沒選擇");
                }
                else if ( strCageType.equals("1") ) //龍車 - 離站
                {
                    //Constants.toast(CageTestFragment.this.getFragmentActivity(), "目前狀態是 龍車 - 離站");
                }
                else if ( strCageType.equals("2") ) //龍車 - 進站
                {
                    //Constants.toast(CageTestFragment.this.getFragmentActivity(), "目前狀態是 龍車 - 進站");
                }
                else if ( strCageType.equals("3") ) //龍車 - 上車
                {
                    //Constants.toast(CageTestFragment.this.getFragmentActivity(), "目前狀態是 龍車 - 上車");
                }
                else if ( strCageType.equals("4") ) //龍車 - 下車
                {
                    //Constants.toast(CageTestFragment.this.getFragmentActivity(), "目前狀態是 龍車 - 下車");
                }
                else if ( strCageType.equals("5") ) //籠車 - 站所盤點
                {
                    //Constants.toast(CageTestFragment.this.getFragmentActivity(), "目前狀態是 龍車 - 下車");
                }
                else if ( strCageType.equals("6") ) //從 "等候處理" 刪除RFID標記
                {
                    //Constants.toast(CageTestFragment.this.getFragmentActivity(), "目前狀態是 龍車 - 下車");
                    btnSave.setVisibility(View.GONE);
                    txtRFID.setVisibility(View.VISIBLE);
                    txtDel.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    //取得站所分區
    private void getStationType(){

        //Constants.toast(getFragmentActivity(), "配達分區-資料取得中,請稍後...");

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("strType", "STATION");

        RequestManager.instance().doRequest(Constants.API_POST_GET_DELIVERY_TYPE, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                List<String> returnlist = new ArrayList<String>();
                ArrayList<String> returnIdlist = new ArrayList<String>();

                Log.e("----------------",jsonObject.toString());
                Log.e("----------------",jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    JsonArray objData = jsonObject.getAsJsonArray("choicelist");
                    Log.e("----------------",objData.toString());
                    Log.e("----------------",""+objData.size() );
                    Log.e("----------------",objData.get(0).toString());
                    Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
                    for ( int i = 0 ; i < objData.size() ; i++)
                    {
                        returnlist.add( ((JsonObject)objData.get(i)).get("choice").toString().replace("\"","").replace("\"","") );
                        returnIdlist.add( ((JsonObject)objData.get(i)).get("code_id").toString().replace("\"","").replace("\"","") );
                    }

                    if ( returnlist.size() > 1 )
                    {
                        CageTestFragment.this.spinnerStationList = returnlist.toArray(new String[0]);
                        CageTestFragment.this.spinnerStationIdList= returnIdlist.toArray(new String[0]);
                        getStationType2();
                    }

                }

            }

            @Override
            public void onFail(String reason) {

            }
        });


    }


    private void getStationType2()
    {
        Log.e("----------------","here" );
        Log.e("spinnerStationList.toString()",""+spinnerStationList.length );

        strStationType = "";

        this.spinnerStationStatus = (Spinner) view.findViewById(R.id.tvStationType);

        ArrayAdapter arrayAdapter = new ArrayAdapter( CageTestFragment.this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, CageTestFragment.this.spinnerStationList);
        this.spinnerStationStatus.setAdapter((SpinnerAdapter) arrayAdapter);

        this.spinnerStationStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                strStationType = spinnerStationIdList[position];
                //Constants.toast(CageTestFragment.this.getFragmentActivity(), strStationType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private String x = "";
    private String y = "";

    private void doGPS()
    {
        GPSLocation location = GPSLocation.instance(CageTestFragment.this.getFragmentActivity(), false);

        location.setCallback(new GPSLocation.GPSCallback() {
            @Override
            public void onLocation(Location location) {
                Log.e("Location", "getLongitude:" + location.getLongitude() + "getLatitude:" + location.getLatitude());

                CageTestFragment.this.x = String.valueOf(location.getLongitude());
                CageTestFragment.this.y = String.valueOf(location.getLatitude());
            }

            @Override
            public void onUserAccept() {

            }

            @Override
            public void onUserReject() {

            }
        });
        location.StartLocation(CageTestFragment.this.getFragmentActivity());
    }

    int iThis = 0 ;
    int iTotal = 0 ;
    private void doSave()
    {

        btnSave.setEnabled(false);

        if ( strCageType.equals("0"))
        {
            Constants.showPromptDialog(this.getFragmentActivity(), "提示", "您沒有選擇功能類別", new PromptDialog.ButtonListener() {
                @Override
                public void onClick(View param1View) {

                }
            });
            btnSave.setEnabled(true);
            return ;
        }
        if ( RfidList.size() < 1)
        {
            Constants.showPromptDialog(this.getFragmentActivity(), "提示", "掃描標記數量為 0 ", new PromptDialog.ButtonListener() {
                @Override
                public void onClick(View param1View) {

                }
            });
            btnSave.setEnabled(true);
            return ;
        }
        else
        {
            Constants.toast(this.getFragmentActivity(),"即將開始上傳 , 請勿離開");
        }
        iThis = 0 ;
        iTotal = RfidList.size() ;
        for ( int i = 0 ; i < RfidList.size() ; i++)
        {
            //doSaveDetail(RfidList.get(i));

            String t = RfidList.get(i) ;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doSaveDetail(t);
                }
            }, 500 + i * 200);

        }

        //rfid_ok  rfid_process
        //Rfid_okList
        //((TextView) view.findViewById(R.id.rfid_ok)).append( ((TextView) view.findViewById(R.id.rfid_process)).getText());
//        ((TextView) view.findViewById(R.id.rfid_process)).setText("");

//        RfidList.clear();

    }

    private void doSaveDetail(String CageId)
    {

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();

        data.put("CageId", CageId);
        data.put("CageType", strCageType);
        data.put("station_scode", strStationType);
        data.put("driver_code",  AccountManager.instance().getDriverCode(CageTestFragment.this.getFragmentActivity()) );
        data.put("wgs_x", x);
        data.put("wgs_y", y);



        RequestManager.instance().doRequest(Constants.API_POST_INSERT_CAGE_RECORD, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                CageTestFragment.this.iThis +=1 ;

                if ( CageTestFragment.this.iThis >= CageTestFragment.this.iTotal)
                {
                    CageTestFragment.this.btnSave.setEnabled(true);
                }

                List<String> returnlist = new ArrayList<String>();

                Log.e("----------------",jsonObject.toString());
                Log.e("----------------",jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    Rfid_okList.add(CageId);
                    ((TextView) view.findViewById(R.id.rfid_ok)).setText("");

                    for ( int i = 0 ; i < Rfid_okList.size() ; i++)
                    {
                        ((TextView) view.findViewById(R.id.rfid_ok)).append(Rfid_okList.get(i) + "\n");
                    }

                    if ( CageTestFragment.this.iThis % 20 == 10 )
                    {
                        Constants.toast( CageTestFragment.this.getFragmentActivity(),"總筆數" + CageTestFragment.this.iTotal + " ; 正在進行第" + iThis + "筆" );
                    }

                    if ( CageTestFragment.this.iThis >= CageTestFragment.this.iTotal)
                    {
                        RfidList.clear();
                        ((TextView) view.findViewById(R.id.rfid_process)).setText("");
                        Constants.toast(CageTestFragment.this.getFragmentActivity(),"處理完成");
                        //btnSave.setEnabled(true);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        {
                            CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">0</font>) / (" + Rfid_okList.size() + ")", Html.FROM_HTML_MODE_COMPACT));
                        } else
                        {
                            CageTestFragment.this.tvTotalCount.setText(Html.fromHtml("(<font color=\"red\">0</font>) / (" + Rfid_okList.size() + ")"));
                        }

                    }

//                    JsonArray objData = jsonObject.getAsJsonArray("choicelist");
//                    Log.e("----------------",objData.toString());
//                    Log.e("----------------",""+objData.size() );
//                    Log.e("----------------",objData.get(0).toString());
//                    Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
//                    for ( int i = 0 ; i < objData.size() ; i++)
//                    {
//                        returnlist.add( ((JsonObject)objData.get(i)).get("choice").toString().replace("\"","").replace("\"","") );
//                    }
//
//                    if ( returnlist.size() > 1 )
//                    {
//                        CageTestFragment.this.spinnerStationList = returnlist.toArray(new String[0]);
//                        getStationType2();
//                    }

                }

            }

            @Override
            public void onFail(String reason) {
                CageTestFragment.this.iThis +=1 ;

                if ( CageTestFragment.this.iThis >= CageTestFragment.this.iTotal)
                {
                    CageTestFragment.this.btnSave.setEnabled(true);
                }

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {

    }

}