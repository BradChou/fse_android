package tw.com.fsexpress.Menu.delivery;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.SurfaceView;

public class CustomSurfaceView extends SurfaceView {

    private float bigRecHeight;
    private Path bigRecPath;
    private Paint linePaint;
    private int mHeight;
    private float mPaintWidth;
    private int mWidth;
    private float scale;
    private float smallRecHeight;
    private float smallRecWidth;

    public CustomSurfaceView(Context arg1) {
        super(arg1);
        this.mPaintWidth = 15f;
        this.scale = 0.4f;
        this.mWidth = -1;
        this.mHeight = -1;
    }

    public CustomSurfaceView(Context arg1, AttributeSet arg2) {
        super(arg1, arg2);
        this.mPaintWidth = 15f;
        this.scale = 0.4f;
        this.mWidth = -1;
        this.mHeight = -1;
    }

    public CustomSurfaceView(Context arg1, AttributeSet arg2, int arg3) {
        super(arg1, arg2, arg3);
        this.mPaintWidth = 15f;
        this.scale = 0.4f;
        this.mWidth = -1;
        this.mHeight = -1;
    }

    public float getBigRecHeight() {
        return this.bigRecHeight;
    }

    public float getScale() {
        return this.scale;
    }

    private void init() {
        Paint v0 = new Paint();
        this.linePaint = v0;
        v0.setColor(this.getResources().getColor(android.R.color.darker_gray));
        this.linePaint.setStrokeWidth(this.mPaintWidth);
        this.linePaint.setStyle(Paint.Style.STROKE);
        this.linePaint.setAntiAlias(true);
        float v0_1 = (float)this.getHeight();
        float v1 = this.scale;
        float v0_2 = v0_1 * v1;
        this.bigRecHeight = v0_2;
        this.smallRecHeight = v0_2 * v1;
        this.smallRecWidth = ((float)this.getWidth()) * this.scale;
        Path v0_3 = new Path();
        this.bigRecPath = v0_3;
        v0_3.moveTo(0f, this.mPaintWidth / 2f);
        this.bigRecPath.lineTo(((float)this.getWidth()) - this.mPaintWidth / 2f, this.mPaintWidth / 2f);
        this.bigRecPath.rLineTo(0f, this.bigRecHeight);
        this.bigRecPath.rLineTo(-(((float)this.getWidth()) - this.mPaintWidth), 0f);
        this.bigRecPath.rLineTo(0f, -this.bigRecHeight);
        this.bigRecPath.moveTo(0f, this.bigRecHeight - this.smallRecHeight);
        this.bigRecPath.rLineTo(this.smallRecWidth, 0f);
        this.bigRecPath.rLineTo(0f, this.smallRecHeight);
    }

    @Override  // android.view.View
    protected void onDraw(Canvas arg3) {
        super.onDraw(arg3);
        if(this.linePaint == null) {
            this.init();
        }

        arg3.drawPath(this.bigRecPath, this.linePaint);
    }

    @Override  // android.view.SurfaceView
    protected void onMeasure(int arg4, int arg5) {
        int v0 = this.mWidth;
        if(-1 != v0) {
            int v2 = this.mHeight;
            if(-1 != v2) {
                this.setMeasuredDimension(v0, v2);
                return;
            }
        }

        super.onMeasure(arg4, arg5);
    }

    public void resize(int arg2, int arg3) {
        this.mWidth = arg2;
        this.mHeight = arg3;
        this.getHolder().setFixedSize(arg2, arg3);
        this.requestLayout();
        this.invalidate();
    }

}
