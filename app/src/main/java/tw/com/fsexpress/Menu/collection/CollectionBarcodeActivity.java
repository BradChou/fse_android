package tw.com.fsexpress.Menu.collection;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CollectionBarcodeActivity extends Activity implements BarcodeCallback {

    /*
    20.取件成功
    7 .已遭取走
    18.無貨可取
    17.客戶取消退貨
    12.地址錯誤
    6 .聯絡不上
    16.約定再取件
    22.公司休息
     */
    private static final String TAG = "Collection";

    private String[] spinnerContentA = new String[]{"20.取件成功","7 .已遭取走","18.無貨可取","17.客戶取消退貨","12.地址錯誤","6 .聯絡不上","16.約定再取件","22.公司休息","24.未用momo原包裝"};
    private Spinner spinnerAbnormalStatus;

    private String deliverySelect = "";
    private String selectSpinnerId = "20";

    private long DELAY_TIME = 2000L;

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();

    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private TextView txtGoodNum, txtGoodNumFail, txtBarcode, txtTotalNum;
    private boolean isStop = false;
    private ScrollView scrollView, scrollViewFail;
    private String strGoods = "";
    private EditText editPlatesNum;
    private ToneGenerator toneGenerator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_collection_barcode);


        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.txtBarcode = (TextView)findViewById(R.id.txt_now_good_num);
        this.txtTotalNum = (TextView)findViewById(R.id.txt_total_num);

        this.scrollView = (ScrollView)findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollViewFail = (ScrollView)findViewById(R.id.scrollViewFail);
        this.scrollViewFail.fullScroll(130);

        this.txtGoodNum = (TextView)findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)findViewById(R.id.goods_num_fail);

        this.editPlatesNum = (EditText)findViewById(R.id.edit_plates_num);

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });

        if (this.spinnerAbnormalStatus == null){
            getDevliveryType();

        }

        /*
        if (this.spinnerAbnormalStatus == null){
            this.spinnerAbnormalStatus = (Spinner) findViewById(R.id.spinner_partition);
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
            this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

            this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    deliverySelect = spinnerContentA[position];

                    selectSpinnerId = deliverySelect.substring(0, 2);
                    selectSpinnerId = selectSpinnerId.trim();

                    Log.e(TAG, "select = " + selectSpinnerId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

         */

    }

    private void addResultToList(String paramString) {
        if (!this.isStop) {

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            this.isStop = true;

            if (paramString.length() < 10){
                Constants.ErrorAlertDialog(this, "集貨", "條碼必須長度需大於10碼", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();
                    }
                }, null);
                return;
            }

            requestUpload(paramString);

        }
    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                CollectionBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }


    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0){
            JSONArray arrJson = new JSONArray(this.arrBarcode);
            CollectionBarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
            CollectionBarcodeActivity.this.setResult(RESULT_OK, getIntent());
            CollectionBarcodeActivity.this.finish();

        }else{
            RequestResultListDialog.instance(this).showUploadDialog(arrLogs, "集貨", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {
                    handlerScan();
                }
            });
        }


    }

    private void requestUpload(String barcode){

//        AlertDialog dialog = Constants.ProgressBar(this, "資料上傳中...請稍候");
//        dialog.show();

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "5");
        data.put("driver_code", AccountManager.instance().getDriverCode(this));
        data.put("check_number", barcode);
        data.put("scan_date", Constants.getTime());
        data.put("receive_option", this.selectSpinnerId);
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", this.editPlatesNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

//                dialog.cancel();

                handlerScan();

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(CollectionBarcodeActivity.this,jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(CollectionBarcodeActivity.this, String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("5", "集貨", barcode);

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(CollectionBarcodeActivity.this, String.format("[%s]-上傳失敗", barcode));

                }

            }

            @Override
            public void onFail(String reason) {

//                dialog.cancel();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }


    //取得配達分區
    private void getDevliveryType(){

        //Constants.toast(getFragmentActivity(), "集貨分區-資料取得中,請稍後...");

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("strType", "RO");

        RequestManager.instance().doRequest(Constants.API_POST_GET_DELIVERY_TYPE, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                List<String> returnlist = new ArrayList<String>();

                Log.e("----------------",jsonObject.toString());
                Log.e("----------------",jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    JsonArray objData = jsonObject.getAsJsonArray("choicelist");
                    Log.e("----------------",objData.toString());
                    Log.e("----------------",""+objData.size() );
                    Log.e("----------------",objData.get(0).toString());
                    Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
                    for ( int i = 0 ; i < objData.size() ; i++)
                    {
                        returnlist.add( ((JsonObject)objData.get(i)).get("choice").toString().replace("\"","").replace("\"","") );
                    }

                    if ( returnlist.size() > 1 )
                    {
                        CollectionBarcodeActivity.this.spinnerContentA = returnlist.toArray(new String[0]);
                    }

                }else{


                }

                doSpinnerContentMain();
            }

            @Override
            public void onFail(String reason) {
                doSpinnerContentMain();
            }
        });


    }

    public void doSpinnerContentMain()
    {

        this.spinnerAbnormalStatus = (Spinner) findViewById(R.id.spinner_partition);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deliverySelect = spinnerContentA[position];

                selectSpinnerId = deliverySelect.substring(0, 2);
                selectSpinnerId = selectSpinnerId.trim();

                Log.e(TAG, "select = " + selectSpinnerId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Constants.toast(getFragmentActivity(), "集貨分區-更新完成");
    }


}
