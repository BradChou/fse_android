package tw.com.fsexpress.Menu.unload_collection;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.DischargeData;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.WareHouseManager;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class UnloadCollectionBarcodeActivity extends Activity implements BarcodeCallback {

    private long DELAY_TIME = 2000L;


    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();

    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private TextView goodsNum;
    private boolean isStop = false;

    private int scanItem = 0;
    private ScrollView scrollView, scrollViewFail;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum, txtGoodNum, txtGoodNumFail, txtTotalNum;
    private TextView txtScanNum, txtChoose;
    private String strGoods = "";
    private String currentSelectCode = "";
    private List<DataObject> arrWare = null;
    private ToneGenerator toneGenerator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_unload_collection_barcode);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.scrollView = (ScrollView)findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollViewFail = (ScrollView)findViewById(R.id.scrollViewFail);
        this.scrollViewFail.fullScroll(130);
        this.txtGoodNum = (TextView)findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)findViewById(R.id.goods_num_fail);

        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.txtTotalNum = (TextView)findViewById(R.id.txt_total_num);

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);


        this.arrWare = WareHouseManager.instance().getWareHouse();
        this.txtChoose = (TextView)findViewById(R.id.txt_choose_unloading);
        this.findViewById(R.id.txt_choose_unloading).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> arrTitle = new ArrayList<String>();
                for (DataObject d: arrWare) {
                    String title = String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString());
                    arrTitle.add(title);
                }
                Constants.AlertWithMultipleSelect(UnloadCollectionBarcodeActivity.this, "卸貨站選擇：", arrTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.e("Jerry", "which:"+which);
                        DataObject d = arrWare.get(which);
                        UnloadCollectionBarcodeActivity.this.currentSelectCode = d.getVariable("code").toString();
                        txtChoose.setText(String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString()));

                    }
                }, null).show();
            }
        });

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });
    }

    private void addResultToList(String paramString) {

        if (!this.isStop) {

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            this.isStop = true;

            if (paramString.length() < 10){
                Constants.ErrorAlertDialog(this, "卸集", "請確認您的貨號是否正確。", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();
                    }
                }, null);
                return;
            }

            /*
            2022-02-16 v.43

            修正 取消卸集站的選擇

             */
            /*
            if (this.currentSelectCode.equals("")) {
                Constants.ErrorAlertDialog(this, "卸集", "請選擇卸貨站。", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();
                    }
                }, null);
                return;
            }
            */

            requestUpload(paramString);

        }
    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "6");
        data.put("driver_code", AccountManager.instance().getDriverCode(UnloadCollectionBarcodeActivity.this));
        data.put("check_number", barcode);
        data.put("receive_option", "");
        data.put("scan_date", Constants.getTime());
        data.put("area_arrive_code", "");//UnloadCollectionBarcodeActivity.this.currentSelectCode);
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", "");
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                handlerScan();

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(UnloadCollectionBarcodeActivity.this,jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    txtNowGoodnum.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(UnloadCollectionBarcodeActivity.this, String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("6", "卸集",barcode);
                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(UnloadCollectionBarcodeActivity.this, String.format("[%s]-上傳失敗", barcode));
                }


            }

            @Override
            public void onFail(String reason) {

            }
        });

        insertData(barcode); // 寫資料進room
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                UnloadCollectionBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }


    private void beepAndPause() {
        if (this.toneG == null)
            this.toneG = new ToneGenerator(2, 100);
        this.toneG.startTone(24);
        this.isStop = true;
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                UnloadCollectionBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }

    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0){
            JSONArray arrJson = new JSONArray(this.arrBarcode);
            UnloadCollectionBarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
            UnloadCollectionBarcodeActivity.this.setResult(RESULT_OK, getIntent());
            UnloadCollectionBarcodeActivity.this.finish();

        }else{
            RequestResultListDialog.instance(this).showUploadDialog(arrLogs, "集貨", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {
                    handlerScan();
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    // 將掃讀成功的資料寫進room
    private void insertData(String checkNumber) {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(this).toUpperCase();
            DischargeData dischargeData = new DischargeData(UUID.randomUUID().toString(), driverCode, checkNumber, Constants.getTime(), Constants.getNowTimestamp());
            AppDataBase.getInstance(this).getDischargeDao().insertData(dischargeData);
        }).start();
    }
}
