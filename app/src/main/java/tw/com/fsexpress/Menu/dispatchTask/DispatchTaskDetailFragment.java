package tw.com.fsexpress.Menu.dispatchTask;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.ui.GHWIndexPath;
import tw.com.fsexpress.framework.ui.GHWListView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DispatchTaskDetailFragment extends GHWFragment {
    private static final String TAG = "ALVIN";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    // variables
    private String type = "0";

    private GHWListView listView = null;
    private List<DataObject> arr_data = new ArrayList<DataObject>();
    private DispatchTaskDetailFragment.ListViewHolder holder;

    static class ListViewHolder
    {
        TextView textDate;
        TextView textSupName;
        TextView textMark;
    }

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        String title = "";
        switch (this.type) {
            default:
                break;
            case "0":
                title = "自營客戶集貨";
                break;
            case "1":
                title = "B段派遣";
                break;
            case "2":
                title = "專車";
                break;
            case "3":
                title = "派遣任務查詢";
                break;
        }
        this.textTitle.setText(title);
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) DispatchTaskDetailFragment.this.getFragmentActivity()).popFragment();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);


        this.listView = (GHWListView) view.findViewById(R.id.rv_detail);
        this.setListView();

        this.listView.reloadData();
    }

    private void setListView() {
        this.listView.delegate = new GHWListView.Delegate()
        {
            @Override
            public void didSelectRowAtIndexPath(View view, GHWIndexPath indexPath)
            {
                DispatchTaskSubDetailFragment fg = (DispatchTaskSubDetailFragment)new DispatchTaskSubDetailFragment().initWithLayoutResourceId(R.layout.layout_dispatch_sub_detail);
                DataObject d = arr_data.get(indexPath.row);
                fg.setTaskId(d.getVariable("task_id").toString());
                ((GHWFragmentActivity) DispatchTaskDetailFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        };

        this.listView.dataSource = new GHWListView.DataSource()
        {
            @Override
            public int numberOfSectionsInListView(GHWListView.GHWBaseAdapter arg0)
            {
                return 0;
            }

            @Override
            public int numberOfRowsInSection(GHWListView.GHWBaseAdapter arg0, int arg1)
            {
                return arr_data.size();
            }

            @Override
            public View getSectionView(GHWListView arg0, View arg1, int arg2)
            {
                return null;
            }

            @Override
            public View getItemView(GHWListView parent, View cell, GHWIndexPath indexPath)
            {
                if (cell == null)
                {
                    cell = DispatchTaskDetailFragment.this.getFragmentActivity().getLayoutInflater().inflate(R.layout.cell_dispatch_item, null);

                    holder = new DispatchTaskDetailFragment.ListViewHolder();
                    holder.textDate = (TextView) cell.findViewById(R.id.txt_date);
                    holder.textMark = (TextView) cell.findViewById(R.id.txt_remark);
                    holder.textSupName = (TextView) cell.findViewById(R.id.txt_supplier_name);;
                    cell.setTag(holder);

                } else
                {
                    holder = (DispatchTaskDetailFragment.ListViewHolder) cell.getTag();
                }

                DataObject d = arr_data.get(indexPath.row);
                holder.textDate.setText(String.format("%s", d.getVariable("task_date").toString()));
                holder.textMark.setText(String.format("%s", d.getVariable("memo").toString()));
                holder.textSupName.setText(String.format("%s", d.getVariable("supplier_shortname").toString()));

                return cell;
            }
        };
    }

    public void setType(String type) {
        this.type = type;
    }
    public void setData(JsonArray arr) {
        this.arr_data.clear();

        List<String> keys = Arrays.asList("task_id", "task_type", "type_name", "task_date", "supplier_no", "supplier_shortname",
                "driver", "memo", "car_license");
        for (JsonElement o : arr) {

            DataObject d = new DataObject();
            for (String key : keys) {
                String value = o.getAsJsonObject().get(key).getAsString();
                d.setVariable(key, value);
            }
            arr_data.add(d);
        }
    }
}
