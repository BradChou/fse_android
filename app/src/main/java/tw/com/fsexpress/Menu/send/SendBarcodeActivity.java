package tw.com.fsexpress.Menu.send;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.WareHouseManager;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SendBarcodeActivity extends Activity implements BarcodeCallback {

    private long DELAY_TIME = 2000L;

    private DecoratedBarcodeView barcodeView;
    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private Handler handler;
    private boolean isStop = false;

    private int scanItem = 0;
    private ScrollView scrollView, scrollViewFail;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum, txtGoodNum, txtGoodNumFail, txtTotalNum;
    private String strGoods = "";
    private String currentSelectCode = "";
    private List<DataObject> arrWare = null;
    private EditText editPlatesNum, editShiftNum;
    private ToneGenerator toneGenerator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_send_barcode);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.scrollView = (ScrollView)findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollViewFail = (ScrollView)findViewById(R.id.scrollViewFail);
        this.scrollViewFail.fullScroll(130);
        this.txtGoodNum = (TextView)findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)findViewById(R.id.goods_num_fail);

        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.txtTotalNum = (TextView)findViewById(R.id.txt_total_num);

        this.scrollView = (ScrollView) findViewById(R.id.scrollBarcode);
        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.editPlatesNum = (EditText)findViewById(R.id.edit_plates_num);
        this.editShiftNum = (EditText)findViewById(R.id.edit_shift_num);

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.arrWare = WareHouseManager.instance().getWareHouse();

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });

    }

    private void addResultToList(String paramString) {

        if (!this.isStop) {

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            this.isStop = true;

            if (this.editPlatesNum.getText().toString().equals("")){
                Constants.ErrorAlertDialog(this, "發送", "請填寫板數", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();
                    }
                }, null);
                return;
            }
            requestUpload(paramString);

        }
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                SendBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }


    private void requestUpload(String barcode){


        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "7");
        data.put("driver_code", AccountManager.instance().getDriverCode(SendBarcodeActivity.this));
        data.put("check_number", barcode);
        data.put("scan_date", Constants.getTime());
        data.put("receive_option", "");
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", this.editShiftNum.getText().toString());
        data.put("plates", this.editPlatesNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                handlerScan();

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(SendBarcodeActivity.this,jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    txtNowGoodnum.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(SendBarcodeActivity.this, String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("7", "發送", barcode);

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(SendBarcodeActivity.this, String.format("[%s]-上傳失敗", barcode));


                }


            }

            @Override
            public void onFail(String reason) {



            }
        });


    }


    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0){
            JSONArray arrJson = new JSONArray(this.arrBarcode);
            SendBarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
            SendBarcodeActivity.this.setResult(RESULT_OK, getIntent());
            SendBarcodeActivity.this.finish();

        }else{
            RequestResultListDialog.instance(this).showUploadDialog(arrLogs, "發送", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {
                    handlerScan();
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }
}
