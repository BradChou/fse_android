package tw.com.fsexpress.Menu.dispatchTask;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.ui.GHWIndexPath;
import tw.com.fsexpress.framework.ui.GHWListView;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DispatchTaskSubDetailFragment extends GHWFragment {
    private static final String TAG = "ALVIN";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    // variables
    private String taskId = "";

    private GHWListView listView = null;
    private List<DataObject> arr_data = new ArrayList<DataObject>();
    private DispatchTaskSubDetailFragment.ListViewHolder holder;

    static class ListViewHolder
    {
        TextView textPricingName;
        TextView textPrintDate;
        TextView textCheckNumber;
        TextView textSendContact;
        TextView textSendAddress;
        TextView textSendPlates;
        TextView textSendPieces;
        TextView textReceiveContact;
        TextView textSupplierName;
    }

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        String title = "派遣任務\n托運單明細";

        this.textTitle.setText(title);
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) DispatchTaskSubDetailFragment.this.getFragmentActivity()).popFragment();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);


        this.listView = (GHWListView) view.findViewById(R.id.rv_detail);
        this.setListView();

        this.requestGetDispatchTaskDetail();
    }

    private void setListView() {
        this.listView.delegate = new GHWListView.Delegate()
        {
            @Override
            public void didSelectRowAtIndexPath(View view, GHWIndexPath indexPath)
            {
//                ReverseDetailFragment fg = (ReverseDetailFragment)new ReverseDetailFragment().initWithLayoutResourceId(R.layout.layout_reverse_detail);
//                DataObject d = arr_data.get(indexPath.row);
//                fg.setData(d);
//                ((GHWFragmentActivity) ReverseFragment.this.getFragmentActivity()).pushFragment(fg, true);
            }
        };

        this.listView.dataSource = new GHWListView.DataSource()
        {
            @Override
            public int numberOfSectionsInListView(GHWListView.GHWBaseAdapter arg0)
            {
                return 0;
            }

            @Override
            public int numberOfRowsInSection(GHWListView.GHWBaseAdapter arg0, int arg1)
            {
                return arr_data.size();
            }

            @Override
            public View getSectionView(GHWListView arg0, View arg1, int arg2)
            {
                return null;
            }

            @Override
            public View getItemView(GHWListView parent, View cell, GHWIndexPath indexPath)
            {
                if (cell == null)
                {
                    cell = DispatchTaskSubDetailFragment.this.getFragmentActivity().getLayoutInflater().inflate(R.layout.cell_dispatch_subitem, null);

                    holder = new DispatchTaskSubDetailFragment.ListViewHolder();
                    holder.textCheckNumber = (TextView) cell.findViewById(R.id.check_number);
                    holder.textPricingName = (TextView) cell.findViewById(R.id.pricing_name);
                    holder.textPrintDate = (TextView) cell.findViewById(R.id.print_date);
                    holder.textReceiveContact = (TextView) cell.findViewById(R.id.receive_contact);
                    holder.textSendAddress = (TextView) cell.findViewById(R.id.send_address);
                    holder.textSendContact = (TextView) cell.findViewById(R.id.send_contact);
                    holder.textSupplierName = (TextView) cell.findViewById(R.id.supplier_name);
                    holder.textSendPlates = (TextView) cell.findViewById(R.id.send_plates);
                    holder.textSendPieces = (TextView) cell.findViewById(R.id.send_pieces);
                    cell.setTag(holder);

                } else
                {
                    holder = (DispatchTaskSubDetailFragment.ListViewHolder) cell.getTag();
                }

                DataObject d = arr_data.get(indexPath.row);
                holder.textCheckNumber.setText(d.getVariable("check_number").toString());
                holder.textPricingName.setText(d.getVariable("pricing_name").toString());
                holder.textPrintDate.setText(d.getVariable("print_date").toString());
                holder.textReceiveContact.setText(d.getVariable("receive_contact").toString());
                holder.textSendAddress.setText(d.getVariable("send_address").toString());
                holder.textSendContact.setText(d.getVariable("send_contact").toString());
                holder.textSupplierName.setText(d.getVariable("supplier_name").toString());
                holder.textSendPlates.setText(d.getVariable("plates").toString());
                holder.textSendPieces.setText(d.getVariable("pieces").toString());

                return cell;
            }
        };
    }

    protected void requestGetDispatchTaskDetail() {
        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("driver_code", AccountManager.instance().getDriverCode(ReverseFragment.this.getFragmentActivity()));
        params.put("task_id", this.taskId);
        Log.e(TAG, "task id = " + this.taskId);
        RequestManager.instance().doRequest(Constants.API_POST_GET_DISPATCH_TASK_DETAIL, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    Log.e(TAG, "TaskDetail : " + jsonObject.toString());
                    arr_data.clear();

                    List<String> keys = Arrays.asList("task_id", "pricing_name", "print_date", "check_number", "receive_contact",
                            "send_contact", "send_address", "plates", "pieces", "supplier_code", "supplier_name", "area_arrive_code",
                            "area_arrive_name", "status_5", "status_6");
                    for (JsonElement o : jsonObject.get("tasks").getAsJsonArray()) {

                        DataObject d = new DataObject();
                        for (String key : keys) {
                            String value = o.getAsJsonObject().get(key).getAsString();
                            d.setVariable(key, value);
                        }
                        arr_data.add(d);
                    }

                    listView.reloadData();
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    public void setTaskId(String id) {
        this.taskId = id;
    }
}
