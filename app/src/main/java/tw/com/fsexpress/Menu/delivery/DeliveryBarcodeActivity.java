package tw.com.fsexpress.Menu.delivery;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.request.RequestManager;

import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DeliveryBarcodeActivity extends Activity implements BarcodeCallback {

    private long DELAY_TIME = 2000L;

    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private TextView goodsNum;
    private boolean isStop = false;
    private List<String> arrBarcode = new ArrayList<String>();
    private int scanItem = 0;
    private ScrollView scrollView;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum;
    private TextView txtScanNum;
    private String strGoods = "", strBarcode = "";
    private ToneGenerator toneGenerator;

    /*
    配達2.0 + 拍照2.0
    本頁特歸為橫式
    但是執行完成後
    要轉為直式 ( 要接軌其他頁面 )
    不然會退到主選單
    因為
    多數頁面的底層邏輯是 Activity -> Fragment
    並不是獨立的 Activity
    <!--  landscape,橫屏  portrait,豎屏 -->
    */
    private String SCREEN_TYPE = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {



        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_barcode);


        this.barcodeView = (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
//        barcodeView.getLayoutParams().height = FrameLayout.LayoutParams.FILL_PARENT;
        /*
        配達2.0 + 拍照2.0
        本頁特歸為橫式
        但是執行完成後
        要轉為直式 ( 要接軌其他頁面 )
        不然會退到主選單
        因為
        多數頁面的底層邏輯是 Activity -> Fragment
        並不是獨立的 Activity
        <!--  landscape,橫屏  portrait,豎屏 -->
        */
        //String SCREEN_TYPE = "";
        try
        {
            Bundle bundle = getIntent().getExtras();
            SCREEN_TYPE = bundle.getString("SCREEN_TYPE");
        }
        catch(Exception ex)
        {
            SCREEN_TYPE = "";
        }

        Log.e("SCREEN_TYPE",SCREEN_TYPE);

        if ( SCREEN_TYPE.equals("LANDSCAPE"))
        {
//            //把模式轉成橫的
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
//            barcodeView.setRotation(90);
//
            barcodeView.getLayoutParams().height = FrameLayout.LayoutParams.FILL_PARENT; //(int)(barcodeView.getLayoutParams().height * 2.5) ;

        }
        else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }



        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.scrollView = (ScrollView) findViewById(R.id.scrollBarcode);
        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.txtScanNum = (TextView) findViewById(R.id.txt_scan_num);
        this.goodsNum = (TextView) findViewById(R.id.goods_num);



        this.barcodeView.decodeContinuous(this);

        //laser
//        CameraSettings c = new CameraSettings();
//        c.setExposureEnabled(false);
//        barcodeView.setCameraSettings(c);

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });
    }

    private void addResultToList(String paramString) {
        if (!this.isStop) {

            this.isStop = true;

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            strBarcode = paramString;
            strGoods = strGoods + "\n" + paramString;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("result: ");
            stringBuilder.append(paramString);
            this.arrBarcode.add(paramString);
            this.scrollView.fullScroll(130);
            this.txtNowGoodnum.setText(paramString);
            this.txtScanNum.setText(String.valueOf(this.arrBarcode.size()));
            this.goodsNum.setText(strGoods);
            Log.e("Jerry", "backWithResultbackWithResultbackWithResultbackWithResult");
            backWithResult();
        }
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                DeliveryBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }


//    private void beepAndPause() {
//        if (this.toneG == null)
//            this.toneG = new ToneGenerator(2, 100);
//        this.toneG.startTone(24);
//        this.isStop = true;
//        getHandler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                DeliveryBarcodeActivity.this.isStop = false;
//            }
//        }, DELAY_TIME);
//    }

    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        //拍完照把模式轉回直的
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

//        String userCode = AccountManager.instance().getDriverCode(DeliveryBarcodeActivity.this);
//
//        if(userCode.toUpperCase().startsWith("Z")) {
//            JSONArray arrJson = new JSONArray(this.arrBarcode);
//            DeliveryBarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
//
//        } else {
//            Log.e("Jerry", "backWithResult:"+this.strBarcode);
//        }

        DeliveryBarcodeActivity.this.getIntent().putExtra("barcodes", this.strBarcode);
        DeliveryBarcodeActivity.this.setResult(RESULT_OK, getIntent());
        DeliveryBarcodeActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        String userCode = AccountManager.instance().getDriverCode(DeliveryBarcodeActivity.this);

        //拍完照把模式轉回直的
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // 拍照2.0
        if ( SCREEN_TYPE.equals("LANDSCAPE"))
        {
            //存檔
            saveBarcodeImage(result);
        }



        addResultToList(result.getText());

    }


    private String filename = "";

    //取得scan barcode 的圖片 並且存檔
    private void saveBarcodeImage(BarcodeResult rawResult) {

        Log.e("error","------00001");

        Bitmap bmp = rawResult.getBitmap();

        DeliveryBarcodeActivity.this.getIntent().putExtra("img9", DeliveryBarcodeActivity.this.saveBitmap(bmp, "9"));


    }


    public String saveBitmap(Bitmap paramBitmap, String paramString) {
        File file = new File(Environment.getExternalStorageDirectory(), "SKSCS1");
        if (!file.exists())
            file.mkdirs();
        Log.d("dir path", file.getAbsolutePath());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(file.getPath());
        stringBuilder.append(File.separator);
        stringBuilder.append(simpleDateFormat.format(new Date()));
        stringBuilder.append(paramString);
        stringBuilder.append(".jpg");
        this.filename = stringBuilder.toString();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.filename);
            paramBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
            try {
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException iOException) {
                Log.e("iOException.toString()",iOException.toString());
                Log.e("iOException.getLocalizedMessage()",iOException.getLocalizedMessage());
                iOException.printStackTrace();
            }
        } catch (FileNotFoundException fileNotFoundException) {
            Log.e("fileNotFoundException.toString()",fileNotFoundException.toString());
            Log.e("fileNotFoundException.getLocalizedMessage()",fileNotFoundException.getLocalizedMessage());
            fileNotFoundException.printStackTrace();
        }
        return this.filename;
    }


    private void setResultToUpload(String paramString) {
        if (!this.isStop) {
            strGoods = strGoods + "\n" + paramString;
            this.strBarcode = paramString;
            this.scrollView.fullScroll(130);
            this.txtNowGoodnum.setText(paramString);
//            this.textPieceNum.setText(String.valueOf(this.arrBarcode.size()));
            this.goodsNum.setText(strGoods);

            if (paramString.length() < 10){
                Constants.ErrorAlertDialog(this, "配達", "條碼必須長度需大於10碼", "確定", null, null, null);
                return;
            }
//            requestUpload(paramString);
        }
    }

    private void requestUpload(String barcode){

        AlertDialog dialog = Constants.ProgressBar(this, "資料上傳中...請稍候");
        dialog.show();

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "4");
        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryBarcodeActivity.this));
        data.put("check_number", barcode);
        data.put("receive_option", "");
        data.put("scan_date", "");
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", "");
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                dialog.cancel();

                if (jsonObject.get("resultcode").getAsBoolean()) {
                    TransferDataManager.instance().insertTransferData("3", "配達", barcode);
                    Constants.ErrorAlertDialog(DeliveryBarcodeActivity.this, "配達", "上傳成功", "確定", null, null, null);
                }else{
                    Constants.ErrorAlertDialog(DeliveryBarcodeActivity.this, "配達", "上傳失敗", "確定", null, null, null);
                }
            }

            @Override
            public void onFail(String reason) {
                dialog.cancel();
            }
        });
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }
}
