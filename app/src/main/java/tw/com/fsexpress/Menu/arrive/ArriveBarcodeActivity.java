package tw.com.fsexpress.Menu.arrive;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.WareHouseManager;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArriveBarcodeActivity extends Activity implements BarcodeCallback {

    private long DELAY_TIME = 2000L;

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private boolean isStop = false;
    private int scanItem = 0;
    private ScrollView scrollView;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum, txtGoodNum, txtGoodNumFail, txtTotalNum,txtSDMD_value;
    private TextView txtChoose;
    private EditText editGoodPiecesNum;
    private EditText editPlatesNum;
    private String strGoods = "";
    private String currentSelectCode = "";
    private List<DataObject> arrWare = null;
//    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.誤訂", "3.多出"};
    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.短少", "3.多出", "4.喪失", "5.誤訂"};
    private Spinner spinnerAbnormalStatus;
    private String abnormalStatus = "";
    private ToneGenerator toneGenerator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_arrive_barcode);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.scrollView = (ScrollView) findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollView.smoothScrollTo(0,0);
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);

        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.txtGoodNum = (TextView)findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)findViewById(R.id.goods_num_fail);
        this.txtTotalNum = (TextView)findViewById(R.id.txt_total_num);

        this.editPlatesNum = (EditText)findViewById(R.id.edit_plates_num);
        this.editGoodPiecesNum = (EditText)findViewById(R.id.edit_good_pieces_num);

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.txtSDMD_value = (TextView)findViewById(R.id.SDMD_value);

        this.spinnerAbnormalStatus = (Spinner)findViewById(R.id.spinner_abnormal_status);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, abnormalArray);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter)arrayAdapter);
        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String strStatus = abnormalArray[position];
                abnormalStatus = strStatus.substring(0, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        this.arrWare = WareHouseManager.instance().getWareHouse();
        this.txtChoose = (TextView)findViewById(R.id.txt_choose_unloading);
        this.findViewById(R.id.txt_choose_unloading).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> arrTitle = new ArrayList<String>();
                for (DataObject d: arrWare) {
                    String title = String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString());
                    arrTitle.add(title);
                }
                Constants.AlertWithMultipleSelect(ArriveBarcodeActivity.this, "卸貨站選擇：", arrTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

//                        Log.e("Jerry", "which:"+which);
                        DataObject d = arrWare.get(which);
                        ArriveBarcodeActivity.this.currentSelectCode = d.getVariable("code").toString();
                        txtChoose.setText(String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString()));

                    }
                }, null).show();
            }
        });

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });
    }

    private void addResultToList(String paramString) {

        if (!this.isStop) {

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            this.isStop = true;

//            strGoods = strGoods + "\n" + paramString;
//            StringBuilder stringBuilder = new StringBuilder();
//            stringBuilder.append("result: ");
//            stringBuilder.append(paramString);
//            this.arrBarcode.add(paramString);
//            this.scrollView.fullScroll(130);
//            this.txtNowGoodnum.setText(paramString);
//            this.txtScanNum.setText(String.valueOf(this.arrBarcode.size()));
//            this.goodsNum.setText(strGoods);

            if (paramString.length() < 10){

                Constants.ErrorAlertDialog(this, "到著", "請確認您的貨號是否正確。", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();

                    }
                }, null);
                return;
            }
           /*
           2022-02-16 v.43

           移除到著站所

            */
            /*
            if (this.currentSelectCode.equals("")) {

                Constants.ErrorAlertDialog(this, "到著", "請選擇站所碼。", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();

                    }
                }, null);
                return;
            }
            */
            requestUpload(paramString);

        }
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                ArriveBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){


        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "1");
        data.put("driver_code", AccountManager.instance().getDriverCode(ArriveBarcodeActivity.this));
        data.put("check_number", barcode);
        data.put("scan_date", Constants.getTime());
        data.put("receive_option", "");
        data.put("area_arrive_code", "");//ArriveBarcodeActivity.this.currentSelectCode);
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", abnormalStatus.replace("0",""));
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", this.editGoodPiecesNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", this.editPlatesNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                handlerScan();

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(ArriveBarcodeActivity.this,jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    txtNowGoodnum.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(ArriveBarcodeActivity.this, String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("1", "到著", barcode);

                    getArriveSDMD(barcode);

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(ArriveBarcodeActivity.this, String.format("[%s]-上傳失敗", barcode));


                }

            }

            @Override
            public void onFail(String reason) {

            }
        });


    }



    private void getArriveSDMD(String barcode){

        Log.e("getArriveSDMD",barcode);

        txtSDMD_value.setText("--");

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("check_number", barcode);


        RequestManager.instance().doRequest(Constants.API_POST_GET_ARRIVE_SDMD, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("jsonObject", jsonObject.toString());

                String sdmd_type = "";
                String sdmd_code = "";

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){
                    if ( jsonObject.has("resultdesc") && jsonObject.has("sdmd_type")  && jsonObject.has("sdmd_code") )
                    {
                        sdmd_type = jsonObject.get("sdmd_type").getAsString() ;
                        sdmd_code = jsonObject.get("sdmd_code").getAsString() ;
                        Log.e("sdmd_type", sdmd_type.toString());
                        Log.e("sdmd_code", sdmd_code.toString());

                        if ( sdmd_code != "" && sdmd_code != null && !sdmd_code.equals(""))
                        {
                            txtSDMD_value.setText(sdmd_code);
                        }
                    }

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    Constants.toast(ArriveBarcodeActivity.this, String.format("[%s]-SDMD取得失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });




    }




    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0){
            JSONArray arrJson = new JSONArray(this.arrBarcode);
            ArriveBarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
            ArriveBarcodeActivity.this.setResult(RESULT_OK, getIntent());
            ArriveBarcodeActivity.this.finish();

        }else{
            RequestResultListDialog.instance(this).showUploadDialog(arrLogs, "發送", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {
                    handlerScan();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {

        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }
}
