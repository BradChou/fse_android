package tw.com.fsexpress.Menu.camera;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import androidx.core.content.FileProvider;

import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TakePhotoManager {
    public static String AUTHORITY = "tw.com.fsexpress.fileprovider";
    public static int PRIVATE_PATH = 1;
    public static int PUBLIC_PATH = 0;
    public static int TAKE_PHOTO = 2;
    private int path;

    static {
    }

    TakePhotoManager(int arg1) {
        this.path = arg1;
    }

    public static String bitmapToBase64(Bitmap arg3) {
        String v3_2;
        ByteArrayOutputStream v1;
        ByteArrayOutputStream v0 = null;
        if(arg3 == null) {
            v3_2 = "";
        }
        else {
            try {
                v1 = new ByteArrayOutputStream();
                arg3.compress(Bitmap.CompressFormat.PNG, 80, v1);
                v1.flush();
                v1.close();
                v3_2 = Base64.encodeToString(v1.toByteArray(), 0);
            }
            catch(IOException v3_1) {
                try {
                    return "解析失敗->" + v3_1.toString();
                }
                catch(Throwable v3) {
                    throw v3;
                }
            }
            catch(Throwable v3) {
                throw v3;
            }

            v0 = v1;
        }

        if(v0 != null) {
            try {
                v0.flush();
                v0.close();
            }
            catch(IOException unused_ex) {
            }

            return v3_2;
        }

        return v3_2;
    }

    private File createImageFile(Activity arg6) {
        File v1_1;
        String v0 = "JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date()) + "_";
        if(this.path == TakePhotoManager.PUBLIC_PATH) {
            File v1 = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "junfu");
            String v2 = v1.getAbsolutePath();
            if(!v1.exists() && !v1.mkdir()) {
                // ALVIN等待處理
//                arg6.onError("創建文件夾失敗，請重試2");
                return null;
            }

            v1_1 = new File(v2);
            try {
                return File.createTempFile(v0, ".jpg", v1_1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        v1_1 = arg6.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            return File.createTempFile(v0, ".jpg", v1_1);
        }
        catch(IOException v0_1) {
            // ALVIN等待處理
//            arg6.onError("創建照片失敗，請重試，錯誤訊息->" + v0_1);
            return null;
        }
    }

    public static boolean deleteFileByPath(Activity arg2, String arg3) {
        try {
            File v0 = new File(arg3);
            Uri v3 = Build.VERSION.SDK_INT < 24 ? Uri.fromFile(v0) : FileProvider.getUriForFile(arg2, TakePhotoManager.AUTHORITY, v0);
            arg2.getContentResolver().delete(v3, null, null);
            return true;
        }
        catch(Exception unused_ex) {
            Log.e("-------Exception------",unused_ex.toString());
            //return false;
        }
        try {
            File v0 = new File(arg3);
            v0.delete();
            return true;
        }
        catch(Exception unused_ex) {
            Log.e("-------Exception------",unused_ex.toString());
            return false;
        }
    }

    public void dispatchTakePictureIntent(Activity arg5) {
        Intent v0 = new Intent("android.media.action.IMAGE_CAPTURE");
        if(v0.resolveActivity(arg5.getPackageManager()) != null) {
            File v1 = this.createImageFile(arg5);
            if(v1 != null) {
                Log.d("測試一下", "path: " + v1.getAbsolutePath());
                Uri v1_1 = Build.VERSION.SDK_INT < 24 ? Uri.fromFile(v1) : FileProvider.getUriForFile(arg5, TakePhotoManager.AUTHORITY, v1);
                v0.putExtra("output", v1_1);
                // ALVIN等待處理
//                arg5.setNowPhotoUri(v1_1);
                arg5.startActivityForResult(v0, TakePhotoManager.TAKE_PHOTO);
            }
        }
    }

    public static Bitmap drawTextToBitmap(Activity arg5, Bitmap arg6, String arg7) {
        Paint v0 = new Paint(1);
        v0.setColor(arg5.getResources().getColor(R.color.white));
        v0.setTextSize(Constants.dp2px(arg5, 4f));
        Rect v2 = new Rect();
        v0.getTextBounds(arg7, 0, arg7.length(), v2);
        Bitmap.Config v2_1 = arg6.getConfig();
        v0.setDither(true);
        v0.setFilterBitmap(true);
        if(v2_1 == null) {
            v2_1 = Bitmap.Config.ARGB_8888;
        }

        Bitmap v6 = arg6.copy(v2_1, true);
        new Canvas(v6).drawText(arg7, Constants.dp2px(arg5, 3f), Constants.dp2px(arg5, 6f), v0);
        return v6;
    }

    public static String filePath2Base64(String arg0) {
        Bitmap v0 = BitmapFactory.decodeFile(arg0);
        return v0 == null ? "" : TakePhotoManager.bitmapToBase64(v0);
    }

    public static Bitmap getBitMap(String arg2, Context arg3) {
        File v0 = new File(arg2);
        Log.d("cameraFile path", v0.getAbsolutePath());
        Uri v2 = Uri.fromFile(v0);
        try {
            return BitmapFactory.decodeStream(arg3.getContentResolver().openInputStream(v2));
        }
        catch(FileNotFoundException v2_1) {
            v2_1.printStackTrace();
            return null;
        }
    }

    public static TakePhotoManager getPrivatePathInstance() {
        return new TakePhotoManager(TakePhotoManager.PRIVATE_PATH);
    }

    public static TakePhotoManager getPublicPathInstance() {
        return new TakePhotoManager(TakePhotoManager.PUBLIC_PATH);
    }

    public static Bitmap getResizedBitmap(Bitmap arg7, int arg8) {
        float v3;
        int v0 = arg7.getWidth();
        int v1 = arg7.getHeight();
        if(v0 < arg8 && v1 < arg8) {
            return arg7;
        }

        if(v0 > v1 && v0 > arg8) {
            v3 = ((float)arg8) / ((float)v0);
        }
        else if(v1 > arg8) {
            v3 = ((float)arg8) / ((float)v1);
        }
        else {
            v3 = 1f;
        }

        if(v3 >= 1f) {
            return arg7;
        }

        float v2 = (float)v0;
        int v4 = (int)(v2 * v3);
        float v5 = (float)v1;
        int v6 = (int)(v5 * v3);
        if(v4 <= 0 || v6 <= 0) {
            if(v0 > v1 && v0 > arg8) {
                v3 = ((float)arg8) / v2;
            }
            else if(v1 > arg8) {
                v3 = ((float)arg8) / v5;
            }

            v4 = (int)(v2 * v3);
            v6 = (int)(v5 * v3);
        }

        return ThumbnailUtils.extractThumbnail(arg7, v4, v6);
    }

    public String saveBitmap(Activity arg5, Bitmap arg6) {
        FileOutputStream v2 = null;
        File v5 = this.createImageFile(arg5);
        if(v5 != null) {
            String v1 = v5.getAbsolutePath();
            try {
                v2 = new FileOutputStream(v5);
                arg6.compress(Bitmap.CompressFormat.JPEG, 100, v2);
                v2.flush();
                v2.close();
            }
            catch(IOException unused_ex) {
                if(v2 != null) {
                    try {
                        v2.flush();
                        v2.close();
                        return null;
                    }
                    catch(IOException unused_i_ex) {
                    }
                }

                return null;
            }
            catch(Throwable v5_1) {
                throw v5_1;
            }

            try {
                v2.flush();
                v2.close();
            }
            catch(IOException unused_ex) {
            }

            return v1;
        }

        return null;
    }
}
