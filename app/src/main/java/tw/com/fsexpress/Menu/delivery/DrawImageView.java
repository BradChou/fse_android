package tw.com.fsexpress.Menu.delivery;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.widget.AppCompatImageView;

import tw.com.fsexpress.R;


public class DrawImageView extends AppCompatImageView {

    private float mBoundaryHeight = 0.0F;

    private float mBoundaryWidth = 0.0F;

    private RectF mFrameRect;

    private RectF mFrameRect2;

    private RectF mFrameRect3;

    private RectF mFrameRect4;

    int mWindowHeight1;

    int mWindowWidth1;

    Paint paint;

    Paint paint2;

    Paint paint3;

    private String str_secondn = "不隱藏";

    int x;

    int x1;

    int y;

    int y1;

    public DrawImageView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        Paint paint = new Paint();
        this.paint = paint;
        paint.setAntiAlias(true);
        this.paint.setColor(paramContext.getResources().getColor(R.color.black));
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(15.0F);
        paint = new Paint();
        this.paint2 = paint;
        paint.setAntiAlias(true);
        this.paint2.setColor(paramContext.getResources().getColor(R.color.white));
        //this.paint2.setAlpha(150);
        paint = new Paint();
        this.paint3 = paint;
        paint.setAntiAlias(true);
        this.paint3.setColor(paramContext.getResources().getColor(R.color.black));
        this.paint3.setStyle(Paint.Style.STROKE);
        this.paint3.setStrokeWidth(15.0F);
    }

    public RectF getFrameRect() {
        return this.mFrameRect;
    }

    public RectF getmFrameRect2() {
        return this.mFrameRect2;
    }

    public void onDraw(Canvas paramCanvas) {
        super.onDraw(paramCanvas);
        this.mFrameRect = new RectF(this.x, this.y, this.x1, this.y1);
        float f = this.x;
        int i = this.y1;
        this.mFrameRect2 = new RectF(f, ((this.y + i) / 2 + 30), (this.x1 / 3 + 30), i);
        this.mFrameRect3 = new RectF(0.0F, 0.0F, this.mWindowWidth1, this.y);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("cscsc");
        stringBuilder.append(Integer.toString(this.y1));
        Log.d("xx", stringBuilder.toString());
        if (this.str_secondn.equals("不隱藏")) {
        this.paint3.setAlpha(150);
    } else {
        this.paint3.setAlpha(0);
    }
    paramCanvas.drawRect(this.mFrameRect, this.paint);
    paramCanvas.drawRect(this.mFrameRect2, this.paint3);
    paramCanvas.drawRect(this.mFrameRect3, this.paint2);
    paramCanvas.drawRect(new RectF(0.0F, this.y1, this.mWindowWidth1, this.mWindowHeight1), this.paint2);
}

    public void setttt(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
        this.x = paramInt1;
        this.y = paramInt2;
        this.x1 = paramInt3;
        this.y1 = paramInt4;
        this.str_secondn = paramString;
        this.mWindowWidth1 = paramInt5;
        this.mWindowHeight1 = paramInt6;
    }
}
