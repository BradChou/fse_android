package tw.com.fsexpress.Menu.delivery;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.Menu.camera.TakePhotoManager;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.request.RequestResultListDialog;
import tw.com.fsexpress.utils.PhotoDialog;
import tw.com.fsexpress.utils.PromptDialog;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class DeliveryFragment extends GHWFragment {
    private static final String TAG = "ALVIN";

    private int FULL_CAMERA = 101;
    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private TextView txtGoodNum, txtTotalNum, txtAccumulationNum;
    private ScrollView scrollView;

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    // page
    private EditText editGoodNum = null;
    private Button btnPhoto = null, btnSave = null;
    private Spinner spinnerAbnormalStatus;

    private EditText editGoodPiecesNum;
    private TextView editMoney;

    private EditText editPlatesNum;

    //private String[] spinnerContentA = new String[]{"3.正常配交", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達", "A.原單退回"};
    //private String[] spinnerContentB = new String[]{"3.正常配交", "C. 振興券", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達", "A.原單退回"};

    //private String[] spinnerContentA = new String[]{"3.正常配交", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達"};
    //private String[] spinnerContentB = new String[]{"3.正常配交", "C. 振興券", "0.簽單留置", "1.客戶不在", "2.約定再配", "4.拒收", "5.地址錯誤", "6.查無此人", "7.缺件配達", "8.破損配達", "9.短少配達"};

    private String[] spinnerContentA = new String[]{"3 .正常配交", "1 .客戶不在", "2 .約定再配", "4 .拒收", "5 .地址錯誤", "6 .查無此人", "11.公司行號休息","26.聯絡不上","21.到站:自取" };

    private String deliverySelect = "";
    private String selectSpinnerId = "";


    private ImageView imgBig;
    private ImageView imgSmall;
    private LinearLayout llPhoto;

    private TextView txtCheck, txtGoodNumFail, txtBarcode;

    private Bitmap bigBitmap;
    private String bigBitmapUrl = "";
    private Bitmap smallBitmap;
    private String smallBitmapUrl = "";

    private ToneGenerator toneGenerator;
    private boolean isAuto = false;
    private int requestTotal = 0;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("配達");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) DeliveryFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(DeliveryFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "配達", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.txtTotalNum = (TextView) view.findViewById(R.id.txt_total_num);
//        this.txtAccumulationNum = (TextView) view.findViewById(R.id.txt_accumulation_num);

        this.scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollView.smoothScrollTo(0,0);
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);
        this.txtGoodNum = (TextView) view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);

        // page
        if (this.editGoodNum == null){
            this.editGoodNum = (EditText) view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄
                        isAuto = true;
                        DeliveryFragment.this.editGoodNum.setText("");
                        editMoney.setText("0");
                        editGoodPiecesNum.setText("0");

                        boolPreCheckMoney = false ;
                        requestCheckNumber_PreCheckMoney(s.toString().trim() , Constants.enumCHECK_TYPE.DELIVERY.toString());

                        //2021-12-20
                        //改到 AlertDialog 執行
                        //checkUpload(s.toString().trim());

//                        requestUpload(s.toString().trim());


                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入
                        isAuto = false;
                        editMoney.setText("0");
                        editGoodPiecesNum.setText("0");
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }else{
            this.editGoodNum.requestFocus();
        }



        this.btnPhoto = (Button) view.findViewById(R.id.btn_photo);
        this.btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPhoto();
            }
        });

        this.btnSave = (Button) view.findViewById(R.id.btn_save);
        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSave();
            }
        });

        this.editPlatesNum = (EditText) view.findViewById(R.id.edit_plates_num);
        this.editGoodPiecesNum = (EditText) view.findViewById(R.id.edit_good_pieces_num);
        this.editMoney = (TextView) view.findViewById(R.id.textViewMoney);

        this.txtCheck = (TextView) view.findViewById(R.id.txt_check);
        this.txtCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editGoodNum.getText().toString().length() < 10) {
                    Constants.toast((Context) getActivity(), "您的貨號小於10號");
                    return;
                }
                boolPreCheckMoney = false ;
                requestCheckNumber_PreCheckMoney(editGoodNum.getText().toString() , Constants.enumCHECK_TYPE.CHECKSTATUS.toString());

                //2021-12-20
                //改到 AlertDialog 執行
                //requestCheckNumber(editGoodNum.getText().toString());
            }
        });


        this.imgBig = (ImageView) view.findViewById(R.id.img_big);
        this.imgSmall = (ImageView) view.findViewById(R.id.img_small);
        this.llPhoto = (LinearLayout) view.findViewById(R.id.ll_photo);

        this.imgBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bigBitmap != null) {
                    new PhotoDialog(getActivity()).setImageBitmap(bigBitmap).show();
                }
            }
        });

        this.imgSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (smallBitmap != null) {
                    new PhotoDialog(getActivity()).setImageBitmap(smallBitmap).show();
                }
            }
        });

        view.findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scan();
            }
        });

        if (this.spinnerAbnormalStatus == null){
            this.spinnerAbnormalStatus = (Spinner) view.findViewById(R.id.spinner_partition);
            ArrayAdapter arrayAdapter = new ArrayAdapter(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
            this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

            this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    deliverySelect = spinnerContentA[position];

                    selectSpinnerId = deliverySelect.substring(0, 2);
                    selectSpinnerId = selectSpinnerId.trim();

                    Log.e(TAG, "select = " + selectSpinnerId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


    }

    private void scan() {
        this.getFragmentActivity().startActivityForResult(new Intent((Context) this.getFragmentActivity(), DeliveryBarcodeActivity.class), 5);
    }

    private void getPhoto() {
/*
        Bundle bundle = new Bundle();
        Intent intent = new Intent();
        intent.setClass((Context) this.getActivity(), _v2CameraActivity.class);
        bundle.putString("second", "不隱藏");
        bundle.putString("number", this.editGoodNum.getText().toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, this.FULL_CAMERA);
*/

        Bundle bundle = new Bundle();
        Intent intent = new Intent();
        intent.setClass((Context) this.getActivity(), CameraActivity.class);
        bundle.putString("second", "不隱藏");
        bundle.putString("number", this.editGoodNum.getText().toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, this.FULL_CAMERA);

    }

    private void onCheckResult(String cate, String money) {
        String msg = "";
        int hash = cate.hashCode();
        int check;
        if (hash == 0x63F) {
            check = cate.equals("21") ? 0 : -1;
        } else if (hash == 1603) {
            check = cate.equals("25") ? 1 : -1;
        } else if (hash == 0x67D && (cate.equals("41"))) {
            check = 2;
        } else {
            check = -1;
        }

        if (check == 0) {
            msg = "到付貨款,收款金額:" + money;
        } else if (check == 1) {
            msg = "到付追加貨款,收款金額" + money;
        } else if (check == 2) {
            msg = "代收貨款,收款金額" + money;
        } else {
            msg = "無須收款";
        }
        editMoney.setText(msg);

        Constants.showPromptDialog(this.getFragmentActivity(), "提示", msg, new PromptDialog.ButtonListener() {
            @Override
            public void onClick(View param1View) {

            }
        });
    }

    private void onCheckResult_Pre(String checkNum , String cate, String money , String strCheckType) {

        String msg = "";
        int hash = cate.hashCode();
        int check;
        if (hash == 0x63F) {
            check = cate.equals("21") ? 0 : -1;
        } else if (hash == 1603) {
            check = cate.equals("25") ? 1 : -1;
        } else if (hash == 0x67D && (cate.equals("41"))) {
            check = 2;
        } else {
            check = -1;
        }

        if (check == 0) {
            msg = "到付貨款,收款金額:" + money;
        } else if (check == 1) {
            msg = "到付追加貨款,收款金額" + money;
        } else if (check == 2) {
            msg = "代收貨款,收款金額" + money;
        } else {
            msg = "無須收款";
        }
        editMoney.setText(msg);

        if ( strCheckType.equals(Constants.enumCHECK_TYPE.CHECKSTATUS.toString() )  )
        {
            showAlertDialog(this.getFragmentActivity() , checkNum , strCheckType, "提示", msg,"確認繼續","取消");
        }
        else if ( strCheckType.equals(Constants.enumCHECK_TYPE.DELIVERY.toString() ) )
        {
            showAlertDialog(this.getFragmentActivity() , checkNum , strCheckType, "提示", msg,"確認配達","取消");
        }

    }

    private void onCheckResult_receipt(String receipt_flag)
    {
        String msg = "";
        if ( receipt_flag.equals("1"))
        {
            msg = "注意:本件有回單";
        }

        if ( msg.equals("") == false)
        {
            Constants.showPromptDialog(this.getFragmentActivity(), "提示", msg, new PromptDialog.ButtonListener() {
                @Override
                public void onClick(View param1View) {

                }
            });
        }
    }


    public boolean boolPreCheckMoney = false ;

    protected void requestCheckNumber_PreCheckMoney(String checkNum ,  String strCheckType) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check_number", checkNum);
        params.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity()));
        RequestManager.instance().doRequest(Constants.API_POST_CHECK_SCANCODE_2, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {
                if (jsonObject.get("resultcode").getAsBoolean() == true)
                {
                    if ( jsonObject.has("resultdesc") )
                    {
                        if ( jsonObject.get("resultdesc").getAsString().equals("") )
                        {
                            //回單
                            //onCheckResult_receipt(jsonObject.get("receipt_flag").getAsString());

                            editGoodPiecesNum.setText(jsonObject.get("pieces").getAsString());

                            String receiptMsg = "";

                            if ( jsonObject.get("receipt_flag").getAsString().equals("1"))
                            {
                                receiptMsg = "注意:本件有回單";
                            }
                            if ( receiptMsg.equals("") == false)
                            {
                                Constants.showPromptDialog(  DeliveryFragment.this.getFragmentActivity(), "提示", receiptMsg, new PromptDialog.ButtonListener() {
                                    @Override
                                    public void onClick(View param1View) {
                                        onCheckResult_Pre( checkNum ,jsonObject.get("subpoena_category").getAsString(), jsonObject.get("money").getAsString() , strCheckType);
                                    }
                                });
                            }
                            else
                            {
                                onCheckResult_Pre( checkNum ,jsonObject.get("subpoena_category").getAsString(), jsonObject.get("money").getAsString() , strCheckType);
                            }
                            //onCheckResult_Pre( checkNum ,jsonObject.get("subpoena_category").getAsString(), jsonObject.get("money").getAsString() , strCheckType);

                        }
                    }
                    else if ( jsonObject.has("error_msg") )
                    {
                        if (jsonObject.get("error_msg").getAsString().equals("查無此託運單號"))
                        {
                            Constants.showPromptDialog(getFragmentActivity(), "提示", "查無此貨號", new PromptDialog.ButtonListener() {
                                @Override
                                public void onClick(View param1View) {

                                }
                            });
                        }

                    }
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });

    }

    protected void requestCheckNumber(String checkNum) {
        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("driver_code", AccountManager.instance().getDriverCode(ReverseFragment.this.getFragmentActivity()));
        params.put("check_number", checkNum);
        RequestManager.instance().doRequest(Constants.API_POST_CHECK_SCANCODE, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    Log.e(TAG, "check number : " + jsonObject.toString());

                    if ( jsonObject.has("error_msg") )
                    {
                        if (jsonObject.get("error_msg").getAsString().equals("查無此託運單號"))
                        {
                            Constants.showPromptDialog(getFragmentActivity(), "提示", "查無此貨號", new PromptDialog.ButtonListener() {
                                @Override
                                public void onClick(View param1View) {

                                }
                            });
                        }

                    }
                    else
                    {
                        editGoodPiecesNum.setText(jsonObject.get("pieces").getAsString());
                        onCheckResult(jsonObject.get("subpoena_category").getAsString(), jsonObject.get("money").getAsString());
                    }

                }


            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void initPhotoState() {
        this.llPhoto.setVisibility(View.GONE);
        this.bigBitmap = null;
        this.smallBitmap = null;
        this.bigBitmapUrl = "";
        this.smallBitmapUrl = "";
    }

    private void doSave() {

        if (!isAuto){
            String strGoodNum = DeliveryFragment.this.editGoodNum.getText().toString();
            if (strGoodNum.equals("")){
                Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "請確認您的貨號是否正確。", "確定", null, null, null);
                return;
            }else{

                if (strGoodNum.length() < 10){
                    Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "請確認您的貨號是否正確。", "確定", null, null, null);
                    return;
                }
            }
//            requestUpload(strGoodNum);

            requestCheckNumber_PreCheckMoney(strGoodNum , Constants.enumCHECK_TYPE.DELIVERY.toString());
            //2021-12-20
            //改到 AlertDialog 執行
            //checkUpload(strGoodNum);

        }else{
            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "現在為自動上傳模式，如要手動上傳請\"點擊貨號輸入框並輸入條碼\"。", "確定", null, null, null);
        }
    }



    private void checkUpload(String barcode){
        String userCode = AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity());
//        if (!userCode.toUpperCase().startsWith("Z") && (this.bigBitmap == null || this.smallBitmap == null)) {
//            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "請拍攝簽單圖片。", "確定", null, null, null);
//            return;
//        }

        if ( ( !userCode.toUpperCase().startsWith("Z") && !userCode.toUpperCase().startsWith("P") )
                && (this.bigBitmap == null || this.smallBitmap == null)
        )
        {
            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "請拍攝簽單圖片。", "確定", null, null, null);
            return;
        }


        if (this.bigBitmap != null && this.smallBitmap != null) {
            TakePhotoManager takePhotoManager = TakePhotoManager.getPrivatePathInstance();
//            this.bigBitmapUrl = takePhotoManager.saveBitmap(this.getFragmentActivity(), this.bigBitmap);
//            this.smallBitmapUrl = takePhotoManager.saveBitmap(this.getFragmentActivity(), this.smallBitmap);
            this.bigBitmapUrl = TakePhotoManager.bitmapToBase64(this.bigBitmap);
            this.smallBitmapUrl = TakePhotoManager.bitmapToBase64(this.smallBitmap);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("bigBitmapUrl: ");
            stringBuilder.append(this.bigBitmapUrl);
        }

        //&& !userCode.toUpperCase().equals("Z010006")
        if ( userCode.toUpperCase().startsWith("Z") || userCode.toUpperCase().startsWith("P") )
        {
            requestUpload3(false ,barcode);
        }else
        {
            requestUpload3(true , barcode);
            //requestUpload4(barcode);
        }

    }

    private boolean isDoStep3OK = false ;

    private void requestUpload4(String barcode){

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "4");
        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");
        data.put("receive_option", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", this.selectSpinnerId);
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", editGoodPiecesNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", editPlatesNum.getText().toString());
        data.put("sign_form_image", this.bigBitmapUrl);
        data.put("sign_field_image", this.smallBitmapUrl);
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){
                    if (isAuto != true){
                        requestTotal++;
                        DeliveryFragment.this.imgBig.setImageBitmap(null);
                        DeliveryFragment.this.imgSmall.setImageBitmap(null);
                        if (DeliveryFragment.this.bigBitmap != null){
                            DeliveryFragment.this.bigBitmap.recycle();
                            DeliveryFragment.this.bigBitmap = null;
                        }
                        if (DeliveryFragment.this.smallBitmap != null){
                            DeliveryFragment.this.smallBitmap.recycle();
                            DeliveryFragment.this.smallBitmap = null;
                        }


                        if (requestTotal == 2){
                            requestTotal = 0;
                            DeliveryFragment.this.editGoodNum.setText("");
                            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "上傳成功", "確定", null, null, null);
                        }

                    }
                    Constants.toast(getFragmentActivity(), String.format("[%s]-簽單-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("4", "配達", barcode);

                }else{

                    Constants.toast(getFragmentActivity(), String.format("[%s]-簽單-上傳失敗", barcode));
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                }


            }

            @Override
            public void onFail(String reason) {

            }
        });


    }


    private void requestUpload3(boolean isContinueStep4 , String barcode){

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "3");
        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", this.selectSpinnerId);
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", editGoodPiecesNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", editPlatesNum.getText().toString());
        data.put("sign_form_image", this.bigBitmapUrl);
        data.put("sign_field_image", this.smallBitmapUrl);
        data.put("coupon", "");
        data.put("cash", "");
        data.put("receive_option", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg("3" , DeliveryFragment.this.selectSpinnerId , DeliveryFragment.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }

                    if (isAuto != true){
                        requestTotal++;

                        if (requestTotal == 2){
                            requestTotal = 0;
                            DeliveryFragment.this.editGoodNum.setText("");
                            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "上傳成功", "確定", null, null, null);
                        }
                    }

                    if ( isContinueStep4 == true)
                    {
                        requestUpload4(barcode);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("4", "配達", barcode);

                }else{

                    String StrErr = "" ;
                    try {
                        StrErr = jsonObject.get("resultdesc").toString() ;
                        if ( StrErr.equals("API服務異常"))
                        {
                            StrErr = "";
                        }
                    }
                    catch (Exception ex0)
                    {

                    }
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗 ( " + StrErr + ")" , barcode));
                }


            }

            @Override
            public void onFail(String reason) {

            }
        });


    }

//    private void requestUpload(String barcode){
//
//
//        String userCode = AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity());
//        if (!userCode.toUpperCase().startsWith("Z") && (this.bigBitmap == null || this.smallBitmap == null)) {
//            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "請上傳圖片", "確定", null, null, null);
//            return;
//        }
//
//        if (this.bigBitmap != null && this.smallBitmap != null) {
//            TakePhotoManager takePhotoManager = TakePhotoManager.getPrivatePathInstance();
////            this.bigBitmapUrl = takePhotoManager.saveBitmap(this.getFragmentActivity(), this.bigBitmap);
////            this.smallBitmapUrl = takePhotoManager.saveBitmap(this.getFragmentActivity(), this.smallBitmap);
//            this.bigBitmapUrl = TakePhotoManager.bitmapToBase64(this.bigBitmap);
//            this.smallBitmapUrl = TakePhotoManager.bitmapToBase64(this.smallBitmap);
//            StringBuilder stringBuilder = new StringBuilder();
//            stringBuilder.append("bigBitmapUrl: ");
//            stringBuilder.append(this.bigBitmapUrl);
//        }
//
//
//        Log.e("Jerry", "bigBitmapUrl:"+bigBitmapUrl);
//        Log.e("Jerry", "smallBitmapUrl:"+smallBitmapUrl);
//
//
//        String strScanTime = Constants.getTime();
//        HashMap<String, String> data = new HashMap<String, String>();
//        if (userCode.toUpperCase().startsWith("Z")){
//            data.put("scan_item", "3");
//        }else{
//            data.put("scan_item", "4");
//        }
//
//        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity()));
//        data.put("check_number", barcode);
//        data.put("scan_date", strScanTime);
//        data.put("area_arrive_code", "");
//        data.put("platform", "");
//        data.put("car_number", "");
//        data.put("sowage_rate", "");
//        data.put("area", "");
//        data.put("exception_option", "");
//        data.put("arrive_option", this.selectSpinnerId);
//        data.put("ship_mode", "");
//        data.put("goods_type", "");
//        data.put("pieces", editGoodPiecesNum.getText().toString());
//        data.put("weight", "");
//        data.put("runs", "");
//        data.put("plates", editPlatesNum.getText().toString());
//        data.put("sign_form_image", this.bigBitmapUrl);
//        data.put("sign_field_image", this.smallBitmapUrl);
//        data.put("coupon", "");
//        data.put("cash", "");
//
//        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
//            @Override
//            public void onResponse(JsonObject jsonObject) {
//
//                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){
//                    if (isAuto != true){
//                        DeliveryFragment.this.imgBig.setImageBitmap(null);
//                        DeliveryFragment.this.imgSmall.setImageBitmap(null);
//                        if (DeliveryFragment.this.bigBitmap != null){
//                            DeliveryFragment.this.bigBitmap.recycle();
//                            DeliveryFragment.this.bigBitmap = null;
//                        }
//                        if (DeliveryFragment.this.smallBitmap != null){
//                            DeliveryFragment.this.smallBitmap.recycle();
//                            DeliveryFragment.this.smallBitmap = null;
//                        }
//                        DeliveryFragment.this.editGoodNum.setText("");
//                        Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "上傳成功", "確定", null, null, null);
//                    }
//                    TransferDataManager.instance().insertTransferData("4", "配達", barcode);
//                }else{
//                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
//                    try {
//                        Thread.sleep(300);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    toneGenerator.stopTone();
//                }
//
//
//            }
//
//            @Override
//            public void onFail(String reason) {
//
//            }
//        });
//    }


//    private void requestMultipleUpload(){
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//        String strScanTime = Constants.getTime();
//        int idx = 0;
//        for (String barcode:this.arrBarcode) {
//
//            HashMap<String, String> data = new HashMap<String, String>();
//            data.put("scan_item", "3");
//            data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity()));
//            data.put("check_number", barcode);
//            data.put("scan_date", strScanTime);
//            data.put("area_arrive_code", "");
//            data.put("platform", "");
//            data.put("car_number", "");
//            data.put("sowage_rate", "");
//            data.put("area", "");
//            data.put("exception_option", "");
//            data.put("arrive_option", this.selectSpinnerId);
//            data.put("ship_mode", "");
//            data.put("goods_type", "");
//            data.put("pieces", editGoodPiecesNum.getText().toString());
//            data.put("weight", "");
//            data.put("runs", "");
//            data.put("plates", editPlatesNum.getText().toString());
//            data.put("sign_form_image", this.bigBitmapUrl);
//            data.put("sign_field_image", this.smallBitmapUrl);
//            data.put("coupon", "");
//            data.put("cash", "");
//
//            DataObject requestData = new DataObject();
//            requestData.setVariable("param", data);
//            requestData.setVariable("barcode", barcode);
//            requestData.setVariable("url", Constants.API_POST_SEND_UPLOAD_DATA3);
//            requestData.setVariable("method", "POST");
//            requestData.setVariable("id", String.format("%s%d",Constants.md5(data.toString()),idx));
//            RequestMutilController.instance().addRequest(requestData);
//            idx++;
//        }
//
//        RequestMutilController.instance().setCallback(new RequestMutilController.RequestMutilControllerCallback() {
//
//
//            @Override
//            public void onFinish(List<DataObject> data) {
//
//                dialog.cancel();
//                RequestResultListDialog.instance(DeliveryFragment.this.getFragmentActivity()).showUploadDialog(data, "配達");
////                Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//                for (String barcode:arrBarcode) {
//                    TransferDataManager.instance().insertTransferData("3", "配達" ,barcode);
//                }
//
//                DeliveryFragment.this.imgBig.setImageBitmap(null);
//                DeliveryFragment.this.imgSmall.setImageBitmap(null);
//                if (DeliveryFragment.this.bigBitmap != null){
//                    DeliveryFragment.this.bigBitmap.recycle();
//                    DeliveryFragment.this.bigBitmap = null;
//                }
//                if (DeliveryFragment.this.smallBitmap != null){
//                    DeliveryFragment.this.smallBitmap.recycle();
//                    DeliveryFragment.this.smallBitmap = null;
//                }
//
//
//                arrBarcode.clear();
//                updateBarcodeList();
//                DeliveryFragment.this.spinnerAbnormalStatus.setSelection(0);
//
//            }
//
//        });

//        RequestMutilController.instance().setCallback(() -> {
//
//
//            dialog.cancel();
//            Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//
//            TransferDataManager.instance().insertTransferData("1", String.valueOf(arrBarcode.size()));
//
//            arrBarcode.clear();
//            updateBarcodeList();
//
//        });


//        RequestMutilController.instance().startRequest();
//    }

//    private void requestUpload() {
//
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//        HashMap<String, String> data = new HashMap<String, String>();
//        data.put("scan_item", "4");
//        data.put("driver_code", AccountManager.instance().getDriverCode(DeliveryFragment.this.getFragmentActivity()));
//        data.put("check_number", editGoodNum.getText().toString().trim());
//        data.put("scan_date", Constants.getTime());
//        data.put("area_arrive_code", "");
//        data.put("platform", "");
//        data.put("car_number", "");
//        data.put("sowage_rate", "");
//        data.put("area", "");
//        data.put("exception_option", "");
//        data.put("arrive_option", this.selectSpinnerId);
//        data.put("ship_mode", "");
//        data.put("goods_type", "");
//        data.put("pieces", editGoodPiecesNum.getText().toString());
//        data.put("weight", "");
//        data.put("runs", "");
//        data.put("plates", editPlatesNum.getText().toString());
//        data.put("sign_form_image", this.bigBitmapUrl);
//        data.put("sign_field_image", this.smallBitmapUrl);
//        data.put("coupon", "");
//        data.put("cash", "");
//
//        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
//            @Override
//            public void onResponse(JsonObject jsonObject) {
//
//                dialog.cancel();
//
//                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true) {
//                    editGoodNum.setText("");
//                    Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "上傳成功", "確定", null, null, null);
//                } else {
//                    Constants.ErrorAlertDialog(DeliveryFragment.this.getFragmentActivity(), "配達", "上傳失敗", "確定", null, null, null);
//                }
//                Log.e("Jerry", jsonObject.toString());
//
//                if (!bigBitmapUrl.isEmpty()) {
//                    TransferDataManager.instance().insertTransferData("4", "1");  // 有圖
//                } else {
//                    TransferDataManager.instance().insertTransferData("4-1", "1");  // 沒有圖
//                }
//
//                initPhotoState();
//            }
//
//            @Override
//            public void onFail(String reason) {
//                dialog.cancel();
//            }
//        });
//
//    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG, "request code = " + requestCode + ", result code = " + resultCode);
        if (requestCode == 5) {

            if (resultCode == getFragmentActivity().RESULT_OK) {
                try {

                    String strBarcode = data.getStringExtra("barcodes");
//                    DeliveryFragment.this.editGoodNum.setText(strBarcode);
                    Constants.toast(this.getActivity(), String.format("[%s]", strBarcode));

                    boolPreCheckMoney = false ;
                    requestCheckNumber_PreCheckMoney(strBarcode.trim() , Constants.enumCHECK_TYPE.DELIVERY.toString());
                    //2021-12-20
                    //改至 AlertDialog 執行
                    //checkUpload(strBarcode);


//                    requestUpload(strBarcode);

//                    for (String barcode:this.arrBarcode) {
//                        requestUpload(barcode);
//                    }

                } catch (Exception e) {
                    Log.e("Jerry", "Exception:"+e.toString());
                }

            }
        }
        if (resultCode == 3 && requestCode == this.FULL_CAMERA && data != null) {
            this.llPhoto.setVisibility(View.VISIBLE);
            String str2 = ((Bundle) Objects.<Bundle>requireNonNull(data.getExtras())).getString("img1");
            String str1 = data.getExtras().getString("img2");
            this.bigBitmap = TakePhotoManager.getBitMap(str2, (Context) this.getActivity());
            this.smallBitmap = TakePhotoManager.getBitMap(str1, (Context) this.getActivity());
            TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str2);
            TakePhotoManager.deleteFileByPath((Activity) this.getActivity(), str1);
            this.imgBig.setImageBitmap(this.bigBitmap);
            this.imgSmall.setImageBitmap(this.smallBitmap);


        }
    }



    public void showAlertDialog(Context cxt , String checkNum , String strCheckType , String title, String content , String strYesMsg , String strNpMsg) {



        if ( content.equals("無須收款"))
        {
            if ( strCheckType.equals( Constants.enumCHECK_TYPE.DELIVERY.toString()) )
            {
                Log.e("-------------","1");
                checkUpload(checkNum);
            }
            else if ( strCheckType.equals( Constants.enumCHECK_TYPE.CHECKSTATUS.toString()) )
            {
                Log.e("-------------","2");
                requestCheckNumber(checkNum);
            }
            return ;
        }
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        if ( strCheckType.equals( Constants.enumCHECK_TYPE.DELIVERY.toString()) )
                        {
                            Log.e("-------------","ok-1");
                            checkUpload(checkNum);
                        }
                        else if ( strCheckType.equals( Constants.enumCHECK_TYPE.CHECKSTATUS.toString()) )
                        {
                            Log.e("-------------","cancel-1");
                            requestCheckNumber(checkNum);
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        if ( strCheckType.equals( Constants.enumCHECK_TYPE.DELIVERY) )
                        {

                        }
                        else if ( strCheckType.equals( Constants.enumCHECK_TYPE.CHECKSTATUS) )
                        {

                        }
                        break;
                }
            }
        };

        //正常配達或各種缺件破損配達 應該要有收費提示
        if ( this.selectSpinnerId.equals("3") || this.selectSpinnerId.equals("7") || this.selectSpinnerId.equals("8") || this.selectSpinnerId.equals("9") )
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(cxt, R.style.Theme_AppCompat_Light_Dialog_Alert2);
            builder.setMessage(content).setTitle(title).setPositiveButton(strYesMsg, dialogClickListener)
                    .setNegativeButton(strNpMsg, dialogClickListener).show();
        }
        else
        {
            //不是配達系列的
            //就直接處理了
            if ( strCheckType.equals( Constants.enumCHECK_TYPE.DELIVERY.toString()) )
            {
                Log.e("-------------","ok-2");
                checkUpload(checkNum);
            }
            else if ( strCheckType.equals( Constants.enumCHECK_TYPE.CHECKSTATUS.toString()) )
            {
                Log.e("-------------","cancel-2");
                requestCheckNumber(checkNum);
            }
        }


    }



}
