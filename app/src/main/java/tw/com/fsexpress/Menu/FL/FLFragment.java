package tw.com.fsexpress.Menu.FL;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.LoginFragment;
import tw.com.fsexpress.Menu.collection.CollectionFragment;
import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestResultListDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FLFragment extends GHWFragment {

    private Button btnLeft, btnRight;
    private WebView web;
    private String prevUrl = "";
    private androidx.appcompat.app.AlertDialog dialog;
    private int webNum = 0;

    @Override
    protected void viewDidLoad(ViewGroup view, ViewGroup container, Bundle savedInstanceState) {
        super.viewDidLoad(view, container, savedInstanceState);

        this.web = (WebView)view.findViewById(R.id.web_fl);

    }

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);



//        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
//        this.btnLeft.setBackgroundResource(R.mipmap.back);
//        this.btnLeft.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (web.canGoBack()){
//                    web.goBack();
//                }else{
//                    ((GHWFragmentActivity) FLFragment.this.getFragmentActivity()).popFragment();
//                }
//
//            }
//        });
//        this.btnRight = (Button) view.findViewById(R.id.btn_right);
//        this.btnRight.setVisibility(View.INVISIBLE);
//
//        TextView txtTitle = (TextView)view.findViewById(R.id.txt_title);
//        txtTitle.setText("正物流");


        web.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
                WebView newWebView = new WebView(view.getContext());
                newWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                        return true;
                    }
                });
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(newWebView);
                resultMsg.sendToTarget();
                return true;
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {

                Log.e("Jerry onConsoleMessage", consoleMessage.message());
                return super.onConsoleMessage(consoleMessage);

            }
        });

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(web, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }


        final WebSettings websettings = web.getSettings();
        websettings.setJavaScriptEnabled(true);
        websettings.setSupportMultipleWindows(true);

        websettings.setDomStorageEnabled(true);
        websettings.setAllowFileAccess(true);
//        websettings.setSupportZoom(true);
//        websettings.setBuiltInZoomControls(true);
        websettings.setLoadWithOverviewMode(true);
        websettings.setUseWideViewPort(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            websettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        websettings.setJavaScriptCanOpenWindowsAutomatically(true);
        web.addJavascriptInterface(new JavaScriptReceiver(this.getActivity()), "JavaScriptReceiver");

        web.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                return super.shouldOverrideUrlLoading(view, request);



            }



            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                try
                {
//                    FLFragment.this.dialog.cancel();

                    Log.e("jerry", "url:"+url);
//                    if (Constants.URL_FL.equals(url)){
//                        webNum = 0;
//                    }




                } catch (Exception e)
                {

                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);

                Log.e("jerry", "load start:"+url);

                FLFragment.this.dialog = Constants.ProgressBar(FLFragment.this.getFragmentActivity(), "讀取中...請稍候");
                FLFragment.this.dialog.show();


            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                SslCertificate sslCertificate = error.getCertificate();

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("SSL 憑證錯誤");
                builder.setMessage ("無法驗證伺服器SSL憑證。\n仍要繼續嗎?");
                builder.setPositiveButton("繼續", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


        web.loadUrl(Constants.URL_FL);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        web.clearCache(true); // 清除暫存 cache !!
    }

    public class JavaScriptReceiver {
        Context mContext;

        /**
         * Instantiate the receiver and set the context
         */
        JavaScriptReceiver(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void goBack() {

            web.post(new Runnable() {
                @Override
                public void run() {

                    ((GHWFragmentActivity) FLFragment.this.getFragmentActivity()).popFragment();

                }
            });


        }

        @JavascriptInterface
        public void getAccount() {

            web.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("jerry", "getAccount");
                    String jsUrl = String.format("javascript:setAccount('%s')", AccountManager.instance().getDriverCode(FLFragment.this.getFragmentActivity()));
                    Log.e("jerry", "getAccount:"+jsUrl);
                    web.loadUrl(jsUrl);
                }
            });

        }


        @JavascriptInterface
        public void showLoading() {

            FLFragment.this.dialog = Constants.ProgressBar(FLFragment.this.getFragmentActivity(), "讀取中...請稍候");
            FLFragment.this.dialog.show();
        }

        @JavascriptInterface
        public void cancelLoading() {

            FLFragment.this.dialog.cancel();
        }





        //TODO-Jerry:加入行事曆
        @JavascriptInterface
        public void addCalender(String title, String des, String start, String end) {

            try {

                Calendar cal = Calendar.getInstance();
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date dateStart = sdf.parse(start);
                Date dateEnd = sdf.parse(end);

                intent.putExtra("beginTime", dateStart.getTime());
                intent.putExtra("allDay", false);
                intent.putExtra("rrule", "FREQ=YEARLY");
                intent.putExtra("endTime", dateEnd.getTime());
                intent.putExtra("title", title);
                intent.putExtra("description", des);
                startActivity(intent);

            } catch (Exception e) {

            }


        }
    }


}
