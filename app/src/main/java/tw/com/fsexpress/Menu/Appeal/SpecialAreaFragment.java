package tw.com.fsexpress.Menu.Appeal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.Menu.camera.TakePhotoManager;
import tw.com.fsexpress.Menu.delivery.DeliveryBarcodeActivity;
import tw.com.fsexpress.Menu.Appeal.SpecialAreaFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment2;
import tw.com.fsexpress.Menu.delivery._v2CameraActivity;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;
import tw.com.fsexpress.utils.PromptDialog;

public class SpecialAreaFragment extends GHWFragment
{
    private static final String TAG = "SpecialArea";

    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    //是否已驗證完成 CheckNumber
    private boolean isCheckNumberOk = false ;

    //開啟相機拍照的回傳代號
    private int FULL_CAMERA = 101;

    //貨號
    private EditText editGoodNum = null;

    //檢查/辨識
    private TextView txtCheck;

    //處理中的貨號
    private TextView txtBarcode;

    //投訴理由
    private EditText editReasonMemo = null;

    //拍照
    private Button btnPhoto1 = null ;

    //圖片1的Bitmap
    private Bitmap bigBitmap1;
    //圖片1
    private ImageView imgBig1;

    //上傳
    private Button btnSave = null ;

    private ToneGenerator toneGenerator;

    public String strBarcode = "";

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        isCheckNumberOk = false ;
        if (toneGenerator == null)
        {
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }


        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("特服區申訴");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) SpecialAreaFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(SpecialAreaFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "特服區申訴", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);


        //申請理由
        editReasonMemo= (EditText) view.findViewById(R.id.edit_good_reason);

        //處理中貨號
        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);

        //貨號
        if (this.editGoodNum == null){
            this.editGoodNum = (EditText) view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10 ) { //1次進10馬以上 判定為紅外線掃瞄

                        txtBarcode.setText("");
                        requestCheckNumber(editGoodNum.getText().toString().trim());

                    }else if (count > 0){
                        //1次不滿10馬 判定為手動輸入
                    }
                    else
                    {
                        //看起來如果倒退刪除資料會進入這邊
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {
                    //strEditGoodNum = "";
                    //DeliveryFragment2.this.editGoodNum.setText("");
                }
            });
        }else{
            this.editGoodNum.requestFocus();
        }

        //檢查/辨識
        this.txtCheck = (TextView) view.findViewById(R.id.txt_check);
        this.txtCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editGoodNum.getText().toString().length() == 0)
                {
                    //沒有輸入任何貨號
                    //就直接開啟掃 BarCode
                    scan();
                }
                else if ( editGoodNum.getText().toString().length() > 0  && editGoodNum.getText().toString().length() < 10)
                {
                    //貨號長度不對
                    //沒有下一步
                    Constants.toast((Context) getActivity(), "您的貨號小於10號");
                    return;
                }
                else
                {
                    //檢查貨號
                    requestCheckNumber(editGoodNum.getText().toString());
                }

            }
        });


        //圖片1
        this.imgBig1 = (ImageView) view.findViewById(R.id.img_big1);

        //拍照
        this.btnPhoto1 = (Button) view.findViewById(R.id.btn_photo1);
        this.btnPhoto1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                getPhoto1();
            }
        });

        //上傳
        this.btnSave = (Button) view.findViewById(R.id.btn_save);

        this.btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                doSave();
                isCheckNumberOk = false ;
            }
        });
    }


    protected void requestCheckNumber(String checkNum) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("check_number", checkNum);
        params.put("driver_code", AccountManager.instance().getDriverCode(SpecialAreaFragment.this.getFragmentActivity()));
        RequestManager.instance().doRequest(Constants.API_POST_CHECK_SCANCODE_2, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {
                //Log.e(TAG, "STATUS : " + jsonObject.toString());
                //Log.e(TAG, "resultcode : " + jsonObject.get("resultcode").getAsBoolean());
                //Log.e(TAG, "error_msg : " +jsonObject.get("error_msg").getAsString());

                if (jsonObject.get("resultcode").getAsBoolean() == true) {

                    Log.e(TAG, "check number : " + jsonObject.toString());

                    txtBarcode.setText("");
                    editGoodNum.setText("");

                    if ( jsonObject.has("resultdesc") )
                    {
                        if ( jsonObject.get("resultdesc").getAsString().equals("") )
                        {
                            isCheckNumberOk = true ;
                            txtBarcode.setText(checkNum);
                            editGoodNum.setText("");
                        }

                    }

                    if ( jsonObject.has("error_msg") )
                    {
                        if (jsonObject.get("error_msg").getAsString().equals("查無此託運單號"))
                        {
                            Constants.showPromptDialog(getFragmentActivity(), "提示", "查無此貨號", new PromptDialog.ButtonListener() {
                                @Override
                                public void onClick(View param1View) {

                                }
                            });
                        }

                    }

                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    private void scan() {
        this.startActivityForResult(new Intent((Context) this.getFragmentActivity(), DeliveryBarcodeActivity.class), 5);
    }

    private void getPhoto1() {
        Bundle bundle = new Bundle();
        Intent intent = new Intent();
        intent.setClass((Context) this.getActivity(), _v2CameraActivity.class);
        bundle.putString("second", "不隱藏");
        bundle.putString("number", this.editGoodNum.getText().toString());
        intent.putExtras(bundle);
        startActivityForResult(intent, this.FULL_CAMERA);
    }



    private boolean boolLockUpload = false ;
    private void doSave() {

        Log.e("test","1--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );
        String strGoodNum = SpecialAreaFragment.this.txtBarcode.getText().toString();

        if ( isCheckNumberOk == true || strGoodNum.length() > 0 )
        {

        }
        else
        {
            Constants.ErrorAlertDialog(SpecialAreaFragment.this.getFragmentActivity(), "特服區申訴", "請先進行 檢查/驗證 , 確認您的貨號是否正確。", "確定", null, null, null);
            return;
        }

        if (strGoodNum.length() < 10){
            Constants.ErrorAlertDialog(SpecialAreaFragment.this.getFragmentActivity(), "特服區申訴", "請確認您的貨號是否正確。", "確定", null, null, null);
            return;
        }


        if ( boolLockUpload == true)
        {
            //正在上傳中
            Constants.ErrorAlertDialog(SpecialAreaFragment.this.getFragmentActivity(), "特服區申訴", "資料正在上傳中 , 請勿重複處理。", "確定", null, null, null);
            return;
        }
        else
        {
            boolLockUpload = true ;
            try {
                checkUpload(strGoodNum);
            }
            catch(Exception ex)
            {
                SpecialAreaFragment.this.imgBig1.setImageBitmap(null);

                if (SpecialAreaFragment.this.bigBitmap1 != null){
                    SpecialAreaFragment.this.bigBitmap1.recycle();
                    SpecialAreaFragment.this.bigBitmap1 = null;
                }

                editGoodNum.setText("");
                txtBarcode.setText("");
                editReasonMemo.setText("");

                Constants.showPromptDialog(getFragmentActivity(), "錯誤提示", "發生奇怪的錯誤,請確認網路狀態是否正常", new PromptDialog.ButtonListener() {
                    @Override
                    public void onClick(View param1View) {

                    }
                });
                return ;
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    boolLockUpload = false ;
                    Log.d(TAG,"reset btnSave");

                }
            },2000);// set time as per your requirement
        }

    }





    private void checkUpload(String barcode){

        Log.d("test","4--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("ApiToken", "ApiToken");
        data.put("DriverCode", AccountManager.instance().getDriverCode(SpecialAreaFragment.this.getFragmentActivity()));
        data.put("CheckNumber", barcode);
        data.put("ReasonMemo", SpecialAreaFragment.this.editReasonMemo.getText().toString());
        data.put("CheckPic_1",TakePhotoManager.bitmapToBase64(this.bigBitmap1) );

        Log.d("start-1", "start-1");
        Log.d("start-editReasonMemo", SpecialAreaFragment.this.editReasonMemo.getText().toString());

        String strErr = "";

        if ( barcode.equals("") || barcode.length() < 10 )
        {
            strErr += " 沒有輸入貨號 ";
        }
//        if ( editReasonMemo.getText().toString().length() < 3 )
//        {
//            strErr += " 沒有輸入申請理由 ";
//        }
        if (TakePhotoManager.bitmapToBase64(this.bigBitmap1).length() < 10)
        {
            strErr += " 沒有拍照 ";
        }

        if ( ! strErr.equals("") )
        {
            Constants.showPromptDialog(getFragmentActivity(), "錯誤提示", strErr, new PromptDialog.ButtonListener() {
                @Override
                public void onClick(View param1View) {

                }
            });
            return ;
        }

        Log.d("start-2", "start-2");

        RequestManager.instance().doRequest(Constants.API_POST_INSERT_SPECIAL_AREA_AUDIT, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.d("test","5--" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())  ) );
                Log.d("test", jsonObject.toString());

                Log.d("test-resultcode", jsonObject.get("resultcode").toString() );
                //Log.d("test-resultdesc", jsonObject.getAsJsonObject("resultdesc").getAsBoolean() );

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true
                    || Boolean.valueOf(jsonObject.get("resultcode").toString()) == false){

                    SpecialAreaFragment.this.imgBig1.setImageBitmap(null);

                    if (SpecialAreaFragment.this.bigBitmap1 != null){
                        SpecialAreaFragment.this.bigBitmap1.recycle();
                        SpecialAreaFragment.this.bigBitmap1 = null;
                    }

                    editGoodNum.setText("");
                    txtBarcode.setText("");
                    editReasonMemo.setText("");

                    Constants.showPromptDialog(getFragmentActivity(), "提示", "收到申請", new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });

                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();

                }
                /*
                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){


                    SpecialAreaFragment.this.imgBig1.setImageBitmap(null);


                    if (SpecialAreaFragment.this.bigBitmap1 != null){
                        SpecialAreaFragment.this.bigBitmap1.recycle();
                        SpecialAreaFragment.this.bigBitmap1 = null;
                    }


                }else{

                    Constants.toast(getFragmentActivity(), String.format("[%s]-申請失敗", barcode));
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                }
                */
                imgBig1.setImageBitmap(null);

            }

            @Override
            public void onFail(String reason) {

            }
        });


    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5)
        {
            if (resultCode == getFragmentActivity().RESULT_OK)
            {
                try
                {
                    strBarcode = data.getStringExtra("barcodes");
                    Log.e("Jerry", "11111");
                    SpecialAreaFragment.this.editGoodNum.setText(strBarcode);
                } catch (Exception ex)
                {

                }
            }
        }

        if (requestCode == this.FULL_CAMERA && resultCode == 3 && data != null)
        {
            String str2 = ((Bundle) Objects.<Bundle>requireNonNull(data.getExtras())).getString("img1");
            this.bigBitmap1 = TakePhotoManager.getBitMap(str2, (Context) this.getActivity());
            this.imgBig1.setImageBitmap(this.bigBitmap1);
        }
    }


}
