package tw.com.fsexpress.Menu.dispatchTask;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

public class DispatchTaskFragment extends GHWFragment implements View.OnClickListener {
    private static final String TAG = "ALVIN";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    // page
    private TextView txtTaskType0 = null, txtTaskType1 = null, txtTaskType2 = null, txtQuery = null;
    private LinearLayout llType0 = null, llType1 = null, llType2 = null, llTypeQuery = null;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("派遣任務清單");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) DispatchTaskFragment.this.getFragmentActivity()).popFragment();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.txtTaskType0 = (TextView) view.findViewById(R.id.task_type_0);
        this.txtTaskType1 = (TextView) view.findViewById(R.id.task_type_1);
        this.txtTaskType2 = (TextView) view.findViewById(R.id.task_type_2);
        this.txtQuery = (TextView) view.findViewById(R.id.txt_query);

        this.llType0 = (LinearLayout) view.findViewById(R.id.ll_type0);
        this.llType1 = (LinearLayout) view.findViewById(R.id.ll_type1);
        this.llType2 = (LinearLayout) view.findViewById(R.id.ll_type2);
        this.llTypeQuery = (LinearLayout) view.findViewById(R.id.ll_type_query);

        this.llType0.setOnClickListener(this);
        this.llType1.setOnClickListener(this);
        this.llType2.setOnClickListener(this);
        this.llTypeQuery.setOnClickListener(this);

        this.requestGetRemoteDispatchStatistics();
        this.requestGetRemoteUploadedTask();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        int type = 0;

        switch (id) {
            default:
                break;
            case R.id.ll_type0:
                type = 0;
                break;
            case R.id.ll_type1:
                type = 1;
                break;
            case R.id.ll_type2:
                type = 2;
                break;
            case R.id.ll_type_query:
                type = 3;
                break;
        }

        this.requestGetRemoteTask(String.valueOf(type));
    }

    protected void requestGetRemoteDispatchStatistics() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("car_license", AccountManager.instance().getCarNum(DispatchTaskFragment.this.getFragmentActivity()));
//        params.put("car_license", "LAP-128");
        RequestManager.instance().doRequest(Constants.API_POST_GET_DISPATCH_STATISTICS, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    for (JsonElement o : jsonObject.getAsJsonArray("tasks")) {
                        String type = o.getAsJsonObject().get("task_type").getAsString();

                        if (type.equals("0")) {
                            txtTaskType0.setText(o.getAsJsonObject().get("cnt").toString());
                        }
                        if (type.equals("1")) {
                            txtTaskType1.setText(o.getAsJsonObject().get("cnt").toString());
                        }
                        if (type.equals("2")) {
                            txtTaskType2.setText(o.getAsJsonObject().get("cnt").toString());
                        }
                    }
                    Log.e(TAG, "dispatch : " + jsonObject.toString());
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    protected void requestGetRemoteUploadedTask() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("car_license", AccountManager.instance().getCarNum(DispatchTaskFragment.this.getFragmentActivity()));
//        params.put("car_license", "LAP-128");
        params.put("sdate", "");
        params.put("edate", "");
        RequestManager.instance().doRequest(Constants.API_POST_GET_UPLOADED_DISPATCHTASK, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    txtQuery.setText(jsonObject.get("cnt").getAsString());
                    Log.e(TAG, "uploaded : " + jsonObject.toString());
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    protected void requestGetRemoteTask(String type) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("driver_code", AccountManager.instance().getCarNum(DispatchTaskFragment.this.getFragmentActivity()));
//        params.put("car_license", "LAP-128");
        params.put("task_type", type);
        RequestManager.instance().doRequest(Constants.API_POST_GET_DISPATCH_TASK, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    Log.e(TAG, "get remote task : " + jsonObject.toString());
                    DispatchTaskDetailFragment fg = (DispatchTaskDetailFragment) new DispatchTaskDetailFragment().initWithLayoutResourceId(R.layout.layout_dispatch_detail);
                    fg.setType(type);
                    fg.setData(jsonObject.getAsJsonArray("tasks"));
                    ((GHWFragmentActivity) DispatchTaskFragment.this.getFragmentActivity()).pushFragment(fg, true);
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }
}
