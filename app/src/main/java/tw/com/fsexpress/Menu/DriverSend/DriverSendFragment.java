package tw.com.fsexpress.Menu.DriverSend;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.ui.GHWIndexPath;
import tw.com.fsexpress.framework.ui.GHWListView;
import tw.com.fsexpress.framework.ui.GHWListView.GHWBaseAdapter;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;
import tw.com.fsexpress.utils.PromptDialog;


public class DriverSendFragment extends GHWFragment implements AbsListView.OnScrollListener {

    private static final String TAG = "DriverSend";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;
    private Spinner spinner = null;

    private GHWListView listView = null;
    private TextView txtNotRead = null;
    private List<DataObject> arr_data = new ArrayList<DataObject>();
    private DriverSendFragment.ListViewHolder holder;

//    private MediaPlayer mediaPlayer;

    private int type = 0;

    //是否正在更新
    boolean isUpdating = false;
    //下拉選單的類別
    String strListOrderType = "4";

    static class ListViewHolder {
        LinearLayout back;
        TextView position;
        TextView send_person;
        TextView send_address;
        TextView send_count;
        TextView send_DR;
    }


    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);


        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("行動派遣");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((GHWFragmentActivity) DriverSendFragment.this.getFragmentActivity()).popFragment();

                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0) {
                    ((GHWFragmentActivity) DriverSendFragment.this.getFragmentActivity()).popFragment();
                } else {
                    RequestResultListDialog.instance(DriverSendFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "行動派遣", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }

            }
        });


        this.spinner = (Spinner) view.findViewById(R.id.tvOrderType);

        this.arr_data.clear();

        // 取得預設排序
        String type = AccountManager.instance().getListOrderType(DriverSendFragment.this.getFragmentActivity());
        if (type != null) {
            strListOrderType = type;
        } else {
            strListOrderType = "4"; // 沒有預設的話就預設地址排序(排除已集貨)
        }
        spinner.setSelection(Integer.parseInt(strListOrderType) - 1, false);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        strListOrderType = "1";
                        AccountManager.instance().saveListOrderType(DriverSendFragment.this.getFragmentActivity(), "1");
                        break;
                    case 1:
                        strListOrderType = "2";
                        AccountManager.instance().saveListOrderType(DriverSendFragment.this.getFragmentActivity(), "2");
                        break;
                    case 2:
                        strListOrderType = "3";
                        AccountManager.instance().saveListOrderType(DriverSendFragment.this.getFragmentActivity(), "3");
                        break;
                    default:
                        strListOrderType = "4";
                        AccountManager.instance().saveListOrderType(DriverSendFragment.this.getFragmentActivity(), "4");
                }
                Log.e("strListOrderType", strListOrderType);
//                if ( strListOrderType.equals("0") ) //請選擇
//                {
//                    //Constants.toast(DriverSendFragment.this.getFragmentActivity(), "沒有正確選擇排序項目");
//                    return ;
//                }
//                else if ( strListOrderType.equals("1") ) //預設排序(包含已集貨)
//                {
//
//                }
//                else if ( strListOrderType.equals("2") ) //預設排序(排除已集貨)
//                {
//
//                }
//                else if ( strListOrderType.equals("3") ) //地址排序(包含已集貨)
//                {
//
//                }
//                else if ( strListOrderType.equals("4") ) //地址排序(排除已集貨)
//                {
//
//                }
                DriverSendFragment.this.requestGetAppList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ((TextView) view.findViewById(R.id.txt_checkupdate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DriverSendFragment.this.requestGetAppList();
            }
        });

        this.listView = (GHWListView) view.findViewById(R.id.list_reverse);
        this.txtNotRead = (TextView) view.findViewById(R.id.txt_notReadCount);
        this.setListView();

        this.requestGetAppList();
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        strListOrderType = "4";
        spinner.setSelection(3); // 預設地址排序(排除已集貨)
    }

    private void setListView() {
        this.listView.delegate = new GHWListView.Delegate() {
            @Override
            public void didSelectRowAtIndexPath(View view, GHWIndexPath indexPath) {
                if (type == 0) {

                    Log.e("indexPath.row", "" + indexPath.row);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt("DRAIVERSEND_SAVE_POSITION", indexPath.row);
                    editor.commit();

                    DriverSendDetailFragment fg = (DriverSendDetailFragment) new DriverSendDetailFragment().initWithLayoutResourceId(R.layout.layout_driversend_detail);
                    DataObject d = arr_data.get(indexPath.row);
                    fg.setData(d);
                    ((GHWFragmentActivity) DriverSendFragment.this.getFragmentActivity()).pushFragment(fg, true);
                }
            }
        };

        this.listView.dataSource = new GHWListView.DataSource() {
            @Override
            public int numberOfSectionsInListView(GHWBaseAdapter arg0) {
                return 0;
            }

            @Override
            public int numberOfRowsInSection(GHWBaseAdapter arg0, int arg1) {
                return arr_data.size();
            }

            @Override
            public View getSectionView(GHWListView arg0, View arg1, int arg2) {
                return null;
            }

            @SuppressLint("SetTextI18n")
            @Override
            public View getItemView(GHWListView parent, View cell, GHWIndexPath indexPath) {
                if (cell == null) {
                    cell = DriverSendFragment.this.getFragmentActivity().getLayoutInflater().inflate(R.layout.cell_driversend, null);

                    holder = new DriverSendFragment.ListViewHolder();
                    holder.back = (LinearLayout) cell.findViewById(R.id.ll_bg);
                    holder.position = (TextView) cell.findViewById(R.id.txt_position);
                    holder.send_person = (TextView) cell.findViewById(R.id.txt_send_person);
                    holder.send_address = (TextView) cell.findViewById(R.id.txt_send_address);
                    holder.send_count = (TextView) cell.findViewById(R.id.txt_count);
                    holder.send_DR = (TextView) cell.findViewById(R.id.txt_DR);
                    cell.setTag(holder);

                } else {
                    holder = (DriverSendFragment.ListViewHolder) cell.getTag();
                }

                holder.position.setText(indexPath.row + 1 + ".");

                DataObject d = arr_data.get(indexPath.row);
                boolean isRead = Boolean.parseBoolean(String.format("%s", d.getVariable("is_read").toString().equals("0"))); // 未讀

                holder.position.setTextColor(ContextCompat.getColor(DriverSendFragment.this.getFragmentActivity(), R.color.white));
                holder.send_person.setTextColor(ContextCompat.getColor(DriverSendFragment.this.getFragmentActivity(), R.color.white));
                holder.send_count.setTextColor(ContextCompat.getColor(DriverSendFragment.this.getFragmentActivity(), R.color.white));
                holder.send_address.setTextColor(ContextCompat.getColor(DriverSendFragment.this.getFragmentActivity(), R.color.white));
                if (isRead) {
                    holder.back.setBackgroundResource(R.drawable.ripple_dark_orange_radius);
                } else {
                    holder.back.setBackgroundResource(R.drawable.ripple_dark_gray_radius);
                }

                String strName = "";
                if (d.getVariable("customer_code").toString().equals("F1300600002")) {
                    strName = "(甲配)";
                }
                holder.send_person.setText(String.format("%s", strName + d.getVariable("send_contact").toString()));
                holder.send_count.setText(String.format("%s", d.getVariable("pieces").toString() + "件"));

                try {
                    String DR = "";
                    DR = String.format("%s", d.getVariable("DeliveryType").toString());
                    Log.e("-------DeliveryType----------", DR);
                    holder.send_DR.setText(DR);
                    if (DR.equals("D")) {
                        holder.send_DR.setText("正");
                        holder.send_DR.setTextColor(ContextCompat.getColor(DriverSendFragment.this.getFragmentActivity(), R.color.driver_send_fragment_green));
                    } else if (DR.equals("R")) {
                        holder.send_DR.setText("逆");
                        holder.send_DR.setTextColor(ContextCompat.getColor(DriverSendFragment.this.getFragmentActivity(), R.color.driver_send_fragment_red));
                    }
                } catch (Exception ex) {
                    Log.e("-------ERROR----------", "---------");
                }

                holder.send_address.setText(String.format("%s", d.getVariable("send_cityarea").toString() + d.getVariable("send_address").toString()));

                return cell;
            }
        };
        this.listView.setOnScrollListener(this);

        //https://stackoverflow.com/questions/29374894/programmatically-set-position-in-listview-without-scrolling

//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
//        Editor editor = prefs.edit();
//        editor.putBoolean(PREF_NAME, false);
//        editor.commit();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                 DriverSendFragment.this.listView.setSelection(3);
//            }
//        }, 2000);

        //this.listView.setSelectionFromTop();
        //this.listView.setVerticalScrollbarPosition(10);


//        listView.post(new Runnable() {
//            @Override
//            public void run() {
//
//                int idx = listView.getFirstVisiblePosition();
//                View first = listView.getChildAt(4);
//                int position = 0;
//                if (first != null)
//                    position = first.getTop();
//
//                listView.setSelectionFromTop(4,4);
//            }
//        });

        //this.listView.post(() -> listView.smoothScrollToPosition(3));
//        this.listView.setSelection(3);
//        this.listView.smoothScrollToPosition(3);
    }


    protected void requestGetAppList() {

        if (isUpdating) {
            Constants.ErrorAlertDialog(DriverSendFragment.this.getFragmentActivity(), "行動派遣", "資料正在更新中", "確定", null, null, null);
            return;
        }

        Constants.toast(DriverSendFragment.this.getFragmentActivity(), "清單取得中.....");

        isUpdating = true;
        Log.e("test", "start------------------");
        Log.e("test", "start------------------");
        Log.e("test", "start------------------");
        Log.e("strListOrderType", strListOrderType);

//        AlertDialog dialog = Constants.ProgressBar(DriverSendFragment.this.getFragmentActivity(), "讀取中...請稍候");
//        dialog.show();

        this.arr_data.clear();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("driver_code", AccountManager.instance().getDriverCode(DriverSendFragment.this.getFragmentActivity()));
        params.put("ListOrderType", strListOrderType);
        Log.d("test", "driver_code:" + AccountManager.instance().getDriverCode(DriverSendFragment.this.getFragmentActivity()));
        Log.d("test", "ListOrderType:" + strListOrderType);

        RequestManager.instance().doRequest(Constants.API_GET_Driver_Send_Prepare_GetAppList, "POST", params, new RequestManager.RequestManagerCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JsonObject jsonObject) {

                //dialog.cancel();
                Log.e("---test---", "----4----");
                Log.e("---test---", jsonObject.toString());

                JsonArray jArray = jsonObject.get("result").getAsJsonArray();
                Log.e("---test---", "----5----");
                Log.e("---test---", jArray.toString());
                //return ;
//                //Log.e("---test---", jArray.toString());
//
//                //JsonArray jArray = new JsonArray();
//                //JsonParser parser = new JsonParser();
//                //jArray = parser.parse(jsonObject.toString()).getAsJsonArray();
//
//                JsonArray jArray = new Gson().fromJson(jsonObject.toString(), JsonArray.class);
//                Log.d("test",  String.valueOf(jArray.size()) );

                if (jArray.size() < 1) {
                    Constants.showPromptDialog(getFragmentActivity(), "提示", "目前沒有派遣任務", new PromptDialog.ButtonListener() {
                        @Override
                        public void onClick(View param1View) {

                        }
                    });
                }
//
//
                List<String> keys = Arrays.asList("customer_code", "send_cityarea", "send_address", "send_contact", "send_tel", "send_station",
                        "driver_code", "YYYYMMDDHHMM", "YYYYMMDDHHMM_max",
                        "is_read", "counts", "pieces", "collects",
                        "driver_name", "customer_name", "customer_shortname",
                        "shipments_city", "shipments_area", "shipments_road", "telephone",
                        "packageType", "vechileType", "pickUpDate", "remark", "target_date", "id_list", "id_list2", "DeliveryType"
                );
//
                int notRead = 0; // 計算未讀數量
                for (int i = 0; i < jArray.size(); i++) {
                    JsonObject jObj = (JsonObject) jArray.get(i);

                    DataObject d = new DataObject();
                    for (String key : keys) {

                        String value = !jObj.get(key).isJsonNull() ? jObj.get(key).getAsString() : "";
                        d.setVariable(key, value);
                        if (Objects.equals(key, "is_read") && Objects.equals(value, "0")) {
                            notRead++;
                        }
                    }
                    arr_data.add(d);
                }

//                if (notRead > 0) {
//                    mediaPlayer = MediaPlayer.create(DriverSendFragment.this.getFragmentActivity(), R.raw.sound);
//                    mediaPlayer.start();
//                }
                listView.reloadData();
                txtNotRead.setText(Integer.toString(notRead));

//                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
//                Editor editor = prefs.edit();
//                editor.putBoolean(PREF_NAME, false);
//                editor.commit();
//
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                int iSelectionIndex = prefs.getInt("DRAIVERSEND_SAVE_POSITION", 0);

                SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = prefs2.edit();
                editor.putInt("DRAIVERSEND_SAVE_POSITION", 0);
                editor.commit();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DriverSendFragment.this.listView.setSelection(iSelectionIndex);
                    }
                }, 100);

                isUpdating = false;
            }

            @Override
            public void onFail(String reason) {
                //dialog.cancel();
                isUpdating = false;
            }
        });
    }

    @Override
    public void onScroll(AbsListView arg0, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    @Override
    public void onScrollStateChanged(AbsListView arg0, int scrollState) {

    }


//    @Override
//    public void onStop() {
//        if (mediaPlayer != null) {
//            mediaPlayer.release();
//        }
//        super.onStop();
//    }
}
