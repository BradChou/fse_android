package tw.com.fsexpress.Menu.transfer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.GPSLocation;
import tw.com.fsexpress.Menu.RfidCage.CageTestFragment;
import tw.com.fsexpress.R;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.utils.PromptDialog;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TransferActivity extends Activity implements BarcodeCallback, View.OnClickListener {

    private static final String TAG = "ALVIN";

    public static final String TYPE = "TYPE";
    public static final String SCAN_ITEM = "SCAN_ITEM";
    private int type = 1; // 1: 站所盤點 | 2: 連運轉出

    private String scan_item = "1"; // 站所盤點 區分盤點|持回 1: 盤點| 2: 持回

    private long DELAY_TIME = 1500L;

    public static final String INVENTORY = "1";
    public static final String HOLD_BACK = "2";


    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private TextView goodsNum;
    private boolean isStop = false;
    private List<String> arrBarcode = new ArrayList<String>();
    private int scanItem = 0;
    private ScrollView scrollView;
    private ToneGenerator toneG;
    private TextView txtScanNum;
    private String strGoods = "";

    // page
    private Button btnToday = null, btnBack = null, btnChangeMode = null, btnSend = null;
    private TextView btnSave = null;
    private EditText editGoodNum = null;

    private int scanMode = 1; // 1: 掃描 | 2: keyin

    private List<DataObject> arr_data = new ArrayList<DataObject>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_transfer);

        Intent i = this.getIntent();
        Bundle b = i.getExtras();

        if (b.keySet().contains(TYPE)) {
            this.type = b.getInt(TYPE);
        }

        if (b.keySet().contains(SCAN_ITEM)) {
            this.scan_item = b.getString(SCAN_ITEM);
        }

        Log.e("------------ this.type -----------" ,  ""+this.type );
        this.scrollView = (ScrollView) findViewById(R.id.scrollView);
        this.txtScanNum = (TextView) findViewById(R.id.txt_total_num);
        this.goodsNum = (TextView) findViewById(R.id.goods_num);

        this.btnToday = (Button) findViewById(R.id.btn_today);
        this.btnBack = (Button) findViewById(R.id.btn_back);
        this.btnChangeMode = (Button) findViewById(R.id.change_mode);
        this.btnSend = (Button) findViewById(R.id.btn_send);

        this.btnSave = (TextView) findViewById(R.id.btn_save);
        this.editGoodNum = (EditText) findViewById(R.id.edit_good_num);
        this.editGoodNum.requestFocus();
        this.editGoodNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                if (count == 1 && s.toString().length() >= 10){
//                    arrBarcode.add(s.toString().trim());
//                    CollectionFragment.this.editGoodNum.setText("");
//                    updateBarcodeList();
//                }

                if(count >= 10) {
                    arrBarcode.add(s.toString().trim());
                    TransferActivity.this.editGoodNum.setText("");
                    updateBarcodeList();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        this.btnToday.setOnClickListener(this);
        this.btnBack.setOnClickListener(this);
        this.btnChangeMode.setOnClickListener(this);
        this.btnSend.setOnClickListener(this);
        this.btnSave.setOnClickListener(this);

        this.barcodeView = (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.setScanMode(false);

        try
        {
            doGPS();
        }
        catch(Exception ex)
        {

        }

        this.requestGetTransInOrOutData();


    }

    @Override
    public void onBackPressed()
    {
        if ( TransferActivity.this.arrBarcode.size() != 0 )
        {
            //Constants.toast(this, "您有貨號尚未上傳");
            //return;

//            Constants.showPromptDialog(  TransferActivity.this, "提示", "您有貨號尚未上傳", new PromptDialog.ButtonListener() {
//                @Override
//                public void onClick(View param1View) {
//
//                }
//            });

            new AlertDialog.Builder(this)
                    .setTitle("提示")
                    .setMessage("您有貨號尚未上傳 , 確認是否離開")
                    .setPositiveButton("確認離開", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            TransferActivity.super.onBackPressed();
                        }})
                    .setNegativeButton("保留資料", null).show();


            return;
        }
        else
        {
            super.onBackPressed();
        }
    }

    private void updateBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtScanNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.goodsNum.setText(strGoods.substring(1));
        }else{
            this.txtScanNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.goodsNum.setText("");
        }
    }

    private void addResultToList(String paramString) {

        if (!this.isStop && this.scanMode == 1) {
            if (!this.updateShowText(paramString)) {
                return;
            }
            strGoods = strGoods + "\n" + paramString;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("result: ");
            stringBuilder.append(paramString);
            this.arrBarcode.add(paramString);
            this.scrollView.fullScroll(130);
            this.txtScanNum.setText(String.valueOf(this.arrBarcode.size()));
            this.goodsNum.setText(strGoods);
            beepAndPause();

//            beepAndPause();
//            if (this.scanItem == 3)
//                backWithResult();
        }
    }

    private boolean addGoodNumToList(String num) {
        if (this.scanMode == 2) {
            if (!this.updateShowText(num)) {
                return false;
            }
            strGoods = strGoods + "\n" + num;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("result: ");
            stringBuilder.append(num);
            this.arrBarcode.add(num);
            this.scrollView.fullScroll(130);
            this.txtScanNum.setText(String.valueOf(this.arrBarcode.size()));
            this.goodsNum.setText(strGoods);

            return true;
        }

        return false;
    }

    private boolean updateShowText(String text) {
        if (text.length() < 10) {
            Constants.toast(this,"貨號小於10位，請檢查");
            return false;
        }

        return true;
    }

    private void beepAndPause() {
        if (this.toneG == null)
            this.toneG = new ToneGenerator(2, 100);
        this.toneG.startTone(24);
        this.isStop = true;
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TransferActivity.this.isStop = false;
            }
        }, DELAY_TIME);
    }

    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        JSONArray arrJson = new JSONArray(this.arrBarcode);
        TransferActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
        TransferActivity.this.setResult(RESULT_OK, getIntent());
        TransferActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.scanMode == 1){
            this.barcodeView.resume();
        }
//        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        if (this.scanMode == 1){
            this.barcodeView.pause();
        }
//        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {

        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            default:
                break;
            case R.id.change_mode:
                this.setScanMode(false);
                break;
            case R.id.btn_today:
                TransferListDialog.getDialog(this).setData(this.arr_data).setName(this.type == 1 ? "站所盤點" : "聯運轉出").show();
                break;
            case R.id.btn_back:
                if(this.arrBarcode.size() != 0) {
                    //Constants.toast(this, "您有貨號尚未上傳");
                    //return;

                    new AlertDialog.Builder(this)
                            .setTitle("提示")
                            .setMessage("您有貨號尚未上傳 , 確認是否離開")
                            .setPositiveButton("確認離開", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    TransferActivity.this.finish();
                                }})
                            .setNegativeButton("保留資料", null).show();
                }
                else
                {
                    TransferActivity.this.finish();
                }

                break;
            case R.id.btn_send:
                this.requestSendData();
                break;
            case R.id.btn_save:
                if (this.addGoodNumToList(this.editGoodNum.getText().toString()))
                    this.editGoodNum.setText("");
                break;
        }
    }

    private void setScanMode(boolean isInit) {
        if (!isInit)
            this.scanMode = this.scanMode == 1 ? 2 : 1;

        if (this.scanMode == 1){
            this.barcodeView.resume();
        }

        if (this.scanMode == 2){
            this.barcodeView.pause();
        }

        this.barcodeView.setVisibility(this.scanMode == 1 ? View.VISIBLE : View.INVISIBLE);
    }

    protected void requestSendData() {

        try {
            JSONObject mData = AccountManager.instance().getMemberInfo(this);
            HashMap<String, String> params = new HashMap<String, String>();
            Log.e("Jerry", "station:"+mData.getString("station"));
            params.put("station", mData.getString("station"));
            params.put("check_number", TextUtils.join(",", this.arrBarcode));
            params.put("driver_code", AccountManager.instance().getDriverCode(TransferActivity.this));
//            params.put("driver_code", "z010008");
            params.put("scan_date", Constants.getTime());

            if ( this.type == 1 )
            {
                params.put("wgs_x", x);
                params.put("wgs_y", y);
                params.put("scan_item", scan_item);
            }

            String url = this.type == 1 ? Constants.API_POST_SEND_INVENTORY : Constants.API_POST_SEND_TRANSFER;

            Log.e("Jerry", "current url:"+url);
            RequestManager.instance().doRequest(url, "POST", params, new RequestManager.RequestManagerCallback() {
                @Override
                public void onResponse(JsonObject jsonObject) {

                    Log.e("Jerry", jsonObject.toString());

                    if (jsonObject.get("resultcode").getAsBoolean()) {
                        Constants.toast(TransferActivity.this, "傳送成功");
                        arrBarcode.clear();
                        txtScanNum.setText("0");
                        strGoods = "";
                        goodsNum.setText("");

//                        requestGetTransInOrOutData();
                    }
                }

                @Override
                public void onFail(String reason) {

                }
            });
        } catch (Exception e) {

        }

    }

    protected void requestGetTransInOrOutData() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("sdate", "");
        params.put("driver_code", AccountManager.instance().getDriverCode(TransferActivity.this));
//        params.put("driver_code", "z010008");
        params.put("edate", Constants.getTime());
        String url = this.type == 1 ? Constants.API_POST_GET_INVENTORY_DATA : Constants.API_POST_GET_TRANSFER_DATA;
        RequestManager.instance().doRequest(url, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (jsonObject.get("resultcode").getAsBoolean()) {

                    Log.e(TAG, "trans in or out : " + jsonObject.toString());
                    arr_data.clear();

                    List<String> keys = Arrays.asList("id", "station_code", "station_name", "driver_code", "driver_name",
                            "scan_date", "check_number");
                    for (JsonElement o : jsonObject.get("data").getAsJsonArray()) {

                        DataObject d = new DataObject();
                        for (String key : keys) {
                            String value = o.getAsJsonObject().get(key).getAsString();
                            d.setVariable(key, value);
                        }
                        arr_data.add(d);
                    }
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }


    private String x = "--";
    private String y = "--";

    private void doGPS()
    {
        Log.e("Location", "start");
        GPSLocation location = GPSLocation.instance(this, false);

        location.setCallback(new GPSLocation.GPSCallback() {
            @Override
            public void onLocation(Location location) {
                Log.e("Location", "getLongitude:" + location.getLongitude() + "getLatitude:" + location.getLatitude());

                TransferActivity.this.x = String.valueOf(location.getLongitude());
                TransferActivity.this.y = String.valueOf(location.getLatitude());
            }

            @Override
            public void onUserAccept() {
                Log.e("Location", "onUserAccept");
            }

            @Override
            public void onUserReject() {
                Log.e("Location", "onUserReject");
            }
        });
        location.StartLocation(this);
    }

}
