package tw.com.fsexpress.Menu.send;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.CustomFragment;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SendFragment extends CustomFragment {

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private EditText editGoodNum = null, editPlatesNum, editShiftNum;
    private TextView txtGoodNum, txtTotalNum, txtBarcode, txtGoodNumFail;
    private Button btnLeft, btnRight;
    private ScrollView scrollView,scrollViewFail;
    private ToneGenerator toneGenerator;
    private boolean isAuto = false;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        if (this.editGoodNum == null){
            this.editGoodNum = (EditText)view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄
                        isAuto = true;
                        SendFragment.this.editGoodNum.setText("");
                        requestUpload(s.toString().trim());


                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入
                        isAuto = false;
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);
        this.txtTotalNum = (TextView)view.findViewById(R.id.txt_total_num);

        this.scrollView = (ScrollView)view.findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollViewFail = (ScrollView)view.findViewById(R.id.scrollViewFail);
        this.scrollViewFail.fullScroll(130);
        this.txtGoodNum = (TextView)view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);


        this.editPlatesNum = (EditText)view.findViewById(R.id.edit_plates_num);
        this.editShiftNum = (EditText)view.findViewById(R.id.edit_shift_num);

        TextView txtTitle = (TextView)view.findViewById(R.id.txt_title);
        txtTitle.setText("發送");

        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) SendFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(SendFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "發送", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });

        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);



        view.findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendFragment.this.getFragmentActivity().startActivityForResult(new Intent((Context)SendFragment.this.getFragmentActivity(), SendBarcodeActivity.class), 5);
            }
        });

        view.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isAuto){
                    String strGoodNum = SendFragment.this.editGoodNum.getText().toString();
                    if (strGoodNum.equals("")){
                        Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "請確認您的貨號是否正確。", "確定", null, null, null);
                        return;
                    }else{

                        if (strGoodNum.length() < 10){
                            Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "請確認您的貨號是否正確。", "確定", null, null, null);
                            return;
                        }
                    }
                    requestUpload(strGoodNum);
                }else{
                    Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "現在為自動上傳模式，如要手動上傳請\"點擊貨號輸入框並輸入條碼\"。", "確定", null, null, null);
                }

//                String strGoodNum = SendFragment.this.editGoodNum.getText().toString();
//                if (!strGoodNum.equals("")){
//                    if (strGoodNum.length() < 10){
//                        Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "請確認您的貨號是否正確。", "確定", null, null, null);
//                        return;
//                    }
//
//                    arrBarcode.add(strGoodNum);
//
//                }else{
//
//                    if (arrBarcode.size() == 0){
//                        Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "請確認您的貨號是否正確。", "確定", null, null, null);
//                        return;
//                    }
//
//                }
//
//
//                if (SendFragment.this.editPlatesNum.getText().toString().equals("")){
//                    Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "卸集", "請填寫板數", "確定", null, null, null);
//                    return;
//                }
//
//                requestMultipleUpload();

            }
        });

    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){

        if (SendFragment.this.editPlatesNum.getText().toString().equals("")){
            Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "請填寫板數", "確定", null, null, null);
            return;
        }

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "7");
        data.put("driver_code", AccountManager.instance().getDriverCode(SendFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("receive_option", "");
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", this.editShiftNum.getText().toString());
        data.put("plates", this.editPlatesNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(SendFragment.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    if (isAuto != true){
                        SendFragment.this.editGoodNum.setText("");
                        Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "上傳成功", "確定", null, null, null);
                    }
                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("7", "發送", barcode);
                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {
                Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "卸集", reason, "確定", null, null, null);
            }
        });


    }

//    private void requestMultipleUpload(){
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//        String strScanTime = Constants.getTime();
//
//        int idx = 0;
//        for (String barcode:this.arrBarcode) {
//
//            HashMap<String, String> data = new HashMap<String, String>();
//            data.put("scan_item", "7");
//            data.put("driver_code", AccountManager.instance().getDriverCode(SendFragment.this.getFragmentActivity()));
//            data.put("check_number", barcode);
//            data.put("scan_date", strScanTime);
//            data.put("area_arrive_code", "");
//            data.put("platform", "");
//            data.put("car_number", "");
//            data.put("sowage_rate", "");
//            data.put("area", "");
//            data.put("exception_option", "");
//            data.put("arrive_option", "");
//            data.put("ship_mode", "");
//            data.put("goods_type", "");
//            data.put("pieces", "");
//            data.put("weight", "");
//            data.put("runs", this.editShiftNum.getText().toString());
//            data.put("plates", this.editPlatesNum.getText().toString());
//            data.put("sign_form_image", "");
//            data.put("sign_field_image", "");
//            data.put("coupon", "");
//            data.put("cash", "");
//
//            DataObject requestData = new DataObject();
//            requestData.setVariable("param", data);
//            requestData.setVariable("barcode", barcode);
//            requestData.setVariable("url", Constants.API_POST_SEND_UPLOAD_DATA);
//            requestData.setVariable("method", "POST");
//            requestData.setVariable("id", String.format("%s%d",Constants.md5(data.toString()),idx));
//            RequestMutilController.instance().addRequest(requestData);
//            idx++;
//        }
//
//        RequestMutilController.instance().setCallback(new RequestMutilController.RequestMutilControllerCallback() {
//
//
//            @Override
//            public void onFinish(List<DataObject> data) {
//
//                dialog.cancel();
//                RequestResultListDialog.instance(SendFragment.this.getFragmentActivity()).showUploadDialog(data, "發送");
////                Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//
//                for (String barcode:arrBarcode) {
//                    TransferDataManager.instance().insertTransferData("7", "發送", barcode);
//                }
//
//                arrBarcode.clear();
//                updateBarcodeList();
//
//            }
//
//        });

//        RequestMutilController.instance().setCallback(() -> {
//
//
//            dialog.cancel();
//            Constants.ErrorAlertDialog(SendFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//
//            TransferDataManager.instance().insertTransferData("7", String.valueOf(arrBarcode.size()));
//
//            arrBarcode.clear();
//            updateBarcodeList();
//
//        });


//        RequestMutilController.instance().startRequest();
//    }


}
