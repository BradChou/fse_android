package tw.com.fsexpress.Menu.Ships;

import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.CustomFragment;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.DeliveryData;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;

// 配送
public class ShipsFragment extends CustomFragment {

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private TextView txtGoodNum, txtTotalNum;
    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;
    private ScrollView scrollView;
    private int BARCODE_BACK = 5;
    private EditText editGood = null, editPiece = null, editPlate = null;
    private TextView textUnsave = null, textTotal = null, textGoodsNum = null, txtBarcode = null, txtGoodNumFail = null;
    private Spinner spinStatus = null;
    private String abnormalStatus = "";
//    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.誤訂", "3.多出"};
    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.短少", "3.多出", "4.喪失", "5.誤訂"};
    private int maxLine = 3;
    private int currentRequestIndex = 0;
    private ToneGenerator toneGenerator;
    private boolean isAuto = false;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("配送");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) ShipsFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(ShipsFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "配送", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        if (this.editGood == null){
            this.editGood = (EditText) view.findViewById(R.id.edit_good_num);
            this.editGood.requestFocus();
            this.editGood.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄
                        isAuto = true;
                        ShipsFragment.this.editGood.setText("");
                        requestUpload(s.toString().trim());


                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入
                        isAuto = false;
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }else{
            this.editGood.requestFocus();
        }

        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);

        this.txtTotalNum = (TextView)view.findViewById(R.id.txt_total_num);
        this.scrollView = (ScrollView)view.findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollView.smoothScrollTo(0,0);
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);
        this.txtGoodNum = (TextView)view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);

        this.editPiece = (EditText) view.findViewById(R.id.edit_good_pieces_num);
        this.editPiece.clearFocus();
        this.editPiece.setText("0");
        this.editPlate = (EditText) view.findViewById(R.id.edit_plates_num);
        this.editPlate.setText("0");
        this.editPlate.clearFocus();

//        this.textUnsave = (TextView) view.findViewById(R.id.txt_accumulation_num);
//        this.textUnsave.setText("0");
        this.textTotal = (TextView) view.findViewById(R.id.txt_total_num);
        this.textTotal.setText("0");

        this.textGoodsNum = (TextView) view.findViewById(R.id.goods_num);

        this.spinStatus = (Spinner) view.findViewById(R.id.spinner_abnormal_status);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getFragmentActivity(), R.layout.support_simple_spinner_dropdown_item, abnormalArray);
        this.spinStatus.setAdapter((SpinnerAdapter)arrayAdapter);
        this.spinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                abnormalStatus = abnormalArray[position];
                String strStatus = abnormalArray[position];
                abnormalStatus = strStatus.substring(0, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShipsFragment.this.doScan();
            }
        });

        view.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isAuto){
                    String strGoodNum = ShipsFragment.this.editGood.getText().toString();
                    if (strGoodNum.equals("")){
                        Constants.ErrorAlertDialog(ShipsFragment.this.getFragmentActivity(), "配送", "請確認您的貨號是否正確。", "確定", null, null, null);
                        return;
                    }else{

                        if (strGoodNum.length() < 10){
                            Constants.ErrorAlertDialog(ShipsFragment.this.getFragmentActivity(), "配送", "請確認您的貨號是否正確。", "確定", null, null, null);
                            return;
                        }
                    }
                    requestUpload(strGoodNum);
                }else{
                    Constants.ErrorAlertDialog(ShipsFragment.this.getFragmentActivity(), "配送", "現在為自動上傳模式，如要手動上傳請\"點擊貨號輸入框並輸入條碼\"。", "確定", null, null, null);
                }

            }
        });
    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "2");
        data.put("driver_code", AccountManager.instance().getDriverCode(ShipsFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");
        data.put("receive_option", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", abnormalStatus);
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", ShipsFragment.this.editPiece.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", ShipsFragment.this.editPlate.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){


                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(ShipsFragment.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    if (isAuto != true){
                        ShipsFragment.this.editGood.setText("");
                        Constants.ErrorAlertDialog(ShipsFragment.this.getFragmentActivity(), "配送", "上傳成功", "確定", null, null, null);
                    }

                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("2", "配送", barcode);
                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });

        insertData(barcode);
    }

    // 將掃讀成功的資料寫進room
    private void insertData(String checkNumber) {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(ShipsFragment.this.getFragmentActivity()).toUpperCase();
            DeliveryData deliveryData = new DeliveryData(UUID.randomUUID().toString(), driverCode, checkNumber, Constants.getTime(), Constants.getNowTimestamp());
            AppDataBase.getInstance(ShipsFragment.this.getFragmentActivity()).getDeliveryDao().insertData(deliveryData);
        }).start();
    }

//    private void requestMultipleUpload(){
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//
//        String strScanTime = Constants.getTime();
//        int idx = 0;
//        for (String barcode:this.arrBarcode) {
//
//            HashMap<String, String> data = new HashMap<String, String>();
//            data.put("scan_item", "2");
//            data.put("driver_code", AccountManager.instance().getDriverCode(ShipsFragment.this.getFragmentActivity()));
//            data.put("check_number", barcode);
//            data.put("scan_date", strScanTime);
//            data.put("area_arrive_code", "");
//            data.put("platform", "");
//            data.put("car_number", "");
//            data.put("sowage_rate", "");
//            data.put("area", "");
//            data.put("exception_option", abnormalStatus);
//            data.put("arrive_option", "");
//            data.put("ship_mode", "");
//            data.put("goods_type", "");
//            data.put("pieces", ShipsFragment.this.editPiece.getText().toString());
//            data.put("weight", "");
//            data.put("runs", "");
//            data.put("plates", ShipsFragment.this.editPlate.getText().toString());
//            data.put("sign_form_image", "");
//            data.put("sign_field_image", "");
//            data.put("coupon", "");
//            data.put("cash", "");
//
//            DataObject requestData = new DataObject();
//            requestData.setVariable("param", data);
//            requestData.setVariable("barcode", barcode);
//            requestData.setVariable("url", Constants.API_POST_SEND_UPLOAD_DATA);
//            requestData.setVariable("method", "POST");
//            requestData.setVariable("id", String.format("%s%d",Constants.md5(data.toString()),idx));
//            RequestMutilController.instance().addRequest(requestData);
//            idx++;
//        }
//
//        RequestMutilController.instance().setCallback(new RequestMutilController.RequestMutilControllerCallback() {
//
//
//            @Override
//            public void onFinish(List<DataObject> data) {
//
//                dialog.cancel();
//                RequestResultListDialog.instance(ShipsFragment.this.getFragmentActivity()).showUploadDialog(data, "配送");
////                Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//
////                TransferDataManager.instance().insertTransferData("7", String.valueOf(arrBarcode.size()));
//                for (String barcode:arrBarcode) {
//                    TransferDataManager.instance().insertTransferData("2", "配送", barcode);
//                }
//                arrBarcode.clear();
//                updateBarcodeList();
//
//            }
//
//        });
//
//        RequestMutilController.instance().startRequest();
//    }

    public void doScan() {
        startActivityForResult(new Intent(this.getFragmentActivity(), ShipsBarcodeActivity.class), BARCODE_BACK);
    }
}
