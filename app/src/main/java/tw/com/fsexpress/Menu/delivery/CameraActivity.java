package tw.com.fsexpress.Menu.delivery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import tw.com.fsexpress.R;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CameraActivity extends Activity implements SurfaceHolder.Callback {
    private static final String TAG = "ALVIN";

    Camera.AutoFocusCallback autoFacusCallback = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean param1Boolean, Camera param1Camera) {
            if (param1Boolean == true)
                CameraActivity.this.myCamera.takePicture(CameraActivity.this.shutterCallback, CameraActivity.this.rawCallback, CameraActivity.this.myJpegCallback);
        }
    };

    private Button btn_shoot;

    private String filename = "";

    ImageView iii;

    private boolean isPreview = false;

    private Bitmap mBitmap = null;

    DrawImageView mDrawIV;

    private FocusSurfaceView mPreviewSV = null;

    private int mWindowHeight;

    private int mWindowWidth;

    private Camera myCamera = null;

    Camera.PictureCallback myJpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] param1ArrayOfbyte, Camera param1Camera) {
            Log.i(TAG, "myJpegCallback:onPictureTaken...");
            if (param1ArrayOfbyte != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                mBitmap = BitmapFactory.decodeByteArray(param1ArrayOfbyte, 0, param1ArrayOfbyte.length, options);
                isPreview = false;
            }
            Matrix matrix = new Matrix();
            matrix.postRotate(90.0F);
            Bitmap bitmap2 = Bitmap.createBitmap(CameraActivity.this.mBitmap, 0, 0, CameraActivity.this.mBitmap.getWidth(), CameraActivity.this.mBitmap.getHeight(), matrix, true);
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("zzz1");
            stringBuilder2.append(Integer.toString(bitmap2.getWidth()));
            stringBuilder2.append("-");
            stringBuilder2.append(Integer.toString(bitmap2.getHeight()));
            Log.d("", stringBuilder2.toString());
            float f1 = bitmap2.getWidth();
            float f2 = bitmap2.getHeight();
            int i = CameraActivity.this.mPreviewSV.getw();
            int j = CameraActivity.this.mPreviewSV.geth();
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("長");
            stringBuilder2.append(j);
            stringBuilder2.append("寬");
            stringBuilder2.append(i);
            Log.d(TAG, stringBuilder2.toString());
            float f3 = i;
            float f4 = f1 / f3;
            float f5 = j;
            float f6 = f2 / f5;
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("ggdg");
            stringBuilder2.append(Float.toString(f4));
            stringBuilder2.append("dwdwdwddw");
            stringBuilder2.append(Float.toString(f6));
            Log.d(TAG, stringBuilder2.toString());
            RectF rectF = CameraActivity.this.mDrawIV.getFrameRect();
            float f7 = rectF.left;
            float f8 = rectF.top;
            float f9 = rectF.width();
            float f10 = rectF.height();
            rectF = CameraActivity.this.mDrawIV.getmFrameRect2();
            float f11 = rectF.left;
            float f12 = rectF.top;
            float f13 = rectF.width();
            float f14 = rectF.height();
            i = (int) (f7 * f4);
            j = (int) (f8 * f6);
            int k = (int) (f9 * f4);
            int m = (int) (f10 * f6);
            int n = (int) (f11 * f4);
            int i1 = (int) (f12 * f6);
            int i2 = (int) (f13 * f4);
            int i3 = (int) (f14 * f6);
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("picWidth");
            stringBuilder1.append(Float.toString(f1));
            stringBuilder1.append("picHeight");
            stringBuilder1.append(Float.toString(f2));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("preWidth");
            stringBuilder1.append(Float.toString(f3));
            stringBuilder1.append("preHeight");
            stringBuilder1.append(Float.toString(f5));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("preRW");
            stringBuilder1.append(Float.toString(f4));
            stringBuilder1.append("preRH");
            stringBuilder1.append(Float.toString(f6));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("frameLeft");
            stringBuilder1.append(Float.toString(f7));
            stringBuilder1.append("frameTop");
            stringBuilder1.append(Float.toString(f8));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("frameWidth");
            stringBuilder1.append(Float.toString(f9));
            stringBuilder1.append("frameHeight");
            stringBuilder1.append(Float.toString(f10));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("cropLeft");
            stringBuilder1.append(Integer.toString(i));
            stringBuilder1.append("cropTop");
            stringBuilder1.append(Integer.toString(j));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("cropWidth");
            stringBuilder1.append(Integer.toString(k));
            stringBuilder1.append("cropHeight");
            stringBuilder1.append(Integer.toString(m));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("cropLeft");
            stringBuilder1.append(Integer.toString(n));
            stringBuilder1.append("cropTop");
            stringBuilder1.append(Integer.toString(i1));
            Log.d(TAG, stringBuilder1.toString());
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("cropWidth");
            stringBuilder1.append(Integer.toString(i2));
            stringBuilder1.append("cropHeight");
            stringBuilder1.append(Integer.toString(i3));
            Log.d(TAG, stringBuilder1.toString());
            Bitmap bitmap4 = Bitmap.createBitmap(bitmap2, i, j, k, m);
            Bitmap bitmap3 = Bitmap.createBitmap(bitmap2, n, i1, i2, i3);
            CameraActivity cameraActivity1 = CameraActivity.this;
            Bitmap bitmap1 = cameraActivity1.createBitmap(cameraActivity1.reSize(bitmap4, 512), 20);
            CameraActivity cameraActivity2 = CameraActivity.this;
            bitmap3 = cameraActivity2.createBitmap(cameraActivity2.reSize(bitmap3, 512), 20);
            try {
                CameraActivity.this.myCamera.stopPreview();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("img1", CameraActivity.this.saveBitmap(bitmap1, "1"));
                bundle.putString("img2", CameraActivity.this.saveBitmap(bitmap3, "2"));
                intent.putExtras(bundle);
                CameraActivity.this.setResult(3, intent);
                CameraActivity.this.finish();
                return;
            } catch (Exception exception) {
                exception.printStackTrace();
                return;
            }
        }
    };

    Camera.Parameters myParam;

    Camera.PictureCallback myRawCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] param1ArrayOfbyte, Camera param1Camera) {
            Log.i("yan", "myRawCallback:onPictureTaken...");
        }
    };

    Camera.ShutterCallback myShutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            Log.i("yan", "myShutterCallback:onShutter...");
        }
    };

    private SurfaceHolder mySurfaceHolder = null;

    private Camera.Size optimalSize;

    Camera.Parameters param;

    private Camera.PictureCallback rawCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] param1ArrayOfbyte, Camera param1Camera) {
        }
    };

    private Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
        }
    };

    private String str_number;

    private String str_second;

    List<Camera.Size> suSize;

    int x;

    int y;

    private Bitmap createBitmap(Bitmap paramBitmap, int paramInt) {
        if (paramBitmap == null)
            return null;
        Bitmap bitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(getResources().getColor(R.color.white));
        paint.setTextSize(paramInt);
        canvas.drawBitmap(paramBitmap, 0.0F, 0.0F, null);
        canvas.drawText(gettime(), 0.0F, 20.0F, paint);
        canvas.drawText(this.str_number, 0.0F, 40.0F, paint);
        canvas.save();
        canvas.restore();
        return bitmap;
    }

    private String gettime() {
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date(System.currentTimeMillis()));
    }

    private void optimization(int paramInt1, int paramInt2) {
        for (int i = 0; i < this.suSize.size(); i++) {
            Camera.Size size = this.suSize.get(i);
            int j = size.width;
            int k = size.height;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(size.width);
            stringBuilder.append("X");
            stringBuilder.append(size.height);
            Log.d("size:", stringBuilder.toString());
            if (j > k) {
                if (j > paramInt1 && j <= paramInt2) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append(size.width);
                    stringBuilder.append("X");
                    stringBuilder.append(size.height);
                    Log.d(TAG, stringBuilder.toString());
                    this.param.setPreviewSize(size.width, size.height);
                    if (paramInt1 == 1000)
                        this.param.setPictureSize(size.width, size.height);
                }
            } else if (k > paramInt1 && k <= paramInt2) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(size.width);
                stringBuilder.append("X");
                stringBuilder.append(size.height);
                Log.d(TAG, stringBuilder.toString());
                this.param.setPreviewSize(size.width, size.height);
                if (paramInt1 == 1000)
                    this.param.setPictureSize(size.width, size.height);
            }
        }
    }

    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.layout_old_camera);
        paramBundle = getIntent().getExtras();
        this.str_second = paramBundle.getString("second");
        this.str_number = paramBundle.getString("number");
        this.mDrawIV = (DrawImageView) findViewById(R.id.drawIV);
        this.mWindowHeight = getWindowManager().getDefaultDisplay().getHeight();
        this.mWindowWidth = getWindowManager().getDefaultDisplay().getWidth();
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("長");
        stringBuilder3.append(this.mWindowHeight);
        stringBuilder3.append("寬");
        stringBuilder3.append(this.mWindowWidth);
        Log.d("長寬", stringBuilder3.toString());
        DrawImageView drawImageView = this.mDrawIV;
        int i = this.mWindowWidth;
        int j = this.mWindowHeight;
        drawImageView.setttt(0, 100, i, j / 3 + 150, i, j, this.str_second);
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("czs");
        stringBuilder2.append(Integer.toString(this.mWindowWidth));
        stringBuilder2.append(Integer.toString(this.mWindowHeight));
        Log.d("ccas", stringBuilder2.toString());
        this.mDrawIV.draw(new Canvas());
        this.mPreviewSV = (FocusSurfaceView) findViewById(R.id.previewSV);
        this.btn_shoot = (Button) findViewById(R.id.btn_shoot);
        SurfaceHolder surfaceHolder = this.mPreviewSV.getHolder();
        this.mySurfaceHolder = surfaceHolder;
        surfaceHolder.setFormat(-3);
        this.mySurfaceHolder.addCallback(this);
        this.mySurfaceHolder.setType(3);
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("xxxcwsd");
        stringBuilder1.append(Float.toString(this.mPreviewSV.getmBoundaryHeight()));
        Log.d(stringBuilder1.toString(), Float.toString(this.mPreviewSV.getmBoundaryWidth()));
        this.iii = (ImageView) findViewById(R.id.imageView);
        this.btn_shoot.setOnClickListener(new PhotoOnClickListener());
    }

    public Bitmap reSize(Bitmap paramBitmap, int paramInt) {
        float f1;
        float f2 = 0;
        int i = paramBitmap.getWidth();
        int j = paramBitmap.getHeight();
        if (i < paramInt && j < paramInt)
            return paramBitmap;
        if (i > j && i > paramInt) {
            f1 = paramInt;
            f2 = i;
        } else if (j > paramInt) {
            f1 = paramInt;
            f2 = j;
        } else {
            f1 = 1.0F;
            if (f1 >= 1.0F)
                return paramBitmap;
        }
        f1 /= f2;
        if (f1 >= 1.0F)
            return paramBitmap;

        return paramBitmap;
    }

    public String saveBitmap(Bitmap paramBitmap, String paramString) {
        File file ;
        if (Build.VERSION.SDK_INT >= 29)//Build.VERSION_CODES.Q
        {
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SKSCS1");
        }
        else
        {
            file = new File(Environment.getExternalStorageDirectory(), "SKSCS1");
        }
        //File file = new File(Environment.getExternalStorageDirectory(), "SKSCS1");
        //File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SKSCS1");

        if (!file.exists())
            file.mkdirs();
        Log.d("dir path", file.getAbsolutePath());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        StringBuilder stringBuilder = new StringBuilder();
        if (Build.VERSION.SDK_INT >= 29)//Build.VERSION_CODES.Q
        {
            stringBuilder.append(file.getAbsolutePath());
        }
        else
        {
            stringBuilder.append(file.getPath());
        }
        //stringBuilder.append(file.getPath());
        //stringBuilder.append(file.getAbsolutePath());
        stringBuilder.append(File.separator);
        stringBuilder.append(simpleDateFormat.format(new Date()));
        stringBuilder.append(paramString);
        stringBuilder.append(".jpg");
        this.filename = stringBuilder.toString();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.filename);
            paramBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            try {
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException iOException) {
                iOException.printStackTrace();
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return this.filename;
    }

    public void saveJpeg(Bitmap paramBitmap) {
        //File file = new File("/mnt/sdcard/rectPhoto/");
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SKSCS1");
        if (!file.exists())
            file.mkdir();
        long l = System.currentTimeMillis();
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("/mnt/sdcard/rectPhoto/");
        stringBuilder1.append(l);
        stringBuilder1.append(".jpg");
        String str = stringBuilder1.toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("saveJpeg:jpegName--");
        stringBuilder2.append(str);
        Log.i("yan", stringBuilder2.toString());
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str));
            paramBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            Log.i(TAG, "saveJpeg：儲備完畢");
            return;
        } catch (IOException iOException) {
            Log.i(TAG, "saveJpeg：儲存失敗");
                    iOException.printStackTrace();
            return;
        }
    }

    public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {
        Log.i(TAG, "SurfaceHolder.Callback:surfaceChanged!");
    }

    public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {
        try {
            Camera camera = Camera.open(0);
            this.myCamera = camera;
            camera.setPreviewDisplay(this.mySurfaceHolder);
            Camera.Parameters parameters = this.myCamera.getParameters();
            this.param = parameters;
            this.suSize = parameters.getSupportedPictureSizes();
            try {
                optimization(1000, 2000);
                this.myCamera.setParameters(this.param);
            } catch (Exception exception) {
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("sfsfsfsf");
                    stringBuilder.append(exception.toString());
                    Log.d("sfsf", stringBuilder.toString());
                    optimization(400, 1000);
                    this.myCamera.setParameters(this.param);
                } catch (Exception exception1) {
                    Camera.Parameters parameters1 = this.myCamera.getParameters();
                    this.myCamera.setParameters(parameters1);
                }
            }
            this.myCamera.setDisplayOrientation(90);
            this.myCamera.startPreview();
            Log.i(TAG, "SurfaceHolder.Callback: surfaceCreated!");
            return;
        } catch (Exception exception) {
            Log.d("ddsa", "madsda");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("相機發生錯誤訊息");
                    stringBuilder.append(exception.toString());
            Toast.makeText((Context) this, stringBuilder.toString(), Toast.LENGTH_SHORT).show();
            Camera camera = this.myCamera;
            if (camera != null) {
                camera.release();
                this.myCamera = null;
            }
            exception.printStackTrace();
            return;
        }
    }

    public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {
        Camera camera = this.myCamera;
        if (camera != null) {
            camera.stopPreview();
            this.myCamera.release();
            this.myCamera = null;
        }
    }

    public class MyOnTouchListener implements View.OnTouchListener {
        public final float[] BT_NOT_SELECTED = new float[]{
                1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F,
                0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F};

        public final float[] BT_SELECTED = new float[]{
                2.0F, 0.0F, 0.0F, 0.0F, 2.0F, 0.0F, 2.0F, 0.0F, 0.0F, 2.0F,
                0.0F, 0.0F, 2.0F, 0.0F, 2.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F};

        public boolean onTouch(View param1View, MotionEvent param1MotionEvent) {
            if (param1MotionEvent.getAction() == 0) {
                param1View.getBackground().setColorFilter((ColorFilter) new ColorMatrixColorFilter(this.BT_SELECTED));
                param1View.setBackgroundDrawable(param1View.getBackground());
            } else if (param1MotionEvent.getAction() == 1) {
                param1View.getBackground().setColorFilter((ColorFilter) new ColorMatrixColorFilter(this.BT_NOT_SELECTED));
                param1View.setBackgroundDrawable(param1View.getBackground());
            }
            return false;
        }
    }

    public class PhotoOnClickListener implements View.OnClickListener {
        public void onClick(View param1View) {
            try {
                CameraActivity.this.myCamera.autoFocus(CameraActivity.this.autoFacusCallback);
                return;
            } catch (Exception exception) {
                return;
            }
        }
    }
}
