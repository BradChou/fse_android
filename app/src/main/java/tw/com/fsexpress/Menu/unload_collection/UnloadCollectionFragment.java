package tw.com.fsexpress.Menu.unload_collection;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.CustomFragment;
import tw.com.fsexpress.Menu.collection.CollectionFragment2;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.CollectGoodsData;
import tw.com.fsexpress.framework.database.room.entity.DischargeData;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.WareHouseManager;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class UnloadCollectionFragment extends CustomFragment {

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();
    private List<DataObject> arrWare = null;
    private TextView txtChoose = null;
    private EditText editGoodNum = null;
    private String currentSelectCode = "";
    private Button btnLeft, btnRight;
    private ScrollView scrollView, scrollViewFail;
    private TextView txtGoodNum, txtGoodNumFail, txtTotalNum, txtBarcode;
    private ToneGenerator toneGenerator;
    private boolean isAuto = false;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        TextView txtTitle = (TextView)view.findViewById(R.id.txt_title);
        txtTitle.setText("卸集");

        this.txtBarcode = (TextView)view.findViewById(R.id.textBarcode);

        this.txtTotalNum = (TextView)view.findViewById(R.id.txt_total_num);
//        this.txtAccumulationNum = (TextView)view.findViewById(R.id.txt_accumulation_num);

        this.scrollView = (ScrollView)view.findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);

        this.scrollViewFail = (ScrollView)view.findViewById(R.id.scrollViewFail);
        this.scrollViewFail.fullScroll(130);

        this.txtGoodNum = (TextView)view.findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)view.findViewById(R.id.goods_num_fail);


        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commander cmd = Commander.instance();
                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                List<DataObject> arrLogs = handler.getDataByCriteria(null);
                if (arrLogs.size() == 0){
                    ((GHWFragmentActivity) UnloadCollectionFragment.this.getFragmentActivity()).popFragment();
                }else{
                    RequestResultListDialog.instance(UnloadCollectionFragment.this.getFragmentActivity()).showUploadDialog(arrLogs, "卸集", new RequestResultListDialog.RequestResultDialogCallback() {
                        @Override
                        public void onClearFinished() {

                        }
                    });
                }
            }
        });

        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.arrWare = WareHouseManager.instance().getWareHouse();
        if (this.editGoodNum == null){
            this.editGoodNum = (EditText)view.findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄
                        isAuto = true;
                        UnloadCollectionFragment.this.editGoodNum.setText("");
                        requestUpload(s.toString().trim());


                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入
                        isAuto = false;
                    }

//                    if(count >= 10) {
//                        arrBarcode.add(s.toString().trim());
//                        UnloadCollectionFragment.this.editGoodNum.setText("");
//                        updateBarcodeList();
//                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


        this.txtChoose = (TextView)view.findViewById(R.id.txt_choose_unloading);
        view.findViewById(R.id.txt_choose_unloading).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> arrTitle = new ArrayList<String>();
                for (DataObject d: arrWare) {
                    String title = String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString());
                    arrTitle.add(title);
                }
                Constants.AlertWithMultipleSelect(UnloadCollectionFragment.this.getFragmentActivity(), "卸貨站選擇：", arrTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        DataObject d = arrWare.get(which);
                        UnloadCollectionFragment.this.currentSelectCode = d.getVariable("code").toString();
                        txtChoose.setText(String.format("%s(%s)", d.getVariable("code").toString(),d.getVariable("name").toString()));

                    }
                }, null).show();
            }
        });

        view.findViewById(R.id.btn_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnloadCollectionFragment.this.getFragmentActivity().startActivityForResult(new Intent((Context)UnloadCollectionFragment.this.getFragmentActivity(), UnloadCollectionBarcodeActivity.class), 5);
            }
        });

        view.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isAuto){

                    String strGoodNum = UnloadCollectionFragment.this.editGoodNum.getText().toString();
                    if (strGoodNum.equals("")){
                        Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "請確認您的貨號是否正確。", "確定", null, null, null);
                        return;
                    }else{

                        if (strGoodNum.length() < 10){
                            Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "請確認您的貨號是否正確。", "確定", null, null, null);
                            return;
                        }
                    }
                    requestUpload(strGoodNum);
                }else{
                    Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "現在為自動上傳模式，如要手動上傳請\"點擊貨號輸入框並輸入條碼\"。", "確定", null, null, null);
                }

//                String strGoodNum = UnloadCollectionFragment.this.editGoodNum.getText().toString();
//                if (!strGoodNum.equals("")){
//                    if (strGoodNum.length() < 10){
//                        Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "請確認您的貨號是否正確。", "確定", null, null, null);
//                        return;
//                    }
//
//                    arrBarcode.add(strGoodNum);
//
//                }else{
//
//                    if (arrBarcode.size() == 0){
//                        Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "請確認您的貨號是否正確。", "確定", null, null, null);
//                        return;
//                    }
//
//                }


//                if (UnloadCollectionFragment.this.currentSelectCode.equals("")) {
//                    Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "請選擇卸貨站。", "確定", null, null, null);
//                    return;
//                }

//                requestMultipleUpload();

            }
        });

    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){

        /*
        2022-02-16 v.43

        修正 取消卸集站的選擇

         */
        /*
        if (UnloadCollectionFragment.this.currentSelectCode.equals("")) {
            Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "請選擇卸貨站。", "確定", null, null, null);
            return;
        }
        */

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "6");
        data.put("driver_code", AccountManager.instance().getDriverCode(UnloadCollectionFragment.this.getFragmentActivity()));
        data.put("check_number", barcode);
        data.put("receive_option", "");
        data.put("scan_date", strScanTime);
        data.put("area_arrive_code", "");//UnloadCollectionFragment.this.currentSelectCode);
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", "");
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(getFragmentActivity()));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(getFragmentActivity()));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(getFragmentActivity()));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(UnloadCollectionFragment.this.getFragmentActivity(),jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    if (isAuto != true){
                        UnloadCollectionFragment.this.editGoodNum.setText("");
                        Constants.ErrorAlertDialog(UnloadCollectionFragment.this.getFragmentActivity(), "卸集", "上傳成功", "確定", null, null, null);
                    }
                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("6", "卸集",barcode);
                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(getFragmentActivity(), String.format("[%s]-上傳失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
        insertData(barcode);
    }

    // 將掃讀成功的資料寫進room
    private void insertData(String checkNumber) {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(UnloadCollectionFragment.this.getFragmentActivity()).toUpperCase();
            DischargeData dischargeData = new DischargeData(UUID.randomUUID().toString(), driverCode, checkNumber, Constants.getTime(), Constants.getNowTimestamp());
            AppDataBase.getInstance(UnloadCollectionFragment.this.getFragmentActivity()).getDischargeDao().insertData(dischargeData);
        }).start();
    }

//    private void requestMultipleUpload(){
//        AlertDialog dialog = Constants.ProgressBar(this.getFragmentActivity(), "資料上傳中...請稍候");
//        dialog.show();
//
//        String strScanTime = Constants.getTime();
//        int idx = 0;
//        for (String barcode:this.arrBarcode) {
//
//            HashMap<String, String> data = new HashMap<String, String>();
//            data.put("scan_item", "6");
//            data.put("driver_code", AccountManager.instance().getDriverCode(UnloadCollectionFragment.this.getFragmentActivity()));
//            data.put("check_number", barcode);
//            data.put("scan_date", strScanTime);
//            data.put("area_arrive_code", UnloadCollectionFragment.this.currentSelectCode);
//            data.put("platform", "");
//            data.put("car_number", "");
//            data.put("sowage_rate", "");
//            data.put("area", "");
//            data.put("exception_option", "");
//            data.put("arrive_option", "");
//            data.put("ship_mode", "");
//            data.put("goods_type", "");
//            data.put("pieces", "");
//            data.put("weight", "");
//            data.put("runs", "");
//            data.put("plates", "");
//            data.put("sign_form_image", "");
//            data.put("sign_field_image", "");
//            data.put("coupon", "");
//            data.put("cash", "");
//
//            DataObject requestData = new DataObject();
//            requestData.setVariable("param", data);
//            requestData.setVariable("barcode", barcode);
//            requestData.setVariable("url", Constants.API_POST_SEND_UPLOAD_DATA);
//            requestData.setVariable("method", "POST");
//            requestData.setVariable("id", String.format("%s%d",Constants.md5(data.toString()),idx));
//            RequestMutilController.instance().addRequest(requestData);
//            idx++;
//        }
//
//        RequestMutilController.instance().setCallback(new RequestMutilController.RequestMutilControllerCallback() {
//
//
//            @Override
//            public void onFinish(List<DataObject> data) {
//
//                dialog.cancel();
//                RequestResultListDialog.instance(UnloadCollectionFragment.this.getFragmentActivity()).showUploadDialog(data, "卸集");
////                Constants.ErrorAlertDialog(ArriveFragment.this.getFragmentActivity(), "發送", "上傳成功。", "確定", null, null, null);
//
//                for (String barcode:arrBarcode) {
//                    TransferDataManager.instance().insertTransferData("6", "卸集",barcode);
//                }
//
//                arrBarcode.clear();
//
//            }
//
//        });
//
//
//        RequestMutilController.instance().startRequest();
//    }


}
