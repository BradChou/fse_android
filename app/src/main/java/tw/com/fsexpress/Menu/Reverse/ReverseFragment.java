package tw.com.fsexpress.Menu.Reverse;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.ui.GHWListView;
import tw.com.fsexpress.framework.ui.GHWIndexPath;
import tw.com.fsexpress.framework.ui.GHWListView.GHWBaseAdapter;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

// 逆物流
public class ReverseFragment extends GHWFragment implements AbsListView.OnScrollListener {

    // TAG
    private static final String TAG = "SHAWN";

    // bar
    private Button btnLeft = null, btnRight = null;
    private TextView textTitle = null;

    private TextView textComplete = null, textUncompleted = null;

    private GHWListView listView = null;
    private List<DataObject> arr_data = new ArrayList<DataObject>();
    private ListViewHolder holder;

    private int type = 0;

    static class ListViewHolder
    {
        LinearLayout back;
        TextView single_num;
        TextView send_person;
        TextView send_phone;
        TextView send_address;
    }

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        // bar
        this.textTitle = (TextView) view.findViewById(R.id.txt_title);
        this.textTitle.setText("逆物流");
        this.btnLeft = (Button) view.findViewById(R.id.btn_left);
        this.btnLeft.setBackgroundResource(R.mipmap.back);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GHWFragmentActivity) ReverseFragment.this.getFragmentActivity()).popFragment();
            }
        });
        this.btnRight = (Button) view.findViewById(R.id.btn_right);
        this.btnRight.setVisibility(View.INVISIBLE);

        this.textComplete = (TextView) view.findViewById(R.id.txt_complete);
        this.textComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textComplete.setBackgroundResource(R.color.select_red);
                textUncompleted.setBackgroundResource(R.color.unselected_grey);
                requestReverse(1);
            }
        });
        this.textUncompleted = (TextView) view.findViewById(R.id.txt_uncompleted);
        this.textUncompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textComplete.setBackgroundResource(R.color.unselected_grey);
                textUncompleted.setBackgroundResource(R.color.select_red);
                requestReverse(0);
            }
        });

        this.listView = (GHWListView) view.findViewById(R.id.list_reverse);
        this.arr_data.clear();
        this.setListView();

        this.requestReverse(0);
    }

    private void setListView()
    {
        this.listView.delegate = new GHWListView.Delegate()
        {
            @Override
            public void didSelectRowAtIndexPath(View view, GHWIndexPath indexPath)
            {
                if(type == 0) {
                    ReverseDetailFragment fg = (ReverseDetailFragment)new ReverseDetailFragment().initWithLayoutResourceId(R.layout.layout_reverse_detail);
                    DataObject d = arr_data.get(indexPath.row);
                    fg.setData(d);
                    ((GHWFragmentActivity) ReverseFragment.this.getFragmentActivity()).pushFragment(fg, true);
                }
            }
        };

        this.listView.dataSource = new GHWListView.DataSource()
        {
            @Override
            public int numberOfSectionsInListView(GHWBaseAdapter arg0)
            {
                return 0;
            }

            @Override
            public int numberOfRowsInSection(GHWBaseAdapter arg0, int arg1)
            {
                return arr_data.size();
            }

            @Override
            public View getSectionView(GHWListView arg0, View arg1, int arg2)
            {
                return null;
            }

            @Override
            public View getItemView(GHWListView parent, View cell, GHWIndexPath indexPath)
            {
                if (cell == null)
                {
                    cell = ReverseFragment.this.getFragmentActivity().getLayoutInflater().inflate(R.layout.cell_reverse, null);

                    holder = new ListViewHolder();
                    holder.back = (LinearLayout) cell.findViewById(R.id.ll_bg);
                    holder.single_num = (TextView) cell.findViewById(R.id.txt_single_num);
                    holder.send_person = (TextView) cell.findViewById(R.id.txt_send_person);
                    holder.send_phone = (TextView) cell.findViewById(R.id.txt_send_phone);
                    holder.send_address = (TextView) cell.findViewById(R.id.txt_send_address);
                    cell.setTag(holder);

                } else
                {
                    holder = (ListViewHolder) cell.getTag();
                }

                if(indexPath.row % 2 == 0) {
                    holder.back.setBackgroundResource(R.drawable.ripple_gray_radius);
                }
                else {
                    holder.back.setBackgroundResource(R.drawable.ripple_dark_gray_radius);
                }

                DataObject d = arr_data.get(indexPath.row);
                holder.single_num.setText(String.format("%s", d.getVariable("check_number").toString()));
                holder.send_person.setText(String.format("%s", d.getVariable("send_contact").toString()));
                holder.send_phone.setText(String.format("%s", d.getVariable("send_tel").toString()));
                holder.send_address.setText(String.format("%s", d.getVariable("send_address").toString()));

                return cell;
            }
        };
        this.listView.setOnScrollListener(this);
    }

    protected void requestReverse(int type) {

        AlertDialog dialog = Constants.ProgressBar(ReverseFragment.this.getFragmentActivity(), "讀取中...請稍候");
        dialog.show();

        this.arr_data.clear();

        this.type = type;
        // type 1 -> complete
        // type 0 -> uncompleted

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("driver_code", AccountManager.instance().getDriverCode(ReverseFragment.this.getFragmentActivity()));
        params.put("status", this.type == 0 ? "0" : "1");

        RequestManager.instance().doRequest(Constants.API_POST_GET_RETURN_DELIVERY, "POST", params, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                dialog.cancel();
                if (jsonObject.get("resultcode").getAsBoolean()) {

                    List<String> keys = Arrays.asList("request_id", "check_number", "order_number", "receive_customer_code", "receive_tel",
                            "receive_contact", "receive_address", "send_contact", "send_tel", "send_address", "memo");
                    for (JsonElement o : jsonObject.getAsJsonArray("tasks")) {

                        DataObject d = new DataObject();
                        for (String key : keys) {

                            String value = !o.getAsJsonObject().get(key).isJsonNull() ? o.getAsJsonObject().get(key).getAsString() : "";
                            d.setVariable(key, value);
                        }
                        arr_data.add(d);
                    }
                    listView.reloadData();
                    Log.e(TAG, "arr_data count = " + arr_data.size());
                }
            }

            @Override
            public void onFail(String reason) {
                dialog.cancel();
            }
        });
    }

    @Override
    public void onScroll(AbsListView arg0, int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
    }

    @Override
    public void onScrollStateChanged(AbsListView arg0, int scrollState)
    {
    }
}
