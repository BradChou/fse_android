package tw.com.fsexpress.Menu.Ships;

import android.app.Activity;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.DeliveryData;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ShipsBarcodeActivity extends Activity implements BarcodeCallback {

    private long DELAY_TIME = 2000L;

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();

    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private boolean isStop = false;


    private int scanItem = 0;
    private ScrollView scrollView;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum, txtGoodNum, txtGoodNumFail, txtTotalNum;
    private EditText editPlateNum = null, editPieceNum = null;
    private String strGoods = "";

//    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.誤訂", "3.多出"};
    private String[] abnormalArray = new String[] { "0.無", "1.破損", "2.短少", "3.多出", "4.喪失", "5.誤訂"};
    private Spinner spinnerAbnormalStatus;
    private String abnormalStatus = "";
    private ToneGenerator toneGenerator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_ships_barcode);

        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }


        this.scrollView = (ScrollView) findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollView.smoothScrollTo(0,0);
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);

        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.txtGoodNum = (TextView)findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)findViewById(R.id.goods_num_fail);
        this.txtTotalNum = (TextView)findViewById(R.id.txt_total_num);

        this.editPieceNum = (EditText) findViewById(R.id.edit_good_pieces_num);
        this.editPieceNum.setText("0");
        this.editPlateNum = (EditText) findViewById(R.id.edit_plates_num);
        this.editPlateNum.setText("0");

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.spinnerAbnormalStatus = (Spinner)findViewById(R.id.spinner_abnormal_status);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, abnormalArray);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter)arrayAdapter);
        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String strStatus = abnormalArray[position];
                abnormalStatus = strStatus.substring(0, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                ShipsBarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }

    private void backWithResult() {

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0){
            JSONArray arrJson = new JSONArray(this.arrBarcode);
            ShipsBarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
            ShipsBarcodeActivity.this.setResult(RESULT_OK, getIntent());
            ShipsBarcodeActivity.this.finish();

        }else{
            RequestResultListDialog.instance(this).showUploadDialog(arrLogs, "集貨", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {
                    handlerScan();
                }
            });
        }


    }

    private void addResultToList(String paramString) {
        if (!this.isStop) {

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            this.isStop = true;


            if (paramString.length() < 10){
                Constants.ErrorAlertDialog(this, "配送", "條碼必須長度需大於10碼", "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();
                    }
                }, null);
                return;
            }

            requestUpload(paramString);

        }
    }

//    private void setResultToUpload(String paramString) {
//        if (!this.isStop) {
//            strGoods = strGoods + "\n" + paramString;
//            this.arrBarcode.add(paramString);
//            this.scrollView.fullScroll(130);
//            this.textGoodNum.setText(paramString);
////            this.textPieceNum.setText(String.valueOf(this.arrBarcode.size()));
//            this.goodsNum.setText(strGoods);
//            beepAndPause();
//
//            if (paramString.length() < 10){
//                Constants.ErrorAlertDialog(this, "配送", "條碼必須長度需大於10碼", "確定", null, null, null);
//                return;
//            }
//
////            Integer num = Integer.parseInt(this.editPlateNum.getText().toString());
////            if (editPlateNum.getText().toString().isEmpty() || num == 0){
////                Constants.ErrorAlertDialog(this, "配送", "請確認板數是否填寫正確。", "確定", null, null, null);
////                return;
////            }
//
//            requestUpload(paramString);
//        }
//    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void requestUpload(String barcode){

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "2");
        data.put("driver_code", AccountManager.instance().getDriverCode(ShipsBarcodeActivity.this));
        data.put("check_number", barcode);
        data.put("receive_option", "");
        data.put("scan_date", Constants.getTime());
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", abnormalStatus);
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", this.editPieceNum.getText().toString());
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", this.editPlateNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                handlerScan();

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(ShipsBarcodeActivity.this,jsonObject);
                    if ( isStop == true)
                    {
                        return;
                    }
                    //endregion

                    txtNowGoodnum.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(ShipsBarcodeActivity.this, String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("2", "配送", barcode);

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(ShipsBarcodeActivity.this, String.format("[%s]-上傳失敗", barcode));

                }
            }

            @Override
            public void onFail(String reason) {

            }
        });

        insertData(barcode);
    }

    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    // 將掃讀成功的資料寫進room
    private void insertData(String checkNumber) {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(this).toUpperCase();
            DeliveryData deliveryData = new DeliveryData(UUID.randomUUID().toString(), driverCode, checkNumber, Constants.getTime(), Constants.getNowTimestamp());
            AppDataBase.getInstance(this).getDeliveryDao().insertData(deliveryData);
        }).start();
    }
}
