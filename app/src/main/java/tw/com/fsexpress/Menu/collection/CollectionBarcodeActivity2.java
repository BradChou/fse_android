package tw.com.fsexpress.Menu.collection;

import android.app.Activity;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import tw.com.fsexpress.AccountManager;
import tw.com.fsexpress.Constants;
import tw.com.fsexpress.R;
import tw.com.fsexpress.TransferDataManager;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.database.room.AppDataBase;
import tw.com.fsexpress.framework.database.room.entity.CollectGoodsData;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.request.RequestManager;
import tw.com.fsexpress.request.RequestResultListDialog;

public class CollectionBarcodeActivity2 extends Activity implements BarcodeCallback {

    /*
    20.取件成功
    7 .已遭取走
    18.無貨可取
    17.客戶取消退貨
    12.地址錯誤
    6 .聯絡不上
    16.約定再取件
    22.公司休息
     */
    private static final String TAG = "Collection";


    private String[] spinnerContentA = new String[]{"20.取件成功","7 .已遭取走","18.無貨可取","17.客戶取消退貨","12.地址錯誤","6 .聯絡不上","16.約定再取件","22.公司休息","24.未用momo原包裝"};
    private Spinner spinnerAbnormalStatus;

    private String deliverySelect = "";
    private String selectSpinnerId = "20";

    private long DELAY_TIME = 2000L;

    private List<String> arrBarcode = new ArrayList<String>();
    private List<String> arrBarcodeFail = new ArrayList<String>();

    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private TextView txtGoodNum, txtGoodNumFail, txtBarcode, txtTotalNum;
    private boolean isStop = false;
    private ScrollView scrollView, scrollViewFail;
    private String strGoods = "";
    private EditText editPlatesNum;
    private ToneGenerator toneGenerator;

    private boolean isAuto = false;
    private EditText editGoodNum = null;

    ////集貨2.0 集貨功能的項目
    private String[] spinnerCollectType = new String[]{"一般集貨","月結/預購產品綁定","月結/預購產品解除綁定"};
    private String strCollectType = "";

    //集貨功能選擇
    private Spinner spinnerCollectTypeStatus;

    //集貨2.0 集貨功能要控制顯示/隱藏的項目
    //月結袋
    private TextView tv01 ;
    private EditText et02 ;
    //集貨成功的貨號
    private TextView tv03 ;
    private TextView tv04 ;
    //集貨分區
    private TextView tv05 ;
    private Spinner sp06 ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_collection_barcode2);


        if (toneGenerator == null){
            toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        }

        this.txtBarcode = (TextView)findViewById(R.id.txt_now_good_num);
        this.txtTotalNum = (TextView)findViewById(R.id.txt_total_num);

        this.scrollView = (ScrollView)findViewById(R.id.scrollView);
        this.scrollView.fullScroll(130);
        this.scrollViewFail = (ScrollView)findViewById(R.id.scrollViewFail);
        this.scrollViewFail.fullScroll(130);

        this.txtGoodNum = (TextView)findViewById(R.id.goods_num);
        this.txtGoodNumFail = (TextView)findViewById(R.id.goods_num_fail);

        this.editPlatesNum = (EditText)findViewById(R.id.edit_plates_num);

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });


        if (this.editGoodNum == null){
            this.editGoodNum = (EditText)findViewById(R.id.edit_good_num);
            this.editGoodNum.requestFocus();
            this.editGoodNum.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if(count >= 10) { //1次進10馬以上 判定為紅外線掃瞄
                        isAuto = true;
                        //CollectionFragment2.this.editGoodNum.setText("");
                        //requestUpload(s.toString().trim());
                        requestUploadPreDo();

                    }else if (count > 0){ //1次不滿10馬 判定為手動輸入
                        isAuto = false;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        /*
        if (this.spinnerAbnormalStatus == null){
            this.spinnerAbnormalStatus = (Spinner) findViewById(R.id.spinner_partition);
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
            this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

            this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    deliverySelect = spinnerContentA[position];

                    selectSpinnerId = deliverySelect.substring(0, 2);
                    selectSpinnerId = selectSpinnerId.trim();

                    Log.e(TAG, "select = " + selectSpinnerId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        */


        tv01 = (TextView) findViewById(R.id.textMonthBagTitle) ;
        et02 = (EditText) findViewById(R.id.textMonthBag) ;
        tv03 = (TextView) findViewById(R.id.textView3) ;
        tv04 = (TextView) findViewById(R.id.txt_now_good_num) ;
        tv05 = (TextView) findViewById(R.id.textView18) ;
        sp06 = (Spinner) findViewById(R.id.spinner_partition) ;

        this.et02.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count >= 2) { //1次進2碼以上 判定為紅外線掃瞄
                    requestUploadPreDo();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //集貨分區的選項
        if (this.spinnerAbnormalStatus == null){
            getDevliveryType();

        }

        //集貨功能選擇
        if (this.spinnerCollectTypeStatus == null )
        {
            doCollectTypeMain();
        }


        //取得月結袋的清單
        getBagListType();

    }

    private void addResultToList(String paramString) {
        if (!this.isStop) {

            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();

            this.isStop = true;
/*
            if (paramString.length() < 10){
                Constants.ErrorAlertDialog(this, "集貨2.0", "條碼必須長度需大於10碼" + paramString.length(), "確定", null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handlerScan();
                    }
                }, null);
                return;
            }
*/
            requestUploadPreDo();
            //requestUpload(paramString);

        }
    }

    private void updateSuccessBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcode.size(); idx++){
            String str = this.arrBarcode.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtTotalNum.setText(String.format("%d", this.arrBarcode.size()));
//            this.txtAccumulationNum.setText(String.format("%d", this.arrBarcode.size()));
            this.txtGoodNum.setText(strGoods.substring(1));
        }else{
            this.txtTotalNum.setText("0");
//            this.txtAccumulationNum.setText("0");
            this.txtGoodNum.setText("");
        }
    }

    private void updateFailBarcodeList(){
        String strGoods = "";
        for (int idx = 0; idx < this.arrBarcodeFail.size(); idx++){
            String str = this.arrBarcodeFail.get(idx).toString();
            strGoods = strGoods + "\n" + str;
        }
        if (!strGoods.equals("")){
            this.txtGoodNumFail.setText(strGoods.substring(1));
        }else{
            this.txtGoodNumFail.setText("");
        }
    }

    private void handlerScan(){
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                CollectionBarcodeActivity2.this.isStop = false;
            }}, DELAY_TIME);
    }


    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        List<DataObject> arrLogs = handler.getDataByCriteria(null);
        if (arrLogs.size() == 0){
            JSONArray arrJson = new JSONArray(this.arrBarcode);
            CollectionBarcodeActivity2.this.getIntent().putExtra("barcodes", arrJson.toString());
            CollectionBarcodeActivity2.this.setResult(RESULT_OK, getIntent());
            CollectionBarcodeActivity2.this.finish();

        }else{
            RequestResultListDialog.instance(this).showUploadDialog(arrLogs, "集貨2.0", new RequestResultListDialog.RequestResultDialogCallback() {
                @Override
                public void onClearFinished() {
                    handlerScan();
                }
            });
        }


    }


    private void requestUploadPreDo()
    {
        String strData1 = editGoodNum.getText().toString().trim() ;
        String strData2 = et02.getText().toString().trim()  ;

        //textMonthBag
        if ( strCollectType == spinnerCollectType[0] ) //strCollectType.equals("一般集貨"))
        {
            if ( !strData1.equals(""))
            {
                requestUpload(strData1);
                editGoodNum.setText("");
            }
            else
            {
                Constants.ErrorAlertDialog(this, "集貨2.0", "請確認輸入的貨號是否正確", "確定", null, null, null);
            }
        }
        else if ( strCollectType == spinnerCollectType[1] ) //strCollectType.equals("月結袋集貨"))
        {
            Log.e("strCollectType",strCollectType);

            if ( !strData1.equals("") && !strData2.equals("") )
            {
                requestUpload_Bag(true,strData1,strData2);
            }
            else if ( !strData1.equals("") && strData2.equals("") )
            {
                et02.requestFocus();
            }
            else if ( strData1.equals("") && !strData2.equals("") )
            {
                editGoodNum.requestFocus();
            }
            else
            {
                Constants.ErrorAlertDialog(this, "集貨2.0", "請確認輸入的貨號/月結袋是否正確", "確定", null, null, null);
            }
        }
        else if ( strCollectType == spinnerCollectType[2] ) //strCollectType.equals("月結袋解除綁定"))
        {
            if ( !strData1.equals(""))
            {
                requestUpload_Bag(false,strData1,strData2);
            }
            else
            {
                Constants.ErrorAlertDialog(this, "集貨2.0", "請確認輸入的貨號是否正確", "確定", null, null, null);
            }
        }
    }


    private void requestUpload_Bag(boolean isBind , String barcode , String bagType){

        bagType = bagType.trim();
        bagType = bagType.toUpperCase(Locale.ROOT);

        if ( bagType.length() > 7)
        {
            Constants.ErrorAlertDialog(this, "集貨2.0", "月結袋資料錯誤!! ", "確定", null, null, null);
            et02.setText("");
            return ;
        }
        else
        {
            boolean isBagOk = false ;

            for ( int i = 0 ; i < Baglist.size() ; i++  )
            {
                if ( bagType.equals( (Baglist.toArray(new String[0]))[i] ) )
                {
                    isBagOk = true ;
                }
            }
            if ( isBagOk == true //bagType.equals("B01") || bagType.equals("B02") || bagType.equals("B03") || bagType.equals("B04") || bagType.equals("B05")
                )
            {

            }
            else
            {
                if (isBind == true)
                {
                    Constants.ErrorAlertDialog(this, "集貨2.0", "月結袋資料錯誤!! ", "確定", null, null, null);
                    et02.setText("");
                    return ;
                }

            }
        }
        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();

        data.put("driverCode", AccountManager.instance().getDriverCode(this));
        data.put("checkNumber", barcode);
        data.put("bagNo", bagType);
        data.put("bind", ( isBind == true) ? "1" : "0");


        RequestManager.instance().doRequest(Constants.API_POST_SEND_BIND_BAG, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(CollectionBarcodeActivity2.this,jsonObject);
                    if ( isStop == true)
                    {
                        editGoodNum.setText("");
                        et02.setText("");
                        editGoodNum.requestFocus();
                        return;
                    }
                    //endregion

                    if ( isBind == true)
                    {
                        requestUpload(barcode);
                    }
                    else
                    {
                        editGoodNum.setText("");
                        et02.setText("");
                        Constants.toast(CollectionBarcodeActivity2.this, String.format("[%s]-上傳成功", barcode));

                    }


                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(CollectionBarcodeActivity2.this, String.format("[%s]-綁定失敗", barcode));
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });


    }

    private void requestUpload(String barcode){

//        AlertDialog dialog = Constants.ProgressBar(this, "資料上傳中...請稍候");
//        dialog.show();

        HashMap<String, String> data = new HashMap<String, String>();
        data.put("scan_item", "5");
        data.put("driver_code", AccountManager.instance().getDriverCode(this));
        data.put("check_number", barcode);
        data.put("scan_date", Constants.getTime());
        data.put("receive_option", this.selectSpinnerId);
        data.put("area_arrive_code", "");
        data.put("platform", "");
        data.put("car_number", "");
        data.put("sowage_rate", "");
        data.put("area", "");
        data.put("exception_option", "");
        data.put("arrive_option", "");
        data.put("ship_mode", "");
        data.put("goods_type", "");
        data.put("pieces", "");
        data.put("weight", "");
        data.put("runs", "");
        data.put("plates", this.editPlatesNum.getText().toString());
        data.put("sign_form_image", "");
        data.put("sign_field_image", "");
        data.put("coupon", "");
        data.put("cash", "");
        data.put("Work_Station_Scode", AccountManager.instance().getWorkStationCode(this));
        data.put("Work_Send_Code", AccountManager.instance().getWorkSendCode(this));
        data.put("Work_Delivery_Code", AccountManager.instance().getWorkDeliveryCode(this));

        RequestManager.instance().doRequest(Constants.API_POST_SEND_UPLOAD_DATA, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

//                dialog.cancel();

                handlerScan();

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    //region 檢查  本筆貨號資料 是否已配達 如果已配達 後續不應繼續處理
                    boolean isStop = false ;
                    isStop = RequestManager.instance().isDeliveryOkandAlertMsg(CollectionBarcodeActivity2.this,jsonObject);
                    if ( isStop == true)
                    {
                        et02.setText("");
                        CollectionBarcodeActivity2.this.editGoodNum.setText("");
                        CollectionBarcodeActivity2.this.editGoodNum.requestFocus();
                        return;
                    }
                    //endregion

                    et02.setText("");
                    CollectionBarcodeActivity2.this.editGoodNum.setText("");
                    CollectionBarcodeActivity2.this.editGoodNum.requestFocus();

                    txtBarcode.setText(barcode);
                    arrBarcode.add(barcode);
                    updateSuccessBarcodeList();
                    Constants.toast(CollectionBarcodeActivity2.this, String.format("[%s]-上傳成功", barcode));
                    TransferDataManager.instance().insertTransferData("5", "集貨", barcode);

                }else{
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    toneGenerator.stopTone();
                    arrBarcodeFail.add(barcode);
                    updateFailBarcodeList();
                    Constants.toast(CollectionBarcodeActivity2.this, String.format("[%s]-上傳失敗", barcode));

                }

            }

            @Override
            public void onFail(String reason) {

//                dialog.cancel();

            }
        });

        if (this.selectSpinnerId.equals("20")) {
            insertData(barcode);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {


        if (!this.isStop) {

            this.isStop = true;
            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toneGenerator.stopTone();
            this.isStop = true;


            //this.editGoodNum.setText(result.getText().toString().trim());

            String strData1 = editGoodNum.getText().toString().trim() ;
            String strData2 = et02.getText().toString().trim() ;

            if ( strCollectType == spinnerCollectType[0] ) //strCollectType.equals("一般集貨"))
            {
                editGoodNum.setText( result.getText().toString().trim() );
            }
            else if ( strCollectType == spinnerCollectType[1] ) //strCollectType.equals("月結袋集貨"))
            {
                if ( strData1.equals("") || strData1.length() < 5 )
                {
                    editGoodNum.setText( result.getText().toString().trim() );
                }
                else
                {
                    et02.setText( result.getText().toString().trim() );
                }
            }
            else if ( strCollectType == spinnerCollectType[2] ) //strCollectType.equals("月結袋解除綁定"))
            {
                editGoodNum.setText( result.getText().toString().trim() );
            }



        }

        handlerScan();

        //addResultToList(this.editGoodNum.getText().toString());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }


    //取得配達分區
    private void getDevliveryType(){

        //Constants.toast(getFragmentActivity(), "集貨分區-資料取得中,請稍後...");

        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("strType", "RO");

        RequestManager.instance().doRequest(Constants.API_POST_GET_DELIVERY_TYPE, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                List<String> returnlist = new ArrayList<String>();

                Log.e("----------------",jsonObject.toString());
                Log.e("----------------",jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    JsonArray objData = jsonObject.getAsJsonArray("choicelist");
                    Log.e("----------------",objData.toString());
                    Log.e("----------------",""+objData.size() );
                    Log.e("----------------",objData.get(0).toString());
                    Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
                    for ( int i = 0 ; i < objData.size() ; i++)
                    {
                        returnlist.add( ((JsonObject)objData.get(i)).get("choice").toString().replace("\"","").replace("\"","") );
                    }

                    if ( returnlist.size() > 1 )
                    {
                        CollectionBarcodeActivity2.this.spinnerContentA = returnlist.toArray(new String[0]);
                    }

                }else{


                }

                doSpinnerContentMain();
            }

            @Override
            public void onFail(String reason) {
                doSpinnerContentMain();
            }
        });


    }

    public void doSpinnerContentMain()
    {

        this.spinnerAbnormalStatus = (Spinner) findViewById(R.id.spinner_partition);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, this.spinnerContentA);
        this.spinnerAbnormalStatus.setAdapter((SpinnerAdapter) arrayAdapter);

        this.spinnerAbnormalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deliverySelect = spinnerContentA[position];

                selectSpinnerId = deliverySelect.substring(0, 2);
                selectSpinnerId = selectSpinnerId.trim();

                Log.e(TAG, "select = " + selectSpinnerId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Constants.toast(getFragmentActivity(), "集貨分區-更新完成");
    }

    private void doCollectTypeMain()
    {

        this.spinnerCollectTypeStatus = (Spinner) findViewById(R.id.tvCollectType);

        this.spinnerCollectTypeStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                strCollectType = spinnerCollectType[position] ;
                if ( strCollectType == spinnerCollectType[0] ) //strCollectType.equals("一般集貨"))
                {
//textMonthBagTitle
//textMonthBag
                    tv01.setVisibility(View.GONE);
                    et02.setVisibility(View.GONE);
//textBarcodeTitle
//textBarcode
                    tv03.setVisibility(View.VISIBLE);
                    tv04.setVisibility(View.VISIBLE);
//textView18
//spinner_partition
                    tv05.setVisibility(View.VISIBLE);
                    sp06.setVisibility(View.VISIBLE);


                }
                else if ( strCollectType == spinnerCollectType[1] ) //strCollectType.equals("月結袋集貨"))
                {
//textMonthBagTitle
//textMonthBag
                    tv01.setVisibility(View.VISIBLE);
                    et02.setVisibility(View.VISIBLE);
//textBarcodeTitle
//textBarcode
                    tv03.setVisibility(View.VISIBLE);
                    tv04.setVisibility(View.VISIBLE);
//textView18
//spinner_partition
                    tv05.setVisibility(View.VISIBLE);
                    sp06.setVisibility(View.VISIBLE);
                }
                else if ( strCollectType == spinnerCollectType[2] ) //strCollectType.equals("月結袋解除綁定"))
                {
//textMonthBagTitle
//textMonthBag
                    tv01.setVisibility(View.GONE);
                    et02.setVisibility(View.GONE);
//textBarcodeTitle
//textBarcode
                    tv03.setVisibility(View.GONE);
                    tv04.setVisibility(View.GONE);
//textView18
//spinner_partition
                    tv05.setVisibility(View.GONE);
                    sp06.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private List<String> Baglist = new ArrayList<String>();

    private void getBagListType(){

        //Constants.toast(getFragmentActivity(), "集貨分區-資料取得中,請稍後...");
        String strScanTime = Constants.getTime();
        HashMap<String, String> data = new HashMap<String, String>();

        RequestManager.instance().doRequest(Constants.API_POST_GET_BAG_LIST, "POST", data, new RequestManager.RequestManagerCallback() {
            @Override
            public void onResponse(JsonObject jsonObject) {

                Log.e("----------------",jsonObject.toString());
                Log.e("----------------",jsonObject.get("resultcode").toString());

                if (Boolean.valueOf(jsonObject.get("resultcode").toString()) == true){

                    JsonArray objData = jsonObject.getAsJsonArray("arriveOptions");
                    //Log.e("----------------",objData.toString());
                    //Log.e("----------------",""+objData.size() );
                    //Log.e("----------------",objData.get(0).toString());
                    //Log.e("----------------",((JsonObject)objData.get(0)).get("choice").toString().replace("\"","").replace("\"",""));
                    for ( int i = 0 ; i < objData.size() ; i++)
                    {
                        Baglist.add( ((JsonObject)objData.get(i)).get("ContainerCode").toString().replace("\"","").replace("\"","") );
                    }

                }else{


                }


            }

            @Override
            public void onFail(String reason) {
                doSpinnerContentMain();
            }
        });


    }

    // 將掃讀 20.取件成功 的資料寫進room
    private void insertData(String checkNumber) {
        new Thread(() -> {
            String driverCode = AccountManager.instance().getDriverCode(this).toUpperCase();
            CollectGoodsData collectGoodsData = new CollectGoodsData(UUID.randomUUID().toString(), driverCode, checkNumber, Constants.getTime(), Constants.getNowTimestamp());
            AppDataBase.getInstance(this).getCollectGoodsDao().insertData(collectGoodsData);
        }).start();
    }


}
