package tw.com.fsexpress.Menu.delivery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

public class FocusSurfaceView extends SurfaceView {
//public class FocusSurfaceView {
    private static final int DEFAULT_ANIMATION_DURATION_MILLIS = 100;

    private static final float DEFAULT_INITIAL_FRAME_SCALE = 0.75F;

    private static final int FRAME_STROKE_WEIGHT_IN_DP = 1;

    private static final int GUIDE_STROKE_WEIGHT_IN_DP = 1;

    private static final int HANDLE_SIZE_IN_DP = 14;

    private static final int MIN_FRAME_SIZE_IN_DP = 50;

    private static final int TRANSLUCENT_BLACK = -1157627904;

    private static final int TRANSLUCENT_WHITE = -1140850689;

    private static final int TRANSPARENT = 0;

    private static final int WHITE = -1;

    private final Interpolator DEFAULT_INTERPOLATOR;

    private int mAnimationDurationMillis;

    private float mBoundaryHeight = 0.0F;

    private RectF mBoundaryRect;

    private float mBoundaryWidth = 0.0F;

    private int mCropHeight;

    private CropMode mCropMode;

    private int mCropWidth;

    private PointF mCustomRatio;

    private Drawable mFrameBackground;

    private int mFrameColor;

    private RectF mFrameRect;

    private float mFrameStrokeWeight;

    private int mGuideColor;

    private ShowMode mGuideShowMode;

    private float mGuideStrokeWeight;

    private int mHandleColor;

    private ShowMode mHandleShowMode;

    private int mHandleSize;

    private float mInitialFrameScale;

    private Interpolator mInterpolator;

    private boolean mIsAnimating = false;

    private boolean mIsAnimationEnabled;

    private boolean mIsChangeEnabled;

    private boolean mIsCropEnabled;

    private boolean mIsEnabled;

    private boolean mIsHandleShadowEnabled;

    private boolean mIsInitialized = false;

    private boolean mIsRotating = false;

    private float mLastX;

    private float mLastY;

    private float mMinFrameSize;

    private int mOverlayColor;

    private Paint mPaintFrame;

    private Paint mPaintTranslucent;

    private boolean mShowGuide;

    private boolean mShowHandle;

    private TouchArea mTouchArea;

    private int mTouchPadding;

    public FocusSurfaceView(Context paramContext) {
        this(paramContext, (AttributeSet)null);
    }

    public FocusSurfaceView(Context paramContext, AttributeSet paramAttributeSet) {
        this(paramContext, paramAttributeSet, 0);
    }

    public FocusSurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator();
        this.DEFAULT_INTERPOLATOR = (Interpolator)decelerateInterpolator;
        this.mInterpolator = (Interpolator)decelerateInterpolator;
        this.mTouchArea = TouchArea.OUT_OF_BOUNDS;
        this.mCropMode = CropMode.SQUARE;
        this.mGuideShowMode = ShowMode.SHOW_ALWAYS;
        this.mHandleShowMode = ShowMode.SHOW_ALWAYS;
        this.mTouchPadding = 0;
        this.mShowGuide = true;
        this.mShowHandle = true;
        this.mIsCropEnabled = true;
        this.mIsEnabled = true;
        this.mIsChangeEnabled = false;
        this.mFrameStrokeWeight = 2.0F;
        this.mGuideStrokeWeight = 2.0F;
        this.mIsAnimationEnabled = true;
        this.mAnimationDurationMillis = 100;
        this.mIsHandleShadowEnabled = true;
        float f = getDensity();
        this.mHandleSize = (int)(14.0F * f);
        this.mMinFrameSize = 50.0F * f;
        f *= 1.0F;
        this.mFrameStrokeWeight = f;
        this.mGuideStrokeWeight = f;
        this.mPaintFrame = new Paint();
        this.mPaintTranslucent = new Paint();
        this.mFrameColor = -1;
        this.mOverlayColor = -1157627904;
        this.mHandleColor = -1;
        this.mGuideColor = -1140850689;
    }

    private RectF calcFrameRect(RectF paramRectF) {
        float f1 = getRatioX(paramRectF.width());
        float f2 = getRatioY(paramRectF.height());
        float f6 = paramRectF.width() / paramRectF.height();
        float f5 = f1 / f2;
        f1 = paramRectF.left;
        f2 = paramRectF.top;
        float f3 = paramRectF.right;
        float f4 = paramRectF.bottom;
        if (f5 >= f6) {
            f1 = paramRectF.left;
            f3 = paramRectF.right;
            f4 = (paramRectF.top + paramRectF.bottom) * 0.5F;
            f5 = paramRectF.width() / f5 * 0.5F;
            f2 = f4 - f5;
            f4 += f5;
        } else if (f5 < f6) {
            f2 = paramRectF.top;
            f4 = paramRectF.bottom;
            f1 = (paramRectF.left + paramRectF.right) * 0.5F;
            f5 = paramRectF.height() * f5 * 0.5F;
            f3 = f1 + f5;
            f1 -= f5;
        }
        f3 -= f1;
        f4 -= f2;
        f1 += f3 / 2.0F;
        f2 += f4 / 2.0F;
        f5 = this.mInitialFrameScale;
        f3 = f3 * f5 / 2.0F;
        f4 = f4 * f5 / 2.0F;
        return new RectF(f1 - f3, f2 - f4, f1 + f3, f2 + f4);
    }

    private void checkMoveBounds() {
        float f = this.mFrameRect.left - this.mBoundaryRect.left;
        if (f < 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.left -= f;
            rectF = this.mFrameRect;
            rectF.right -= f;
        }
        f = this.mFrameRect.right - this.mBoundaryRect.right;
        if (f > 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.left -= f;
            rectF = this.mFrameRect;
            rectF.right -= f;
        }
        f = this.mFrameRect.top - this.mBoundaryRect.top;
        if (f < 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.top -= f;
            rectF = this.mFrameRect;
            rectF.bottom -= f;
        }
        f = this.mFrameRect.bottom - this.mBoundaryRect.bottom;
        if (f > 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.top -= f;
            rectF = this.mFrameRect;
            rectF.bottom -= f;
        }
    }

    private void checkScaleBounds() {
        float f1 = this.mFrameRect.left - this.mBoundaryRect.left;
        float f2 = this.mFrameRect.right - this.mBoundaryRect.right;
        float f3 = this.mFrameRect.top - this.mBoundaryRect.top;
        float f4 = this.mFrameRect.bottom - this.mBoundaryRect.bottom;
        if (f1 < 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.left -= f1;
        }
        if (f2 > 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.right -= f2;
        }
        if (f3 < 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.top -= f3;
        }
        if (f4 > 0.0F) {
            RectF rectF = this.mFrameRect;
            rectF.bottom -= f4;
        }
    }

    private void checkTouchArea(float paramFloat1, float paramFloat2) {
        if (isInsideCornerLeftTop(paramFloat1, paramFloat2)) {
            this.mTouchArea = TouchArea.LEFT_TOP;
            if (this.mHandleShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowHandle = true;
            if (this.mGuideShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowGuide = true;
            return;
        }
        if (isInsideCornerRightTop(paramFloat1, paramFloat2)) {
            this.mTouchArea = TouchArea.RIGHT_TOP;
            if (this.mHandleShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowHandle = true;
            if (this.mGuideShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowGuide = true;
            return;
        }
        if (isInsideCornerLeftBottom(paramFloat1, paramFloat2)) {
            this.mTouchArea = TouchArea.LEFT_BOTTOM;
            if (this.mHandleShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowHandle = true;
            if (this.mGuideShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowGuide = true;
            return;
        }
        if (isInsideCornerRightBottom(paramFloat1, paramFloat2)) {
            this.mTouchArea = TouchArea.RIGHT_BOTTOM;
            if (this.mHandleShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowHandle = true;
            if (this.mGuideShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowGuide = true;
            return;
        }
        if (isInsideFrame(paramFloat1, paramFloat2)) {
            if (this.mGuideShowMode == ShowMode.SHOW_ON_TOUCH)
                this.mShowGuide = true;
            this.mTouchArea = TouchArea.CENTER;
            return;
        }
        this.mTouchArea = TouchArea.OUT_OF_BOUNDS;
    }

    private float constrain(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
        return (paramFloat1 >= paramFloat2) ? ((paramFloat1 > paramFloat3) ? paramFloat4 : paramFloat1) : paramFloat4;
    }

    private int dip2px(Context paramContext, float paramFloat) {
        return (int)(paramFloat * (paramContext.getResources().getDisplayMetrics()).density + 0.5F);
    }

    private void drawCropFrame(Canvas paramCanvas) {
        if (!this.mIsCropEnabled)
            return;
        if (this.mIsRotating)
            return;
        drawOverlay(paramCanvas);
        drawFrame(paramCanvas);
        if (this.mShowGuide)
            drawGuidelines(paramCanvas);
        if (this.mShowHandle)
            drawHandles(paramCanvas);
    }

    private void drawFrame(Canvas paramCanvas) {
        this.mPaintFrame.setAntiAlias(true);
        this.mPaintFrame.setFilterBitmap(true);
        this.mPaintFrame.setStyle(Paint.Style.STROKE);
        this.mPaintFrame.setColor(this.mFrameColor);
        this.mPaintFrame.setStrokeWidth(this.mFrameStrokeWeight);
        paramCanvas.drawRect(this.mFrameRect, this.mPaintFrame);
    }

    private void drawGuidelines(Canvas paramCanvas) {
        this.mPaintFrame.setColor(this.mGuideColor);
        this.mPaintFrame.setStrokeWidth(this.mGuideStrokeWeight);
        float f1 = this.mFrameRect.left + (this.mFrameRect.right - this.mFrameRect.left) / 3.0F;
        float f2 = this.mFrameRect.right - (this.mFrameRect.right - this.mFrameRect.left) / 3.0F;
        float f3 = this.mFrameRect.top + (this.mFrameRect.bottom - this.mFrameRect.top) / 3.0F;
        float f4 = this.mFrameRect.bottom - (this.mFrameRect.bottom - this.mFrameRect.top) / 3.0F;
        paramCanvas.drawLine(f1, this.mFrameRect.top, f1, this.mFrameRect.bottom, this.mPaintFrame);
        paramCanvas.drawLine(f2, this.mFrameRect.top, f2, this.mFrameRect.bottom, this.mPaintFrame);
        paramCanvas.drawLine(this.mFrameRect.left, f3, this.mFrameRect.right, f3, this.mPaintFrame);
        paramCanvas.drawLine(this.mFrameRect.left, f4, this.mFrameRect.right, f4, this.mPaintFrame);
    }

    private void drawHandleShadows(Canvas paramCanvas) {
        this.mPaintFrame.setStyle(Paint.Style.FILL);
        this.mPaintFrame.setColor(-1157627904);
        RectF rectF = new RectF(this.mFrameRect);
        rectF.offset(0.0F, 1.0F);
        paramCanvas.drawCircle(rectF.left, rectF.top, this.mHandleSize, this.mPaintFrame);
        paramCanvas.drawCircle(rectF.right, rectF.top, this.mHandleSize, this.mPaintFrame);
        paramCanvas.drawCircle(rectF.left, rectF.bottom, this.mHandleSize, this.mPaintFrame);
        paramCanvas.drawCircle(rectF.right, rectF.bottom, this.mHandleSize, this.mPaintFrame);
    }

    private void drawHandles(Canvas paramCanvas) {
        if (this.mIsHandleShadowEnabled)
            drawHandleShadows(paramCanvas);
        this.mPaintFrame.setStyle(Paint.Style.FILL);
        this.mPaintFrame.setColor(this.mHandleColor);
        paramCanvas.drawCircle(this.mFrameRect.left, this.mFrameRect.top, this.mHandleSize, this.mPaintFrame);
        paramCanvas.drawCircle(this.mFrameRect.right, this.mFrameRect.top, this.mHandleSize, this.mPaintFrame);
        paramCanvas.drawCircle(this.mFrameRect.left, this.mFrameRect.bottom, this.mHandleSize, this.mPaintFrame);
        paramCanvas.drawCircle(this.mFrameRect.right, this.mFrameRect.bottom, this.mHandleSize, this.mPaintFrame);
    }

    private void drawOverlay(Canvas paramCanvas) {
        this.mPaintTranslucent.setAntiAlias(true);
        this.mPaintTranslucent.setFilterBitmap(true);
        this.mPaintTranslucent.setColor(this.mOverlayColor);
        this.mPaintTranslucent.setStyle(Paint.Style.FILL);
        Path path = new Path();
        if (!this.mIsAnimating && (this.mCropMode == CropMode.CIRCLE || this.mCropMode == CropMode.CIRCLE_SQUARE)) {
            path.addRect(this.mBoundaryRect, Path.Direction.CW);
            PointF pointF = new PointF((this.mFrameRect.left + this.mFrameRect.right) / 2.0F, (this.mFrameRect.top + this.mFrameRect.bottom) / 2.0F);
            float f = (this.mFrameRect.right - this.mFrameRect.left) / 2.0F;
            path.addCircle(pointF.x, pointF.y, f, Path.Direction.CCW);
            paramCanvas.drawPath(path, this.mPaintTranslucent);
            return;
        }
        path.addRect(this.mBoundaryRect, Path.Direction.CW);
        path.addRect(this.mFrameRect, Path.Direction.CCW);
        paramCanvas.drawPath(path, this.mPaintTranslucent);
    }

    private float getDensity() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    private float getFrameHeight() {
        return this.mFrameRect.bottom - this.mFrameRect.top;
    }

    private float getFrameWidth() {
        return this.mFrameRect.right - this.mFrameRect.left;
    }

    private float getRatioX() {
        int i = this.mCropMode.ordinal();
        return (i != 1) ? ((i != 10) ? ((i != 3) ? ((i != 4) ? ((i != 5) ? ((i != 6) ? 1.0F : 9.0F) : 16.0F) : 3.0F) : 4.0F) : this.mCustomRatio.x) : this.mBoundaryRect.width();
    }

    private float getRatioX(float paramFloat) {
        switch (this.mCropMode) {
            case FREE:
            default:
                return paramFloat;
            case CUSTOM:
                return this.mCustomRatio.x;
            case SQUARE:
            case CIRCLE:
            case CIRCLE_SQUARE:
                return 1.0F;
            case RATIO_9_16:
                return 9.0F;
            case RATIO_16_9:
                return 16.0F;
            case RATIO_3_4:
                return 3.0F;
            case RATIO_4_3:
                return 4.0F;
            case FIT_IMAGE:
                break;
        }
        return this.mBoundaryRect.width();
    }

    private float getRatioY() {
        int i = this.mCropMode.ordinal();
        return (i != 1) ? ((i != 10) ? ((i != 3) ? ((i != 4) ? ((i != 5) ? ((i != 6) ? 1.0F : 16.0F) : 9.0F) : 4.0F) : 3.0F) : this.mCustomRatio.y) : this.mBoundaryRect.height();
    }

    private float getRatioY(float paramFloat) {
        switch (this.mCropMode) {
            case FREE:
            default:
                return paramFloat;
            case CUSTOM:
                return this.mCustomRatio.y;
            case SQUARE:
            case CIRCLE:
            case CIRCLE_SQUARE:
                return 1.0F;
            case RATIO_9_16:
                return 16.0F;
            case RATIO_16_9:
                return 9.0F;
            case RATIO_3_4:
                return 4.0F;
            case RATIO_4_3:
                return 3.0F;
            case FIT_IMAGE:
                break;
        }
        return this.mBoundaryRect.height();
    }

    private boolean isHeightTooSmall() {
        return (getFrameHeight() < this.mMinFrameSize);
    }

    private boolean isInsideCornerLeftBottom(float paramFloat1, float paramFloat2) {
        paramFloat1 -= this.mFrameRect.left;
        paramFloat2 -= this.mFrameRect.bottom;
        return (sq((this.mHandleSize + this.mTouchPadding)) >= paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    private boolean isInsideCornerLeftTop(float paramFloat1, float paramFloat2) {
        paramFloat1 -= this.mFrameRect.left;
        paramFloat2 -= this.mFrameRect.top;
        return (sq((this.mHandleSize + this.mTouchPadding)) >= paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    private boolean isInsideCornerRightBottom(float paramFloat1, float paramFloat2) {
        paramFloat1 -= this.mFrameRect.right;
        paramFloat2 -= this.mFrameRect.bottom;
        return (sq((this.mHandleSize + this.mTouchPadding)) >= paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    private boolean isInsideCornerRightTop(float paramFloat1, float paramFloat2) {
        paramFloat1 -= this.mFrameRect.right;
        paramFloat2 -= this.mFrameRect.top;
        return (sq((this.mHandleSize + this.mTouchPadding)) >= paramFloat1 * paramFloat1 + paramFloat2 * paramFloat2);
    }

    private boolean isInsideFrame(float paramFloat1, float paramFloat2) {
        if (this.mFrameRect.left <= paramFloat1 && this.mFrameRect.right >= paramFloat1 && this.mFrameRect.top <= paramFloat2 && this.mFrameRect.bottom >= paramFloat2) {
            this.mTouchArea = TouchArea.CENTER;
            return true;
        }
        return false;
    }

    private boolean isInsideHorizontal(float paramFloat) {
        return (this.mBoundaryRect.left <= paramFloat && this.mBoundaryRect.right >= paramFloat);
    }

    private boolean isInsideVertical(float paramFloat) {
        return (this.mBoundaryRect.top <= paramFloat && this.mBoundaryRect.bottom >= paramFloat);
    }

    private boolean isWidthTooSmall() {
        return (getFrameWidth() < this.mMinFrameSize);
    }

    private void moveFrame(float paramFloat1, float paramFloat2) {
        RectF rectF = this.mFrameRect;
        rectF.left += paramFloat1;
        rectF = this.mFrameRect;
        rectF.right += paramFloat1;
        rectF = this.mFrameRect;
        rectF.top += paramFloat2;
        rectF = this.mFrameRect;
        rectF.bottom += paramFloat2;
        checkMoveBounds();
    }

    private void moveHandleLB(float paramFloat1, float paramFloat2) {
        if (this.mCropMode == CropMode.FREE) {
            RectF rectF1 = this.mFrameRect;
            rectF1.left += paramFloat1;
            rectF1 = this.mFrameRect;
            rectF1.bottom += paramFloat2;
            if (isWidthTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameWidth();
                rectF1 = this.mFrameRect;
                rectF1.left -= paramFloat1 - paramFloat2;
            }
            if (isHeightTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameHeight();
                rectF1 = this.mFrameRect;
                rectF1.bottom += paramFloat1 - paramFloat2;
            }
            checkScaleBounds();
            return;
        }
        paramFloat2 = getRatioY() * paramFloat1 / getRatioX();
        RectF rectF = this.mFrameRect;
        rectF.left += paramFloat1;
        rectF = this.mFrameRect;
        rectF.bottom -= paramFloat2;
        if (isWidthTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameWidth();
            rectF = this.mFrameRect;
            rectF.left -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.bottom += paramFloat1;
        }
        if (isHeightTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameHeight();
            rectF = this.mFrameRect;
            rectF.bottom += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.left -= paramFloat1;
        }
        if (!isInsideHorizontal(this.mFrameRect.left)) {
            paramFloat1 = this.mBoundaryRect.left - this.mFrameRect.left;
            rectF = this.mFrameRect;
            rectF.left += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.bottom -= paramFloat1;
        }
        if (!isInsideVertical(this.mFrameRect.bottom)) {
            paramFloat1 = this.mFrameRect.bottom - this.mBoundaryRect.bottom;
            rectF = this.mFrameRect;
            rectF.bottom -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.left += paramFloat1;
        }
    }

    private void moveHandleLT(float paramFloat1, float paramFloat2) {
        if (this.mCropMode == CropMode.FREE) {
            RectF rectF1 = this.mFrameRect;
            rectF1.left += paramFloat1;
            rectF1 = this.mFrameRect;
            rectF1.top += paramFloat2;
            if (isWidthTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameWidth();
                rectF1 = this.mFrameRect;
                rectF1.left -= paramFloat1 - paramFloat2;
            }
            if (isHeightTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameHeight();
                rectF1 = this.mFrameRect;
                rectF1.top -= paramFloat1 - paramFloat2;
            }
            checkScaleBounds();
            return;
        }
        paramFloat2 = getRatioY() * paramFloat1 / getRatioX();
        RectF rectF = this.mFrameRect;
        rectF.left += paramFloat1;
        rectF = this.mFrameRect;
        rectF.top += paramFloat2;
        if (isWidthTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameWidth();
            rectF = this.mFrameRect;
            rectF.left -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.top -= paramFloat1;
        }
        if (isHeightTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameHeight();
            rectF = this.mFrameRect;
            rectF.top -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.left -= paramFloat1;
        }
        if (!isInsideHorizontal(this.mFrameRect.left)) {
            paramFloat1 = this.mBoundaryRect.left - this.mFrameRect.left;
            rectF = this.mFrameRect;
            rectF.left += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.top += paramFloat1;
        }
        if (!isInsideVertical(this.mFrameRect.top)) {
            paramFloat1 = this.mBoundaryRect.top - this.mFrameRect.top;
            rectF = this.mFrameRect;
            rectF.top += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.left += paramFloat1;
        }
    }

    private void moveHandleRB(float paramFloat1, float paramFloat2) {
        if (this.mCropMode == CropMode.FREE) {
            RectF rectF1 = this.mFrameRect;
            rectF1.right += paramFloat1;
            rectF1 = this.mFrameRect;
            rectF1.bottom += paramFloat2;
            if (isWidthTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameWidth();
                rectF1 = this.mFrameRect;
                rectF1.right += paramFloat1 - paramFloat2;
            }
            if (isHeightTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameHeight();
                rectF1 = this.mFrameRect;
                rectF1.bottom += paramFloat1 - paramFloat2;
            }
            checkScaleBounds();
            return;
        }
        paramFloat2 = getRatioY() * paramFloat1 / getRatioX();
        RectF rectF = this.mFrameRect;
        rectF.right += paramFloat1;
        rectF = this.mFrameRect;
        rectF.bottom += paramFloat2;
        if (isWidthTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameWidth();
            rectF = this.mFrameRect;
            rectF.right += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.bottom += paramFloat1;
        }
        if (isHeightTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameHeight();
            rectF = this.mFrameRect;
            rectF.bottom += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.right += paramFloat1;
        }
        if (!isInsideHorizontal(this.mFrameRect.right)) {
            paramFloat1 = this.mFrameRect.right - this.mBoundaryRect.right;
            rectF = this.mFrameRect;
            rectF.right -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.bottom -= paramFloat1;
        }
        if (!isInsideVertical(this.mFrameRect.bottom)) {
            paramFloat1 = this.mFrameRect.bottom - this.mBoundaryRect.bottom;
            rectF = this.mFrameRect;
            rectF.bottom -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.right -= paramFloat1;
        }
    }

    private void moveHandleRT(float paramFloat1, float paramFloat2) {
        if (this.mCropMode == CropMode.FREE) {
            RectF rectF1 = this.mFrameRect;
            rectF1.right += paramFloat1;
            rectF1 = this.mFrameRect;
            rectF1.top += paramFloat2;
            if (isWidthTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameWidth();
                rectF1 = this.mFrameRect;
                rectF1.right += paramFloat1 - paramFloat2;
            }
            if (isHeightTooSmall()) {
                paramFloat1 = this.mMinFrameSize;
                paramFloat2 = getFrameHeight();
                rectF1 = this.mFrameRect;
                rectF1.top -= paramFloat1 - paramFloat2;
            }
            checkScaleBounds();
            return;
        }
        paramFloat2 = getRatioY() * paramFloat1 / getRatioX();
        RectF rectF = this.mFrameRect;
        rectF.right += paramFloat1;
        rectF = this.mFrameRect;
        rectF.top -= paramFloat2;
        if (isWidthTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameWidth();
            rectF = this.mFrameRect;
            rectF.right += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.top -= paramFloat1;
        }
        if (isHeightTooSmall()) {
            paramFloat1 = this.mMinFrameSize - getFrameHeight();
            rectF = this.mFrameRect;
            rectF.top -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.right += paramFloat1;
        }
        if (!isInsideHorizontal(this.mFrameRect.right)) {
            paramFloat1 = this.mFrameRect.right - this.mBoundaryRect.right;
            rectF = this.mFrameRect;
            rectF.right -= paramFloat1;
            paramFloat1 = paramFloat1 * getRatioY() / getRatioX();
            rectF = this.mFrameRect;
            rectF.top += paramFloat1;
        }
        if (!isInsideVertical(this.mFrameRect.top)) {
            paramFloat1 = this.mBoundaryRect.top - this.mFrameRect.top;
            rectF = this.mFrameRect;
            rectF.top += paramFloat1;
            paramFloat1 = paramFloat1 * getRatioX() / getRatioY();
            rectF = this.mFrameRect;
            rectF.right -= paramFloat1;
        }
    }

    private void onCancel() {
        this.mTouchArea = TouchArea.OUT_OF_BOUNDS;
        invalidate();
    }

    private void onDown(MotionEvent paramMotionEvent) {
        invalidate();
        this.mLastX = paramMotionEvent.getX();
        this.mLastY = paramMotionEvent.getY();
        checkTouchArea(paramMotionEvent.getX(), paramMotionEvent.getY());
    }

    private void onMove(MotionEvent paramMotionEvent) {
        float f1 = paramMotionEvent.getX() - this.mLastX;
        float f2 = paramMotionEvent.getY() - this.mLastY;
        int i = this.mTouchArea.ordinal();
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        if (i == 5)
                            moveHandleRB(f1, f2);
                    } else {
                        moveHandleLB(f1, f2);
                    }
                } else {
                    moveHandleRT(f1, f2);
                }
            } else {
                moveHandleLT(f1, f2);
            }
        } else {
            moveFrame(f1, f2);
        }
        invalidate();
        this.mLastX = paramMotionEvent.getX();
        this.mLastY = paramMotionEvent.getY();
    }

    private void onUp(MotionEvent paramMotionEvent) {
        if (this.mGuideShowMode == ShowMode.SHOW_ON_TOUCH)
            this.mShowGuide = false;
        if (this.mHandleShowMode == ShowMode.SHOW_ON_TOUCH)
            this.mShowHandle = false;
        this.mTouchArea = TouchArea.OUT_OF_BOUNDS;
        invalidate();
    }

    private void setupLayout() {
        if (this.mBoundaryWidth != 0.0F) {
            if (this.mBoundaryHeight == 0.0F)
                return;
            this.mBoundaryRect = new RectF(0.0F, 0.0F, this.mBoundaryWidth, this.mBoundaryHeight);
            float f1 = this.mBoundaryWidth;
            int i = this.mCropWidth;
            f1 = (f1 - i) / 2.0F;
            float f2 = this.mBoundaryHeight;
            int j = this.mCropHeight;
            f2 = (f2 - j) / 2.0F;
            float f3 = i;
            float f4 = j;
            if (this.mFrameRect == null) {
                this.mFrameRect = new RectF(f1, f2, f3 + f1, f4 + f2);
                this.mIsInitialized = true;
            }
        }
    }

    private float sq(float paramFloat) {
        return paramFloat * paramFloat;
    }

    public Bitmap getCircularBitmap(Bitmap paramBitmap) {
        if (paramBitmap == null)
            return null;
        Bitmap bitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Rect rect = new Rect(0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
        Canvas canvas = new Canvas(bitmap);
        int i = paramBitmap.getWidth() / 2;
        int j = paramBitmap.getHeight() / 2;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        canvas.drawCircle(i, j, Math.min(i, j), paint);
        paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(paramBitmap, rect, rect, paint);
        return bitmap;
    }

    public RectF getFrameRect() {
        return this.mFrameRect;
    }

    public Bitmap getPicture(byte[] paramArrayOfbyte) {
        Bitmap bitmap1 = BitmapFactory.decodeByteArray(paramArrayOfbyte, 0, paramArrayOfbyte.length);
        float f1 = bitmap1.getWidth();
        float f2 = bitmap1.getHeight();
        float f3 = getWidth();
        float f4 = getHeight();
        float f5 = f1 / f3;
        float f6 = f2 / f4;
        RectF rectF = getFrameRect();
        float f7 = rectF.left;
        float f8 = rectF.top;
        float f9 = rectF.width();
        float f10 = rectF.height();
        int i = (int)(f7 * f5);
        int j = (int)(f8 * f6);
        int k = (int)(f9 * f5);
        int m = (int)(f10 * f6);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("picWidth");
        stringBuilder.append(Float.toString(f1));
        stringBuilder.append("picHeight");
        stringBuilder.append(Float.toString(f2));
        Log.d("", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("preWidth");
        stringBuilder.append(Float.toString(f3));
        stringBuilder.append("preHeight");
        stringBuilder.append(Float.toString(f4));
        Log.d("", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("preRW");
        stringBuilder.append(Float.toString(f5));
        stringBuilder.append("preRH");
        stringBuilder.append(Float.toString(f6));
        Log.d("", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("frameLeft");
        stringBuilder.append(Float.toString(f7));
        stringBuilder.append("frameTop");
        stringBuilder.append(Float.toString(f8));
        Log.d("", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("frameWidth");
        stringBuilder.append(Float.toString(f9));
        stringBuilder.append("frameHeight");
        stringBuilder.append(Float.toString(f10));
        Log.d("", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("cropLeft");
        stringBuilder.append(Integer.toString(i));
        stringBuilder.append("cropTop");
        stringBuilder.append(Integer.toString(j));
        Log.d("", stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("cropWidth");
        stringBuilder.append(Integer.toString(k));
        stringBuilder.append("cropHeight");
        stringBuilder.append(Integer.toString(m));
        Log.d("", stringBuilder.toString());
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap1, i, j, k, m);
        bitmap1 = bitmap2;
        if (this.mCropMode == CropMode.CIRCLE)
            bitmap1 = getCircularBitmap(bitmap2);
        return bitmap1;
    }

    public int geth() {
        return getHeight();
    }

    public float getmBoundaryHeight() {
        return this.mBoundaryHeight;
    }

    public float getmBoundaryWidth() {
        return this.mBoundaryWidth;
    }

    public int getw() {
        return getWidth();
    }

    public void onDraw(Canvas paramCanvas) {
        if (this.mIsInitialized) {
            if (this.mFrameBackground != null) {
                paramCanvas.save();
                paramCanvas.translate((getWidth() / 2), (getHeight() / 2));
                Drawable drawable = this.mFrameBackground;
                int i = this.mCropWidth;
                int j = -i / 2;
                int k = this.mCropHeight;
                drawable.setBounds(j, -k / 2, i / 2, k / 2);
                this.mFrameBackground.draw(paramCanvas);
                paramCanvas.restore();
            }
            drawCropFrame(paramCanvas);
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
        setupLayout();
    }

    protected void onMeasure(int paramInt1, int paramInt2) {
        paramInt1 = View.MeasureSpec.getSize(paramInt1);
        paramInt2 = View.MeasureSpec.getSize(paramInt2);
        setMeasuredDimension(paramInt1, paramInt2);
        this.mBoundaryWidth = (paramInt1 - getPaddingLeft() - getPaddingRight());
        this.mBoundaryHeight = (paramInt2 - getPaddingTop() - getPaddingBottom());
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent) {
        if (!this.mIsChangeEnabled)
            return false;
        if (!this.mIsInitialized)
            return false;
        if (!this.mIsCropEnabled)
            return false;
        if (!this.mIsEnabled)
            return false;
        if (this.mIsRotating)
            return false;
        if (this.mIsAnimating)
            return false;
        int i = paramMotionEvent.getAction();
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3)
                        return false;
                    getParent().requestDisallowInterceptTouchEvent(false);
                    onCancel();
                    return true;
                }
                onMove(paramMotionEvent);
                if (this.mTouchArea != TouchArea.OUT_OF_BOUNDS)
                    getParent().requestDisallowInterceptTouchEvent(true);
                return true;
            }
            getParent().requestDisallowInterceptTouchEvent(false);
            onUp(paramMotionEvent);
            return true;
        }
        onDown(paramMotionEvent);
        return true;
    }

    public void setCropEnabled(boolean paramBoolean) {
        this.mIsCropEnabled = paramBoolean;
        invalidate();
    }

    public void setCropMode(CropMode paramCropMode) {
        setCropMode(paramCropMode, this.mAnimationDurationMillis);
    }

    public void setCropMode(CropMode paramCropMode, int paramInt) {
        if (paramCropMode == CropMode.CUSTOM) {
            setCustomRatio(1, 1);
            return;
        }
        this.mCropMode = paramCropMode;
    }

    public void setCustomRatio(int paramInt1, int paramInt2) {
        setCustomRatio(paramInt1, paramInt2, this.mAnimationDurationMillis);
    }

    public void setCustomRatio(int paramInt1, int paramInt2, int paramInt3) {
        if (paramInt1 != 0) {
            if (paramInt2 == 0)
                return;
            this.mCropMode = CropMode.CUSTOM;
            this.mCustomRatio.set(paramInt1, paramInt2);
        }
    }

    public void setEnabled(boolean paramBoolean) {
        super.setEnabled(paramBoolean);
        this.mIsEnabled = paramBoolean;
    }

    public void setFrameColor(int paramInt) {
        this.mFrameColor = paramInt;
        invalidate();
    }

    public void setFrameStrokeWeightInDp(int paramInt) {
        this.mFrameStrokeWeight = paramInt * getDensity();
        invalidate();
    }

    public void setGuideColor(int paramInt) {
        this.mGuideColor = paramInt;
        invalidate();
    }

    public void setGuideShowMode(ShowMode paramShowMode) {
        this.mGuideShowMode = paramShowMode;
        int i = paramShowMode.ordinal();
        if (i != 1) {
            if (i == 2 || i == 3)
                this.mShowGuide = false;
        } else {
            this.mShowGuide = true;
        }
        invalidate();
    }

    public void setGuideStrokeWeightInDp(int paramInt) {
        this.mGuideStrokeWeight = paramInt * getDensity();
        invalidate();
    }

    public void setHandleColor(int paramInt) {
        this.mHandleColor = paramInt;
        invalidate();
    }

    public void setHandleShadowEnabled(boolean paramBoolean) {
        this.mIsHandleShadowEnabled = paramBoolean;
    }

    public void setHandleShowMode(ShowMode paramShowMode) {
        this.mHandleShowMode = paramShowMode;
        int i = paramShowMode.ordinal();
        if (i != 1) {
            if (i == 2 || i == 3)
                this.mShowHandle = false;
        } else {
            this.mShowHandle = true;
        }
        invalidate();
    }

    public void setHandleSizeInDp(int paramInt) {
        this.mHandleSize = (int)(paramInt * getDensity());
    }

    public void setInitialFrameScale(float paramFloat) {
        this.mInitialFrameScale = constrain(paramFloat, 0.01F, 1.0F, 0.75F);
    }

    public void setMinFrameSizeInDp(int paramInt) {
        this.mMinFrameSize = paramInt * getDensity();
    }

    public void setMinFrameSizeInPx(int paramInt) {
        this.mMinFrameSize = paramInt;
    }

    public void setOverlayColor(int paramInt) {
        this.mOverlayColor = paramInt;
        invalidate();
    }

    public void setTouchPaddingInDp(int paramInt) {
        this.mTouchPadding = (int)(paramInt * getDensity());
    }

    public enum CropMode {
        FIT_IMAGE(0), RATIO_4_3(1), RATIO_3_4(2), SQUARE(3), RATIO_16_9(4), RATIO_9_16(5), FREE(
                6), CUSTOM(7), CIRCLE(8), CIRCLE_SQUARE(9);
        private final int ID;

        CropMode(final int id) {
            this.ID = id;
        }

        public int getId() {
            return ID;
        }
    }

//    public enum CropMode {
//        CIRCLE,
//        CIRCLE_SQUARE,
//        CUSTOM,
//        FIT_IMAGE(0),
//        FREE(0),
//        RATIO_16_9(0),
//        RATIO_3_4(0),
//        RATIO_4_3(1),
//        RATIO_9_16(1),
//        SQUARE(1);
//
//        private final int ID;
//
//        static {
//            RATIO_16_9 = new CropMode("RATIO_16_9", 4, 4);
//            RATIO_9_16 = new CropMode("RATIO_9_16", 5, 5);
//            FREE = new CropMode("FREE", 6, 6);
//            CUSTOM = new CropMode("CUSTOM", 7, 7);
//            CIRCLE = new CropMode("CIRCLE", 8, 8);
//            CropMode cropMode = new CropMode("CIRCLE_SQUARE", 9, 9);
//            CIRCLE_SQUARE = cropMode;
//            $VALUES = new CropMode[] { FIT_IMAGE, RATIO_4_3, RATIO_3_4, SQUARE, RATIO_16_9, RATIO_9_16, FREE, CUSTOM, CIRCLE, cropMode };
//        }
//
//        CropMode(int param1Int1) {
//            this.ID = param1Int1;
//        }
//
//        public int getId() {
//            return this.ID;
//        }
//    }

//    public enum ShowMode {
//        SHOW_ALWAYS(1),
//        NOT_SHOW(1),
//        SHOW_ON_TOUCH(2);
//
//        private final int ID;
//
//        static {
//            ShowMode showMode = new ShowMode("NOT_SHOW", 2, 3);
//            NOT_SHOW = showMode;
//            $VALUES = new ShowMode[] { SHOW_ALWAYS, SHOW_ON_TOUCH, showMode };
//        }
//
//        ShowMode(int param1Int1) {
//            this.ID = param1Int1;
//        }
//
//        public int getId() {
//            return this.ID;
//        }
//    }

    public enum ShowMode {
        SHOW_ALWAYS(1), SHOW_ON_TOUCH(2), NOT_SHOW(3);
        private final int ID;

        ShowMode(final int id) {
            this.ID = id;
        }

        public int getId() {
            return ID;
        }
    }

//    private enum TouchArea {
//        OUT_OF_BOUNDS(),
//        RIGHT_BOTTOM,
//        RIGHT_TOP,
//        CENTER(2),
//        LEFT_BOTTOM(2),
//        LEFT_TOP(2);
//
//        static {
//            LEFT_BOTTOM = new TouchArea("LEFT_BOTTOM", 4);
//            TouchArea touchArea = new TouchArea("RIGHT_BOTTOM", 5);
//            RIGHT_BOTTOM = touchArea;
//            $VALUES = new TouchArea[] { OUT_OF_BOUNDS, CENTER, LEFT_TOP, RIGHT_TOP, LEFT_BOTTOM, touchArea };
//        }
//    }

    private enum TouchArea {
        OUT_OF_BOUNDS, CENTER, LEFT_TOP, RIGHT_TOP, LEFT_BOTTOM, RIGHT_BOTTOM
    }
}
