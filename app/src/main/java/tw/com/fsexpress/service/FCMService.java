package tw.com.fsexpress.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Objects;
import java.util.Random;

import tw.com.fsexpress.Constants;
import tw.com.fsexpress.LoginActivity;
import tw.com.fsexpress.R;

public class FCMService extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Constants.set(this, "fcm_token", token);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage message) {
        super.onMessageReceived(message);
        Log.d("hardy", "message:" + message.getData());
        Log.d("hardy", "notification:" + message.getNotification());
        String title;
        String body;
        String sound;
        String newsId;
        title = message.getData().get("title");
        body = message.getData().get("body");
        sound = message.getData().get("sound");
        newsId = message.getData().get("news_id");
        if (title == null) title = Objects.requireNonNull(message.getNotification()).getTitle();
        if (body == null) body = Objects.requireNonNull(message.getNotification()).getBody();
        showNotification(title, body, sound, newsId);
    }

    private void showNotification(String title, String body, String sound, String newsId) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra("fcmNewsId", "yes");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { // android 8 以上用這段 目前全速配手機是android 7
            NotificationChannel notificationChannel = new NotificationChannel("CHANNEL_ID", "myID", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("myChannel");
            notificationChannel.enableLights(true);
            notificationChannel.setShowBadge(false);
            if (sound != null) {
                if (sound.equals("Enabled")) {
                    Uri fcmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.fcm_sound);
                    AudioAttributes audioAttributes = new AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setUsage(AudioAttributes.USAGE_ALARM)
                            .build();
                    notificationChannel.setSound(fcmSound, audioAttributes);
                }
            }
            notificationManager.createNotificationChannel(notificationChannel);
        }

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        powerManager.isInteractive();


        long[] pattern = {1000, 500, 1000, 400, 1000, 300, 1000, 200, 1000, 100};

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID");

        Uri fcmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.fcm_sound);

        notificationBuilder.setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(body)
                .setTicker(body)
                .setWhen(System.currentTimeMillis())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(pattern)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                .setLights(Color.BLACK, 10, 10)
                .setSound(fcmSound, AudioManager.STREAM_ALARM)
                .setContentIntent(pendingIntent);

        notificationManager.notify(new Random().nextInt(), notificationBuilder.build());
    }
}
