package tw.com.fsexpress.service;

import android.util.Log;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.Criteria;
import tw.com.fsexpress.framework.database.CriteriaCompo;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;
import tw.com.fsexpress.okhttputils.OkHttpUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class ServiceRequestObject {


    private DataObject data = null;
    public static String statusFinish = "1";
    public static String statusNotFinish = "2";
    public static String statusDownloading = "3";
    private ServiceRequestCallback callback = null;
    private String download_status = "2";

    public ServiceRequestObject (DataObject d) {
        data = d;
    }

    public void setStatus(String s){

        this.download_status = s;
    }

    public String getStatus(){
        return download_status;
    }
    public void setCallback(ServiceRequestCallback _callback){
        this.callback = _callback;
    }

    public void request()  {

        try{

            String requestUrl = this.data.getVariable("request_name").toString();
            String requestMethod = this.data.getVariable("request_method").toString();
            String param = this.data.getVariable("request_parameter").toString();

            RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");
            RequestObject requestObject = (RequestObject) new RequestObject();
            requestObject.setUrl(requestUrl);
            if (requestMethod.equals("POST")) {
                requestObject.setMethod(RequestObject.METHOD.POST);
            }else{
                requestObject.setMethod(RequestObject.METHOD.GET);
            }

            JSONObject objJson = new JSONObject(param);
            Iterator<String> keysItr = objJson.keys();
            while (keysItr.hasNext()) {
                String key = keysItr.next();
                String value = objJson.get(key).toString();
                requestObject.addParameter(key, value);
            }

            request.addListener(new RequestCallback() {
                @Override
                public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {
                    if (requestStatus == RequestStatus.Finish){
                        try{

                            String result = OkHttpUtil.getTextFromXML(requestHandler.getReceiveString());
                            Log.e("result", "result : "+result);
                            JsonObject convertedObject = new Gson().fromJson(result, JsonObject.class);
                            if (Boolean.valueOf(convertedObject.get("resultcode").toString()) == true){
                                Commander cmd = Commander.instance();
                                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.request");
                                handler.deleteByCriteria(new Criteria("id", ServiceRequestObject.this.data.getVariable("id").toString()));
                            }else{

                            }

                        }catch (Exception e){



                        }
                    }
                    if (requestStatus == RequestStatus.Fail || requestStatus == RequestStatus.NoNetworkConnection){

                        if (data.getVariable("count") != null){

                            Integer count =(Integer)data.getVariable("count");
                            Commander cmd = Commander.instance();
                            DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.request");
                            if (count == 50){
                                handler.deleteByDataObject(data);
                            }else{
                                count++;
                                data.setVariable("count", count);
                                for (String key : data.getVariables().keySet())
                                {
                                    HashMap<String, Object> hash = new HashMap<String, Object>();
                                    hash.put(key, data.getVariable(key).toString());
                                    CriteriaCompo cCompo = new CriteriaCompo();
                                    cCompo.add(new Criteria("id", data.getVariable("id").toString()));
                                    handler.updateByHashMap(hash, cCompo);
                                }

                            }
                        }


                    }
                }
            });
            request.request(requestObject);
        }catch (Exception e){
            Log.e("Jerry", e.toString());
        }

    }
}
