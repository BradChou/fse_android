package tw.com.fsexpress.service;

import tw.com.fsexpress.framework.dataobject.DataObject;

import java.util.HashMap;

public class ServiceRequestController {

    private static ServiceRequestController mInstance = null;
    private HashMap<String, DataObject> hash = new HashMap<String, DataObject>();
    private HashMap<String, ServiceRequestObject> hashRequest = new HashMap<String, ServiceRequestObject>();
    private int api_count = 0;
    private int MaxLine = 3;

    public static ServiceRequestController intance()
    {
        if (mInstance == null) {

            mInstance = new ServiceRequestController();
        }

        return mInstance;
    }

    private void checkRequest()
    {
        for (Object key : hash.keySet()) {

            DataObject d = hash.get(key);

            ServiceRequestObject o = getRequest(d);

            if (o.getStatus() != ServiceRequestObject.statusDownloading) {

                if (api_count < MaxLine)
                {
                    o.setStatus(ServiceRequestObject.statusDownloading);

                    o.request();

                    api_count = api_count + 1;
                }
            }
        }
    }

    public void addRequest(DataObject d)
    {

        ServiceRequestObject o = new ServiceRequestObject(d);

        this.hashRequest.put(d.getVariable("id").toString(), o);
    }

    public ServiceRequestObject getRequest(DataObject d)
    {
        return this.hashRequest.get(d.getVariable("id").toString());
    }

    public void removeRequest(DataObject d)
    {
        this.hashRequest.remove(d.getVariable("id").toString());

        hash.remove(d.getVariable("id").toString());

        api_count = api_count - 1;

        checkRequest();
    }

    public void startRequest(DataObject d)
    {

        hash.put(d.getVariable("id").toString(), d);

        if (hash.size() <= MaxLine) {

            api_count = api_count + 1;

            ServiceRequestObject o = this.hashRequest.get(d.getVariable("id").toString());

            o.setStatus(ServiceRequestObject.statusDownloading);

            o.setCallback(new ServiceRequestCallback() {
                @Override
                public void onDownloading() {

                }

                @Override
                public void onDownloadFinished(DataObject data) {
                    ServiceRequestController.this.removeRequest(data);
                }

                @Override
                public void onDownloadFail() {
                }
            });

            o.request();
        }
    }


}
