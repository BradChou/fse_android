package tw.com.fsexpress.service;

import tw.com.fsexpress.framework.dataobject.DataObject;

/**
 * Created by jerry on 2018/4/18.
 */

public interface ServiceRequestCallback {

    public void onDownloading();
    public void onDownloadFinished(DataObject data);
    public void onDownloadFail();


}
