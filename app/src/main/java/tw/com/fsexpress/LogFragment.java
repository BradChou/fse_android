package tw.com.fsexpress;

import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragment;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.framework.ui.GHWIndexPath;
import tw.com.fsexpress.framework.ui.GHWListView;

import java.util.ArrayList;
import java.util.List;

public class LogFragment extends GHWFragment {

    private GHWListView listView = null;
    private List<DataObject> arrData = null;
    private LogFragment.ListViewHolder holder;

    static class ListViewHolder
    {
        TextView txtBarcode;
        TextView txtLogDate;
        TextView txtLogDescription;
    }

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);

        view.findViewById(R.id.clear_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Constants.ErrorAlertDialog(LogFragment.this.getFragmentActivity(), "訊息記錄", "是否要刪除錯誤訊息。", "確定", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Commander cmd = Commander.instance();
                        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
                        handler.deleteByCriteria(null);

                        arrData.clear();
                        listView.reloadData();
                    }
                }, null);




            }
        });

        Commander cmd = Commander.instance();
        DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.logs");
        this.arrData = handler.getDataByCriteria(null);
        if (this.arrData == null){
            this.arrData = new ArrayList<DataObject>();
        }

        listView = (GHWListView) view.findViewById(R.id.rv);
        listView.dataSource = new GHWListView.DataSource() {
            @Override
            public int numberOfSectionsInListView(GHWListView.GHWBaseAdapter ad) {
                return 0;
            }

            @Override
            public int numberOfRowsInSection(GHWListView.GHWBaseAdapter ad, int section) {
                return arrData.size();
            }

            @Override
            public View getItemView(GHWListView parent, View cell, GHWIndexPath indexPath) {
                if (cell == null)
                {
                    cell = LogFragment.this.getFragmentActivity().getLayoutInflater().inflate(R.layout.cell_log, null);

                    holder = new LogFragment.ListViewHolder();
                    holder.txtLogDate = (TextView) cell.findViewById(R.id.txtLogDate);
                    holder.txtLogDescription = (TextView) cell.findViewById(R.id.txtDescription);
                    holder.txtBarcode = (TextView) cell.findViewById(R.id.txtBarcode);
                    cell.setTag(holder);

                } else
                {
                    holder = (LogFragment.ListViewHolder) cell.getTag();
                }

                DataObject d = arrData.get(indexPath.row);

                holder.txtLogDescription.setText(d.getVariable("log_description").toString());
//                long log_date = Long.valueOf(d.getVariable("log_date").toString());
//                String strDate = Constants.convertUnixToDataFormat(log_date, "yyyy/MM/dd HH:mm:ss");
//                holder.txtLogDate.setText(strDate);
                holder.txtLogDate.setText(d.getVariable("log_date").toString());
//                Log.e("Jerry", "barcode:"+d.getVariable("log_barcode").toString());
                holder.txtBarcode.setText(d.getVariable("log_barcode").toString());

//                holder.textDate.setText(String.format("%s", d.getVariable("task_date").toString()));
//                holder.textMark.setText(String.format("%s", d.getVariable("memo").toString()));
//                holder.textSupName.setText(String.format("%s", d.getVariable("supplier_shortname").toString()));

                return cell;
            }

            @Override
            public View getSectionView(GHWListView parent, View convertView, int section) {
                return null;
            }
        };












    }
}
