package tw.com.fsexpress;

import android.os.Bundle;
import android.view.Window;

import com.facebook.stetho.Stetho;

import tw.com.fsexpress.framework.activity.GHWFragmentActivity;

//import com.gaohowa.android.framework.activity.GHWFragmentActivity;

public class LoginActivity extends GHWFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        Stetho.initializeWithDefaults(this);//設置資料庫監視

        String fcmNewsId = getIntent().getStringExtra("fcmNewsId");
        LoginFragment fg = new LoginFragment();
        fg.setLayoutResourceId(R.layout.layout_login);
        fg.setFcmNewsId(fcmNewsId);
        this.initWithRootFragment(fg);
    }
}
