package tw.com.fsexpress;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.database.DatabaseHandler;
import tw.com.fsexpress.framework.dataobject.DataObject;
import tw.com.fsexpress.service.ServiceRequestController;

import java.util.Date;
import java.util.List;

public class MyService extends Service {

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground(1,new Notification());
//        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try
        {
            Log.e("MyService", "executed at " + new Date().toString());

            Commander cmd = Commander.instance();
            DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.request");
            List<DataObject> arrData = handler.getDataByCriteria(null);
            //Log.e("---------------arrData------------", "arrData:"+arrData.size());
            //Log.e("---------------arrData------------", "arrData:"+arrData.toString());
            if (arrData.size() != 0){

                for (int idx=0; idx < arrData.size(); idx++){
                    DataObject d = arrData.get(idx);
//                for (String key : d.getVariables().keySet())
//                {
//                    Log.e("Jerry", "key:"+key+" value:"+ d.getVariable(key));
//                }
                    ServiceRequestController.intance().addRequest(d);
                }

                ServiceRequestController.intance().startRequest(arrData.get(0));

            }


            AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
            int anHour = 5 * 60 * 1000; // 5分鐘
            long triggerAtTime = SystemClock.elapsedRealtime() + anHour;
            Intent i = new Intent(this, AlarmReceiver.class);
            PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
            manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
        }
        catch(Exception ex)
        {
            Log.e("---------------MyService ERROR------------", ex.toString());
        }
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Log.e("MyService", "executed at " + new Date().
//                        toString());
//
//                Commander cmd = Commander.instance();
//                DatabaseHandler handler = (DatabaseHandler) cmd.handlerWithName("ghw.db.junfu.request");
//                List<DataObject> arrData = handler.getDataByCriteria(null);
//
//            }
//        }).start();




        return super.onStartCommand(intent, flags, startId);
    }


}