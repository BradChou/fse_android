package tw.com.fsexpress;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import tw.com.fsexpress.framework.activity.GHWFragment;

import org.json.JSONObject;

public class DriverInputFragment extends GHWFragment {


    private EditText editCarNum, editName, editDriverPhone;
    private String accountType;

    @Override
    protected void viewDidAppear(ViewGroup view) {
        super.viewDidAppear(view);


        this.editCarNum = (EditText)view.findViewById(R.id.edit_car_num);
        this.editName = (EditText)view.findViewById(R.id.edit_name);
        this.editDriverPhone = (EditText)view.findViewById(R.id.edit_cell_num);




        try{
            JSONObject jsonMember = AccountManager.instance().getMemberInfo(getFragmentActivity());
            this.accountType = jsonMember.getString("type");
            Log.e("Jerry", "type:"+this.accountType);

            if (!this.accountType.equals("1")){

                this.editName.setVisibility(View.GONE);
                this.editDriverPhone.setVisibility(View.GONE);

            }

        }catch(Exception e){

        }

        view.findViewById(R.id.btn_start_driver).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DriverInputFragment.this.editCarNum.getText().toString().equals("")){
                    Constants.ErrorAlertDialog(DriverInputFragment.this.getFragmentActivity(), "車牌號碼", "請輸入車牌號碼。", "確定", null, null, null);
                    return;
                }
                AccountManager.instance().saveCarNum(DriverInputFragment.this.getFragmentActivity(), DriverInputFragment.this.editCarNum.getText().toString());
                if (DriverInputFragment.this.accountType.equals("1")){
                    if (DriverInputFragment.this.editName.getText().toString().equals("")){
                        Constants.ErrorAlertDialog(DriverInputFragment.this.getFragmentActivity(), "司機姓名", "請輸入司機姓名。", "確定", null, null, null);
                        return;
                    }
                    if (DriverInputFragment.this.editDriverPhone.getText().toString().equals("")){
                        Constants.ErrorAlertDialog(DriverInputFragment.this.getFragmentActivity(), "司機電話", "請輸入司機電話。", "確定", null, null, null);
                        return;
                    }
                    if (DriverInputFragment.this.editDriverPhone.getText().toString().length() != 10){
                        Constants.ErrorAlertDialog(DriverInputFragment.this.getFragmentActivity(), "司機電話", "電話格式不符。", "確定", null, null, null);
                        return;

                    }


                    AccountManager.instance().saveDriverName(DriverInputFragment.this.getFragmentActivity(), DriverInputFragment.this.editName.getText().toString());
                    AccountManager.instance().saveDriverPhone(DriverInputFragment.this.getFragmentActivity(), DriverInputFragment.this.editDriverPhone.getText().toString());
                }
                Intent intent = new Intent(DriverInputFragment.this.getFragmentActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                DriverInputFragment.this.getFragmentActivity().finish();
            }
        });








    }
}
