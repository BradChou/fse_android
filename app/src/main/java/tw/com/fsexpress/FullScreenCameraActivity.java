package tw.com.fsexpress;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;

import androidx.core.content.ContextCompat;

import tw.com.fsexpress.Menu.camera.TakePhotoManager;
import tw.com.fsexpress.Menu.delivery.CustomSurfaceView;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class FullScreenCameraActivity extends Activity implements SurfaceHolder.Callback {
    private static final String TAG = "ALVIN";

    private Handler handler;

    public static String BIG_PIC;

    private static final SparseIntArray ORIENTATIONS;

    public static String SMALL_PIC;

    private static final String mCameraId = "0";

    private View container;

    private CameraCaptureSession mCameraCaptureSession;

    private CameraDevice mCameraDevice;

    private CameraDevice.StateCallback mCameraDeviceCallBack = new CameraDevice.StateCallback() {
        public void onDisconnected(CameraDevice param1CameraDevice) {
            FullScreenCameraActivity.this.mCameraOpenCloseLock.release();
            if (FullScreenCameraActivity.this.mCameraDevice != null) {
                FullScreenCameraActivity.this.mCameraDevice.close();
                mCameraDevice = null;
            }
        }

        public void onError(CameraDevice param1CameraDevice, int param1Int) {
        }

        public void onOpened(CameraDevice param1CameraDevice) {
            try {
                FullScreenCameraActivity.this.mCameraOpenCloseLock.release();
                mCameraDevice = param1CameraDevice;
                FullScreenCameraActivity.this.createCameraCaptureSession();
                return;
            } catch (CameraAccessException cameraAccessException) {
                cameraAccessException.printStackTrace();
                return;
            }
        }
    };

    private CameraManager mCameraManager;

    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    private CustomSurfaceView mCameraSurfaceView;

    private CaptureRequest.Builder mCaptureRequestBuilder;

    private ImageReader.OnImageAvailableListener mImageAvailableListenter = new ImageReader.OnImageAvailableListener() {
        public void onImageAvailable(ImageReader param1ImageReader) {
            Image image = null;
            FullScreenCameraActivity.this.mCameraDevice.close();
            param1ImageReader = null;
            try {
                Image image1 = FullScreenCameraActivity.this.mImageReader.acquireLatestImage();
                image = image1;
                ByteBuffer byteBuffer = image1.getPlanes()[0].getBuffer();
                image = image1;
                int i = byteBuffer.capacity();
                image = image1;
                byte[] arrayOfByte = new byte[i];
                image = image1;
                byteBuffer.get(arrayOfByte);
                image = image1;
                Bitmap bitmap1 = BitmapFactory.decodeByteArray(arrayOfByte, 0, i);
                if (bitmap1 == null) {
                    image = image1;
                    Constants.toast((Context) FullScreenCameraActivity.this, "解析失敗！請重拍一次");
                    return;
                }
                image = image1;
                Matrix matrix = new Matrix();
                image = image1;
                matrix.postRotate(90.0F);
                image = image1;
                Bitmap bitmap2 = Bitmap.createBitmap(bitmap1, 0, 0, bitmap1.getWidth(), bitmap1.getHeight(), matrix, true);
                image = image1;
                bitmap1 = Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), (int) (bitmap2.getHeight() * FullScreenCameraActivity.this.mCameraSurfaceView.getScale()));
                image = image1;
                float f = FullScreenCameraActivity.this.mCameraSurfaceView.getScale() * (1.0F - FullScreenCameraActivity.this.mCameraSurfaceView.getScale());
                image = image1;
                bitmap2 = Bitmap.createBitmap(bitmap2, 0, (int) (bitmap2.getHeight() * f), (int) (bitmap2.getWidth() * FullScreenCameraActivity.this.mCameraSurfaceView.getScale()), (int) (bitmap2.getHeight() * FullScreenCameraActivity.this.mCameraSurfaceView.getScale()) - (int) (bitmap2.getHeight() * f));
                image = image1;
                Intent intent = new Intent();
                image = image1;
                intent.putExtra(FullScreenCameraActivity.BIG_PIC, (Parcelable) TakePhotoManager.getResizedBitmap(bitmap1, 350));
                image = image1;
                intent.putExtra(FullScreenCameraActivity.SMALL_PIC, (Parcelable) TakePhotoManager.getResizedBitmap(bitmap2, 350));
                image = image1;
                FullScreenCameraActivity.this.setResult(-1, intent);
                image = image1;
                FullScreenCameraActivity.this.finish();
                return;
            } catch (Exception exception) {
                if (image != null)
                    image.close();
                FullScreenCameraActivity fullScreenCameraActivity = FullScreenCameraActivity.this;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("請重試！解析失敗->");
                stringBuilder.append(exception);
                Constants.toast(FullScreenCameraActivity.this, stringBuilder.toString());
                FullScreenCameraActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FullScreenCameraActivity.this.finish();
                    }
                });
                return;
            }
        }
    };

    private ImageReader mImageReader;

    private CameraCaptureSession.StateCallback mSessionPreviewStateCallback = new CameraCaptureSession.StateCallback() {
        public void onConfigureFailed(CameraCaptureSession param1CameraCaptureSession) {
            Constants.toast((Context) FullScreenCameraActivity.this, "預覽失敗，請重試");
            FullScreenCameraActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FullScreenCameraActivity.this.finish();
                }
            });
        }

        public void onConfigured(CameraCaptureSession param1CameraCaptureSession) {
            try {
                mCameraCaptureSession = param1CameraCaptureSession;
                FullScreenCameraActivity.this.mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, Integer.valueOf(4));
                CaptureRequest captureRequest = FullScreenCameraActivity.this.mCaptureRequestBuilder.build();
                FullScreenCameraActivity.this.mCameraCaptureSession.setRepeatingRequest(captureRequest, null, FullScreenCameraActivity.this.getHandler());
                return;
            } catch (CameraAccessException cameraAccessException) {
                cameraAccessException.printStackTrace();
                return;
            }
        }
    };

    private SurfaceHolder mSurfaceHolder;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        ORIENTATIONS = sparseIntArray;
        BIG_PIC = "big_pic";
        SMALL_PIC = "small_pic";
        sparseIntArray.append(0, 90);
        ORIENTATIONS.append(1, 0);
        ORIENTATIONS.append(2, 270);
        ORIENTATIONS.append(3, 180);
    }

    private void ResumeFixCamera2() {
        this.mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        this.mSurfaceHolder.setFixedSize(this.mCameraSurfaceView.getWidth(), this.mCameraSurfaceView.getHeight());
    }

    private void adjustScreen(View paramView) {
        int i = paramView.getHeight();
        int j = paramView.getWidth();
        if (i > j) {
            float f1 = j * 4.0F / 3.0F;
            this.container.setScaleX(i / f1);
            return;
        }
        float f = i * 4.0F / 3.0F;
        this.container.setScaleY(j / f);
    }

    private boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), "android.permission.CAMERA") != 0) {
            Constants.toast(getApplicationContext(), "沒有相機權限");
            return false;
        }
        return true;
    }

    private void createCameraCaptureSession() throws CameraAccessException {
        CaptureRequest.Builder builder = this.mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        this.mCaptureRequestBuilder = builder;
        builder.addTarget(this.mSurfaceHolder.getSurface());
        this.mCameraDevice.createCaptureSession(Arrays.asList(new Surface[]{this.mSurfaceHolder.getSurface(), this.mImageReader.getSurface()}), this.mSessionPreviewStateCallback, getHandler());
    }

    private void initCamera2(int paramInt1, int paramInt2) {
        if (checkCameraPermission()) {
            try {
                String str = this.mCameraManager.getCameraIdList()[0];
                StreamConfigurationMap streamConfigurationMap = (StreamConfigurationMap) this.mCameraManager.getCameraCharacteristics(str).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                (new HandlerThread("Camera2")).start();
                ImageReader imageReader = ImageReader.newInstance(paramInt1, paramInt2, 256, 2);
                this.mImageReader = imageReader;
                imageReader.setOnImageAvailableListener(this.mImageAvailableListenter, getHandler());
                this.mCameraManager.openCamera("0", this.mCameraDeviceCallBack, getHandler());
                return;
            } catch (CameraAccessException cameraAccessException) {
                cameraAccessException.printStackTrace();
                return;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FullScreenCameraActivity.this.finish();
            }
        });
    }

    private void initSurfaceView() {
        this.mCameraSurfaceView.setBackgroundColor(40);
        SurfaceHolder surfaceHolder = this.mCameraSurfaceView.getHolder();
        this.mSurfaceHolder = surfaceHolder;
        surfaceHolder.setKeepScreenOn(true);
        this.mSurfaceHolder.addCallback(this);
    }

    public int getLayoutView() {
        return R.layout.layout_full_screen_camera;
    }

    protected boolean hideActionBar() {
        return true;
    }

    public void initListener() {
        initSurfaceView();
    }

    public void initView() {
        CustomSurfaceView customSurfaceView = (CustomSurfaceView) findViewById(R.id.custom_surface_view);
        this.mCameraSurfaceView = customSurfaceView;
        customSurfaceView.post(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    protected void onResume() {
        ResumeFixCamera2();
        super.onResume();
    }

    public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {
    }

    public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {
        initCamera2(this.mCameraSurfaceView.getWidth(), this.mCameraSurfaceView.getHeight());
    }

    public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {
        CameraDevice cameraDevice = this.mCameraDevice;
        if (cameraDevice != null)
            cameraDevice.close();
    }

    public void takePic(View paramView) {
        if (this.mCameraDevice == null)
            return;
        try {
            this.mCaptureRequestBuilder.addTarget(this.mImageReader.getSurface());
            this.mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, Integer.valueOf(4));
            this.mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, Integer.valueOf(2));
            int i = getWindowManager().getDefaultDisplay().getRotation();
            this.mCaptureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, Integer.valueOf(ORIENTATIONS.get(i)));
            CaptureRequest captureRequest = this.mCaptureRequestBuilder.build();
            this.mCameraCaptureSession.capture(captureRequest, null, getHandler());
            return;
        } catch (CameraAccessException cameraAccessException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("takePic: ");
            stringBuilder.append(cameraAccessException);
            Log.d(TAG, stringBuilder.toString());
            cameraAccessException.printStackTrace();
            return;
        }
    }

    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }
}
