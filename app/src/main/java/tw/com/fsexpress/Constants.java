package tw.com.fsexpress;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AlertDialog;

import android.util.Base64;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import tw.com.fsexpress.utils.PromptDialog;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by alvinhsieh on 2018/1/15.
 */

public class Constants {

    public static final boolean isDebug = true;

    //正式站的版本驗證ID
    public static final String APP_CHECK_VERSION_ID_PROD = "FSE";
    //測試站的版本驗證ID
    public static final String APP_CHECK_VERSION_ID_TEST = "FSE_DEBUG";
    //版本驗證ID
    public static final String APP_CHECK_VERSION_ID = Constants.isDebug ? Constants.APP_CHECK_VERSION_ID_TEST : Constants.APP_CHECK_VERSION_ID_PROD;

    // Domain
    //正式
    public static final String PROD_API_DOMAIN = "https://webservice.fs-express.com.tw/Webservice.asmx/";
    //測試
    public static final String TEST_API_DOMAIN = "http://testwebservice.fs-express.com.tw/Webservice.asmx/";
    //正式   //"http://testexpressappeal2.fs-express.com.tw/api/";   //"http://expressappeal.fs-express.com.tw/api/";
    public static final String PROD_API_DOMAIN_Appeal = "http://expressappeal.fs-express.com.tw/api/";
    //測試
    public static final String TEST_API_DOMAIN_Appeal = "http://testexpressappeal.fs-express.com.tw/api/";
    //正式
    public static final String PROD_DIGI_SIGN_DOMAIN = "https://app_sign_web.fs-express.com.tw/";
    //測試
    public static final String TEST_DIGI_SIGN_DOMAIN = "http://test_app_sign_web.fs-express.com.tw/";


    public static final String API_DOMAIN = Constants.isDebug ? Constants.TEST_API_DOMAIN : Constants.PROD_API_DOMAIN;

    public static final String API_DOMAIN_Appeal = Constants.isDebug ? Constants.TEST_API_DOMAIN_Appeal : Constants.PROD_API_DOMAIN_Appeal;

    public static final String DIGI_SIGN_DOMAIN = Constants.isDebug ? Constants.TEST_DIGI_SIGN_DOMAIN : Constants.PROD_DIGI_SIGN_DOMAIN;

    //DIGI_SIGN
    public static final String DIGI_SIGN_POST_SIGN_PAGE_MAIN = DIGI_SIGN_DOMAIN + "SignPageMain.aspx?";
    public static final String DIGI_SIGN_POST_SET_SIGN_CODE = DIGI_SIGN_DOMAIN + "SetSignCode.aspx?";

    // API
    public static final String API_POST_DO_LINK_WORK = API_DOMAIN + "PostCollectionMoneyNoBinding";

    public static final String API_POST_CHECK_SCANCODE = API_DOMAIN + "checkScancode";
    public static final String API_POST_CHECK_SCANCODE_2 = API_DOMAIN + "Getchecknumber";
    public static final String API_POST_CHECK_DELIVERY_STATUS = API_DOMAIN + "CheckDeliveryStatus";

    public static final String API_POST_GET_APPVERSION = API_DOMAIN + "getAppVersion";
    public static final String API_POST_GET_UPLOADDATA = API_DOMAIN + "getUploadData";

    public static final String API_POST_GET_DELIVERY_TYPE = API_DOMAIN + "getDeliveryType";

    public static final String API_POST_GET_DISPATCH_STATISTICS = API_DOMAIN + "get_dispatchstatistics";
    public static final String API_POST_GET_DISPATCH_TASK = API_DOMAIN + "get_dispatchtask";
    public static final String API_POST_GET_DISPATCH_TASK_DETAIL = API_DOMAIN + "get_dispatchtask_detail";
    public static final String API_POST_GET_INVENTORY_DATA = API_DOMAIN + "get_inventory_data";
    public static final String API_POST_GET_RE_CODE = API_DOMAIN + "get_re_code";
    public static final String API_POST_GET_RETURN_CODE = API_DOMAIN + "get_return_code";
    public static final String API_POST_GET_RETURN_DELIVERY = API_DOMAIN + "get_return_delivery";
    public static final String API_POST_GET_RETURN_UNPICKCNT = API_DOMAIN + "get_return_unpickcnt";
    public static final String API_POST_GET_TRANSFER_DATA = API_DOMAIN + "get_transfer_data";
    public static final String API_POST_GET_UPLOADED_DISPATCHTASK = API_DOMAIN + "get_uploaded_dispatchtask";
    public static final String API_POST_GET_WAREHOUSE = API_DOMAIN + "get_warehouse";

    public static final String API_POST_GET_BAG_LIST = API_DOMAIN + "GetContainerCode";


    public static final String API_POST_LOGIN_CHECK = API_DOMAIN + "loginCheck";
    public static final String API_POST_LOGIN_CHECK_V2 = API_DOMAIN + "loginCheckV2";
    public static final String API_POST_LOGIN_CHECK_V3 = API_DOMAIN + "loginCheckV3";
    //    public static final String API_POST_SEND_UPLOAD_DATA_NEW =          API_DOMAIN + "sendUploadData5";
    public static final String API_POST_SEND_UPLOAD_DATA_NEW = API_DOMAIN + "sendUploadData9";
    //    public static final String API_POST_SEND_UPLOAD_DATA =          API_DOMAIN + "sendUploadData4";
    public static final String API_POST_SEND_UPLOAD_DATA = API_DOMAIN + "sendUploadData8";
    public static final String API_POST_SEND_UPLOAD_DATA3 = API_DOMAIN + "sendUploadData3";
    public static final String API_POST_SEND_BIND_BAG = API_DOMAIN + "BindBagNo";
    public static final String API_POST_SEND_XY_LOCATION = API_DOMAIN + "sendXYLocation";
    public static final String API_POST_SEND_DISPATCH = API_DOMAIN + "send_dispatch";
    public static final String API_POST_SEND_INVENTORY = API_DOMAIN + "send_inventory_v3";  //""send_inventory";
    public static final String API_POST_SEND_PICKUP_DATA = API_DOMAIN + "send_pickup_data1";
    public static final String API_POST_SEND_TRANSFER = API_DOMAIN + "send_transfer";

    public static final String API_POST_INSERT_CAGE_RECORD = API_DOMAIN + "CageMana_InsertCageRecord";

    public static final String API_POST_GET_ARRIVE_SDMD = API_DOMAIN + "getArriveSDMDCode";


    public static final String API_POST_INSERT_SPECIAL_AREA_AUDIT = API_DOMAIN_Appeal + "AppealApi/InsertSpecialAreaAudit";
    public static final String API_POST_INSERT_CBM_SIZE_AUDIT = API_DOMAIN_Appeal + "AppealApi/InsertCbmSizeAudit";
    public static final String API_GET_Driver_Send_Prepare_GetAppList = API_DOMAIN_Appeal + "DriverSendPrepare/GetAppList";
    public static final String API_GET_Driver_Send_Prepare_UpdateIsRead = API_DOMAIN_Appeal + "DriverSendPrepare/UpdateIsRead2";
//    public static final String URL_FL = "http://172.20.10.11:3077/#/forwardLogistics";
//    public static final String URL_FL = "http://192.168.1.41:3077/#/forwardLogistics";


    public static final String API_POST_GET_DELIVERY_WHITE_USER = API_DOMAIN + "GetDeliveryWhiteUser";

    public static final String API_POST_GET_DATA_BY_STATION_CODE = API_DOMAIN + "GetDataByStationScode";

    public static final String API_POST_UPDATE_DRIVER = API_DOMAIN + "UpdateDriver";

    //正式
    public static final String Prod_URL_FL = "http://barcode.fs-express.com.tw/#/forwardLogistics";
    //測試
    public static final String TEST_URL_FL = "http://testbarcode.fs-express.com.tw/#/forwardLogistics";
    public static final String URL_FL = Constants.isDebug ? Constants.TEST_URL_FL : Constants.Prod_URL_FL;

    // room
    public static final String COLLECT_GOODS = "COLLECT_GOODS"; // 集貨
    public static final String DISCHARGE = "DISCHARGE";
    public static final String DELIVERY = "DELIVERY";
    public static final String MATCHED = "MATCHED";


    public static ProgressDialog ProgressDialog(Activity activity, String message) {
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setCancelable(false);
        dialog.setMessage(message);
        return dialog;
    }

    // API > 26.0 needed
    public static AlertDialog ProgressBar(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message);

        builder.setCancelable(false);

        builder.setView(new ProgressBar(context));

        return builder.create();
    }


    public static AlertDialog ErrorAlertDialog(Activity act, String title, String message, String left, String right,
                                               DialogInterface.OnClickListener l_listener, DialogInterface.OnClickListener r_listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);

        builder.setTitle(title);

        builder.setCancelable(false);

        builder.setMessage(message);

        if (left != null) {
            builder.setNegativeButton(left, l_listener);
        }

        if (right != null) {
            builder.setNeutralButton(right, r_listener);
        }

        return builder.show();
    }

    public static AlertDialog.Builder AlertWithMultipleSelect(Activity activity, String title, List<String> selects,
                                                              DialogInterface.OnClickListener listener, DialogInterface.OnClickListener cancel_listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setCancelable(false);

        builder.setTitle(title);

        String[] selectsArray = selects.toArray(new String[selects.size()]);

        builder.setItems(selectsArray, listener);

        builder.setPositiveButton("取消", cancel_listener);

        return builder;
    }

    public static AlertDialog.Builder AlertWithMultipleSelection(Activity activity, String title, List<String> selects, List<Boolean> checks
            , DialogInterface.OnMultiChoiceClickListener checkListener, DialogInterface.OnClickListener listener, DialogInterface.OnClickListener cancel_listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setCancelable(false);

        builder.setTitle(title);

        CharSequence[] selectsArray = selects.toArray(new CharSequence[selects.size()]);

        boolean[] checksArray = new boolean[checks.size()];

        for (int i = 0; i < checks.size(); i++) {
            Boolean b = checks.get(i);
            checksArray[i] = b.booleanValue();
        }

        builder.setMultiChoiceItems(selectsArray, checksArray, checkListener);

        builder.setNegativeButton("確定", listener);
        builder.setPositiveButton("取消", cancel_listener);

        return builder;
    }

    public static String convertUnixToDataFormat(long unix, String format) {
        Date date = new Date(unix * 1000L); // *1000 is to convert seconds to
        // milliseconds

        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.TAIWAN); // the
        // format
        // of
        // your
        // date

        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    // get now unix time
    public static long getNow() {
        return System.currentTimeMillis() / 1000L;
    }

    public static void set(Context c, String key, String value) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String get(Context c, String key) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);

        return preferences.getString(key, null);
    }

    public static void setInterger(Context c, String key, int value) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInterger(Context c, String key) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);

        return preferences.getInt(key, 0);
    }

    public static void setBoolean(Context c, String key, boolean value) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolean(Context c, String key) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);

        return preferences.getBoolean(key, false);
    }

    public static void clear(Context c, String key) {
        SharedPreferences preferences = c.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();

    }

    public static String Encode64(String str) {
        byte[] data = str.getBytes();
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

        return base64;
    }

    public static String Decode64(String str) {
        try {
            byte[] data = Base64.decode(str, Base64.DEFAULT);
            String text = new String(data, "UTF-8");
            return text;
        } catch (Exception e) {
            return null;
        }

    }

    public static String getFolderPath(Context con) {

//		File direct = new File(Environment.getExternalStorageDirectory() +
//				 "/ly/");
//
//				 return direct.getAbsolutePath();


        File direct = new File(con.getExternalFilesDir(null).getAbsolutePath() + "/mtbs/");

        return direct.getAbsolutePath();


    }

    public static String getTime() {
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())).format(new Date(System.currentTimeMillis()));
    }

    public static Long getNowTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    public static String getTimeMinute() {
        return (new SimpleDateFormat("HH:mm", Locale.getDefault())).format(new Date(System.currentTimeMillis()));
    }

    public static String getYesterdayDate() {
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -24);
        String yesterdayDate = dateFormat.format(calendar.getTime());
        return yesterdayDate;
    }

    public static Long getThreeDayAgoTimestamp() {
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -72);
        return calendar.getTime().getTime() / 1000;
    }

    public static String getTomorrowDate() {
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, +24);
        String tomorrowDate = dateFormat.format(calendar.getTime());
        return tomorrowDate;
    }

    public static String getTodayDate() {
        return (new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())).format(new Date(System.currentTimeMillis()));
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
//            MessageDigest digest = MessageDigest.getInstance("MD5");
//            digest.update(s.getBytes());
//            byte messageDigest[] = digest.digest();
//
//            // Create Hex String
//            StringBuffer hexString = new StringBuffer();
//            for (int i = 0; i < messageDigest.length; i++)
//                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
//            return hexString.toString();

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(s.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            return hashtext;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void createFolder(Context con) {
//		 File folder = new File(Environment.getExternalStorageDirectory() +"/ly/");
//		 if ( folder.exists() == false )
//		 {
//			 folder.mkdirs();
//		 }

        String tSDPath = con.getExternalFilesDir(null).getAbsolutePath();
        Log.e("Jerry", tSDPath);
        File tFolder = new File(tSDPath, "mtbs");
        if (tFolder.exists() == false) {
            tFolder.mkdirs();
        }

    }

    public static String findFileName(Context con, String url) {
        File direct = new File(getFolderPath(con));

        File files[] = direct.listFiles();

        String pdfName = md5(url);

        System.out.println(direct);

        if (files != null) {
            for (File a : files) {
                String fileName = a.getName();

                if (fileName.length() < pdfName.length())
                    continue;

                if (fileName.substring(0, pdfName.length()).equals(pdfName)) {
                    if (fileName.split("_").length == 2) {
                        return fileName;
                    }

                }
            }
        }


        return null;
    }

    public static void newFile(Context con, String file_url, long size) {
        int index = file_url.lastIndexOf(".");
        String subFile = file_url.substring(index, file_url.length());
        String fileName = String.format("%s_%d%s", md5(file_url), size, subFile);
        File pdf = new File(getFolderPath(con) + "/" + fileName);

        try {
            pdf.createNewFile();
            Log.e("Jerry", "new file:" + pdf.getAbsolutePath());
        } catch (Exception e) {

            Log.e("Jerry", "newFile error" + e.toString());
        }

    }

    /**
     * get App versionCode
     *
     * @param context
     * @return
     */
    public static String getVersionCode(Context context) {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String versionCode = "";
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionCode = packageInfo.versionCode + "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    /**
     * get App versionName
     *
     * @param context
     * @return
     */
    public static String getVersionName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String versionName = "";
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    public static String getVersionName2(Context context) {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String versionName = "";
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionName = versionName + (Constants.isDebug ? " - 工程師測試版" : "");
        return versionName;
    }

    public static void toast(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }

    public static void showPromptDialog(Context cxt, String title, String content, PromptDialog.ButtonListener clickListener) {
        new PromptDialog(cxt, R.style.iosDialog).setTitle(title).setContent(content).setOnClickListener(clickListener).show();  // style:iosDialog
    }

    public static enum enumCHECK_TYPE {
        CHECKSTATUS,
        DELIVERY;
    }

    /*
    public static boolean returnValue = false;
    public static boolean showAlertDialog(Context cxt, String title, String content , String strYesMsg , String strNpMsg) {
       //final boolean returnValue = false;
        returnValue = false ;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Log.e("------------------","OKOK");
                        returnValue = true ;
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        returnValue = false ;
                        Log.e("------------------","cancel");
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(cxt);
        builder.setMessage(content).setTitle(title).setPositiveButton(strYesMsg, dialogClickListener)
                .setNegativeButton(strNpMsg, dialogClickListener).show();

        return returnValue ;
    }
    */

    public static float dp2px(Context paramContext, float paramFloat) {
        return paramFloat * (paramContext.getResources().getDisplayMetrics()).density;
    }
}
