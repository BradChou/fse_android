package tw.com.fsexpress;

import android.os.Bundle;
import android.view.Window;

import tw.com.fsexpress.framework.activity.GHWFragmentActivity;


public class DriverInputActivity extends GHWFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        DriverInputFragment fg = new DriverInputFragment();
        fg.setLayoutResourceId(R.layout.layout_start_driver);
        this.initWithRootFragment(fg);
    }
}