package tw.com.fsexpress;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;


import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Shin-Mac on 2018/1/17.
 */

public class LaunchActivity extends Activity {

    private Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_launch);

        this.goNext();
    }

    private void goNext()
    {
        timer.schedule(new TimerTask() {

            @Override
            public void run()
            {
                Message msg = new Message();
                msg.what = 1;

                mainThread.sendMessage(msg);
            }

        }, 2000);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        timer.cancel();
    }

    private Handler mainThread = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == 1)
            {
                timer.cancel();

                Intent intent = new Intent(LaunchActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();
            }
        }
    };
}
