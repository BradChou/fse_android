package tw.com.fsexpress;


import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.Date;

import tw.com.fsexpress.Menu.collection.CollectionFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment;
import tw.com.fsexpress.Menu.delivery.DeliveryFragment2;
import tw.com.fsexpress.framework.Commander;
import tw.com.fsexpress.framework.activity.GHWFragmentActivity;
import tw.com.fsexpress.framework.request.RequestCallback;
import tw.com.fsexpress.framework.request.RequestHandler;
import tw.com.fsexpress.framework.request.RequestObject;

/**
 * Created by Shin-Mac on 2018/2/12.
 */

public class MainActivity extends GHWFragmentActivity {

    public boolean isFirst = true;
    public Date openTime = new Date();

    public void checkOpenTime() {

        Date nowTime = Calendar.getInstance().getTime();
        long diff = nowTime.getTime() - openTime.getTime();
        Log.e("------openTime------openTime", openTime.toString());
        Log.e("------openTime------nowTime", nowTime.toString());

        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        //long days = hours / 24;

        //Toast.makeText(this, ""+ minutes + "---" + seconds , Toast.LENGTH_LONG).show();
        //Constants.toast(this,"提醒:\r\n您在目前的功能停留時間過久,\r\n請抽空返回系統主選單.");
        Log.e("-------diff ", "" + diff);
        Log.e("-------diff hours ", Long.toString(hours));

        if (hours < 96) {
            Log.e("------openTime------", "block 1");
        } else if (hours >= 96 && hours < 168) {
            Log.e("------openTime------", "block 2");
            Constants.toast(this, "提醒:\r\n您在目前的功能停留時間過久,\r\n請抽空返回系統主選單.");
        } else if (hours >= 168) {
            Log.e("------openTime------", "block 3");
            openTime = Calendar.getInstance().getTime();
            Constants.toast(this, "提醒:即將返回系統主選單,進行系統檢查");

            MainFragment fg = new MainFragment();
            fg.setLayoutResourceId(R.layout.layout_main);
            this.initWithRootFragment(fg);
        } else {
            Log.e("------openTime------", "block 4");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //2022-02-18 v.43
        // 新增如果在六大歷程的畫面超過4天(96小時)
        // 會提示返回主選單
        //
        // 如果超過一星期 ( 168小時 )
        // 會強迫返回主選單 [確保APP版本更新有跟上]
        //
        Log.e("-------openTime", openTime.toString());

        if (isFirst == true) {
            openTime = Calendar.getInstance().getTime();
            isFirst = false;
        } else {

            //checkOpenTime();

//            Date nowTime = Calendar.getInstance().getTime();
//            long diff = nowTime.getTime() - openTime.getTime();
//
//            long seconds = diff / 1000;
//            long minutes = seconds / 60;
//            long hours = minutes / 60;
//            //long days = hours / 24;
//
//            //Toast.makeText(this, ""+ minutes + "---" + seconds , Toast.LENGTH_LONG).show();
//            //Constants.toast(this,"提醒:\r\n您在目前的功能停留時間過久,\r\n請抽空返回系統主選單.");
//
//            Log.e("-------diff hours ", Long.toString(hours) );
//
//            if (hours < 96)
//            {
//
//            }
//            else if (hours >= 96 && hours < 168)
//            {
//                Constants.toast(this,"提醒:\r\n您在目前的功能停留時間過久,\r\n請抽空返回系統主選單.");
//            }
//            else if (hours >= 168 )
//            {
//                openTime = Calendar.getInstance().getTime();
//                Constants.toast(this,"提醒:即將返回系統主選單");
//
//                MainFragment fg = new MainFragment();
//                fg.setLayoutResourceId(R.layout.layout_main);
//                this.initWithRootFragment(fg);
//            }
//            else
//            {
//
//            }
///*
//            if ( 1==1 )
//            {
//                MainFragment fg = new MainFragment();
//                fg.setLayoutResourceId(R.layout.layout_main);
//                this.initWithRootFragment(fg);
//            }
//*/
        }
        //Toast.makeText(this, openTime.toString(), Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window window = this.getWindow();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.action_bar_blue));


        Log.e("---------------Build.VERSION.SDK_INT------------", "" + Build.VERSION.SDK_INT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            this.startForegroundService(new Intent(this, MyService.class));
//        } else {
            this.startService(new Intent(this, MyService.class));
        }


//        Intent intent = new Intent(this, MyService.class);
//        startService(intent);

        GPSLocation location = GPSLocation.instance(this, false);
        location.setCallback(new GPSLocation.GPSCallback() {
            @Override
            public void onLocation(Location location) {
                Log.e("Location", "getLongitude:" + location.getLongitude() + "getLatitude:" + location.getLatitude());

                MainActivity.this.requestLocation(location);

            }

            @Override
            public void onUserAccept() {

            }

            @Override
            public void onUserReject() {

            }
        });
        location.StartLocation(this);

        String fcmNewsId = getIntent().getStringExtra("fcmNewsId");
        MainFragment fg = new MainFragment();
        fg.setLayoutResourceId(R.layout.layout_main);
        fg.setFcmNewsId(fcmNewsId);
        this.initWithRootFragment(fg);
    }

    private void requestLocation(Location ll) {

        RequestHandler request = (RequestHandler) Commander.instance().handlerWithName("ghw.request");
        RequestObject requestObject = (RequestObject) new RequestObject();
        requestObject.setUrl(Constants.API_POST_SEND_XY_LOCATION);
        requestObject.setMethod(RequestObject.METHOD.POST);
        requestObject.addParameter("driver_code", AccountManager.instance().getDriverCode(this));
        requestObject.addParameter("X", String.valueOf(ll.getLongitude()));
        requestObject.addParameter("Y", String.valueOf(ll.getLatitude()));

        request.addListener(new RequestCallback() {
            @Override
            public void detectRequestStatus(RequestHandler requestHandler, RequestStatus requestStatus) {
                if (requestStatus == RequestStatus.Finish) {

                    try {

                        Log.e("Jerry", "location:" + requestHandler.getReceiveString());


                    } catch (Exception e) {
                        Log.e("Jerry", e.toString());
                    }
                }

            }
        });
        request.request(requestObject);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            androidx.fragment.app.FragmentManager manager = this.getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 1) {
                super.onBackPressed();
            } else {
                ConfirmExit();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void ConfirmExit() {
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.this);
        ad.setTitle("離開");
        ad.setMessage("是否要離開？");
        ad.setPositiveButton("是", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            public void onClick(DialogInterface dialog, int i) {
                requestLogout();
            }
        });
        ad.setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {

            }
        });
        ad.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("----onActivityResult------", "MainActivity - Here");
        Log.e("----requestCode------", "" + requestCode);
        Log.e("----resultCode------", "" + resultCode);

        if (requestCode == 5 || requestCode == 9) {
            if (resultCode == MainActivity.this.RESULT_OK) {
                for (Fragment fragment : MainActivity.this.getSupportFragmentManager().getFragments()) {
                    if (fragment instanceof CollectionFragment
                            || fragment instanceof DeliveryFragment
                            || fragment instanceof DeliveryFragment2
                    ) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }

            }
        }
    }
}