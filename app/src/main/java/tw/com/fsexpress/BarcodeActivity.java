package tw.com.fsexpress;

import android.app.Activity;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class BarcodeActivity extends Activity implements BarcodeCallback {

    private long DELAY_TIME = 1500L;

    private DecoratedBarcodeView barcodeView;
    private Handler handler;
    private TextView goodsNum;
    private boolean isStop = false;
    private List<String> arrBarcode = new ArrayList<String>();
    private int scanItem = 0;
    private ScrollView scrollView;
    private ToneGenerator toneG;
    private TextView txtNowGoodnum;
    private TextView txtScanNum;
    private String strGoods = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.layout_barcode);

        this.scrollView = (ScrollView) findViewById(R.id.scrollBarcode);
        this.txtNowGoodnum = (TextView) findViewById(R.id.txt_now_good_num);
        this.txtScanNum = (TextView) findViewById(R.id.txt_scan_num);
        this.goodsNum = (TextView) findViewById(R.id.goods_num);

        this.barcodeView = (DecoratedBarcodeView)findViewById(R.id.zxing_barcode_scanner);
        this.barcodeView.decodeContinuous(this);

        this.findViewById(R.id.textView15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backWithResult();
            }
        });
    }

    private void addResultToList(String paramString) {
        if (!this.isStop) {
            strGoods = strGoods + "\n" + paramString;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("result: ");
            stringBuilder.append(paramString);
            this.arrBarcode.add(paramString);
            this.scrollView.fullScroll(130);
            this.txtNowGoodnum.setText(paramString);
            this.txtScanNum.setText(String.valueOf(this.arrBarcode.size()));
            this.goodsNum.setText(strGoods);
            beepAndPause();

//            beepAndPause();
//            if (this.scanItem == 3)
//                backWithResult();
        }
    }


    private void beepAndPause() {
        if (this.toneG == null)
            this.toneG = new ToneGenerator(2, 100);
        this.toneG.startTone(24);
        this.isStop = true;
        getHandler().postDelayed(new Runnable(){
            @Override
            public void run() {
                BarcodeActivity.this.isStop = false;
            }}, DELAY_TIME);
    }

    protected Handler getHandler() {
        if (this.handler == null)
            this.handler = new Handler(Looper.getMainLooper());
        return this.handler;
    }

    private void backWithResult() {

        JSONArray arrJson = new JSONArray(this.arrBarcode);
        BarcodeActivity.this.getIntent().putExtra("barcodes", arrJson.toString());
        BarcodeActivity.this.setResult(RESULT_OK, getIntent());
        BarcodeActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.barcodeView.resume();
    }

    @Override  // androidx.fragment.app.FragmentActivity
    protected void onPause() {
        super.onPause();
        this.barcodeView.pause();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        addResultToList(result.getText());
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }
}